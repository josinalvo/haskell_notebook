
import Data.Map ( (!), fromList )
import qualified Data.Array as A

clearCanvas :: Canvas
clearCanvas = A.listArray range (repeat '_')

draw :: [Pos] -> A.Array (Int, Int) Char
draw lPos = clearCanvas A.// a
    where a = map (,'1') $ filter (A.inRange range) lPos

breakCoins :: [Int] -> Int -> Integer
breakCoins list n = allBreaks ! list !! n
      where breakCoinsGen _ 0 = 1
            breakCoinsGen _ n | (n < 0) = 0
            breakCoinsGen all@(coin:coins) n = (allBreaks ! all) !! (n-coin) + (allBreaks ! coins) !! n
            breakCoinsGen [] n = 0
            keys2 all@(coin:coins)     = all: keys2 coins
            keys2 [] = [[]]
            pairs                     = [(key,calculated key [0,1..]) | key <- (keys2 list)]
            calculated key positions  = fmap (breakCoinsGen key) positions
            allBreaks                 = fromList pairs
            
main = print (breakCoins [10,5] $ 100*1000)