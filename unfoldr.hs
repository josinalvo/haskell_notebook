module Unfold where

import Data.List

f :: (Ord b, Num b, Show b) => b -> Maybe (String, b)
f n 
  | n < 20    = Just(show(n),n+1)
  | otherwise = Nothing

-- |
-- >>> unfoldr f 13
-- ["13","14","15","16","17","18","19"]

myunfoldr :: (t -> Maybe (a, t)) -> t -> [a]
myunfoldr f b = case f b of
     Just(a,b') -> a:myunfoldr f b'
     Nothing    -> []

-- |
-- >>> myunfoldr f 13
-- ["13","14","15","16","17","18","19"]

-- |
-- >>> take 15 $ iterate (+3) 0
-- [0,3,6,9,12,15,18,21,24,27,30,33,36,39,42]

myiter f start = start : myiter f (f start)

-- |
-- >>> take 15 $ myiter (+3) 0
-- [0,3,6,9,12,15,18,21,24,27,30,33,36,39,42]

while :: (a -> Bool) -> (a->a) -> a -> a
while cond update val = if cond new
                        then while cond update new
                        else new
      where new = update val

while' cond update = head . dropWhile cond . iterate update

-- |
-- >>> while (<15) (+3) 10
-- 16
-- >>> while' (<15) (+3) 10
-- 16

traceExec :: (a -> Bool) -> (a -> a) -> a -> [a]
traceExec continue next = takeWhile continue . iterate next

traceExec' :: (a -> Bool) -> (a -> a) -> a -> [a]
traceExec' continue next start = unfoldr maybeNext start
    where maybeNext v = if continue v
                        then Just (v, next v)
                        else Nothing

-- |
-- >>> traceExec  (<15) (+4) 0
-- [0,4,8,12]
-- >>> traceExec' (<15) (+4) 0
-- [0,4,8,12]


main :: IO ()
main = print 42