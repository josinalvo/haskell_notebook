import pyrsistent
import pyrsistent.typing
a = pyrsistent.pset([1,2,3]) 
#reveal_type(a) #mypy file.py, gives type pyrsistent.typing.PSet[buildins.int]

b : pyrsistent.typing.PSet[int] = a #when running: AttributeError: module 'pyrsistent' has no attribute 'typing'
#mypy does not complain, though

print ( a )