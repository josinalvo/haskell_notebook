#https://www.hackerrank.com/challenges/kingdom-division/
#nao roda por causa dos imports, baixei uns inputs e parece funcionar

from dataclasses import dataclass
from functools import partial
import typing
from functools import reduce
my_set = typing.Any



# o tipo foi descoberto colocando isso num arquivo teste.py, e rodando 'mypy teste.py'
# import pyrsistent
# a = pyrsistent.pset() 
# reveal_type(a)

def somar(a : int ,
          b : int,
          c : int) -> int:
    return a+b+c

r = partial(somar,2,3)

def test1() -> None:
    """
    Example doctest
    >>> r(5)
    10
    >>> r(c=5)
    10
    """
    pass

@dataclass
class Tree:
    root: int
    connections: list[set[int]] 
    max_ver: int

a = Tree(root=0,connections=[set([1,2,3]), set([3,4,5])], max_ver=10)

def nums2prob(tup : tuple[int, list[tuple[int,int]]]) -> Tree:
    (max_ver,vertices) = tup
    initial_adjs: list[set[int]] = [set() for i in range(0,max_ver+1)] 
    t = Tree (1,initial_adjs,max_ver=max_ver)
    for (a,b) in vertices:
        t.connections[a].add(b)
        t.connections[b].add(a)
    return t

def line2tuple(string):
    (a,b)= [int(x) for x in string.split(' ')]
    return (a,b)

def readProb(string : str) -> tuple[int, list[tuple[int,int]]]:
    lines         = string.splitlines()
    max_ver       = int(lines[0])
    vertices      = [line2tuple(line) for line in lines[1:]]
    if (any ([x == None for x in vertices])):
        raise ValueError("parsing the vertices failed", vertices, lines[1:])
    return (max_ver,vertices)

# all variables assume red root 

#slightly_invalid_possibilities: all children are valid, all children have one colour, you have another
#                              : multiply the valid_possibilities for each child (invert their colors, they have blue root, whole tree has red root)

#valid possibilities: for each child, pick either a valid (blue or red in the root) or invalid possibility (red in the root)
#                   : then remove all the invalids (see above)

def product(list :list[int]) -> int:
    """
    Example doctest
    >>> product([])
    1
    >>> product([1,2,3,4])
    24
    >>> product([1,2,4])
    8
    """
    if list == []:
        return 1
    return reduce ((lambda x,y: x*y), list)

valid_possibilities_d : dict[int, int]= {}
def valid_possibilities(tree : Tree, root : int, used : my_set) -> int:
    if root in valid_possibilities_d:
        return valid_possibilities_d[root]
    neighboors = availableAdj(tree,root,used)
    if len(neighboors) == 0:
        valid_possibilities_d[root] = 0
        return valid_possibilities_d[root]
    new_used   = used.add(root)
    amounts    = [valid_possibilities(tree,n,new_used)*2 + invalid_possibilities(tree,n,new_used) for n in neighboors]
    correction = invalid_possibilities(tree,root,new_used)
    ans = (product(amounts) - correction)
    valid_possibilities_d[root] = ans
    return valid_possibilities_d[root]

invalid_possibilities_d : dict[int, int] = {}
def invalid_possibilities(tree : Tree, root : int, used : my_set) -> int:
    if root in invalid_possibilities_d:
        return invalid_possibilities_d[root]
    neighboors = availableAdj(tree,root,used)
    if len(neighboors) == 0:
        invalid_possibilities_d[root] = 1
        return invalid_possibilities_d[root]
    new_used   = used.add(root)
    correction = [valid_possibilities(tree,n,new_used) for n in neighboors]
    invalid_possibilities_d[root] = product(correction)
    return invalid_possibilities_d[root]

def solve(tree : Tree) -> int:
    if tree.max_ver == 1:
        return 2
    used = pset() 
    return 2*valid_possibilities(tree, root=1, used=used)

A = typing.TypeVar('A')
B = typing.TypeVar('B')
C = typing.TypeVar('C')
D = typing.TypeVar('D')
E = typing.TypeVar('E')

def composite2(f : typing.Callable[[B],C], g : typing.Callable[[A],B]) -> typing.Callable[[A],C]:
    def result(x : A) -> C:
        a = g(x)
        return f(a)
    return result

def composite3(f : typing.Callable[[C],D], 
               g : typing.Callable[[B],C], 
               h : typing.Callable[[A],B]) -> typing.Callable[[A],D]:
    def result(a0 : A) -> D:
        a1 = h(a0)
        a2 = g(a1)
        a3 = f(a2)
        return a3
    return result


def composite4(f : typing.Callable[[D],E], 
               g : typing.Callable[[C],D], 
               h : typing.Callable[[B],C], 
               i : typing.Callable[[A],B]) -> typing.Callable[[A],E]:
    def result(a0 : A) -> E:
        a1 = i(a0)
        a2 = h(a1)
        a3 = g(a2)
        a4 = f(a3)
        return a4
    return result


def f (x : int) -> int: 
       return x+1

ff = composite2(f,f)

ff2: typing.Callable[[int], int] = ff #no error given!


def availableAdj (tree: Tree, vertex: int, used : my_set) -> set[int]:
    allAdj = tree.connections[vertex]
    return allAdj - set(used)

def test() -> None:
    import doctest
    doctest.testmod()

def main() -> None:
    import sys
    problem = sys.stdin.read()
    def f (x : int) -> int : return x % (10**9+7)
    run     = composite4(f,solve,nums2prob,readProb)
    print(run(problem))
    #print(nums2prob(readProb(problem)))


main()