import re
try:
    from inspect import Parameter, signature
except ImportError:
    signature = None
    from inspect import getfullargspec


_EMPTY_SENTINEL = object()


def inc(x):
    
    return x + 1


def dec(x):
    
    return x - 1


def discard(evolver, key):
    
    try:
        del evolver[key]
    except KeyError:
        pass



def rex(expr):
    
    r = re.compile(expr)
    return lambda key: isinstance(key, str) and r.match(key)


def ny(_):
    
    return True



def _chunks(l, n):
    for i in range(0, len(l), n):
        yield l[i:i + n]


def transform(structure, transformations):
    r = structure
    for path, command in _chunks(transformations, 2):
        r = _do_to_path(r, path, command)
    return r


def _do_to_path(structure, path, command):
    if not path:
        return command(structure) if callable(command) else command

    kvs = _get_keys_and_values(structure, path[0])
    return _update_structure(structure, kvs, path[1:], command)


def _items(structure):
    try:
        return structure.items()
    except AttributeError:
        
        return list(enumerate(structure))


def _get(structure, key, default):
    try:
        if hasattr(structure, '__getitem__'):
            return structure[key]

        return getattr(structure, key)

    except (IndexError, KeyError):
        return default


def _get_keys_and_values(structure, key_spec):
    if callable(key_spec):
        
        arity = _get_arity(key_spec)
        if arity == 1:
            
            
            return [(k, v) for k, v in _items(structure) if key_spec(k)]
        elif arity == 2:
            
            
            return [(k, v) for k, v in _items(structure) if key_spec(k, v)]
        else:
            
            raise ValueError(
                "callable in transform path must take 1 or 2 arguments"
            )

    
    return [(key_spec, _get(structure, key_spec, _EMPTY_SENTINEL))]


if signature is None:
    def _get_arity(f):
        argspec = getfullargspec(f)
        return len(argspec.args) - len(argspec.defaults or ())
else:
    def _get_arity(f):
        return sum(
            1
            for p
            in signature(f).parameters.values()
            if p.default is Parameter.empty
            and p.kind in (Parameter.POSITIONAL_ONLY, Parameter.POSITIONAL_OR_KEYWORD)
        )


def _update_structure(structure, kvs, path, command):
    
    e = structure.evolver()
    if not path and command is discard:
        
        for k, v in reversed(kvs):
            discard(e, k)
    else:
        for k, v in kvs:
            is_empty = False
            if v is _EMPTY_SENTINEL:
                
                
                is_empty = True
                v = pmap()

            result = _do_to_path(v, path, command)
            if result is not v or is_empty:
                e[k] = result

    return e.persistent()

from abc import abstractmethod, ABCMeta
from collections.abc import Sequence, Hashable
from numbers import Integral
import operator



def _bitcount(val):
    return bin(val).count("1")

BRANCH_FACTOR = 32
BIT_MASK = BRANCH_FACTOR - 1
SHIFT = _bitcount(BIT_MASK)


def compare_pvector(v, other, operator):
    return operator(v.tolist(), other.tolist() if isinstance(other, PVector) else other)


def _index_or_slice(index, stop):
    if stop is None:
        return index

    return slice(index, stop)


class PythonPVector(object):
    
    __slots__ = ('_count', '_shift', '_root', '_tail', '_tail_offset', '__weakref__')

    def __new__(cls, count, shift, root, tail):
        self = super(PythonPVector, cls).__new__(cls)
        self._count = count
        self._shift = shift
        self._root = root
        self._tail = tail

        
        self._tail_offset = self._count - len(self._tail)
        return self

    def __len__(self):
        return self._count

    def __getitem__(self, index):
        if isinstance(index, slice):
            
            
            if index.start is None and index.stop is None and index.step is None:
                return self

            
            
            return _EMPTY_PVECTOR.extend(self.tolist()[index])

        if index < 0:
            index += self._count

        return PythonPVector._node_for(self, index)[index & BIT_MASK]

    def __add__(self, other):
        return self.extend(other)

    def __repr__(self):
        return 'pvector({0})'.format(str(self.tolist()))

    def __str__(self):
        return self.__repr__()

    def __iter__(self):
        
        
        return iter(self.tolist())

    def __ne__(self, other):
        return not self.__eq__(other)

    def __eq__(self, other):
        return self is other or (hasattr(other, '__len__') and self._count == len(other)) and compare_pvector(self, other, operator.eq)

    def __gt__(self, other):
        return compare_pvector(self, other, operator.gt)

    def __lt__(self, other):
        return compare_pvector(self, other, operator.lt)

    def __ge__(self, other):
        return compare_pvector(self, other, operator.ge)

    def __le__(self, other):
        return compare_pvector(self, other, operator.le)

    def __mul__(self, times):
        if times <= 0 or self is _EMPTY_PVECTOR:
            return _EMPTY_PVECTOR

        if times == 1:
            return self

        return _EMPTY_PVECTOR.extend(times * self.tolist())

    __rmul__ = __mul__

    def _fill_list(self, node, shift, the_list):
        if shift:
            shift -= SHIFT
            for n in node:
                self._fill_list(n, shift, the_list)
        else:
            the_list.extend(node)

    def tolist(self):
        
        the_list = []
        self._fill_list(self._root, self._shift, the_list)
        the_list.extend(self._tail)
        return the_list

    def _totuple(self):
        
        return tuple(self.tolist())

    def __hash__(self):
        
        return hash(self._totuple())

    def transform(self, *transformations):
        return transform(self, transformations)

    def __reduce__(self):
        
        return pvector, (self.tolist(),)

    def mset(self, *args):
        if len(args) % 2:
            raise TypeError("mset expected an even number of arguments")

        evolver = self.evolver()
        for i in range(0, len(args), 2):
            evolver[args[i]] = args[i+1]

        return evolver.persistent()

    class Evolver(object):
        __slots__ = ('_count', '_shift', '_root', '_tail', '_tail_offset', '_dirty_nodes',
                     '_extra_tail', '_cached_leafs', '_orig_pvector')

        def __init__(self, v):
            self._reset(v)

        def __getitem__(self, index):
            if not isinstance(index, Integral):
                raise TypeError("'%s' object cannot be interpreted as an index" % type(index).__name__)

            if index < 0:
                index += self._count + len(self._extra_tail)

            if self._count <= index < self._count + len(self._extra_tail):
                return self._extra_tail[index - self._count]

            return PythonPVector._node_for(self, index)[index & BIT_MASK]

        def _reset(self, v):
            self._count = v._count
            self._shift = v._shift
            self._root = v._root
            self._tail = v._tail
            self._tail_offset = v._tail_offset
            self._dirty_nodes = {}
            self._cached_leafs = {}
            self._extra_tail = []
            self._orig_pvector = v

        def append(self, element):
            self._extra_tail.append(element)
            return self

        def extend(self, iterable):
            self._extra_tail.extend(iterable)
            return self

        def set(self, index, val):
            self[index] = val
            return self

        def __setitem__(self, index, val):
            if not isinstance(index, Integral):
                raise TypeError("'%s' object cannot be interpreted as an index" % type(index).__name__)

            if index < 0:
                index += self._count + len(self._extra_tail)

            if 0 <= index < self._count:
                node = self._cached_leafs.get(index >> SHIFT)
                if node:
                    node[index & BIT_MASK] = val
                elif index >= self._tail_offset:
                    if id(self._tail) not in self._dirty_nodes:
                        self._tail = list(self._tail)
                        self._dirty_nodes[id(self._tail)] = True
                        self._cached_leafs[index >> SHIFT] = self._tail
                    self._tail[index & BIT_MASK] = val
                else:
                    self._root = self._do_set(self._shift, self._root, index, val)
            elif self._count <= index < self._count + len(self._extra_tail):
                self._extra_tail[index - self._count] = val
            elif index == self._count + len(self._extra_tail):
                self._extra_tail.append(val)
            else:
                raise IndexError("Index out of range: %s" % (index,))

        def _do_set(self, level, node, i, val):
            if id(node) in self._dirty_nodes:
                ret = node
            else:
                ret = list(node)
                self._dirty_nodes[id(ret)] = True

            if level == 0:
                ret[i & BIT_MASK] = val
                self._cached_leafs[i >> SHIFT] = ret
            else:
                sub_index = (i >> level) & BIT_MASK  
                ret[sub_index] = self._do_set(level - SHIFT, node[sub_index], i, val)

            return ret

        def delete(self, index):
            del self[index]
            return self

        def __delitem__(self, key):
            if self._orig_pvector:
                
                l = PythonPVector(self._count, self._shift, self._root, self._tail).tolist()
                l.extend(self._extra_tail)
                self._reset(_EMPTY_PVECTOR)
                self._extra_tail = l

            del self._extra_tail[key]

        def persistent(self):
            result = self._orig_pvector
            if self.is_dirty():
                result = PythonPVector(self._count, self._shift, self._root, self._tail).extend(self._extra_tail)
                self._reset(result)

            return result

        def __len__(self):
            return self._count + len(self._extra_tail)

        def is_dirty(self):
            return bool(self._dirty_nodes or self._extra_tail)

    def evolver(self):
        return PythonPVector.Evolver(self)

    def set(self, i, val):
        
        
        

        if not isinstance(i, Integral):
            raise TypeError("'%s' object cannot be interpreted as an index" % type(i).__name__)

        if i < 0:
            i += self._count

        if 0 <= i < self._count:
            if i >= self._tail_offset:
                new_tail = list(self._tail)
                new_tail[i & BIT_MASK] = val
                return PythonPVector(self._count, self._shift, self._root, new_tail)

            return PythonPVector(self._count, self._shift, self._do_set(self._shift, self._root, i, val), self._tail)

        if i == self._count:
            return self.append(val)

        raise IndexError("Index out of range: %s" % (i,))

    def _do_set(self, level, node, i, val):
        ret = list(node)
        if level == 0:
            ret[i & BIT_MASK] = val
        else:
            sub_index = (i >> level) & BIT_MASK  
            ret[sub_index] = self._do_set(level - SHIFT, node[sub_index], i, val)

        return ret

    @staticmethod
    def _node_for(pvector_like, i):
        if 0 <= i < pvector_like._count:
            if i >= pvector_like._tail_offset:
                return pvector_like._tail

            node = pvector_like._root
            for level in range(pvector_like._shift, 0, -SHIFT):
                node = node[(i >> level) & BIT_MASK]  

            return node

        raise IndexError("Index out of range: %s" % (i,))

    def _create_new_root(self):
        new_shift = self._shift

        
        if (self._count >> SHIFT) > (1 << self._shift): 
            new_root = [self._root, self._new_path(self._shift, self._tail)]
            new_shift += SHIFT
        else:
            new_root = self._push_tail(self._shift, self._root, self._tail)

        return new_root, new_shift

    def append(self, val):
        if len(self._tail) < BRANCH_FACTOR:
            new_tail = list(self._tail)
            new_tail.append(val)
            return PythonPVector(self._count + 1, self._shift, self._root, new_tail)

        
        new_root, new_shift = self._create_new_root()
        return PythonPVector(self._count + 1, new_shift, new_root, [val])

    def _new_path(self, level, node):
        if level == 0:
            return node

        return [self._new_path(level - SHIFT, node)]

    def _mutating_insert_tail(self):
        self._root, self._shift = self._create_new_root()
        self._tail = []

    def _mutating_fill_tail(self, offset, sequence):
        max_delta_len = BRANCH_FACTOR - len(self._tail)
        delta = sequence[offset:offset + max_delta_len]
        self._tail.extend(delta)
        delta_len = len(delta)
        self._count += delta_len
        return offset + delta_len

    def _mutating_extend(self, sequence):
        offset = 0
        sequence_len = len(sequence)
        while offset < sequence_len:
            offset = self._mutating_fill_tail(offset, sequence)
            if len(self._tail) == BRANCH_FACTOR:
                self._mutating_insert_tail()

        self._tail_offset = self._count - len(self._tail)

    def extend(self, obj):
        
        
        l = obj.tolist() if isinstance(obj, PythonPVector) else list(obj)
        if l:
            new_vector = self.append(l[0])
            new_vector._mutating_extend(l[1:])
            return new_vector

        return self

    def _push_tail(self, level, parent, tail_node):
        
        ret = list(parent)

        if level == SHIFT:
            ret.append(tail_node)
            return ret

        sub_index = ((self._count - 1) >> level) & BIT_MASK  
        if len(parent) > sub_index:
            ret[sub_index] = self._push_tail(level - SHIFT, parent[sub_index], tail_node)
            return ret

        ret.append(self._new_path(level - SHIFT, tail_node))
        return ret

    def index(self, value, *args, **kwargs):
        return self.tolist().index(value, *args, **kwargs)

    def count(self, value):
        return self.tolist().count(value)

    def delete(self, index, stop=None):
        l = self.tolist()
        del l[_index_or_slice(index, stop)]
        return _EMPTY_PVECTOR.extend(l)

    def remove(self, value):
        l = self.tolist()
        l.remove(value)
        return _EMPTY_PVECTOR.extend(l)

class PVector(metaclass=ABCMeta):
    

    @abstractmethod
    def __len__(self):
        pass
        

    @abstractmethod
    def __getitem__(self, index):
        pass

        """
        Get value at index. Full slicing support.

        >>> v1 = v(5, 6, 7, 8)
        >>> v1[2]
        7
        >>> v1[1:3]
        pvector([6, 7])
        """

    @abstractmethod
    def __add__(self, other):
        pass

        """
        >>> v1 = v(1, 2)
        >>> v2 = v(3, 4)
        >>> v1 + v2
        pvector([1, 2, 3, 4])
        """

    @abstractmethod
    def __mul__(self, times):
        pass

        """
        >>> v1 = v(1, 2)
        >>> 3 * v1
        pvector([1, 2, 1, 2, 1, 2])
        """

    @abstractmethod
    def __hash__(self):
        pass

        """
        >>> v1 = v(1, 2, 3)
        >>> v2 = v(1, 2, 3)
        >>> hash(v1) == hash(v2)
        True
        """

    @abstractmethod
    def evolver(self):
        pass

        """
        Create a new evolver for this pvector. The evolver acts as a mutable view of the vector
        with "transaction like" semantics. No part of the underlying vector i updated, it is still
        fully immutable. Furthermore multiple evolvers created from the same pvector do not
        interfere with each other.

        You may want to use an evolver instead of working directly with the pvector in the
        following cases:

        * Multiple updates are done to the same vector and the intermediate results are of no
          interest. In this case using an evolver may be a more efficient and easier to work with.
        * You need to pass a vector into a legacy function or a function that you have no control
          over which performs in place mutations of lists. In this case pass an evolver instance
          instead and then create a new pvector from the evolver once the function returns.

        The following example illustrates a typical workflow when working with evolvers. It also
        displays most of the API (which i kept small by design, you should not be tempted to
        use evolvers in excess ;-)).

        Create the evolver and perform various mutating updates to it:

        >>> v1 = v(1, 2, 3, 4, 5)
        >>> e = v1.evolver()
        >>> e[1] = 22
        >>> _ = e.append(6)
        >>> _ = e.extend([7, 8, 9])
        >>> e[8] += 1
        >>> len(e)
        9

        The underlying pvector remains the same:

        >>> v1
        pvector([1, 2, 3, 4, 5])

        The changes are kept in the evolver. An updated pvector can be created using the
        persistent() function on the evolver.

        >>> v2 = e.persistent()
        >>> v2
        pvector([1, 22, 3, 4, 5, 6, 7, 8, 10])

        The new pvector will share data with the original pvector in the same way that would have
        been done if only using operations on the pvector.
        """

    @abstractmethod
    def mset(self, *args):
        pass

        """
        Return a new vector with elements in specified positions replaced by values (multi set).

        Elements on even positions in the argument list are interpreted as indexes while
        elements on odd positions are considered values.

        >>> v1 = v(1, 2, 3)
        >>> v1.mset(0, 11, 2, 33)
        pvector([11, 2, 33])
        """

    @abstractmethod
    def set(self, i, val):
        pass

        """
        Return a new vector with element at position i replaced with val. The original vector remains unchanged.

        Setting a value one step beyond the end of the vector is equal to appending. Setting beyond that will
        result in an IndexError.

        >>> v1 = v(1, 2, 3)
        >>> v1.set(1, 4)
        pvector([1, 4, 3])
        >>> v1.set(3, 4)
        pvector([1, 2, 3, 4])
        >>> v1.set(-1, 4)
        pvector([1, 2, 4])
        """

    @abstractmethod
    def append(self, val):
        pass

        """
        Return a new vector with val appended.

        >>> v1 = v(1, 2)
        >>> v1.append(3)
        pvector([1, 2, 3])
        """

    @abstractmethod
    def extend(self, obj):
        pass

        """
        Return a new vector with all values in obj appended to it. Obj may be another
        PVector or any other Iterable.

        >>> v1 = v(1, 2, 3)
        >>> v1.extend([4, 5])
        pvector([1, 2, 3, 4, 5])
        """

    @abstractmethod
    def index(self, value, *args, **kwargs):
        pass

        """
        Return first index of value. Additional indexes may be supplied to limit the search to a
        sub range of the vector.

        >>> v1 = v(1, 2, 3, 4, 3)
        >>> v1.index(3)
        2
        >>> v1.index(3, 3, 5)
        4
        """

    @abstractmethod
    def count(self, value):
        pass

        """
        Return the number of times that value appears in the vector.

        >>> v1 = v(1, 4, 3, 4)
        >>> v1.count(4)
        2
        """

    @abstractmethod
    def transform(self, *transformations):
        pass

        """
        Transform arbitrarily complex combinations of PVectors and PMaps. A transformation
        consists of two parts. One match expression that specifies which elements to transform
        and one transformation function that performs the actual transformation.

        >>> #from pyrsistent import freeze, ny
        >>> news_paper = freeze({'articles': [{'author': 'Sara', 'content': 'A short article'},
        ...                                   {'author': 'Steve', 'content': 'A slightly longer article'}],
        ...                      'weather': {'temperature': '11C', 'wind': '5m/s'}})
        >>> short_news = news_paper.transform(['articles', ny, 'content'], lambda c: c[:25] + '...' if len(c) > 25 else c)
        >>> very_short_news = news_paper.transform(['articles', ny, 'content'], lambda c: c[:15] + '...' if len(c) > 15 else c)
        >>> very_short_news.articles[0].content
        'A short article'
        >>> very_short_news.articles[1].content
        'A slightly long...'

        When nothing has been transformed the original data structure is kept

        >>> short_news is news_paper
        True
        >>> very_short_news is news_paper
        False
        >>> very_short_news.articles[0] is news_paper.articles[0]
        True
        """

    @abstractmethod
    def delete(self, index, stop=None):
        pass

        """
        Delete a portion of the vector by index or range.

        >>> v1 = v(1, 2, 3, 4, 5)
        >>> v1.delete(1)
        pvector([1, 3, 4, 5])
        >>> v1.delete(1, 3)
        pvector([1, 4, 5])
        """

    @abstractmethod
    def remove(self, value):
        pass

        """
        Remove the first occurrence of a value from the vector.

        >>> v1 = v(1, 2, 3, 2, 1)
        >>> v2 = v1.remove(1)
        >>> v2
        pvector([2, 3, 2, 1])
        >>> v2.remove(1)
        pvector([2, 3, 2])
        """


_EMPTY_PVECTOR = PythonPVector(0, SHIFT, [], [])
PVector.register(PythonPVector)
Sequence.register(PVector)
Hashable.register(PVector)

def python_pvector(iterable=()):
    
    return _EMPTY_PVECTOR.extend(iterable)

try:
    
    import os
    if os.environ.get('PYRSISTENT_NO_C_EXTENSION'):
        pvector = python_pvector
    else:
        from pvectorc import pvector
        PVector.register(type(pvector()))
except ImportError:
    pvector = python_pvector


def v(*elements):
    
    return pvector(elements)

from collections.abc import Mapping, Hashable
from itertools import chain



class PMapView:
    
    
    def __init__(self, m):
        
        if not isinstance(m, PMap):
            
            if isinstance(m, Mapping):
                m = pmap(m)
            else:
                raise TypeError("PViewMap requires a Mapping object")
        object.__setattr__(self, '_map', m)

    def __len__(self):
        return len(self._map)

    def __setattr__(self, k, v):
        raise TypeError("%s is immutable" % (type(self),))

    def __reversed__(self):
        raise TypeError("Persistent maps are not reversible")

class PMapValues(PMapView):
    
    def __iter__(self):
        return self._map.itervalues()

    def __contains__(self, arg):
        return arg in self._map.itervalues()

    
    def __str__(self):
        return f"pmap_values({list(iter(self))})"
    
    def __repr__(self):
        return f"pmap_values({list(iter(self))})"
    
    def __eq__(self, x):
        
        
        if x is self: return True
        else: return False
    
class PMapItems(PMapView):
    
    def __iter__(self):
        return self._map.iteritems()

    def __contains__(self, arg):
        try: (k,v) = arg
        except Exception: return False
        return k in self._map and self._map[k] == v

    
    def __str__(self):
        return f"pmap_items({list(iter(self))})"
    
    def __repr__(self):
        return f"pmap_items({list(iter(self))})"
        
    def __eq__(self, x):
        if x is self: return True
        elif not isinstance(x, type(self)): return False
        else: return self._map == x._map

class PMap(object):
    
    __slots__ = ('_size', '_buckets', '__weakref__', '_cached_hash')

    def __new__(cls, size, buckets):
        self = super(PMap, cls).__new__(cls)
        self._size = size
        self._buckets = buckets
        return self

    @staticmethod
    def _get_bucket(buckets, key):
        index = hash(key) % len(buckets)
        bucket = buckets[index]
        return index, bucket

    @staticmethod
    def _getitem(buckets, key):
        _, bucket = PMap._get_bucket(buckets, key)
        if bucket:
            for k, v in bucket:
                if k == key:
                    return v

        raise KeyError(key)

    def __getitem__(self, key):
        return PMap._getitem(self._buckets, key)

    @staticmethod
    def _contains(buckets, key):
        _, bucket = PMap._get_bucket(buckets, key)
        if bucket:
            for k, _ in bucket:
                if k == key:
                    return True

            return False

        return False

    def __contains__(self, key):
        return self._contains(self._buckets, key)

    get = Mapping.get

    def __iter__(self):
        return self.iterkeys()

    
    
    
    def __reversed__(self):
        raise TypeError("Persistent maps are not reversible")

    def __getattr__(self, key):
        try:
            return self[key]
        except KeyError as e:
            raise AttributeError(
                "{0} has no attribute '{1}'".format(type(self).__name__, key)
            ) from e

    def iterkeys(self):
        for k, _ in self.iteritems():
            yield k

    
    
    
    def itervalues(self):
        for _, v in self.iteritems():
            yield v

    def iteritems(self):
        for bucket in self._buckets:
            if bucket:
                for k, v in bucket:
                    yield k, v

    def values(self):
        return PMapValues(self)

    def keys(self):
        from ._pset import PSet
        return PSet(self)

    def items(self):
        return PMapItems(self)

    def __len__(self):
        return self._size

    def __repr__(self):
        return 'pmap({0})'.format(str(dict(self)))

    def __eq__(self, other):
        if self is other:
            return True
        if not isinstance(other, Mapping):
            return NotImplemented
        if len(self) != len(other):
            return False
        if isinstance(other, PMap):
            if (hasattr(self, '_cached_hash') and hasattr(other, '_cached_hash')
                    and self._cached_hash != other._cached_hash):
                return False
            if self._buckets == other._buckets:
                return True
            return dict(self.iteritems()) == dict(other.iteritems())
        elif isinstance(other, dict):
            return dict(self.iteritems()) == other
        return dict(self.iteritems()) == dict(other.items())

    __ne__ = Mapping.__ne__

    def __lt__(self, other):
        raise TypeError('PMaps are not orderable')

    __le__ = __lt__
    __gt__ = __lt__
    __ge__ = __lt__

    def __str__(self):
        return self.__repr__()

    def __hash__(self):
        if not hasattr(self, '_cached_hash'):
            self._cached_hash = hash(frozenset(self.iteritems()))
        return self._cached_hash

    def set(self, key, val):
        
        return self.evolver().set(key, val).persistent()

    def remove(self, key):
        
        return self.evolver().remove(key).persistent()

    def discard(self, key):
        
        try:
            return self.remove(key)
        except KeyError:
            return self

    def update(self, *maps):
        
        return self.update_with(lambda l, r: r, *maps)

    def update_with(self, update_fn, *maps):
        
        evolver = self.evolver()
        for map in maps:
            for key, value in map.items():
                evolver.set(key, update_fn(evolver[key], value) if key in evolver else value)

        return evolver.persistent()

    def __add__(self, other):
        return self.update(other)

    __or__ = __add__

    def __reduce__(self):
        
        return pmap, (dict(self),)

    def transform(self, *transformations):
        
        return transform(self, transformations)

    def copy(self):
        return self

    class _Evolver(object):
        __slots__ = ('_buckets_evolver', '_size', '_original_pmap')

        def __init__(self, original_pmap):
            self._original_pmap = original_pmap
            self._buckets_evolver = original_pmap._buckets.evolver()
            self._size = original_pmap._size

        def __getitem__(self, key):
            return PMap._getitem(self._buckets_evolver, key)

        def __setitem__(self, key, val):
            self.set(key, val)

        def set(self, key, val):
            kv = (key, val)
            index, bucket = PMap._get_bucket(self._buckets_evolver, key)
            reallocation_required = len(self._buckets_evolver) < 0.67 * self._size
            if bucket:
                for k, v in bucket:
                    if k == key:
                        if v is not val:
                            new_bucket = [(k2, v2) if k2 != k else (k2, val) for k2, v2 in bucket]
                            self._buckets_evolver[index] = new_bucket

                        return self

                
                
                if reallocation_required:
                    self._reallocate()
                    return self.set(key, val)

                new_bucket = [kv]
                new_bucket.extend(bucket)
                self._buckets_evolver[index] = new_bucket
                self._size += 1
            else:
                if reallocation_required:
                    self._reallocate()
                    return self.set(key, val)

                self._buckets_evolver[index] = [kv]
                self._size += 1

            return self

        def _reallocate(self):
            new_size = 2 * len(self._buckets_evolver)
            new_list = new_size * [None]
            buckets = self._buckets_evolver.persistent()
            for k, v in chain.from_iterable(x for x in buckets if x):
                index = hash(k) % new_size
                if new_list[index]:
                    new_list[index].append((k, v))
                else:
                    new_list[index] = [(k, v)]

            
            
            self._buckets_evolver = pvector().evolver()
            self._buckets_evolver.extend(new_list)

        def is_dirty(self):
            return self._buckets_evolver.is_dirty()

        def persistent(self):
            if self.is_dirty():
                self._original_pmap = PMap(self._size, self._buckets_evolver.persistent())

            return self._original_pmap

        def __len__(self):
            return self._size

        def __contains__(self, key):
            return PMap._contains(self._buckets_evolver, key)

        def __delitem__(self, key):
            self.remove(key)

        def remove(self, key):
            index, bucket = PMap._get_bucket(self._buckets_evolver, key)

            if bucket:
                new_bucket = [(k, v) for (k, v) in bucket if k != key]
                if len(bucket) > len(new_bucket):
                    self._buckets_evolver[index] = new_bucket if new_bucket else None
                    self._size -= 1
                    return self

            raise KeyError('{0}'.format(key))

    def evolver(self):
        
        return self._Evolver(self)

Mapping.register(PMap)
Hashable.register(PMap)


def _turbo_mapping(initial, pre_size):
    if pre_size:
        size = pre_size
    else:
        try:
            size = 2 * len(initial) or 8
        except Exception:
            
            
            size = 8

    buckets = size * [None]

    if not isinstance(initial, Mapping):
        
        
        
        initial = dict(initial)

    for k, v in initial.items():
        h = hash(k)
        index = h % size
        bucket = buckets[index]

        if bucket:
            bucket.append((k, v))
        else:
            buckets[index] = [(k, v)]

    return PMap(len(initial), pvector().extend(buckets))


_EMPTY_PMAP = _turbo_mapping({}, 0)


def pmap(initial={}, pre_size=0):
    
    if not initial and pre_size == 0:
        return _EMPTY_PMAP

    return _turbo_mapping(initial, pre_size)


def m(**kwargs):
    
    return pmap(kwargs)

from collections.abc import Set, Hashable
import sys



class PSet(object):
    
    __slots__ = ('_map', '__weakref__')

    def __new__(cls, m):
        self = super(PSet, cls).__new__(cls)
        self._map = m
        return self

    def __contains__(self, element):
        return element in self._map

    def __iter__(self):
        return iter(self._map)

    def __len__(self):
        return len(self._map)

    def __repr__(self):
        if not self:
            return 'p' + str(set(self))

        return 'pset([{0}])'.format(str(set(self))[1:-1])

    def __str__(self):
        return self.__repr__()

    def __hash__(self):
        return hash(self._map)

    def __reduce__(self):
        
        return pset, (list(self),)

    @classmethod
    def _from_iterable(cls, it, pre_size=8):
        return PSet(pmap(dict((k, True) for k in it), pre_size=pre_size))

    def add(self, element):
        
        return self.evolver().add(element).persistent()

    def update(self, iterable):
        
        e = self.evolver()
        for element in iterable:
            e.add(element)

        return e.persistent()

    def remove(self, element):
        
        if element in self._map:
            return self.evolver().remove(element).persistent()

        raise KeyError("Element '%s' not present in PSet" % repr(element))

    def discard(self, element):
        
        if element in self._map:
            return self.evolver().remove(element).persistent()

        return self

    class _Evolver(object):
        __slots__ = ('_original_pset', '_pmap_evolver')

        def __init__(self, original_pset):
            self._original_pset = original_pset
            self._pmap_evolver = original_pset._map.evolver()

        def add(self, element):
            self._pmap_evolver[element] = True
            return self

        def remove(self, element):
            del self._pmap_evolver[element]
            return self

        def is_dirty(self):
            return self._pmap_evolver.is_dirty()

        def persistent(self):
            if not self.is_dirty():
                return  self._original_pset

            return PSet(self._pmap_evolver.persistent())

        def __len__(self):
            return len(self._pmap_evolver)

    def copy(self):
        return self

    def evolver(self):
        
        return PSet._Evolver(self)

    
    
    
    
    __le__ = Set.__le__
    __lt__ = Set.__lt__
    __gt__ = Set.__gt__
    __ge__ = Set.__ge__
    __eq__ = Set.__eq__
    __ne__ = Set.__ne__

    __and__ = Set.__and__
    __or__ = Set.__or__
    __sub__ = Set.__sub__
    __xor__ = Set.__xor__

    issubset = __le__
    issuperset = __ge__
    union = __or__
    intersection = __and__
    difference = __sub__
    symmetric_difference = __xor__

    isdisjoint = Set.isdisjoint

Set.register(PSet)
Hashable.register(PSet)

_EMPTY_PSET = PSet(pmap())


def pset(iterable=(), pre_size=8):
    
    if not iterable:
        return _EMPTY_PSET

    return PSet._from_iterable(iterable, pre_size=pre_size)


def s(*elements):
    
    return pset(elements)

#https://www.hackerrank.com/challenges/kingdom-division/
#nao roda por causa dos imports, baixei uns inputs e parece funcionar

from dataclasses import dataclass
from functools import partial
import typing
from functools import reduce
my_set = typing.Any



# o tipo foi descoberto colocando isso num arquivo teste.py, e rodando 'mypy teste.py'
# import pyrsistent
# a = pyrsistent.pset() 
# reveal_type(a)

def somar(a : int ,
          b : int,
          c : int) -> int:
    return a+b+c

r = partial(somar,2,3)

def test1() -> None:
    """
    Example doctest
    >>> r(5)
    10
    >>> r(c=5)
    10
    """
    pass

@dataclass
class Tree:
    root: int
    connections: list[set[int]] 
    max_ver: int

a = Tree(root=0,connections=[set([1,2,3]), set([3,4,5])], max_ver=10)

def nums2prob(tup : tuple[int, list[tuple[int,int]]]) -> Tree:
    (max_ver,vertices) = tup
    initial_adjs: list[set[int]] = [set() for i in range(0,max_ver+1)] 
    t = Tree (1,initial_adjs,max_ver=max_ver)
    for (a,b) in vertices:
        t.connections[a].add(b)
        t.connections[b].add(a)
    return t

def line2tuple(string):
    (a,b)= [int(x) for x in string.split(' ')]
    return (a,b)

def readProb(string : str) -> tuple[int, list[tuple[int,int]]]:
    lines         = string.splitlines()
    max_ver       = int(lines[0])
    vertices      = [line2tuple(line) for line in lines[1:]]
    if (any ([x == None for x in vertices])):
        raise ValueError("parsing the vertices failed", vertices, lines[1:])
    return (max_ver,vertices)

# all variables assume red root 

#slightly_invalid_possibilities: all children are valid, all children have one colour, you have another
#                              : multiply the valid_possibilities for each child (invert their colors, they have blue root, whole tree has red root)

#valid possibilities: for each child, pick either a valid (blue or red in the root) or invalid possibility (red in the root)
#                   : then remove all the invalids (see above)

def product(list :list[int]) -> int:
    """
    Example doctest
    >>> product([])
    1
    >>> product([1,2,3,4])
    24
    >>> product([1,2,4])
    8
    """
    if list == []:
        return 1
    return reduce ((lambda x,y: x*y), list)

valid_possibilities_d : dict[int, int]= {}
def valid_possibilities(tree : Tree, root : int, used : my_set) -> int:
    if root in valid_possibilities_d:
        return valid_possibilities_d[root]
    neighboors = availableAdj(tree,root,used)
    if len(neighboors) == 0:
        valid_possibilities_d[root] = 0
        return valid_possibilities_d[root]
    new_used   = used.add(root)
    amounts    = [valid_possibilities(tree,n,new_used)*2 + invalid_possibilities(tree,n,new_used) for n in neighboors]
    correction = invalid_possibilities(tree,root,new_used)
    ans = (product(amounts) - correction)
    valid_possibilities_d[root] = ans
    return valid_possibilities_d[root]

invalid_possibilities_d : dict[int, int] = {}
def invalid_possibilities(tree : Tree, root : int, used : my_set) -> int:
    if root in invalid_possibilities_d:
        return invalid_possibilities_d[root]
    neighboors = availableAdj(tree,root,used)
    if len(neighboors) == 0:
        invalid_possibilities_d[root] = 1
        return invalid_possibilities_d[root]
    new_used   = used.add(root)
    correction = [valid_possibilities(tree,n,new_used) for n in neighboors]
    invalid_possibilities_d[root] = product(correction)
    return invalid_possibilities_d[root]

def solve(tree : Tree) -> int:
    if tree.max_ver == 1:
        return 2
    used = pset() 
    return 2*valid_possibilities(tree, root=1, used=used)

A = typing.TypeVar('A')
B = typing.TypeVar('B')
C = typing.TypeVar('C')
D = typing.TypeVar('D')
E = typing.TypeVar('E')

def composite2(f : typing.Callable[[B],C], g : typing.Callable[[A],B]) -> typing.Callable[[A],C]:
    def result(x : A) -> C:
        a = g(x)
        return f(a)
    return result

def composite3(f : typing.Callable[[C],D], 
               g : typing.Callable[[B],C], 
               h : typing.Callable[[A],B]) -> typing.Callable[[A],D]:
    def result(a0 : A) -> D:
        a1 = h(a0)
        a2 = g(a1)
        a3 = f(a2)
        return a3
    return result


def composite4(f : typing.Callable[[D],E], 
               g : typing.Callable[[C],D], 
               h : typing.Callable[[B],C], 
               i : typing.Callable[[A],B]) -> typing.Callable[[A],E]:
    def result(a0 : A) -> E:
        a1 = i(a0)
        a2 = h(a1)
        a3 = g(a2)
        a4 = f(a3)
        return a4
    return result


def f (x : int) -> int: 
       return x+1

ff = composite2(f,f)

ff2: typing.Callable[[int], int] = ff #no error given!


def availableAdj (tree: Tree, vertex: int, used : my_set) -> set[int]:
    allAdj = tree.connections[vertex]
    return allAdj - set(used)

def test() -> None:
    import doctest
    doctest.testmod()

def main() -> None:
    import sys
    problem = sys.stdin.read()
    def f (x : int) -> int : return x % (10**9+7)
    run     = composite4(f,solve,nums2prob,readProb)
    print(run(problem))
    #print(nums2prob(readProb(problem)))


main()