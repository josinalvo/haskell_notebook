import qualified Data.Set as Set 
import qualified Data.Map as Map

data Tree = Tree {root ::Int, max_ver :: Int, connections :: Map.Map Int (Set.Set Int) } deriving Show

getTree :: Int -> [(Int,Int)] -> Tree
getTree max_ver connections = Tree 1 max_ver mapComesNGoes
    where startingMap  = Map.fromList (zip [1..max_ver] (repeat Set.empty))
          mapGoes      = Prelude.foldr (\(a,b) map -> Map.adjust (Set.insert b) a map) startingMap connections
          mapComesNGoes= Prelude.foldr (\(a,b) map -> Map.adjust (Set.insert a) b map) mapGoes     connections

data SI = SI {sustentadosVermelhos :: Integer, insustentadosVermelhos :: Integer}

-- TODO: nome decente
banana :: Int -> Set.Set Int -> Tree -> SI
banana root used tree = SI sustentados insustentados
    where new_used          = Set.insert root used
          neighbors         = Set.difference ((connections tree) Map.! root)  used
          bananas           = fmap (\root2 -> banana root2 new_used tree) (Set.toList neighbors)
          allPossibilities  = product [2*(sustentadosVermelhos a) + (insustentadosVermelhos a) | a <- bananas]
          badPossibilities  = product [sustentadosVermelhos a | a <- bananas]
          sustentados       = if (Set.size neighbors == 0) then 0 else allPossibilities - badPossibilities
          insustentados     = if (Set.size neighbors == 0) then 1 else badPossibilities

solve :: Tree -> Integer
solve tree@(Tree{max_ver=a}) = if (a == 1) then 2 else (2 * sustentados)
    where (SI sustentados _) = banana 1 Set.empty tree 

readTuple :: String -> (Int, Int)
readTuple s = (a,b)
    where [a,b] = fmap read (words s)

readTuples :: String -> [(Int,Int)]
readTuples s = fmap readTuple (lines s)

readInt :: String -> Int
readInt = read

mainProblem = do
    max_ver <- readInt <$> getLine
    edges <- readTuples <$> getContents
    let tree = getTree max_ver edges
    -- print tree
    print (mod (solve tree) (10^9+7))

mainFakeProblem n = do
    let max_ver = n
    let edges = zip [1..(n-1)] [2..n]
    let tree = getTree max_ver edges
    print (mod (solve tree) (10^9+7))



foolsLine :: Int -> Int
foolsLine n = (2*) $ foolsLine' n

results :: [Int]
results = [0,1,1] ++ fmap foolsLine' [3..]

foolsLine' :: Int -> Int
foolsLine' n  = (+1) $ sum possibleLines
    where possibleSteps = [2..(n-2)] 
          possibleLines = fmap (\i -> results !! (n-i) ) possibleSteps

mainTest n = print (foolsLine n)

-- main = mainTest 5 >> mainFakeProblem 5
main = mainProblem
