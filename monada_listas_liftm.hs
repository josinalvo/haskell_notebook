import Control.Applicative

newtype ListaFeia a = ListaFeia [a] deriving Show

instance Functor ListaFeia where
    fmap = liftMum
    


liftMum :: (a -> b) -> ListaFeia a -> ListaFeia b
liftMum f m = do
           v <- m
           return $ f v

-- (>>=) ListaFeia a -> (a -> ListaFeia b) -> ListaFeia b
liftMtres :: (a -> b) -> ListaFeia a -> ListaFeia b
liftMtres f listaFeia = listaFeia >>= (\v -> return $ f v)

-- >>> fmap (+1) $ ListaFeia [10,20,30]
-- ListaFeia [11,21,31]

instance Applicative ListaFeia where
    -- pure = return
    pure = return
    (<*>) = app
    --(<*>) = undefined
    -- liftA2 = liftA2Chong

liftA2Meu :: (a->b->c) -> ListaFeia a -> ListaFeia b -> ListaFeia c
liftA2Meu f as bs = as >>= (\a -> (bs >>= \b -> return $ f a b))


liftA2Chong :: (a->b->c) -> ListaFeia a -> ListaFeia b -> ListaFeia c
liftA2Chong f as bs = cs 
    where f1 = fmap f as -- :: [(b->c)]
          cs = f1 >>= (\f -> fmap f bs)

--mf `app` mv = mf >>= (`fmap` mv) 
--app monfuncs monvals = monfuncs >>= (`fmap` monvals) 
app monfuncs = (monfuncs >>=) . (flip fmap) 

-- >>> pure (+) <*> ListaFeia [10,20,30] <*> ListaFeia [1,5]
-- ListaFeia [11,15,21,25,31,35]


instance Monad ListaFeia where
    return a = ListaFeia $ return a
    (ListaFeia a) >>= f = ListaFeia feia
        where unFeia (ListaFeia a) = a
              feia  = (a >>= unFeia . f)


a :: ListaFeia Int
a = do 
    n1 <- ListaFeia [1,2,3]
    n2 <- ListaFeia [10,20,30]
    return $ n1+n2

-- >>> a
-- ListaFeia [11,21,31,12,22,32,13,23,33]

