module ZipZip where

import Control.Applicative

-- ziplists aparentemente desrespeitam alguma das leis de monada
-- comecei e nao terminei de implementar uma monada pra elas

-- |
-- >>> fmap (+1) $ ZipList [10,20,30]
-- ZipList {getZipList = [11,21,31]}
-- >>> pure (+) <*> ZipList [10,20,30] <*> ZipList [100,200]
-- ZipList {getZipList = [110,220]}

instance Monad ZipList where
    return = pure
    (>>=)  = undefined

g = do 
    a <- (ZipList [10,20,30])
    return (a+1)

f = do
    a <- (ZipList [10,20,30])
    b <- ZipList [100,200,300]
    return (a+b)