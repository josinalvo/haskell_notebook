{-# OPTIONS_GHC -Wno-unrecognised-pragmas #-}
{-# HLINT ignore "Eta reduce" #-}

import Control.Lens
import GHC.Base
data Coisa a b = Coisa a (Maybe b) deriving Show 

instance Monoid a => Functor (Coisa a) where
   fmap f (Coisa n l1) = Coisa n (fmap f l1)

instance Monoid a => Applicative (Coisa a) where
   pure a = Coisa mempty (Just a)
   --(<*>) (Coisa n1 l1) (Coisa n2 l2) = Coisa (n1 <> n2) (l1 <*> l2)
   liftA2 f (Coisa n1 l1) (Coisa n2 l2) = Coisa (n1 <> n2) (liftA2 f l1 l2)



-- | 
-- >>> (Coisa "ban" (Just (+2))) <*> (Coisa "ana" Nothing)
-- Coisa "banana" Nothing



fold' list = getConst . sequenceA . fmap Const $ list

getCoisa (Coisa a b) = a

fold'' list = -- getCoisa . 
              sequenceA . fmap (\x -> Coisa x (Just 2)) $ list


-- >>> fold'' ["ba","na","ni","nha"]
-- Coisa "bananinha" (Just [2,2,2,2])


-- >>> sequenceA [Just 1,Just 1,Just 1,Just 1]
-- [[1,1,1,1]]

