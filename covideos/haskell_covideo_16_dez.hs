-- operacoes prefixos tem que ser letra,
-- operacoes infixas tem que ser simbolo
-- data constructors infixos tem que começar com :

import Data.Maybe

a = [Just 12, Just 15, Nothing , Just 30]

b = catMaybes a

c = concat [[1,2],[3,4,5],[6]]

-- $
-- >>> b
-- [12,15,30]
-- >>> c
-- [1,2,3,4,5,6]

main = print 12