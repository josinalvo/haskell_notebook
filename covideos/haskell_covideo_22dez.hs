sumCubesM n = (map f [0..]) !! n
   where f 0 = 0
         f 1 = 1
         f n = n^3 + sumCubesM (n-1)

list = (map f [0..])
f 0 = 0
f 1 = 1
f n = n^3 + sumCubesRM (n-1)
sumCubesRM n = list !! n 

sumCubes 0 = 0
sumCubes n = n^3 + sumCubes (n-1)

x = sumCubes $ 3000*1000
y = sumCubes $ (3000*1000-1)

x' = sumCubesM (30*1000)
y' = sumCubesM (30*1000-1)


x'' = sumCubesRM (100*1000)
y'' = sumCubesRM (100*1000-1)

asTypeOf' :: a -> a -> a
asTypeOf' x y = y

-- sequences sao legais pra mudar 1 valor
-- vector vc quer mudar tudo
--sec a b
--tb $!

--b contém a, forço a avaliação de a e retorno b
--
--stack new project simple-hpack
