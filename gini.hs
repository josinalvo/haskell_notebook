import Data.List.Extra (sum')
import Data.List (sort, scanl', foldl') -- scanl' strict 

gini :: [Double] -> Double
gini l = sum'(diffs) / (size*size*2)
    where avg   = (sum' l)/size
          diffs = [abs(x1/avg-x2/avg) | x1 <- l, x2 <- l]
          size  = (fromIntegral $ length l)

test_case :: [Double]
test_case = (replicate 10 1)

gini2 l = (ideal_area - empirical_area)/ideal_area
    where sorted = sort l
          accs :: [Double]
          accs   = scanl' (+) 0 sorted
          empirical_area :: Double
          empirical_area = sum' accs
          all_the_cash :: Double
          all_the_cash = last accs
          ideal_area  = sum' $ scanl' (+) 0 (replicate sizeInt (all_the_cash/size)) -- TODO: verificar erro por 1
          size   = fromIntegral $ length l :: Double
          sizeInt= length l 

ginis l = (gini l, gini2 l)

-- |
-- >>> ginis $ 1: (replicate 9 0)
-- >>> ginis (replicate 10 1)
-- >>> ginis $ 1 : (replicate 999 0)
-- (0.9,0.8181818181818182)
-- (0.0,0.0)
-- (0.999,0.998001998001998)



main = print $ gini2 test_case
