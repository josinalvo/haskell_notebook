relist_exclude_h :: Int -> [a] -> (a,[a])
relist_exclude_h n list = foldr aux (list!!0, []) (zip [0..] list)
    where aux (_,_) _  = (list!!0,[])

relist_exclude_p :: Int -> [a] -> (a,[a])
relist_exclude_p n list = foldr aux (list!!0, []) (zip [0..] list)
        where aux (_,_) tuple  = (list!!0,[])
    

relist_exclude_z :: Int -> [a] -> (a,[a])
relist_exclude_z n list = foldr aux (list!!0, []) (zip [0..] list)
    where aux (_,_) (_,_)  = (list!!0,[])



relist_exclude_omega :: Int -> [a] -> (a,[a])
relist_exclude_omega n list = foldr aux (list!!0, []) (zip [0..] list)
    where aux (pos, val) tuple  = if pos == n
                                  then (val,(snd tuple))
                                  else ((fst tuple),val:(snd tuple))


inf = [0..]
h = fst $ relist_exclude_h 3 inf
p = fst $ relist_exclude_p 3 inf
z = fst $ relist_exclude_z 3 inf
omega = fst $ relist_exclude_omega 3 inf

-- relist :: [a] -> [a]
-- relist = foldr (:) []

-- relist2 :: [a] -> [a]
-- relist2 list = foldr (\a b -> (snd a):b) [] (zip [0..] list)

-- relist_exclude :: Int -> [a] -> [a]
-- relist_exclude n list = foldr aux [] (zip [0..] list)
--     where aux (pos, val) rest = if pos == n
--                                 then rest
--                                 else val:rest

-- relist_exclude_h :: Int -> [a] -> (a,[a])
-- relist_exclude_h n list = foldr aux (list!!0, []) (zip [0..] list)
--     where aux _ _  = (list!!0,[])

-- relist_exclude_j :: Int -> [a] -> (a,[a])
-- relist_exclude_j n list = foldr aux (list!!0, []) (zip [0..] list)
--     where aux (pos, val) tuple  = if pos == n
--                                   then (val,(snd tuple))
--                                   else (list!!0,val:(snd tuple))


-- relist_exclude_z :: Int -> [a] -> (a,[a])
-- relist_exclude_z n list = foldr aux (list!!0, []) (zip [0..] list)
--     where aux (_,_) (_,_)  = (list!!0,[])


-- relist_exclude0 :: Int -> [a] -> (a,[a])
-- relist_exclude0 n list = foldr aux (list!!0, []) (zip [0..] list)
--     where aux (pos, val) (_, _)  = (list!!0,[])




-- relist_exclude1 :: Int -> [a] -> (a,[a])
-- relist_exclude1 n list = foldr aux (list!!0, []) (zip [0..] list)
--     where aux (pos, val) (_, rest)  = if pos == n
--                                       then (list!!0,rest)
--                                       else (list!!0,val:rest)


-- relist_exclude3 :: Int -> [a] -> (a,[a])
-- relist_exclude3 n list = foldr aux (list!!0, []) (zip [0..] list)
--     where aux (pos, val) (selected, built_list) 
--              | pos == n = (val,built_list)
--              | pos < n  = (selected, val:built_list)
--              | pos > n  = (list!!0, val:built_list)
                                

-- inf = [1..]
