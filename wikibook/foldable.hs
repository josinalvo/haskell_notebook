import Data.Monoid hiding (Endo)
import Data.Semigroup hiding (Endo)
-- foldr :: (a -> b -> b) -> b -> [a] -> b

foldMap :: Monoid m => (a -> m) -> [a] -> m
foldMap g = mconcat . fmap g

newtype Endo b = Endo { appEndo :: b -> b }

instance Semigroup (Endo b) where
    (<>) = undefined

instance Monoid (Endo b) where
    mempty                  = Endo id
    Endo g `mappend` Endo f = Endo (g . f)

foldComposing :: (a -> (b -> b)) -> [a] -> Endo b
foldComposing f = Main.foldMap (Endo . f)

-- (b -> b)
-- faz sentido q seja um monoid

-- Operacao <> do endo é composicao


foldrNosso :: (a -> b -> b) -> b -> [a] -> b
foldrNosso f acc l = finalF acc
    where Endo finalF = foldComposing f l

-- | 
-- >>> foldrNosso (+) 0 [1,2,3]
-- 6


concatMapNosso :: Foldable t => (a -> [b]) -> t a -> [b]
concatMapNosso f list = Prelude.foldMap f list

concatNosso :: Foldable t => t [a] -> [a]
concatNosso list = Prelude.foldMap id list


elemNosso :: (Foldable t, Eq a) => t a -> a -> Bool
elemNosso list target = getAny $ Prelude.foldMap (Any . (== target)) list

-- >>> elemNosso [1,2,4] 3
-- False
-- >>> elemNosso [1,2,4] 2
-- True

-- Ord a => Semigroup (Max a)
-- Semigroup a => Monoid (Maybe a)

safeMaximum :: (Ord a, Foldable t) => t a -> Maybe a
safeMaximum list = undress $ Prelude.foldMap (Just . Max) list
    where undress (Just (Max a)) = Just a
          undress Nothing        = Nothing

-- >>> safeMaximum [1,2,3]
-- Just 3
-- >>> safeMaximum []
-- Nothing

newtype AltS a = AltS a

instance Semigroup (AltS a) where
    AltS x <> AltS y = AltS x

findNosso :: Foldable t => (a -> Bool) -> t a -> Maybe a
findNosso f list = undress $ Prelude.foldMap checker list
    where checker a 
            | f a       = (Just . AltS) a
            | otherwise = Nothing
          undress (Just (AltS a)) = Just a
          undress Nothing        = Nothing