{-# LANGUAGE NamedFieldPuns #-}


data Configuration = Configuration {localHost :: String, remoteHost:: String}

getHostData (Configuration { localHost = lh, remoteHost = rh }) = (lh, rh) 

getHostData2 (Configuration {localHost, remoteHost = rh}) = (localHost, rh)
-- tbhis depends on {-# LANGUAGE NamedFieldPuns #-}

-- bad ?
c1 = Configuration {localHost = "karaboudjan"}

-- | 
-- >>> localHost c1
-- "karaboudjan"
-- >>> remoteHost c1
-- ...Exception...
-- ...

main = print 42