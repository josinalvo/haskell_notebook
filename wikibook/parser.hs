import Text.ParserCombinators.ReadP
import Control.Applicative

data Tree = Branch Tree Tree | Leaf deriving Show

leaf = do char 'o'
          return Leaf

branch = do a <- leaf
            char '&'
            b <- tree
            return (Branch a b)
   
tree = leaf +++ branch

-- |
-- >>> readP_to_S tree "o"
-- [(Leaf,"")]
-- >>> readP_to_S tree "o&o"
-- [(Leaf,"&o"),(Branch Leaf Leaf,"")]

brackets p = do 
    char '('
    r <- p
    char ')'
    return r

branch2 = do a <- leaf +++ brackets tree2
             char '&'
             b <- tree2 
             return (Branch a b)
   
tree2 = leaf +++ branch2 +++ brackets tree2

-- |
-- >>> readP_to_S tree2 "o"
-- [(Leaf,"")]
-- >>> readP_to_S tree2 "o&o"
-- [(Leaf,"&o"),(Branch Leaf Leaf,"")]
-- >>> readP_to_S tree2 "(o&o)&o"
-- ...(Branch (Branch Leaf Leaf) Leaf,"")]
-- >>> readP_to_S tree2 "(o&o)&(o&o)"
-- ...(Branch (Branch Leaf Leaf) (Branch Leaf Leaf),"")]
-- >>> readP_to_S tree2 "(((o&o))&((o&o)))"
-- ...(Branch (Branch Leaf Leaf) (Branch Leaf Leaf),"")]

data Tree3 = Branch3 Tree3 Tree3 | Leaf3 String deriving Show

main = print 42
