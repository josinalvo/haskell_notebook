{-# LANGUAGE NamedFieldPuns #-}
answer = 2 * {-
  block comment, crossing lines and...
  -} 3 {- inline comment. -} * 7

r = r + 1 -- definicao recursiva valida

-- "consequencia", se x = y + 1, podemos definir y antes ou depois

-- *Main> :type negate -- goes from 2 to -2
-- negate :: Num a => a -> a


ff d = case d of
    (_,x:xs) -> x
    (a,_)    -> a

fff x = case x of 0 -> 18
                  1 -> 15
                  2 -> 12
                  _ -> 12 - x

doGuessing num = do
  putStrLn "Enter your guess:"
  guess <- getLine
  case compare (read guess) num of
    LT -> do putStrLn "Too low!"
             doGuessing num
    GT -> do putStrLn "Too high!"
             doGuessing num
    EQ -> putStrLn "You Win!"

j x = let lsq = (log x) ^ 2 in tan lsq
--let é uma expressão, uma conta q pode ir em qq lugar

doStuff :: Int -> String
doStuff x
  | x < 3     = report "less than three"
  | otherwise = report "normal"
  where
    report y = "the input is " ++ y
-- where é uma sintaxe mais a parte (super conveniente pra cases)




for :: a -> (a -> Bool) -> (a -> a) -> (a -> IO ()) -> IO ()
for a pred step ioFun = faux a
      where faux a = if pred a
                     then do
                        ioFun a
                        faux (step a)
                        else (return ())


jj = for 1 (<10) (+1) print

force ma mb = 
    do a <- ma
       b <- mb
       if (a == () && b == ()) then (return ())
                               else (return ())

k :: IO ()
k = foldl1 force (map print [1..10])

sequenceIO :: [IO a] -> IO [a]
sequenceIO [] = return []
sequenceIO (xm:xs) = do
    x <- xm
    rest <- sequenceIO xs
    return (x:rest)


k2 :: IO [()]
k2 = sequenceIO (map print [1..13])

sequenceIO' :: [IO a] -> IO [a]
sequenceIO' l = foldr f (return []) l
    where f m_elem m_list = do 
                            elem <- m_elem
                            list <- m_list
                            return (elem:list)

k3 :: IO [()]
k3 = sequenceIO' (map print [1..8])

mapIO :: (a -> IO b) -> [a] -> IO [b]
mapIO f as = foldr f' (return []) as
  where f' a m_bs = do new_b <- f a
                       bs <- m_bs
                       return (new_b:bs)

printAndAdd :: (Show b, Num b) => b -> IO b
printAndAdd n = do {print n; return (n+1)}

testMap = do list <- sequenceIO' l
             print $ head list
             where l = map printAndAdd [10,20,30] :: [IO Int]
-- you did not use mapIO

main = mapIO print [1,2,3]

testMap2 = do list <- mapIO printAndAdd [100,200,300]
              print $ head list

