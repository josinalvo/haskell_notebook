-- Some of the examples in this chapter require a few GHC extensions:
-- TemplateHaskell is needed for makeLenses; RankNTypes is needed for
-- a few type signatures later on.
{-# LANGUAGE TemplateHaskell, FlexibleContexts #-}

import Control.Lens
import Control.Lens.Tuple
import Test.DocTest

data Point = Point
    { _positionX :: Double
    , _positionY :: Double
    , _eixos     :: (String,String)  
    } deriving (Show)
makeLenses ''Point

data Segment = Segment
    { _segmentStart :: Point
    , _segmentEnd :: Point
    } deriving (Show)
makeLenses ''Segment

makePoint :: (Double, Double) -> Point
makePoint (x, y) = Point x y ("Eixo X", "Eixo Y")

makeSegment :: (Double, Double) -> (Double, Double) -> Segment
makeSegment start end = Segment (makePoint start) (makePoint end)

testSeg = makeSegment (0,1) (2,4)

-- |
-- >>> view (segmentEnd . positionY) testSeg
-- 4.0

-- |
-- >>> view (segmentEnd . eixos . _2) testSeg
-- "Eixo Y"

-- |
-- >>> set (segmentEnd . eixos . _2) "Eixo Caralho" testSeg
-- Segment {_segmentStart = Point {_positionX = 0.0, _positionY = 1.0, _eixos = ("Eixo X","Eixo Y")}, _segmentEnd = Point {_positionX = 2.0, _positionY = 4.0, _eixos = ("Eixo X","Eixo Caralho")}}

-- |
-- >>> _2 .~ "hello" $ (1,(),3,4)
-- (1,"hello",3,4)

-- |
-- >>> view (_2 . _2) (1,(3,"banana"),3,4)
-- "banana"


-- |
-- >>> (_positionY . _segmentEnd) testSeg
-- 4.0

-- |
-- >>> set (segmentEnd.positionY) 9 testSeg
-- Segment {..., _segmentEnd = Point {_positionX = 2.0, _positionY = 9.0}}

-- |
-- >>> over (segmentEnd.positionY) (*2) testSeg
-- Segment {..., _segmentEnd = Point {_positionX = 2.0, _positionY = 8.0}}

main = doctest ["./lenses.hs"]



