import Text.Read
import Data.Maybe

-- IO Int só pra ver se conseguia
first :: IO Int
first = do 
   putStr  "please input a number, and you shall receive it doubled and doubled: "
   n <- getLine
   result $ fmap (*2) $ val n
   return 42
   where 
       val n = (readMaybe n :: Maybe Double )
       result (Just a) = do 
                            putStrLn $ show a
                            return 42
       result (Nothing) = do
                            putStrLn "that was not valid"
                            first

second = do
    putStrLn  "please input two numbers, and you shall receive them joined: "
    n1 <- getLine
    if valid n1
        then do 
             n2 <- getLine
             result $ val n1 n2
        else do putStrLn "your first number is invalid. One per line, please"
                second
    return ()
    where 
       val n1 n2 = ((+) <$> readMaybe n1 <*> readMaybe n2) :: Maybe Double 
       result (Just a) = putStrLn $ show a
    
       result Nothing  = do 
                            putStrLn "Your second number was invalid"
                            second
       valid a = isJust ( readMaybe a :: Maybe Double )
                 

main = second
                            
    
