import Text.ParserCombinators.ReadP

leaf = do s <- many1 (choice (map char ['a'..'z']))
          return (Leaf s)

-- For the next adaptation we try and add a new operation "|" which binders weaker than "&". I.e. "foo&bar|baz" should parse as "(foo&bar)|baz". First we need to update the data type representing syntax:

data Operator = And | Or deriving Show

data Tree = Branch Operator Tree Tree | Leaf String deriving Show

-- The obvious thing to do is duplicate the `branch` function and call it `andBranch` and `orBranch`, and give or precedence using the left choice operator `<++`:

brackets p = do
             char '('
             val <- p
             char ')'
             return val


wandBranch = do a <- leaf +++ brackets wrongTree
                char '&'
                b <- wrongTree
                return (Branch And a b)

worBranch = do a <- leaf +++ brackets  wrongTree
               char '|'
               b <-  wrongTree
               return (Branch Or a b)

wrongTree = leaf +++ (worBranch <++ wandBranch) +++ brackets  wrongTree

-- |
-- >>> readP_to_S wrongTree "o&o"
-- [(Leaf "o","&o"),(Branch And (Leaf "o") (Leaf "o"),"")]
-- >>> last $ readP_to_S wrongTree "a&b|s"
-- (Branch And (Leaf "a") (Branch Or (Leaf "b") (Leaf "s")),"")

atom = leaf +++ brackets tree

andCentered = do a <- atom
                 char '&'
                 b <- atom +++ andCentered
                 return $ Branch And a b


orCentered = do a <- atom +++ andCentered
                char '|'
                b <- tree
                return $ Branch Or a b

tree = atom +++ andCentered +++ orCentered


-- | 
-- >>> readP_to_S tree "(foo&bar|baz)"
-- [(Branch Or (Branch And (Leaf "foo") (Leaf "bar")) (Leaf "baz"),"")]
-- >>> readP_to_S tree "(foo|bar&baz)"
-- [(Branch Or (Leaf "foo") (Branch And (Leaf "bar") (Leaf "baz")),"")]
-- >>> readP_to_S tree "(foo|queijo&bar&baz)"
-- [(Branch Or (Leaf "foo") (Branch And (Leaf "queijo") (Branch And (Leaf "bar") (Leaf "baz"))),"")]
-- >>> readP_to_S tree "(a&b&c)"
-- [(Branch And (Leaf "a") (Branch And (Leaf "b") (Leaf "c")),"")]
-- >>> readP_to_S tree "(a&b|c&d)"
-- [(Branch Or (Branch And (Leaf "a") (Leaf "b")) (Branch And (Leaf "c") (Leaf "d")),"")]
