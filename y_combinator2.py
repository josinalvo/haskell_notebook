'''
primeiramente, assumimos que já da pra entender o fatorial recursivo
normal. Se não der, você deve procurar um artigo especifico sobre
recursão
'''

def fatorial(n):
    if n == 0:
        return 1
    else:
        return fatorial(n-1)*n


'''
essa função usa uma caracteristica importante e não obvia do interpretador
de python: ele consegue ler "fatorial" no meio da definição de fatorial
e não reclamar que fatorial ainda não foi definido

Parando pra pensar, isso é tipo escrever a=a+2, sem ter declarado o valor
de a. Para calcular a eu tenho que saber a.
Por isso eu disse "característica não óbvia".

A nossa pergunta é: será que isso é necessário?
Talvez eu pudesse escrever fatorial recursivamente sem esse suporte da linguagem'''

def fatorial_r(r):
    def middle_r(n):
        if n == 0:
            return 1
        else:
            return r(n-1)*n
    return middle_r

fatorial_v1 = lambda n: fatorial_r(fatorial_r)(3)

'''
Esse plano acima não funciona. A primeira chamada de fatorial até que faz sentido 
fatorial_v1(3) 
    vira
fatorial_r(fatorial_r)(3)
    que vira
middle(3) com r sendo fatorial_r
    que vira fatorial_r(n-1)*n, com n=3. Ou seja,
fatorial_r(2)*3

Mas temos um problema: fatorial_r uma funcao e depois um número, mas só passamos a funcao.
'''

def fatorial_double(p):
    def middle_double(n):
        if n == 0:
            return 1
        else:
            return (p(p)(n-1))*n
    return middle_double

fatorial_v2 = lambda n: fatorial_double(fatorial_double)(n)

'''
Acho que resolvemos o problema!

Fatorial3 sempre tem um fatorial_double pra rodar.
Vamos tentar escrever uma "execução por substituição" de fatorial3(4)

fatorial3(4)
fatorial_double(fatorial_double,3)*4
   mas
   fatorial_double(fatorial_double,3)
   vira
   fatorial_double(fatorial_double,2)*3
   entao temos
fatorial_double(fatorial_double,2)*3*4
   e se continuarmos, teremos, por fim
fatorial_double(fatorial_double,0)*1*2*3*4
1*1*2*3*4
1*2*3*4
   como queriamos
'''

'''
Mas estamos meio tristes...
Em algum sentido, fatorial_r deveria ter funcionado
Ou, pelo menos, podemos dizer que fatorial_r é
uma especificação clara do algoritmo que queremos usar. O python
não roda, mas conseguimos saber exatamente o que é pra fazer.

Entao, o que queremos é uma forma de converter fatorial_r em fatorial_double

Só copiamos as duas definições abaixo
'''

def fatorial_r(r):
    def middle_r(n):
        if n == 0:
            return 1
        else:
            return r(n-1)*n
    return middle_r

def fatorial_double(p):
    def middle_double(n):
        if n == 0:
            return 1
        else:
            return (p(p)(n-1))*n
    return middle_double

fatorial_v2 = lambda n: fatorial_double(fatorial_double)(n)

#e definimos a dobradora -- que transforma uma funcao que recebe f
#e contém aplicação de f em n para uma funcao que recebe f e 
# contém, no mesmo trecho de código aplicação de f(f) em n

def dobradora(func):
    def h(f):
        def new_h(n):
            return f(f)(n)
        return func(new_h)
    return h

# quando h (=dobradora(func)) receber f, executará exatamente
# como func executaria, se tivesse recebido a função f(f)

'''
Vamos tentar deixar essa afirmação mais explicita, calculando
dobradora(fatorial_r)

dobradora(fatorial_r)
    é o mesmo que

def h(f):
    def new_h(n):
        return f(f)(n)
    return func(new_h)

Onde func é fatorial_r. Ou seja, o mesmo que

def h(f):
    def new_h(n):
        return f(f)(n)
    return fatorial_r(new_h)

Ou seja, temos

def fatorial_r(r):
    def middle_r(n):
        if n == 0:
            return 1
        else:
            return r(n-1)*n
    return middle_r

Onde r é new_h. 

Ou seja, temos

def middle_r(n):
    if n == 0:
        return 1
    else:
        return new_h(n-1)*n


'''
fatorial_v3 = lambda n: dobradora(fatorial_r)(n)

#fatorial4 = Z(fatorial_errado1)
parcial = Z_part(fatorial_errado1)
fatorial666 = parcial(parcial)


def Z(func):
    def h(f):
        def new_h(n):
            return f(f)(n)
        return func(new_h)
    h2 = h(h)
    return h2
'''
Teste esse fatorial4. Ele funciona!

O que aconteceu?

Primeiramente, vejamos a função h (que está definida dentro de Z).
Ao criarmos fatorial4, fizemos func = fatorial_errado1.
Nesse contexto, o que é h(rec)? -- rec sendo uma função qualquer 

h(rec)
factorial_errado1(lambda n: rec(rec)(n)) 
    fatorial_errado1 recebeu uma função e retornou a seguinte função
lambda n: 1 if n==0 else n*( rec(rec)(n-1) )

Vale a pena comparar essa expressão com fatorial maroto
'''
def fatorial_maroto(f,n):
    if n == 0:
        return 1
    else:
        return f(f,n-1)*n
'''
h(rec) é a mesma coisa que fatorial_maroto(rec)!

Assim, h(h) é a mesma coisa que fatorial_maroto(fatorial_maroto), que era o plano! :)
'''
def Z(func):
    def h(f):
        def new_h(n):
            return f(f)(n)
        return func(new_h)
    #linha suspeita -- h(rec) onde rec é uma funcao qualquer
    h2 = h(h)
    return h2
