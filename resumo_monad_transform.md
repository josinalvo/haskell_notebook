Prelude> (Left 2) >> (Right 3)
Left 2
Prelude> (Left 2) >> (Left 3)
Left 2
Prelude> (Right 2) >> (Left 3)
Left 3

Data text versus string

either :: (a -> c) -> (b -> c)
       -> Either a b -> c

data EitherIO e a = EitherIO {
    runEitherIO :: IO (Either e a)
}


λ> :type EitherIO
EitherIO :: IO (Either e a) -> EitherIO e a

λ> :type runEitherIO
runEitherIO :: EitherIO e a -> IO (Either e a)

instance Functor (EitherIO e) where
    fmap f = EitherIO . fmap (fmap f) . runEitherIO