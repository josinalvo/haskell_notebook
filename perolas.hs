{-# OPTIONS_GHC -Wno-incomplete-patterns #-}



import Data.List (partition)


-- lista cresc com todos os 2^i * 3^j * 5^k
-- aparentemente é um hello world para algumas pessoas
hamm = 1 : (twos `union` threes `union` fives)
     where twos   = map (*2) hamm
           threes = map (*3) hamm
           fives  = map (*5) hamm
      
union :: Ord a => [a] -> [a] -> [a]
union (x:xs) (y:ys) = case (compare x y) of
          LT   -> x : union  xs  (y:ys)
          EQ   -> x : union  xs     ys 
          GT   -> y : union (x:xs)  ys
union a      b      = error "hamm does not come here"
-- nao precisa pra hamm, pq hamm eh infinito
-- union a      b      = a ++ b

-- |
-- >>> take 11 hamm
-- [1,2,3,4,5,6,8,9,10,12,15]

mergeSort2 :: Ord a => [a] -> [a]
mergeSort2 []    = []
mergeSort2 elems = head $ until (null.tail) step [[x] | x <- elems]
    where step (l1:l2:otherLs)    = merge l1 l2 : step otherLs
          step  l                 = l
          merge (e1:es1) (e2:es2) 
               | (e1 < e2)  = e1 : merge es1 (e2:es2)
               | (e1 >= e2) = e2 : merge (e1:es1) es2
          merge l1        l2      = l1 ++ l2

primesSieve = 2 : sieve [3..] primesSieve
      where sieve numbers (p:ps) = small ++ sieve noMultiplesOfP ps
             where (small,unfilteredBig) = span (< p*p) numbers
                   noMultiplesOfP = [x | x <- unfilteredBig, x `rem` p > 0]
                   -- small contains no composites
                   -- small contains no multiples of p, other primes already
                   -- cleared it, and it contains no multiples of other,
                   -- smaller primes
                   

-- |
-- >>> take 10 primesSieve
-- [2,3,5,7,11,13,17,19,23,29]

primes2 = 2 : sieve [3..] primes2   -- 2 : _Y ((3:) . sieve [5,7..])
             where
             sieve xs (p:ps) | (h,t) <- span (< p*p) xs =
                                h ++ sieve [n | n <- t, rem n p > 0] ps
-- this is a guard that pattern matches




-- |
-- >>> take 10 primes2
-- [2,3,5,7,11,13,17,19,23,29]

primes3 = sieve [2..] primes3   
             where sieve (x:xs) ps = x:(sieve [n | n <- xs, rem n p > 0] rps)
                     where (p:rps) = ps
-- |
-- >>> take 10 primes3
-- [2,3,5,7,11,13,17,19,23,29]

(|||) :: Bool -> Bool -> Bool
False ||| False = False
_     ||| _     = True

factorial n = frec 1 n
    where frec acc 1 = acc
          frec acc n = frec (acc*n) (n-1)

-- |
-- >>> factorial 3
-- 6
-- >>> factorial 4
-- 24
-- >>> factorial 5
-- 120

main = print 42