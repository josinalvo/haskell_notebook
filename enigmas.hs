-- remove the type signature, and get confusion
-- add an argument, and remove confusion

myAnd :: Foldable f => f Bool -> Bool
myAnd = foldr (&&) True
