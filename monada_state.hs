-- import Control.Monad.State (As instancias e o newtype poderiam ser trocados por  import Control.Monad.State)
import Control.Monad  -- you will need to put this towards the top of the file

data TurnstileState = Trancado | Destrancado
  deriving (Eq, Show)

data TurnstileOutput = Valeu | PodePassar | NaoPodje
  deriving (Eq, Show)


virar :: TurnstileState -> (TurnstileOutput, TurnstileState)
virar Trancado = (NaoPodje,Trancado)
virar Destrancado = (PodePassar, Trancado)

colocarBilhete :: TurnstileState -> (TurnstileOutput, TurnstileState)
colocarBilhete _ = (Valeu, Destrancado)

newtype State s a = State { runState :: s -> (a, s) }
state :: (s -> (a, s)) -> State s a
state = State

instance Functor (State s) where
    fmap = undefined -- fmap = liftM

instance Applicative (State s) where
    pure = undefined      -- pure = return
    (<*>) = undefined     -- (<*>) = ap

instance Monad (State s) where
    -- m a -> (a -> m b) -> m b
    -- State s a -> (a -> State s b) -> State s b
    -- (s -> (a,s)) -> (a -> (s -> (b,s))) -> (s -> (b,s))
    bigf@(State f) >>= g = State gof
        where gof s = (b,s3)
                 where (a,s2) = runState bigf s --f s
                       (b,s3) = runState (g a) s2
              
    -- return :: a -> State s a          
    return a = State f
        where f s = (a,s)

virarEncap :: State TurnstileState TurnstileOutput
virarEncap = state virar

colocarBilheteEncap :: State TurnstileState TurnstileOutput
colocarBilheteEncap = state colocarBilhete

segundaFeiraM :: TurnstileState -> ([TurnstileOutput], TurnstileState)
segundaFeiraM estado = runState f estado
    where f = do
            out1 <- virarEncap
            out2 <- virarEncap
            out3 <- colocarBilheteEncap
            out4 <- virarEncap
            return [out1,out2,out3,out4]


-- |
-- >>> segundaFeiraM Trancado
-- ([NaoPodje,NaoPodje,Valeu,PodePassar],Trancado)

-- |
-- >>> segundaFeiraM Destrancado
-- ([PodePassar,NaoPodje,Valeu,PodePassar],Trancado)

main = print 42

-- tercaFeiraM estado = runState f estado
--     where f = do
--             out1 <- virarEncap
--             return [out1]

-- tercaFeiraM' estado = runState f estado
--     where f = virarEncap >>= \out1 -> (return [out1])
            
-- segundaFeira :: TurnstileState -> ([TurnstileOutput], TurnstileState)
-- segundaFeira estado1 = ([out1,out2,out3,out4],estado5)
--     where 
--     (out1,estado2) = virar estado1
--     (out2,estado3) = virar estado2
--     (out3,estado4) = colocarBilhete estado3
--     (out4,estado5) = virar estado4

-- tercaFeira :: TurnstileState -> ([TurnstileOutput], TurnstileState)
-- tercaFeira estado1 = ([out1],estado2)
--     where 
--     (out1,estado2) = virar estado1
