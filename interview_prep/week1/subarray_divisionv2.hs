
{-# OPTIONS_GHC -Wno-unrecognised-pragmas #-}
{-# HLINT ignore "Redundant bracket" #-}
{-# LANGUAGE ParallelListComp #-}

-- fibonacci = [a + b | a <- (1:fibonacci) | b <- (0:1:fibonacci)]

-- somas = 12:[soma - a + b | soma <- somas | a <- list | b <- rest]


solve list sizeOfPiece target = length founds
    where  (soma,_,rest) = prepare list sizeOfPiece
           deltas        = zipWith (-) rest list
           results       = scanl (+) soma deltas
           founds        = [r | r <- results , r == target]

prepare :: [Int] -> Int -> (Int,[Int],[Int])
prepare list sizeOfPiece = (initialSum, list, rest)
    where (start, rest) = splitAt sizeOfPiece list
          initialSum    = sum start

listaTeste :: [Int]
listaTeste  = [2,2,1,3,2]
targetTeste :: Int
targetTeste = 4
sizeTeste :: Int
sizeTeste   = 2

-- |
-- >>> prepare listaTeste sizeTeste
-- (4,[2,2,1,3,2],[1,3,2])

-- |
-- >>> solve listaTeste sizeTeste targetTeste
-- 2


main = do
    _    <- getLine
    list <- (fmap read . words) <$> getLine
    [target,sizeOfPiece] <- (fmap read . words) <$> getLine
    print $ solve list sizeOfPiece target



