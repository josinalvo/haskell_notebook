import Data.List

l = [3,2,1,34,2,4,7,5]

findMedian l = (sort l) !! middle
    where middle = (length l) `div` 2
         

-- a b c d 
-- e f g h
-- i j k l
-- m n o p
-- >>> findMedian l
-- 4
-- >>> sort l
-- [1,2,2,3,4,5,7,34]


m1 :: [[Int]]
m1 = [[0,0,0,0],
      [0,1,0,0],
      [0,0,1,0],
      [0,1,0,0]]

-- type Matrix = [[Int]]

complement n maxPos = maxPos - n

-- bestForPos :: Matrix -> (Int,Int) -> Int
bestForPos matrix (i,j) = maximum possibs
    where maxPos = length (matrix !! 0)-1
          i'     = complement i maxPos
          j'     = complement j maxPos
          possibs= [\ (a,b) -> (matrix !! a !! b)] <*> [(i,j),(i',j),(i,j'),(i',j')]

-- allPos :: Matrix -> [(Int,Int)]
allPos matrix = [(a,b) | a <- [0..maxPos], b <-[0..maxPos]]
    where maxPos = (length (matrix !! 0) `div` 2) - 1

-- solve :: Matrix -> Int
solve matrix = sum (fmap (bestForPos matrix) (allPos matrix))

-- >>> solve m1
-- 2
