import Data.List

solve :: [Int] -> [Int] -> Int -> Bool
solve l1 l2 k = and valid_sums
    where l1_sorted  = sort l1
          l2_reverse = Prelude.reverse $ sort l2
          valid_sums = fmap (>= k) $ zipWith (+) l1_sorted l2_reverse

-- |
-- >>> solve [2,1,3] [7,8,9] 10
-- True


getProblem = do
    [_,k] <- ((map read) . words) <$> getLine
    l1    <- ((map read) . words) <$> getLine
    l2    <- ((map read) . words) <$> getLine
    return $ if solve l1 l2 k then "YES" else "NO"

main = do
    n <- read <$> getLine
    let problems = replicate n (getProblem >>= putStrLn)
    sequence_ problems

-- 2           q = 2
-- 3 10        A[] and B[] size n = 3, k = 10
-- 2 1 3       A = [2, 1, 3]
-- 7 8 9       B = [7, 8, 9]
-- 4 5         A[] and B[] size n = 4, k = 5
-- 1 2 2 1     A = [1, 2, 2, 1]
-- 3 3 3 4     B = [3, 3, 3, 4]


-- 2           
-- 3 10        
-- 2 1 3       
-- 7 8 9       
-- 4 5         
-- 1 2 2 1     
-- 3 3 3 4  