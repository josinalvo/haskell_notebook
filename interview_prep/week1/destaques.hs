slice :: Int -> Int -> [a] -> [a]
slice from to xs = take (to - from + 1) (drop from xs)


recipe = [slice 0 1, slice 3 4, slice 6 7, slice 8 9]
[hours,minutes,seconds,aOrP] = recipe <*> ["11:22:05PM"]

