{-# OPTIONS_GHC -Wno-unrecognised-pragmas #-}
{-# HLINT ignore "Redundant bracket" #-}
{-# OPTIONS_GHC -Wno-incomplete-patterns #-}

import Data.IntMap.Strict hiding  (foldl')
import Data.List (sortOn)
import Data.Foldable (foldl')
import Debug.Trace
test = "5 5\n1 2 60\n3 5 70\n1 4 120\n4 5 150\n2 3 80"
test2 = "5 6\n1 2 30\n2 3 50\n3 4 70\n4 5 90\n1 3 70\n3 5 85"

newtype Parents = Parents (IntMap Int) deriving Show 
newtype Sizes   = Sizes   (IntMap Int) deriving Show 
type    PandS       =  (Parents,Sizes)
type    PandSandM   =  (Parents,Sizes,Int)

merge :: Int -> Int -> (Parents,Sizes) -> (Parents,Sizes)
merge a b all@(parents,Sizes sizes) = if (sizes ! pa) > (sizes ! pb) 
                                         then change b pa betterAll2
                                         else change a pb betterAll2
    where pa = highestParent parents a
          pb = highestParent parents b
          betterAll1 = change a pa all
          betterAll2 = change b pb betterAll1

change :: Int -> Int -> PandS -> PandS
change start end (Parents ps, Sizes sizes)
    | ps ! start == start = (,) (Parents $ insert start end ps) (Sizes $ insert end fullSize sizes)
    where fullSize = if start /= end then sizes ! start + sizes ! end
                                     else sizes ! start

change start end all@(Parents ps, Sizes sizes) = (,) (Parents $ insert start end newPs) newSizes
    where parent =  ps ! start 
          newAll@(Parents newPs, newSizes) = change parent end all



starter size = (,) (Parents $ fromList [(n,n) | n <- [1..size] ])
                   (Sizes $   fromList [(n,1) | n <- [1..size] ])

a = merge 1 2 (starter 4)
-- >>> a
-- (Parents (fromList [(1,2),(2,2),(3,3),(4,4)]),Sizes (fromList [(1,1),(2,2),(3,1),(4,1)]))

b = merge 1 3 a
-- >>> b
-- (Parents (fromList [(1,2),(2,2),(3,2),(4,4)]),Sizes (fromList [(1,1),(2,3),(3,1),(4,1)]))

highestParent :: Parents -> Int -> Int
highestParent pps@(Parents ps) son = if ps ! son == son then son else highestParent pps (ps ! son) 

finished parents target = highestParent parents 1 == highestParent parents target

receiveEdge :: [Int] -> Int -> PandSandM -> PandSandM
-- receiveEdge l t (p,s,m) | trace (show (l,t,p,s,m)) False = undefined
receiveEdge [start,end,cost] target (parents,sizes,maxSoFar) 
        | not $ finished parents target = (p2,s2, max cost maxSoFar)
        where (p2,s2) = merge start end (parents,sizes)

receiveEdge [start,end,cost] target (parents,sizes,maxSoFar) = (parents,sizes,maxSoFar)

-- folderFunc target a b| trace (show (a,b) ++ ", folder") False = undefined
folderFunc target b a = receiveEdge a target b
           

solveString :: String -> String
solveString string = if finished finalParents target 
                            then show finalMax
                            else "NO PATH EXISTS"
    where  [n,e] = head l
           edges = sortOn  (\[a,b,c] -> c) . tail $ l
           l :: [[Int]]
           l     = fmap line2Ints $ lines string
           line2Ints :: String -> [Int]
           line2Ints = (fmap read) . words 
           target = n
           (parents,sizes) = starter n
           (finalParents,_,finalMax) = foldl' (folderFunc target) (parents,sizes,0) edges

-- 5 5
-- 1 2 60
-- 3 5 70
-- 1 4 120
-- 4 5 150
-- 2 3 80

main = interact solveString

-- >>> solveString test
-- "80"
