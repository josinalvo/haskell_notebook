solveRec soma list [] target = val
    where val     = if soma == target then 1 else 0
solveRec soma list rest target = val + valRest
    where val     = if soma == target then 1 else 0
          valRest = solveRec newSum (tail list) (tail rest) target
          newSum  = soma - head list + head rest


-- 1 2 3 4 5 6

-- 1 2 3 4 5 6
-- 3 4 5 6

-- 2 3 4 5 6
-- 4 5 6

-- 3 4 5 6
-- 5 6


solve list sizeOfPiece target = solveRec soma list rest target
    where  (soma,_,rest)          = prepare list sizeOfPiece

prepare :: [Int] -> Int -> (Int,[Int],[Int])
prepare list sizeOfPiece = (initialSum, list, rest)
    where (start, rest) = splitAt sizeOfPiece list
          initialSum    = sum start

listaTeste :: [Int]
listaTeste  = [2,2,1,3,2]
targetTeste :: Int
targetTeste = 4
sizeTeste :: Int
sizeTeste   = 2

-- |
-- >>> prepare listaTeste sizeTeste
-- (4,[2,2,1,3,2],[1,3,2])

-- |
-- >>> solve listaTeste sizeTeste targetTeste
-- 2

main = do
    _    <- getLine
    list <- (fmap read . words) <$> getLine
    [target,sizeOfPiece] <- (fmap read . words) <$> getLine
    print $ solve list sizeOfPiece target



