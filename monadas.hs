module MonadToys where

import Control.Applicative
filter'' p l = do
  x <- l
  if p x
    then return x
    else []

-- |
-- >>> filter'' (<3) ([1..20] ++ [20,19..1])
-- [1,2,2,1]

data OddList a = OddList [a] deriving Show 

instance Functor OddList where
    fmap f (OddList l) = OddList $ fmap f l

instance Applicative OddList where
    pure a = OddList [a]
    (<*>) (OddList l) (OddList f) = OddList $ concat $ fmap (\l1 -> fmap l1 f) l

instance Monad OddList where
    return a  = OddList [a]
    (>>=) (OddList v) f = banana f v
       

-- nao sei fazer mais funcionar
-- banana :: (a -> [b]) -> [a] -> OddList b
-- banana f v = OddList $ concat $ map f v 

banana' :: (a -> OddList b) -> [a] -> OddList b
banana' f v = OddList $ concat $ map unOdd $ fmap f v 
    where unOdd (OddList v) = v

-- se vc nao eh o mendes ignore essas linhas e assuma a banana acima
instance Semigroup (OddList a) where
    (<>) (OddList a) (OddList b) = OddList (a++b)

instance Monoid (OddList a) where
    mempty = OddList []

banana :: (a -> OddList b) -> [a] -> OddList b
banana f v = mconcat $ fmap f v 
-- pode parar de ignorar e voltar a ler    


oddl :: OddList Int 
oddl = do 
    x <- OddList [1,2,3]
    y <- OddList [10,20,30]
    return (x+y)

-- |
-- >>> oddl
-- OddList [11,21,31,12,22,32,13,23,33]

-- |
-- >>> a = [1,2,3]
-- >>> b = [10,20,30]
-- >>> pure(+) <*> a <*> b
-- [11,21,31,12,22...33]

a = ZipList [1,2,3]

-- |
-- >>> a = ZipList [1,2,3]
-- >>> b = ZipList [10,20,30]
-- >>> pure(+) <*> a <*> b
-- ...[11,22,33]...
-- >>> pure(+) <*> a <*> b
-- ZipList {getZipList = [11,22,33]}
