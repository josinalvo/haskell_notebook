foldBool :: a -> a -> Bool -> a
foldBool a b c
    | c == True = a
    | c == False = b


foldBool2 :: a -> a -> Bool -> a
foldBool2 a b c =
    case c of
        True -> a
        False -> b
-- Case funciona como patternmatching?? Certamente ele pode funcionar
-- em alguns casos sem eq

-- guard é sempre booleano

-- pointfree style would be better said as argfree style. Dispences with arguments when possible (especially non-fuction args)

--para compor com tuplas -- funciona no terminal
f = (uncurry (+)) . (uncurry divMod)
                    -- lembrando que divMod é uma funcao que recebe um parametro e retorna uma funcao (que recebe um parametro e retorna uma tupla)

data Nois = Mendes | Seno | Chong | Poodle deriving(Show)

eu :: Nois -> Bool
eu x = case x of 
    Mendes -> True;
            _ -> False

--read vai de string -> b
roundTrip :: (Show a, Read b) => a -> b
roundTrip = read . show
main = do
--print (roundTrip 4) -- b era livre demais Ambiguous type variable
print (roundTrip 4 :: Int)
print (id 4)


