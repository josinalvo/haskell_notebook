{-# LANGUAGE RankNTypes #-}
module Example where

-- import Test.QuickCheck

lista :: [Maybe String]
lista = [Just "A little bit", Just "ooh ahh", Nothing]
pontua :: String -> String
pontua string = string ++ "!"
a1 :: Maybe String
a1 = fmap pontua (Just "yes")
a2 :: [Maybe String]
a2 = fmap (fmap pontua) lista

-- |
-- >>> a1
-- Just "yes!"
-- >>> a2
-- [Just "A little bit!",Just "ooh ahh!",Nothing]


caralinho = (fmap . fmap) pontua lista

-- |
-- >>> caralinho
-- [Just "A little bit!",Just "ooh ahh!",Nothing]

a3 = const 0
-- |
-- >>> a3 lista
-- 0
-- >>> fmap a3 lista
-- [0,0,0]
-- >>> (fmap.fmap) a3 lista
-- [Just 0,Just 0,Nothing]
-- >>> (fmap.fmap.fmap) a3 lista
-- [Just [0,0,0,0,0,0,0,0,0,0,0,0],Just [0,0,0,0,0,0,0],Nothing]


a4 = const '0'
-- |
-- >>> a4 lista
-- '0'
-- >>> fmap a4 lista
-- "000"
-- >>> (fmap.fmap) a4 lista
-- [Just '0',Just '0',Nothing]
-- >>> ((fmap.fmap).fmap) a4 lista
-- [Just "000000000000",Just "0000000",Nothing]
-- >>> (fmap.fmap) (fmap a4) lista
-- [Just "000000000000",Just "0000000",Nothing]

-- >>> ((fmap.fmap).fmap) a4 lista
-- o fmap mais a direita aplica nas strings

-- fmap (f . g) = (fmap f) . (fmap g)
-- fmap id [1,2,3] = id [1,2,3]


-- testador f g = (fmap (f . g)) == (fmap f) . (fmap g)

-- fmap f (12,"banana")
-- f :: Int -> [Int]
-- f :: String -> Char

-- fmap f ([12,22],"banana")
-- f :: Traversable a -> Int
-- f :: Traversable a -> Int

fchong :: (forall c . [c]-> d) -> ([a],[b]) -> (d,d)
-- fchong :: ([c]-> d) -> ([a],[b]) -> (Int,Int)
fchong f (la, lb) = (f la, f lb)

-- Essa assinatura nao se aplica, pq c é unico
-- fchong :: ([c]-> Int) -> ([Char],[Bool]) -> (Int,Int)
-- fchong :: (forall  c . [c]-> Int) -> ([Char],[Bool]) -> (Int,Int)
-- fchong f (a,b) = (f a, f b)

fchong' :: (forall c . [c]-> Int) -> [Char] -> Int
fchong' f l = f l

primeira_letra :: [Char] -> Int
primeira_letra x = 42

a = fchong'' primeira_letra --mas tente fchong' 

fchong'' :: ([c]-> Int) -> [c] -> Int
fchong'' f l = f l 


r = fchong length ("banana",[False, True, True])

-- |
-- >>> r
-- (6,3)


-- fseno :: forall a . forall b . forall f . Functor f => (a -> b) -> f a -> f b
-- fseno = fmap

-- f :: Num a => a -> Int
-- f a = a 

-- f :: Num a => a -> (Int, Double)
-- f a = (a,a) :: (Int, Double)