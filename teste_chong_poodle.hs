-- 9: run-length-encode. Compress list into pairs of (elem, run_count)
encode :: (Eq a) => [a] -> [(a, Int)]
encode [] = []
encode (x:xs) = (x, 1 + (length $ takeWhile (==x) xs)) : encode (dropWhile (==x) xs)

-- 10: run-length-encode. Keep length 1 entries as is
---- Mixed type warning. Needs custom type
data Encoded a = Multiple Int a | Single a deriving Show

toEncode :: (a, Int) -> Encoded a
toEncode (a, 1) = Single a
toEncode (a, n) = Multiple n a

encode2 :: (Eq a) => [a] -> [Encoded a]
encode2 xs = map toEncode $ encode xs

encode3 :: (Eq a) => [a] -> [Encoded a]
encode3 = (map toEncode).encode
encode4 = map toEncode (encode $)
--
func  :: [a] -> [a] -> [a]
func x y = x ++ y

