{-# LANGUAGE TypeSynonymInstances, InstanceSigs #-}
import HsExpr (ApplicativeArg)

-- join :: Monad m => m (m h) -> m h
-- join x = x >>= id

-- x é m a
-- x é m (m h)
--    a é (m h)
-- m b é m h
--    b é h
-- (>>=) :: Monad m => m a -> (a -> m b) -> m b
--    (>>=) :: Monad m => m (m h) -> ((m h) -> m h) -> m h

-- (>>=) :: Monad m => m a -> (a -> m b) -> m b

newtype State s a = S (s -> (a,s))

join :: State s (State s a) -> State s a
-- join :: (mem1 -> ((mem2 -> (Int,mem3)),mem4)) -> (mem5 -> (Int,mem6))
join (S x) = S final
    where final mem1 = (out, mem3)
           where (S f, mem2) = x mem1 
                 (out, mem3) = f mem2
          
join2 :: State s (State s a) -> State s a
-- join :: (mem1 -> ((mem2 -> (Int,mem3)),mem4)) -> (mem5 -> (Int,mem6))
join2 (S x) = S $ \mem1 -> (let (S f, mem2) = x mem1
                                (out , mem3) = f mem2 in
                                (out, mem3))

join3 :: State s (State s a) -> State s a
-- join :: (mem1 -> ((mem2 -> (Int,mem3)),mem4)) -> (mem5 -> (Int,mem6))
join3 (S x) = S f
        where f mem1 = let (S f, mem2) = x mem1
                           (out , mem3) = f mem2 in
                           (out, mem3)
    

-- join  (S x) = undefined 
--     x :: s -> (State s a,s)
--     x :: s -> (S (s -> (a,s) ),s)



instance Monad (State s) where 
        return a = S f 
            where f s = (a,s)
        (>>=) :: State s a -> (a-> State s b) -> State s b  
        (S f1) >>= g = (S h) where
            h s1 = (a2, s3)
                where (a1,s2) = f1 s1
                      S f2    =  (g a1)
                      (a2,s3) = f2 s2

instance Applicative (State s) where
        mf <*> ma = mf >>= (f ma)
            where f ma f2 = fmap f2 ma
        pure  = return 
        
instance Functor (State s) where
        fmap = undefined 


f s = (a,s+1)
    where a = if (s `mod` 2 == 0)
                then ["par"]
                else ["impar"]

-- |
-- >>> f 15
-- (["impar"],16)

s_de_f = S f

-- g_of_f :: State Integer [String]
-- g_of_f = (s_de_f >>= g)

-- g string = S h
--     where h n = (lista, inteiro)
--              where lista = [string]
--                    inteiro = n*n

-- g s  -> f, s2
-- f s' -> (String, s2')

-- |
-- >>> (S gof) = g_of_f
-- >>> gof 0 
-- (["par"],1)
-- >>> gof 2 
-- (["par"],9)


main = print 42