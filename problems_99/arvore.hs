import Data.Maybe ( isNothing )
import Data.List ( partition )
import Data.Monoid

data Arvore a = No a (Arvore a) (Arvore a) | Vazia deriving(Show)

add :: Ord a => a -> Arvore a -> Arvore a
add n (Vazia) = No n Vazia Vazia
add n (No x e d) 
    | (n >= x) = No x e (add n d)
    | n < x    = No x (add n e) d
    | otherwise = (No x e d) 

present :: Ord a => Arvore a -> a -> Bool
present Vazia      _    = False 
present (No x e d) elem 
    | elem > x = present d elem
    | elem < x = present e elem
    | otherwise = True

-- |
-- >>> a = add 15 $ add 40 $ add 20 $ add 2 $ add 10 $ Vazia
-- >>> present a 15 
-- True
-- >>> present a 10 
-- True
-- >>> present a 40 
-- True
-- >>> present a 45 
-- False
-- >>> b = add 40 $ add 20 $ add 2 $ add 10 $ Vazia
-- >>> a == b
-- False
-- >>> a' = add 15 $ b
-- >>> a == a'
-- True

instance (Eq a) => Eq (Arvore a)
   where (==) Vazia Vazia = True
         (==) (No n1 l1 r1) (No n2 l2 r2) = and [(n1 == n1),(l1 == l2),(r1 == r2)]
         (==) _              _            = False

-- | fold works similarly to lists
-- >>> b = add 40 $ add 20 $ add 2 $ add 10 $ Vazia
-- >>> fold (:) [] b
-- [2,10,20,40]
-- >>> fold (+) 0 b
-- 72

fold :: (a -> b -> b) -> b -> Arvore a -> b
fold _ acc Vazia      = acc
fold f acc (No x l r) = left
     where right  = fold f acc r
           middle = f x right
           acc2   = middle 
           left   = fold f acc2 l

foldMap_from_fold :: (Monoid m) => (a->m) -> Arvore a -> m
foldMap_from_fold f arv = fold (\a m -> (f a) <> m) mempty arv

-- nao colocar restricao de typeclass (ex Eq a) para definir novos tipos
-- pode colocar na definicao de uma typeclass

newtype Endo' a = Endo' {appEndo' :: (a -> a)}
-- sou um monoide, id eh o mempty
-- composicao eh a <>
-- tenho operacao de (Endo, Endo) em Endo, associativa
-- essa operacao tem um elemento neutro (pela direita e pela esquerda)

-- fold (+) 0 [1,2,3,4]
-- ((+1) . (+2) . (+3) . (+4)) 0

fold_from_foldMap :: (a -> b -> b) -> b -> Arvore a -> b
fold_from_foldMap f acc arv = appEndo (foldMap' (\x -> (Endo $ f x)) arv) acc

            --    descasca funcao e aplica em acc
                               --   funcao de elem da arvore, para monoide endo
                               --   (a -> (b -> b)) (mais preciso: a -> Endo b)

-- foldr [a1 a2 a3] b
-- foldr [a1 a2] b3
-- foldr [a1] b2
-- foldr b1

-- [a1 a2 a3]
-- [(b2->b1) (b3->b2) (b->b3)]

foldMap' :: (Monoid m) => (a -> m) -> Arvore a -> m
foldMap' _ Vazia       = mempty 
foldMap' f (No x l r)  = (foldMap f l) <> (f x) <> (foldMap f r)

-- foldMap: transforma cada item em um monoide e agrega
-- foldMap -> fold: transforma cada item em um b->b e agrega usando .
-- fold -> foldMap:


instance Foldable Arvore where
    -- choose one
    foldMap = foldMap'
    --foldMap = foldMap_from_fold
    --foldr   = fold
    --foldr = fold_from_foldMap
    
    
-- |     
-- >>> tree = add 40 $ add 20 $ add 2 $ add 10 $ Vazia
-- >>> foldMap (:[]) tree
-- [2,10,20,40]
-- >>> foldMap (\x -> [x]) tree
-- [2,10,20,40]
-- >>> foldMap (Sum) tree
-- Sum {getSum = 72}
-- >>> foldMap (Product) tree
-- Product {getProduct = 16000}




b = and [True, False]

-- |
-- >>> t = add True $ add True $ Vazia
-- >>> t
-- No True Vazia (No True Vazia Vazia)
-- >>> and t
-- True
-- >>> f = add False t
-- >>> and f
-- False

-- |
-- >>> b = add 40 $ add 20 $ add 2 $ add 10 $ Vazia
-- >>> all even b
-- True

unfoldTree' :: (gen -> Maybe (gen,val,gen)) -> gen -> Arvore val
unfoldTree' f gen =  arvore 
    where nexts = f gen
          Just (l_gen,root_val,r_gen) = nexts
          arvore = if   isNothing nexts 
                   then Vazia
                   else No root_val (unfoldTree' f l_gen) (unfoldTree' f r_gen)

unfoldTree :: (gen -> Maybe (gen,val,gen)) -> gen -> Arvore val
unfoldTree f gen =  case f gen of
    Nothing -> Vazia
    Just(l_gen, root_val, r_gen) -> No root_val (unfoldTree f l_gen) (unfoldTree f r_gen)
    
pivot' []   = Nothing
pivot' (list) = Just (menoreq, divisor, maiores)
   where divisor = head list
         resto   = tail list
         menoreq = filter (<= divisor) resto
         maiores = filter (> divisor)  resto

pivot [] = Nothing
pivot (divisor:resto) = Just (menoreq, divisor, maiores)
    where (menoreq,maiores) = partition (<= divisor) resto

-- |
-- >>> b  = add 40 $ add 20 $ add 2 $ add 10 $ Vazia
-- >>> b' = unfoldTree pivot [10,2,20,40]
-- >>> b == b'
-- True
-- >>> c  = add 12 $ add 100 $ add 60 $ add 36 $ add 75 $ add 25 $ add 50 $ Vazia
-- >>> c' = unfoldTree pivot [50,75,25,12,100,60,36]
-- >>> c == c'
-- True
-- >>> wrong = add 14 c
-- >>> c' == wrong
-- False
-- >>> c'' = foldr add Vazia [12,100,60,36,75,25,50]
-- >>> c  == c''
-- True
-- >>> wrong' = foldr add Vazia $ reverse [12,100,60,36,75,25,50]
-- >>> c     == wrong'
-- False
-- >>> c'''   = foldl (flip add) Vazia $ reverse [12,100,60,36,75,25,50]
-- >>> c'''  == c
-- True

main :: IO ()
main = print 42