import Data.List

noRepeated [] = True
noRepeated (x:xs) = not (x `elem` xs) && noRepeated xs

toDownDiag (x,y) = x+y
no_down_diag :: [Int] -> Bool
no_down_diag perm = noRepeated $ map toDownDiag $ zip [1..] $ perm

toUpDiag (x,y) = x-y
no_up_diag :: [Int] -> Bool
no_up_diag perm = noRepeated $ map toUpDiag $ zip [1..] $ perm

no_diag perm = (no_up_diag perm) && (no_down_diag perm)


queens1 :: Int -> [[Int]]
queens1 n =  filter no_diag $ permutations [1..n]

extend :: Int -> [Int] -> [[Int]]
extend max partialPerm = [x:partialPerm | x <-[1..max], (not $ x `elem` partialPerm)]

grow :: [[Int]] -> Int -> [[Int]]
grow [] max = grow (map (:[]) [1..max]) max
grow l max 
  | (length (l!!0) == max) = l
  | otherwise = grow (filter no_diag $ concatMap (extend max) l) max

queens2 :: Int -> [[Int]]
queens2 n =  grow [] n
  
-- from the solutions, from memory
queens3 :: Int -> [[Int]]
queens3 n = queens' n
   where queens' 0 = [[]]
         queens' k = [add:sm| sm <- queens'(k-1), add<-[1..n], safe(add:sm)] 
         --NOTE in queens', aux function using n, this helped!
         unsafeDiag (a,b) (c,d) = abs(a-c) == abs(b-d) -- this diag is better, more semantic
         unsafePair (a,b) (c,d) = unsafeDiag (a,b) (c,d) || a == c || b == d -- one of those two equalities never happens, by construction
         lastUnsafe (p:ps) = or $ map (unsafePair p) ps
         safe l = not $ lastUnsafe $ zip l [1..]


-- copy from solutions
-- queens4 :: Int -> [[Int]]
-- queens4 n = map reverse $ queens' n
--     where queens' 0       = [[]]
--           queens' k       = [q:qs | qs <- queens' (k-1), q <- [1..n], isSafe q qs]
--           isSafe   try qs = not (try `elem` qs || sameDiag try qs)
--           sameDiag try qs = any (\(colDist,q) -> abs (try - q) == colDist) $ zip [1..] qs



