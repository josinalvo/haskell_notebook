-- poodle produziu essa solucao interessante. Note a continuidade que é dada no foldr
getAt' :: [a] -> Int -> a
getAt' list pos = foldr aux (list!!0) (zip [0..] list)
    where aux (n,x) rest = if (n == pos)
                           then x
                           else rest

getTupleAt_n :: [a] -> Int -> (a,[a])
getTupleAt_n list pos = foldr aux (list!!0,[]) (zip [0..] list)
    where aux (n,x) (y,ys) = if (n == pos)
                             then (x, ys)
                             else (y, x:ys)

getTupleAt_w :: [a] -> Int -> (a,[a])
getTupleAt_w list pos = foldr aux (list!!0,[]) (zip [0..] list)
    where aux (n,x) result = if (n == pos)
                             then (x, (snd result))
                             else ((fst result), x:(snd result))


inf = [-1,-2..] :: [Int]
w = fst $ getTupleAt_w inf 3
w2 = take 10 $ snd $ getTupleAt_w inf 3
n = fst $ getTupleAt_n inf 3