dupli list = foldr (\ x l -> x:x:l) [] list

dropEvery n list = map fst
                   $ foldr 
                   (\ x list -> if (snd x /= n) then x:list else list ) 
                   [] $ zip list (cycle [1..n])

dropEvery' n list = map fst
                    $ filter ( (/= n) . snd) $ zip list (cycle [1..n])

--compress "aaaabccaadeeee"
--"abcade"
comp_aux x (y:ys) = if x == y then (y:ys) else (x:y:ys)
comp_aux x [] = [x]
compress :: Eq a => [a] -> [a]
compress list = foldr comp_aux [] list 

comp_aux' [] x = [x]
comp_aux' ys x = if x == (last ys) then ys else ys ++ [x]
compress' :: Eq a => [a] -> [a]
compress' list = foldl comp_aux' [] list 
