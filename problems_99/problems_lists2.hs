part :: [k] -> Int -> [([k],[k])]
part l 0 = [([],l)]
part [] n = []
part (x:xs) n 
    | (current_size >= n) = [(a,x:b)|(a,b) <- without_x] ++  [(x:a,b)|(a,b) <- with_x] 
    | otherwise = []
    where without_x = part xs n 
          with_x = part xs (n-1)
          current_size = 1 + length xs


partIn3 :: [k] -> Int -> Int-> [([k],[k],[k])]
partIn3 l s1 s2 = [(a,b,c)| (a,both) <- sublist, (b,c) <- (part both s2)]
     where sublist = part l s1

partL :: [k] -> Int -> [[[k]]]
partL l n = [[a,b]| (a,b)<-(part l n)]


partInN :: [k] -> [Int] -> [[[k]]]
partInN l [] = [[l]]
partInN l (x:xs) = [a:rest_list| (a,rest) <- sublist, rest_list <- (partInN rest xs)]
      where sublist = part l x