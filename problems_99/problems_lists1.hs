myLast :: [a] -> a
myLast (x:[]) = x
myLast (x:xs) = myLast xs
--foldr1: there is no last value after the list

myLast' :: [a] -> a
myLast' = foldr1 (flip const) --this and the two next use foldr1 the same

myLast'' :: [a] -> a
myLast'' = foldr1 (curry snd)

myLast''' :: [a] -> a
myLast''' = foldr1 (const id) -- this is needlessly tricky, maybe?
                              -- will receive 1, return id. then id
                              -- receives 2

myLast'''' :: [Int] -> Int
myLast'''' = foldl (flip const) (500 :: Int)

mySecond :: [a] -> a
mySecond (x:[_]) = x
mySecond (_:xs) = mySecond xs

--ta errada essa porra
mySecond' :: [a] -> a
mySecond' = foldl1 const


myAsc :: Ord a => [a] -> Bool
myAsc [] = True
myAsc [x] = True
myAsc (x:y:xs) = (x < y) && myAsc (y:xs)

myAsc' :: Ord a => [a] -> Bool
myAsc' list = and . (map (uncurry (<))) $ zip list (drop 1 list)

--NOTA: nao rola um fold?

pack :: Eq a => [a] -> [[a]]
pack [] = []
pack [x] = [[x]]
pack (x:xs) = if (firstL !! 0 == x)
                 then (x:firstL) : rest
                 else [x] : firstL : rest
                 where (firstL:rest) = pack xs

encode :: Eq a => [a] -> [(a,Int)]
encode = map (\list -> (list!!0,length list)) . pack

accumF :: Eq a => a -> [(a,Int)] -> [(a,Int)]
accumF x [] = [(x,1)]
accumF x ((y,n):tuples)
    | (x == y) = (x,n+1):tuples
    | (x /= y) = (x,1):(y,n):tuples

encode' :: Eq a => [a] -> [(a,Int)]
encode' list = foldr accumF [] list

encode'' :: Eq a => [a] -> [(a,Int)]
encode'' [] = []
encode'' (x:xs) = x_tuple:rest
            where x_tuple = (x,1+(length $ takeWhile (== x) xs))
                  rest = encode''(dropWhile (== x) xs)      

decode ::  Eq a =>  [(a,Int)] -> [a]
decode [] = []
decode (t1:ts) = first ++ decode ts
       where amount = snd t1
             first = take amount $ repeat (fst t1)

repli :: [a] -> Int -> [a]
repli [] _ = []
repli (x:xs) n = start ++ repli xs n
           where start = (take n) $ repeat x

repli' list n = foldr (++) [] $ map (replicate n) list
          
-- NOTA: ponto faz sentido com funcoes de 1 argumento
-- a menos que você mó saiba o que está fazendo
specialMap =  uncurry map :: ((a->[b]),[a]) ->[[b]]
concatMap' :: (a->[b]) -> [a] ->[b]
-- concatMap' f list = concat $ map f list
-- concatMap' f list = (concat . specialMap) (f,list)
-- concatMap' f list = (curry (concat . specialMap)) f list
--concatMap' = (curry (concat . specialMap))
concatMap' = curry (concat . (uncurry map))

-- Nota: entendo a natureza do erro, mas gostaria
-- de entender o output
--concatMap'' :: (a->[b]) -> [a] ->[b]
--concatMap'' = concat . map

dropEvery n [] = []
dropEvery n list = take (n-1) list ++ rest
            where clear = drop n list
                  rest = dropEvery n clear



dropEvery' n list = map fst clear
    where toTuples l = [(l!!index,index+1) | index <- take (length l) [0..]]
          tuples = toTuples list
          clear = filter ((/= 0).(`mod`n ). snd) tuples

-- Nota: substituir num por n: erro dificil de debugar?
dropEvery'' :: Integral b => b -> [a] -> [a]
dropEvery'' n list = [ num | (num,idx) <- (zip list [1..]), (idx `mod` n) /= 0]

-- Nota: bonitinho
dropEvery''' :: Integral b => b -> [a] -> [a]
dropEvery''' n list = [ num | (num,idx) <- (zip list $ cycle [1..n]), idx /= n]

split_aux :: Int -> ([a],[a]) -> ([a],[a])
split_aux 0 (xs,y:ys) = (xs,y:ys)
split_aux n (xs,y:ys) = split_aux  (n-1) (xs++[y],ys)
split n list = split_aux n ([],list)

split' n list = (take n list,drop n list)


data Monster = Monster {race :: String, attack :: Int, defense :: Int}
worse :: Monster -> Monster -> Bool
worse a b = ((attack a) <= (attack b) && (defense a) <= (defense b))