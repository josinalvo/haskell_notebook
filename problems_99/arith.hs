data ArTree a  = Mult (ArTree a) (ArTree a) | Sum (ArTree a) (ArTree a) | Minus (ArTree a) (ArTree a) | Div (ArTree a) (ArTree a) | Node a
-- a should be num. Can I fix that?


eval :: Fractional a => ArTree a -> a
eval (Node n) = n
eval (Mult a b) = (eval a) *  (eval b)
eval (Minus a b) = (eval a) - (eval b)
eval (Div a b) = (eval a) / (eval b)
eval (Sum a b) = (eval a) +  (eval b)

instance (Fractional a, Show a) => Show (ArTree a) where 
    show (Node n) = show n
    show (Mult a b) = " (" ++ show a ++ "*" ++ show b ++ ") "
    show (Sum a b) = " (" ++ show a ++ "+" ++ show b ++ ") "
    show (Div a b) = " (" ++ show a ++ "/" ++ show b ++ ") "
    show (Minus a b) = " (" ++ show a ++ "-" ++ show b ++ ") "

a = eval (Node 10)
b = Mult (Sum (Node 10) (Node 15)) (Node 10)
b_eval = eval b

split :: [a] -> [([a],[a])]
split [x,y] = [([x],[y])]
split (x:xs) = (([x],xs) :) $ map (\(a,b) -> (x:a,b)) $ split xs

join_a_few :: (Num a) => [a] -> [[a]]
join_a_few [] = []
join_a_few [x] = [[x]]
join_a_few (x:y:z) = [(x*10+y):z] ++ map (x:) (join_a_few (y:z))



trees [x] = [Node x]
trees list= [ope t_left t_right | 
                 (left,right)<-split list, 
                 t_left <- trees left, t_right <- trees right, 
                 ope <-[Mult, Sum, Div, Minus]]

trees_Tahan list = concatMap trees list_j
       where list_j = join_a_few list

trees_that_sum  target list = filter ( (==target) . snd) $ map (\tree -> (tree,eval tree)) $ trees_Tahan list

-- https://riptutorial.com/haskell/example/16493/associativity
-- infixl vs infixr vs infix describe on which sides the parens will be grouped. For example, consider the following fixity declarations (in base)

-- infixl 6 -
-- infixr 5 :
-- infix  4 ==

-- The infixl tells us that - has left associativity, which means that 1 - 2 - 3 - 4 gets parsed as

-- ((1 - 2) - 3) - 4

-- The infixr tells us that : has right associativity, which means that 1 : 2 : 3 : [] gets parsed as

-- 1 : (2 : (3 : []))

-- The infix tells us that == cannot be used without us including parenthesis, which means that True == False == True is a syntax error. On the other hand, True == (False == True) or (True == False) == True are fine.

-- Operators without an explicit fixity declaration are infixl 9.