import qualified Data.Map as Map
import qualified Data.Set as Set

type Entry = [(String,String)]

parseEntry :: String -> [(String,String)]
parseEntry entry = fmap pair fstStep
    where
        fstStep = lines entry
        pair [a, b, c, _, e, f, g] = ([a,b,c],[e,f,g])

type Graph = Map.Map String [String]

createGraph :: [(String,String)] -> Graph
createGraph entry = Map.fromListWith (++) fstStep
    where
        fstStep = fmap (\(a,b) -> (a,[b])) entry

createDistanceList :: Graph -> [[String]]
createDistanceList graph = takeWhile (/= []) allLevels
    where
        zeroth = ["COM"]
        nextLevel level = [child | parent <-level, child <- Map.findWithDefault [] parent graph]
        allLevels = iterate nextLevel zeroth

computeWeightedSum :: Foldable t => [t a] -> Int
computeWeightedSum distList = sum $ zipWith (*) [0..] $ fmap length distList

solve1 :: String -> String
solve1 = show . computeWeightedSum . createDistanceList . createGraph . parseEntry
        
-- main :: IO ()
-- main = interact solve1

type ParentMap = Map.Map String String

createParentMap :: Entry -> ParentMap
createParentMap entry = Map.fromList fstStep
    where
        fstStep = fmap (\(a,b) -> (b,a)) entry

findDistance :: ParentMap -> Int
findDistance parentMap = Set.size nonCommonAncestors
    where
        allYourAncestors = Set.fromList $ takeWhile (/= "COM") $ iterate (parentMap Map.!) "YOU" 
        allSantasAncestors = Set.fromList $ takeWhile (/= "COM") $ iterate (parentMap Map.!) "SAN" 

        nonCommonAncestors = Set.union (allYourAncestors Set.\\ allSantasAncestors) (allSantasAncestors Set.\\ allYourAncestors)
        

solve2 :: String -> String
solve2 = show . (\x -> x - 2) . findDistance . createParentMap . parseEntry

main = interact solve2