import Data.List (group)

valid :: Int -> Bool
valid n = valid2
    where
        str = show n
        pairs = zip str (tail str)
        nondecreasing = all (uncurry (<=)) pairs
        repeatingPair = any (uncurry (==)) pairs
        repeatingButBla = any (== 2) $ length <$> group str
        valid1 = repeatingPair && nondecreasing
        valid2 = repeatingButBla && nondecreasing

howManyValid :: Int -> Int -> Int
howManyValid start end = length $ filter valid [start..end]

-- |
-- >>> howManyValid 145852 616942
-- WAS 1767
-- NOW 1192
