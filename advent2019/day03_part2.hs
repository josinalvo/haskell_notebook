{-# OPTIONS_GHC -Wno-unrecognised-pragmas #-}
{-# HLINT ignore "Eta reduce" #-}
import qualified Data.Map as M
import Data.Foldable
import Data.List.Extra (replace)
import GHC.Classes (compareInt)


addOnePath   :: (M.Map (Int,Int) Int,((Int,Int),Int)) -> String 
                          -> (M.Map (Int,Int) Int,((Int,Int),Int))
addOnePath (set,p0) command@(direction:amount) = (newSet, last allMoves)
          where move 'D' (x,y) = (x,y-1)
                move 'L' (x,y) = (x-1,y)
                move 'R' (x,y) = (x+1,y)
                move 'U' (x,y) = (x,y+1)
                move _ _       = error "uai"
                move' direction ((x,y),count) = (move direction (x,y),
                                                              count+1)
                allMoves = take (read amount + 1) $ iterate (move' direction) p0
                newSet   = M.union set (M.fromList allMoves) --prefers original map
addOnePath _ _ = error "nope"



allPaths ::  [String] -> M.Map (Int,Int) Int
allPaths commands = set
    where (set,_) = foldl' addOnePath (M.empty,((0,0),0)) commands

-- |
-- >>> allPaths ["U13","R2"]
-- fromList [((0,0),0),((0,1),1),((0,2),2),((0,3),3),((0,4),4),((0,5),5),((0,6),6),((0,7),7),((0,8),8),((0,9),9),((0,10),10),((0,11),11),((0,12),12),((0,13),13),((1,13),14),((2,13),15)]

split :: String -> [String]
split = words . replace "," " "


solve2 :: [Char] -> [Char] -> [((Int, Int), (Int,Int))]
solve2 commands1 commands2 =  take 3 $ M.assocs finalMap
    where sort ((_,_),count) = compareInt count  
          finalMap = M.intersectionWith (,)  (allPaths $ split commands1) (allPaths $ split commands2)

main = print $ solve2 "R75,D30,R83,U83,L12,D49,R71,U7,L72" "U62,R66,U55,R34,D71,R55,D58,R83"
-- |
-- >>> solve2 "R75,D30,R83,U83,L12,D49,R71,U7,L72" "U62,R66,U55,R34,D71,R55,D58,R83"
-- >>> solve2 "R98,U47,R26,D63,R33,U87,L62,D20,R33,U53,R51" "U98,R91,D20,R16,D67,R40,U7,R15,U6,R7"
-- WAS [((0,0),0),((146,46),624),((155,4),726)]
-- WAS [((0,0),0),((107,47),410),((107,51),700)]
-- NOW [((0,0),(0,0)),((146,46),(290,334)),((155,4),(341,385))]
-- NOW [((0,0),(0,0)),((107,47),(154,256)),((107,51),(448,252))]


-- main1 = do
--     commands1 <- getLine
--     commands2 <- getLine
--     print $ solve1 commands1 commands2

-- main = main1

