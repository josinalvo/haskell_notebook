{-# OPTIONS_GHC -Wno-unrecognised-pragmas #-}
{-# HLINT ignore "Eta reduce" #-}
import qualified Data.Set as S
import Data.Foldable
import Data.List.Extra (replace)


addOnePath   :: (S.Set (Int,Int),(Int,Int)) -> String -> (S.Set (Int,Int),(Int,Int))
addOnePath (set,p0) command@(direction:amount) = (newSet, last allMoves)
          where move 'D' (x,y) = (x,y-1)
                move 'L' (x,y) = (x-1,y)
                move 'R' (x,y) = (x+1,y)
                move 'U' (x,y) = (x,y+1)
                move _ _       = error "uai"
                allMoves = take (read amount + 1) $ iterate (move direction) p0
                newSet   = S.union set (S.fromList allMoves)
addOnePath _ _ = error "nope"



allPaths ::  [String] -> S.Set (Int,Int)
allPaths commands = set
    where (set,_) = foldl' addOnePath (S.fromList [],(0,0)) commands

-- |
-- >>> allPaths ["U13","R2"]
-- fromList [(0,0),(0,1),(0,2),(0,3),(0,4),(0,5),(0,6),(0,7),(0,8),(0,9),(0,10),(0,11),(0,12),(0,13),(1,13),(2,13)]

split :: String -> [String]
split = words . replace "," " "

distance (a,b) = abs a +abs b

solve1 :: String -> String -> Int
solve1 commands1 commands2 =  (!! 1) $ S.toAscList $ S.map distance $ S.intersection  (allPaths $ split commands1) 
                                                                                      (allPaths $ split commands2)

-- |
-- >>> solve1 "R75,D30,R83,U83,L12,D49,R71,U7,L72" "U62,R66,U55,R34,D71,R55,D58,R83"
-- >>> solve1 "R98,U47,R26,D63,R33,U87,L62,D20,R33,U53,R51" "U98,R91,D20,R16,D67,R40,U7,R15,U6,R7"
-- 159
-- 135


main1 = do
    commands1 <- getLine
    commands2 <- getLine
    print $ solve1 commands1 commands2

main = main1

