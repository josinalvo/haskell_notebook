import qualified Data.IntMap as M
import Data.Maybe

programList2 = [1,0,0,3,1,1,2,3,1,3,4,3,1,5,0,3,2,6,1,19,1,19,9,23,1,23,9,27,1,10,27,31,1,13,31,35,1,35,10,39,2,39,9,43,1,43,13,47,1,5,47,51,1,6,51,55,1,13,55,59,1,59,6,63,1,63,10,67,2,67,6,71,1,71,5,75,2,75,10,79,1,79,6,83,1,83,5,87,1,87,6,91,1,91,13,95,1,95,6,99,2,99,10,103,1,103,6,107,2,6,107,111,1,13,111,115,2,115,10,119,1,119,5,123,2,10,123,127,2,127,9,131,1,5,131,135,2,10,135,139,2,139,9,143,1,143,2,147,1,5,147,0,99,2,0,14,0]
programList1 = [1,12,2,3,1,1,2,3,1,3,4,3,1,5,0,3,2,6,1,19,1,19,9,23,1,23,9,27,1,10,27,31,1,13,31,35,1,35,10,39,2,39,9,43,1,43,13,47,1,5,47,51,1,6,51,55,1,13,55,59,1,59,6,63,1,63,10,67,2,67,6,71,1,71,5,75,2,75,10,79,1,79,6,83,1,83,5,87,1,87,6,91,1,91,13,95,1,95,6,99,2,99,10,103,1,103,6,107,2,6,107,111,1,13,111,115,2,115,10,119,1,119,5,123,2,10,123,127,2,127,9,131,1,5,131,135,2,10,135,139,2,139,9,143,1,143,2,147,1,5,147,0,99,2,0,14,0]

type Program = M.IntMap Int
data PState  = PState Program Int | Stop Int deriving Show

-- replace position 1 with the value 12 and replace position 2 with the value 2.
-- 1 soma
-- 2 multi
-- op posv1 posv2 pos_alvo

next :: PState -> PState
next a@(PState program position) = if command == 99 then (Stop (program M.! 0)) 
                                   else (PState newProgram (position + 4))
    where command    = (M.!) program position
          pos1       = (M.!) program (position+1)
          pos2       = (M.!) program (position+2)
          posTarget  = (M.!) program (position+3)
          v1         = (M.!) program pos1
          v2         = (M.!) program pos2
          newVal     = case command of 
                            1 -> Just $ v1 + v2
                            2 -> Just $ v1 * v2
                            99-> Nothing
                            _ -> error "safado, cachorro, sem vergonha"
          newProgram = M.insert posTarget (fromJust newVal) program 
next (Stop a) = Stop a
solve1 list = head $ drop (length list) $ iterate next (PState programMap 0)
    where programMap = M.fromList ( zip [0,1..] list )
          
main1_old = print (solve1 programList1)

--main = print (solve1 [1,9,10,3,2,3,11,0,99,30,40,50])
main1 = print (run (PState programMap 0 ))
    where programMap = M.fromList ( zip [0,1..] programList1 )


run :: PState -> Int
run a@(PState program position) = case command of 
                                    1  -> run (PState (newProgram (v1+v2)) nextPos)
                                    2  -> run (PState (newProgram (v1*v2)) nextPos)
                                    99 -> program M.! 0
                                    _  -> error "pqp" 
                                     
    where command    = (M.!) program position
          pos1       = (M.!) program (position+1)
          pos2       = (M.!) program (position+2)
          posTarget  = (M.!) program (position+3)
          v1         = (M.!) program pos1
          v2         = (M.!) program pos2
          nextPos    = position+4
          newProgram v = M.insert posTarget v program 
run (Stop a) = error "meh"


solve list v1 v2 = (run (PState programMap2 0 ))
    where programMap  = M.fromList ( zip [0,1..] programList1 )
          programMap2 = M.insert 2 v2 $ (M.insert 1 v1) programMap

target = 19690720
allAns =  [(v1,v2) | v1 <- [0..99], v2 <- [0..99], (solve programList1 v1 v2) == target]

main2 = print $ v1*100+v2
    where (v1,v2) = head allAns

main = main2