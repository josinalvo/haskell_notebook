import Data.List

buildVecs :: [Char] -> [(Int, Int)]
buildVecs string = relevantPos
    where  numLines    = length $ lines string
           numCols     = length $ (lines string) !! 0
           allPos      = [(x,y) | x <-[1..numCols], y <-[1..numLines]] 
           relevantPos = fmap snd $ filter (\(a,b) -> a == '#') $ zip (filter (/= '\n') string) allPos

simplifyVect :: (Int,Int) -> (Int,Int)
simplifyVect (0,0) = (0,0)
simplifyVect (a,b) =  (,) (div a divisor)  (div b divisor)
    where divisor = gcd a b

minus (x1,y1) (x2,y2) = (x1-x2,y1-y2)

-- (0,-1)  (1,0)   (0,1)   (-1,0 )
--     (1,-1)  (1,1)   (-1,1)   (-1,-1 )
angleFromUp (a,b)
    where hemisfere (a,b) = if a > 0 then "EAST" else "WEST"

solve :: String -> (Int,Int)
solve s = (solution1,solution2)
    where vecs     = buildVecs s
          vecsFrom :: [[(Int,Int)]]
          vecsFrom = [ a:(nub $ fmap (simplifyVect . minus a) vecs)| a <- vecs]
          maxLen = maximum $ fmap length vecsFrom
          solution1 = maxLen - 2
          laserPos  = head $ head $ filter (\l -> length l == maxLen)  vecsFrom
          targetsSimplified = tail $ head $ filter (\l -> length l == maxLen)  vecsFrom
          solution2 = undefined
main = interact (show . solve)