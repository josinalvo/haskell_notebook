

fuel n = div n 3 - 2

fuelConverge = sum .  tail . takeWhile (> 0) . iterate fuel

-- |
-- >>> fuelConverge 1969
-- 966

main1 = do
    a <- getContents
    let numbers = fmap read (lines a)
    let fuels   = fmap fuel numbers
    print $ sum fuels

main2 = do
    a <- getContents
    let numbers = fmap read (lines a)
    let fuels   = fmap fuelConverge numbers
    print $ sum fuels

main = main2