import qualified Data.Map as Map

parseEntry :: String -> [(String,String)]
parseEntry entry = fmap pair fstStep
    where
        fstStep = lines entry
        pair [a, b, c, ')', e, f, g] = ([a,b,c],[e,f,g])

type Graph = Map.Map String [String]

createGraph :: [(String,String)] -> Graph
createGraph entry = Map.fromListWith (++) (oneWay ++ anotherWay)
    where
       oneWay = fmap (\(a,b) -> (a,[b])) entry
       anotherWay = fmap (\(a,b) -> (b,[a])) entry

createDistances :: String -> Graph -> Map.Map String Int
createDistances start graph = distMap
    where
        zeroth = [start]
        nextLevel :: (Map.Map String Int,[String],Int) -> (Map.Map String Int,[String],Int)
        nextLevel (prevMap,level,n) = (nextMap,nextLevel,n+1)
            where nextLevel = [child | parent <-level, child <- Map.findWithDefault [] parent graph, child `Map.notMember` prevMap]
                  nextMap   = Map.union (Map.fromList $ zip nextLevel [n+1,n+1..]) prevMap
        allLevels = iterate nextLevel (Map.fromList [(start,0)],zeroth,0)
        (distMap,_,_) = last $ takeWhile (\(_,list,_) -> list /= []) allLevels

computeSumOfDistances :: Map.Map String Int -> Int
computeSumOfDistances distList = sum $ snd <$> Map.toList distList

solve1 :: String -> String
solve1 = show . computeSumOfDistances . createDistances "COM" . createGraph . parseEntry
        
main :: IO ()
-- main = interact solve1


solve2 :: String -> String
solve2 = show . (\m -> m Map.! "SAN" - 2) . createDistances "YOU" . createGraph . parseEntry
main = interact solve2