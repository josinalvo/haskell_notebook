import Data.List.Split (chunksOf)
import Data.List.Extra (minimumOn)
import Data.List (transpose, find)

string2layers :: String -> Int -> [String]
string2layers s n = filter (/= "\n") $ chunksOf n s

getAmountOf digit = length . filter (== digit)

solve1 :: String -> Int
solve1 s = (getAmountOf '1' specialLayer) * (getAmountOf '2' specialLayer)
    where specialLayer = minimumOn (getAmountOf '0') layers
          layers :: [String]
          layers       = string2layers s (25*6)

main1 = interact (show . solve1)


black = 0
transparent = 2
white = 1

clear :: String -> String
clear s = fmap f s 
    where f '0' = ' '
          f '1' = '8'  

collapse :: [Char] -> Maybe Char
collapse  = find (/= '2')

solve2 :: String -> Maybe String
solve2 s = unlines <$> fmap clear <$> lines
    where 
        finalChars :: Maybe String
        finalChars = sequence . fmap collapse . transpose $ string2layers s (25*6) 
        lines      = fmap (chunksOf 25) finalChars

main = do 
        s <- getContents
        let Just a = solve2 s
        putStr a