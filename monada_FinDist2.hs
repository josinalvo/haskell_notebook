{-# LANGUAGE InstanceSigs #-}

module FinDist where 

import Data.List

import Data.Ratio

import Control.Monad

newtype FinDist a = FinDist {pairs :: [(a,Ratio Int)]} deriving (Show,Eq)

applyVal  f (a,b) = (f a, b)
applyProb f (a,b) = (a, f b)


instance Functor FinDist where
    fmap f (FinDist p) = FinDist $ map (applyVal f) p
        
a = FinDist [(1,0.5),(2,0.3),(3,0.2)]

-- |
-- >>> fmap (+1) a
-- FinDist {pairs = [(2,1 % 2),(3,3 % 10),(4,1 % 5)]}



instance Applicative FinDist where
    (<*>) (FinDist fs) (FinDist vs) = FinDist newList
       where applyPair :: (a->c,Ratio Int) -> (a,Ratio Int) -> (c,Ratio Int)
             applyPair (g,p) (a,b) = (g a, b*p)
             applyPairToList :: (a->c,Ratio Int) -> [(a,Ratio Int)] -> [(c,Ratio Int)]
             applyPairToList p list = map (applyPair p) list
             applyMany :: [(a->c,Ratio Int)] -> [(a,Ratio Int)] -> [(c,Ratio Int)]
             applyMany l1 l2 = concat $ map  ($ l2) manyFuncs
                 where manyFuncs = map (applyPairToList) l1
             newList                         = applyMany fs vs
    pure a = FinDist [(a,1)]

b = FinDist [(10,0.5),(20,0.5)]
-- a = FinDist [(1,0.5),(2,0.3),(3,0.2)]

-- |
-- >>> fmap (+) a <*> b
-- FinDist {pairs = [(11,1 % 4),(21,1 % 4),(12,3 % 20),(22,3 % 20),(13,1 % 10),(23,1 % 10)]}
-- >>> pure (+) <*> a <*> b
-- FinDist {pairs = [(11,1 % 4),(21,1 % 4),(12,3 % 20),(22,3 % 20),(13,1 % 10),(23,1 % 10)]}
-- >>> FinDist[((+),1)] <*> a <*> b
-- FinDist {pairs = [(11,1 % 4),(21,1 % 4),(12,3 % 20),(22,3 % 20),(13,1 % 10),(23,1 % 10)]}


multiplyBy :: Ratio Int -> FinDist a -> FinDist a
multiplyBy p (FinDist l) = FinDist $ map (applyProb (*p)) l

-- |
-- >>> multiplyBy 0.2 a
-- FinDist {pairs = [(1,1 % 10),...

joinDist :: FinDist (FinDist a) -> FinDist a
joinDist dist = FinDist $ allLists dist
    where oneList ((FinDist l),r) = map (applyProb (*r)) l
          allLists (FinDist l )= join $ (map oneList) l


instance Monad FinDist where
    (>>=) dist f = joinDist (fmap f dist)
              
monadic = do 
    p1 <- a 
    p2 <- b
    return (p1+p2)

-- |
-- >>> monadic
-- FinDist {pairs = [(11,1 % 4),(21,1 % 4),(12,3 % 20),(22,3 % 20),(13,1 % 10),(23,1 % 10)]}

monadic2 = do 
    p1 <- a 
    p2 <- b
    return (p1+p2*10)

-- |
-- >>> monadic2
-- FinDist {pairs = [(101,1 % 4),(201,1 % 4),(102,3 % 20),(202,3 % 20),(103,1 % 10),(203,1 % 10)]}

combine :: Eq x => FinDist x -> FinDist x
combine (FinDist list) = FinDist $ do
  let elems = (nub . map fst) list
  x <- elems
  let allX = filter ( (== x) . fst ) list
  let p = sum $ snd <$> allX
  return (x, p)


dice = FinDist [(1,1 % 6),(2,1 % 6),(3,1 % 6),(4,1 % 6),(5,1 % 6),(6,1 % 6)]



monadic3 = combine $ do
    d1 <- dice
    d2 <- dice
    return (d1+d2)

-- |
-- >>> monadic3
-- ...(2,1 % 36)...(7,1 % 6)...(11,1 % 18)...(12,1 % 36)...

