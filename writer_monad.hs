import Control.Monad.Writer  
      
gdc' :: Int -> Int -> Writer [String] Int 
gdc' a 0 = do
           tell ["final: " ++ (show a)]
           return a
gdc' a b = do
           let next = a `mod` b
           tell ["recebi " ++ (show a) ++ "," ++ (show b) ++ ". e agora vou usar " ++ show next]
           (gdc' b next)

-- |
-- >>> fst $ runWriter (gdc' 50 15)
-- 5

logNum :: Int -> Writer [String] Int
logNum n = writer (n, ["got an "++ (evenness n)])
    where evenness n = if (n `mod` 2)==0 then "even" else "odd"
          
val = do
    a <- logNum 12 --unpack -- with let that would be a Writer, with <- it is an int
    b <- logNum 13 --unpack
    return $ a+b   --repack

-- |
-- >>> fst $ runWriter val
-- 25
-- >>> snd $ runWriter val
-- ["got an even","got an odd"]

main = print 42