{-# LANGUAGE InstanceSigs #-}
{-# LANGUAGE ScopedTypeVariables #-}
import Control.Monad
import Control.Monad.Trans.Class
import Data.Maybe
import Control.Applicative (Alternative ((<|>)))
import GHC.Base (Alternative(empty))
import Data.Foldable


newtype TransMaybe m a = TransMaybe {doubleM :: m (Maybe a)}


instance Monad m => Monad (TransMaybe m) where
 return = TransMaybe . return . Just
 mf >>= g = TransMaybe $ do may <- doubleM mf
                            if isNothing may then return Nothing 
                                             else let x = fromJust may in doubleM (g x)
                                                                          
                            -- case may of
                            --     Nothing -> return Nothing
                            --     Just x  -> doubleM (g x)

instance Monad m => Applicative (TransMaybe m)
    where pure  = return
          (<*>) = ap


instance Monad m => Functor (TransMaybe m)
    where fmap = liftM

-- exemplo bobo que eu usei pra localizar o pragma certo
data T a = MkT a a
instance Eq a => Eq (T a) where
  (==) :: T a -> T a -> Bool   -- The signature
  (==) (MkT x1 x2) (MkT y1 y2) = superequal x1
         where  superequal :: a -> Bool
                superequal x1 = x1 == y1 && x2 == y2

instance Monad m => Alternative (TransMaybe m) where
        empty     = TransMaybe $ return Nothing
        -- (<|>) :: TransMaybe m a -> TransMaybe m a-> TransMaybe m a
        -- (<|>) a b = TransMaybe $ (doubleM a) >>= choice
        --     where --choice :: Maybe a -> m a
        --           choice maybe_a = if isNothing maybe_a then doubleM b 
        --                                                 else doubleM a
        x <|> y = TransMaybe $ do maybe_value <- doubleM x
                                --   case maybe_value of
                                --     Nothing    -> doubleM y
                                --     Just _     -> return maybe_value
                                  if isNothing maybe_value 
                                        then doubleM y
                                        else return maybe_value -- (doubleM a)
                                                                -- would mean ask again, execute again


instance MonadTrans (TransMaybe) where
    -- lift = TransMaybe . fmap Just
    lift = TransMaybe . fmap return

validString :: String -> Bool
validString = (>= 8) . length


guard' :: Bool -> TransMaybe IO ()
guard' True  = return ()
guard' False = TransMaybe $ return Nothing 

prendreMot :: TransMaybe IO String
prendreMot = do
             lift $ putStrLn "Insert your new passphrase:" 
             linha <- lift getLine
             guard' (validString linha)
             let b = linha ++ "banana"
            --  lift' $ print b
             return b

teste :: IO ()
teste = do 
         l <- getLine
         print l

askPassphrase :: TransMaybe IO ()
askPassphrase = do value <- asum $ repeat prendreMot
                   lift $ putStrLn $ "Storing in database..." ++ value

main = doubleM askPassphrase

