{-# LANGUAGE FlexibleContexts #-}
{-# HLINT ignore "Redundant lambda" #-}
{-# HLINT ignore "Redundant bracket" #-}
{-# HLINT ignore "Use camelCase" #-}

--pragmas just to save them somewhere

-- https://hackage.haskell.org/package/monad-memo
-- cool :)
module Main where

import Control.Monad.Memo
import Debug.Trace
import Data.Map ( (!), fromList )

--ghci install lazyset


allTheFibs = Prelude.map fib [0 ..]

fib l | trace ("ola seno" ++ show l) False = undefined
fib 0 = 0
fib 1 = 1
fib n = memoized_fib (n-2) + memoized_fib (n-1)


memoized_fib :: Int -> Integer
memoized_fib n = (allTheFibs !!) n
   

-- If we dont point free, we die 
memoized_fib2 :: Int -> Integer
memoized_fib2 n = (allTheFibs !!) n
      where allTheFibs = Prelude.map fib [0 ..]
            fib l | trace ("ola seno" ++ show l) False = undefined
            fib 0 = 0
            fib 1 = 1
            fib n = memoized_fib (n-2) + memoized_fib (n-1)

-- digressao para explicar o trace
f :: (Eq a, Num a) => a -> String
f l | l == 20 = "banana"
f l = "pera"

-- non Memo, runs slow with 10000 [10,5]
breakCoinsNewb _ 0 = 1
breakCoinsNewb _ n | (n < 0) = 0
breakCoinsNewb all@(coin:coins) n = breakCoinsNewb all (n-coin) + breakCoinsNewb coins n
breakCoinsNewb [] n = 0
-- inicial                 = fromList [([],1 : [0,0..])]


-- allTheBreaks :: [Int] -> Map [Int] [Integer]
-- allTheBreaks list = fromList pairs
--       where 
--             --([]:fmap (breakCoins []) [0..])


breakCoins :: [Int] -> Int -> Integer
breakCoins list n = allBreaks ! list !! n
      where breakCoinsGen _ 0 = 1
            breakCoinsGen _ n | (n < 0) = 0
            breakCoinsGen all@(coin:coins) n = (allBreaks ! all) !! (n-coin) + (allBreaks ! coins) !! n
            breakCoinsGen [] n = 0
            keys2 all@(coin:coins)     = all: keys2 coins
            keys2 [] = [[]]
            pairs                     = [(key,calculated key [0,1..]) | key <- (keys2 list)]
            calculated key positions  = fmap (breakCoinsGen key) positions
            allBreaks                 = fromList pairs

breakCoins2 :: [Int] -> Int -> Integer
breakCoins2 list n = allBreaks ! length list !! n
      where --breakCoinsGen a l | trace (show a ++ "ola seno" ++ show l) False = undefined
            breakCoinsGen _ 0 = 1
            breakCoinsGen _ n | (n < 0) = 0
            breakCoinsGen all@(coin:coins) n = (allBreaks ! length all) !! (n-coin) + 
                                               (allBreaks ! length coins) !! n
            breakCoinsGen [] n = 0
            keys2 all@(coin:coins)     = all: keys2 coins
            keys2 [] = [[]]
            pairs                     = [(key,calculated key [0,1..]) | key <- (keys2 list)]
            calculated key positions  = fmap (breakCoinsGen key) positions
            lenPairs (a,b)            = (length a,b)
            allBreaks                 = fromList (map lenPairs pairs)

main = print (breakCoins2 [1,2] $ 10*1000)

fibm :: (MonadMemo Integer Integer m) => Integer -> m Integer
fibm 0 = return 0
fibm 1 = return 1
fibm n = do
  f1 <- memo fibm (n-1)
  f2 <- memo fibm (n-2)
  return (f1+f2)


evalFibm :: Integer -> Integer
evalFibm = startEvalMemo . fibm

-- |
-- >>> evalFibm 10
-- 55
-- >>> evalFibm 100
-- 354224848179261915075

weirdFib (start,0) = start
weirdFib (start,1) = start
weirdFib (start,n) = weirdFib (start,n-1) + weirdFib (start,n-2)

weirdFibm (start,0) = return start
weirdFibm (start,1) = return start
weirdFibm (start,n) = do
      a <- weirdFibm (start,n-1) 
      b <- weirdFibm (start,n-2)
      return (a+b)

evalWeird = startEvalMemo . weirdFibm

test = Prelude.map (\x -> evalWeird (2,x)) [0,1,2,3,4,5]

-- |
-- >>> test
-- [2,2,4,6,10,16]
