{-
Prelude> fifteenDouble + fifteenInt
Couldn't match expected type ‘Double’
with actual type ‘Int’
In the second argument of ‘(+)’,
namely ‘fifteenInt’
In the expression: fifteenDouble + fifteenInt
We can’t add those two values because their types are no
longer polymorphic, and their concrete types are different
-}

-- not Bool a => a -> a -- R not :: Bool -> Bool. Diferença?
-- length a => [a] -> Int -- length :: Foldable t => t a -> Int (foldable generaliza lista)
-- concat a => [[a]] -> [a]
-- head a => [a] -> a -- head :: [a] -> a
-- (<) Ord a => a -> Bool

-- (->) is right associative. So a->a->a means a->(a->a)

-- uncurrying (obvio)f (a,b) = a+b
-- anon anon = \a b -> a+b
-- let curry f a b = f (a, b) -- to curry any function
-- to uncurry any: uncurry f (a,b) = f a b

{-
*Main> inbanana = (`elem` "banana")
*Main> in
inbanana  init      interact
*Main> inbanana 'b'
True
*Main> inbanana 'z'
False
*Main> 

*Main> hasTen = elem 10
*Main> hasTen [1,2,3]
False
*Main> hasTen [1,2,3,10]
True

usual syntax: elem 9 [1,2,3]

-}

--f:: Char a => a->a->a->a
--f = undefined

f2 :: Num a => a -> a -> a -> a -> a
f2 x y z k=x+y+z+k

f3 :: Num a => a -> a -> a -> a -> a
f3 = undefined

--f4 :: String a => a -> a -> a -> a -> a
--f4 = undefined

f5 :: [Char] -> [Char] -> [Char] -> [Char] -> [Char]
f5 x y z k = x

f6 :: [Char] -> [Char] -> [Char] -> [Char] -> [Char]
f6 = undefined

f7 :: (Ord a, Num a) => a->b->a
f7 = undefined

-- types curiosity Num b => b versus just Char or [Char]

--Prelude> :t h 1.0 (5.5 :: Double)
--h 1.0 (5.5 :: Double) :: Double
--Prelude> :t h 1.0 (5.5)
--h 1.0 (5.5) :: Fractional b => b

-- Prelude> kessel :: (Ord a, Num b) => a -> b -> a; kessel = undefined
-- Prelude> :t kessel 1 2
-- kessel 1 2 :: (Ord a, Num a) => a
-- Prelude> :t kessel "a" 2
-- kessel "a" 2 :: [Char]

-- parametric polimorphism versus constrained polimorphism
-- parametric: argumento do tipo a, vira tipo especifico quando conveniente
-- constrained (Num a) => a

-- Will infer the most general type applicable
-- ++ receives two lists (can be of Char)
-- Prelude> :t (++)
-- (++) :: [a] -> [a] -> [a]


