{-# LANGUAGE NoMonomorphismRestriction #-}

module DetermineTheType where
-- simple example
example = 1
-- Tipo expresso como Num p => p

a =  6 * 9
b = head [(0,"doge"),(1,"kitteh")]
c = head (tail [(0 :: Integer,"doge"),(1,"kitteh")])
-- constrangi o segundo tb

e = length [1,2,3]

z y = y * 10
-- Num a => a -> a -- nao pode retornar qq num!

-- Prelude> functionC x y = (x > y)
-- Prelude> :t functionC
-- functionC :: Ord a => a -> a -> Bool
co :: (b -> c) -> (a -> b) -> a -> c 
co f g num = f (g num)


-- *DetermineTheType> a=(1,2)
-- *DetermineTheType> :t a
-- a :: (Num a, Num b) => (a, b)
-- se eu impusesse os dois do mesmo tipo, estaria
-- restringindo as funcoes que aceitam, nao?
-- to na duvida

munge :: (x -> y) -> (y -> (w, z)) -> x -> w
munge f g x = a
          where (a,b) = (g (f x)) 

    
