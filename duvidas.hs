-- concatMap' = concat . map
-- gives an error of kind "no instance for Expected Type", giving me the type of (something like . map) and complaining concat does not have an instance for that
j = map 2 
-- similar to above concatMap'
a = 1 :: Int
--concatMap' = concat . (+a)
--concatMap' = curry (concat . (uncurry map))