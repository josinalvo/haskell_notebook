

-- Distro :: Float -> Float -> Float -> Distro
data Distro = Distro Float Float Float deriving Show

validateDistro :: Distro -> Maybe Distro
validateDistro d@(Distro x y z) = if 0.999 <= s && s <= 1.00001 then Just d else Nothing
    where s = x + y + z

validateFloat :: Float -> Maybe Float
validateFloat x = if 0.0 <= x && x <= 1.0 then Just x else Nothing

a = (())
b = 12

-- validateDistro <$> (Distro <$> validateFloat x <*> validateFloat y <*> validateFloat z)

-- Teorema do Seno: alguns do(s) podem ser traduzidos para applicative
-- Especificamente (<-), (<-), (mais setas), return
-- return Distro <*> validateFloat x <*> validateFloat y <*> validateFloat z

maybeDistro :: Float -> Float -> Float -> Maybe Distro
maybeDistro x y z = do
  x' <- validateFloat x
  y' <- validateFloat y
  z' <- validateFloat z
  validateDistro $ Distro x' y' z'


maybeDistro2 :: Float -> Float -> Float -> Maybe Distro
maybeDistro2 x y z =
    validateFloat x >>= a
    where a :: Float -> Maybe Distro
          a x' = validateFloat y >>= b x'

          b :: Float -> Float -> Maybe Distro
          b x' y' = validateFloat z >>= c x' y'

          c :: Float -> Float -> Float -> Maybe Distro
          c x' y' z' = validateDistro $ Distro x' y' z'

a =  maybeDistro  0.1 0.2 0.7
a2 = maybeDistro2 0.1 0.2 0.7

b =  maybeDistro  0.1 0.2 0.6
b2 = maybeDistro  0.1 0.2 0.6

c =  maybeDistro  0.6 (-0.2) 0.6
c2 = maybeDistro  0.6 (-0.2) 0.6
-- | 
-- >>> a
-- Just (Distro 0.1 0.2 0.7)
-- >>> a2
-- Just (Distro 0.1 0.2 0.7)
-- >>> (b,b2)
-- (Nothing,Nothing)
-- >>> (c,c2)
-- (Nothing,Nothing)

main = print 42
