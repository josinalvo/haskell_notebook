f x = a*x+b
    where a = 2
          b = 3
          c = 5

printor x = let a = 2
                b = 3
          in a*x+b

-- porque sem a declaracao de funcao tenho um erro de sintaxe?
-- essa debaixo está ok no repl, mas nao no arquivo! @#$@S%&!
-- let x = 5; y = 4 in x*y
--
-- multiline do ghci, :{    :}

a = x*3+y
    where x = 3
          y = 1000

b = x + 5
    where x = 10 * 5 + y
          y = 10

-- um 3 solto também dá pau. Let é uma expressao, que precisa ser guardada
-- em algum lugar?

waxOn = x * 5
   where y = z + 8
         z = 7
         x = y ^ 2

triple x = x * 3

waxOff x = triple x

