sayHello :: String -> IO()
sayHello x = putStrLn ("Hello, " ++ x ++ "!")

triple x = x * 3
-- = to declare vars and funcions
-- not to test equality
--
(+) x y = x*x
-- infix by default
-- *Main> 5 Main.+ 10
-- 25
--
-- *Main> :info *
-- infixl 7 * infix, left assoc, 7 is precedence, higher means first
-- left assoc? 3*2*4 = 6*4 = 24 (i.e., order of operations if no precedence involved)


