var_art = 12
-- var_art = 12 -- se eu fizer de novo dah pau

data Mood = Happy | Blah deriving Show

-- changeMood :: a->Mood -- nor working yet
changeMood Happy = Blah
changeMood _ = Happy

myInt :: Int
myInt = 3^81 -- falha e dá overflow

-- no ghci, 2^30 :: Int

{-
Prelude> :t maxBound
maxBound :: Bounded a => a
maxBound :: Int64 (returns an int64)
-}

{-
Prelude> :i Int
data Int = GHC.Types.I# GHC.Prim.Int#
-- Defined in ‘GHC.Enum’
instance Bounded Int   (int belongs to typeClass Bounded)
-}

-- You don't want to use float, unless graphics programming

-- Num is superclass of Fractional. Float is a fractional (dk the name of the relationship)

-- x /= 5 (different)

{-
Prelude> :t (==)
(==) :: Eq a => a -> a -> Bool
Prelude> :t (<)
(<) :: Ord a => a -> a -> Bool
-}

{-
data Bool = False | True
This declaration creates a datatype with the type construc-
tor Bool, and we refer to specific types by their type construc-
tors. True and False are data constructors
-}

-- to compare strings, might be clever to lowercase

{-
"Falsin'"
Prelude> :t if True then t else f
if True then "Truthin'" else "Falsin'"
:: [Char]
-}

{-
2 + fst(1,2)
swap
-}

tupFunc :: (Int, [a])
        -> (Int, [a])
        -> (Int, [a])
tupFunc (a, b) (c, d) = ((a + c), (b ++ d))

a = concat [["cobra","djin","fast"],["missle","bob"]]
{-
Prelude> :t concat
concat :: [[a]] -> [a]
-}

-- just for the exercises
awesome = ["Papuchon", "curry", ":)"]
also = ["Quake", "The Simons"]
allAwesome = [awesome, also]

 
 {-
<interactive>:144:1: error:
    • No instance for (Fractional Int) arising from a use of ‘/’
    • In the expression: (/) 6 (length [1, 2, 3])
      In an equation for ‘it’: it = (/) 6 (length [1, 2, 3])
*Main> y= fromIntegral (length [1,2,3])
*Main> 6/y
2.0
for now I could not write a one liner
quot and div were also available


Note the output syntax for type
*Main> :t 12
12 :: Num p => p

-}

isPalindrome :: (Eq a) => [a] -> Bool
isPalindrome x = ((reverse x) == x)

-- cool type signature
absVal :: (Ord a, Num a) => a -> a
absVal x = if (x < 0) then (-x) else x


-- F x = x+1 -- dá pau porque maiusculas são pra tipos

x = (+)
f xs = x w 1
    where w = length xs

{-
*Main> type Name = String                (1)
*Main> data Pet = Cat | Dog Name deriving Show (2)
*Main> (Dog "rufus")
Dog "rufus"
*Main> :t Dog
Dog :: Name -> Pet
(1) type alias (2) data declaration (defined respectively by the
initial keyword type or data)

Pet is a type constructor and Dog is a data constructor

convetion xs is a list (e.g in separating first term -- (x:xs))
-}

-- a lambda: (\x -> x+1) 11

----
-- Conversa


-- => setinha de tipos, nao de funcao

data Boola =  True
-- Criei o construtor bool que cria um true

-- data Fayner = Ags a b | Chong a b c

quinto_elemento = (drop 4) . (take 5)

nth :: Int -> [a] -> [a]
nth n = (drop (n-1)) . (take n)

last' x = x !! (length x - 1)
comeco x = take (length x - 1) x

splitH :: (Eq a) => a -> [a] -> [[a]] -> [[a]]
splitH s []   hist = hist
splitH s (l:ls) []   
    | s /= l = splitH s ls [[l]]
    | s == l = splitH s ls []

splitH s (l:ls) hist  
    | s == l = splitH s ls (hist ++ [[]])
    | s /= l = splitH s ls hist_novo
        where hist_novo  = comeco hist ++ [(last' hist) ++ [l]]

        
split :: (Eq a) => a -> [a] -> [[a]]
split s [] = [[]]
split s (l:ls) 
    | s == l = spacer ++ split s ls 
    | s /= l = [[l] ++ first_list] ++ other_lists
          where first_list:other_lists = (split s ls)
                spacer = if first_list /= []
                         then [[]]
                         else []

split_r :: (Eq a) => a -> [a] -> [[a]]
split_r s [] = [[]]
split_r s (l:ls) 
    | s == l = split_r s ls ++ [[]]
    | s /= l = other_lists ++ [[l] ++ last_list]
          where recursive = (split_r s ls)
                last_list = recursive !! (length recursive - 1)
                other_lists = take (length recursive - 1) recursive


split_s :: String -> Char -> [String]
split_s [] delim = [""]
split_s (c:cs) delim
    | c == delim = "" : rest
    | otherwise = (c : head rest) : tail rest
    where
        rest = split_s cs delim
              
-- *Main> data Pasta = Fusili | Linguine | Spaguetti deriving Show
-- *Main> data Molho = Carbonara | Ragu | Bechamel deriving Show
-- *Main> data Macarronada = Macarronada Pasta Molho deriving Show
-- *Main> m = Ragu
-- *Main> p = Fusili
-- *Main> Macarronada p m
-- Macarronada Fusili Ragu
-- *Main> data Macarronada a = Mac a Molho deriving Show
-- *Main> Mac 12 Ragu
-- Mac 12 Ragu
data Pasta = Fusili (Integer) | Linguine String -- parenthesis seem irrelevant
grow (Fusili x) = show (x+1)
grow (Linguine y) = y ++ "banana"
        
--splitH s (s:ls) hist = splitH s ls (hist ++ [[]])
conta [] = 0
conta (n:ns) = 1+conta(ns) 

-- Primeiramente, eu escrevi:

-- data SelfApplicable = SelfApplicable -> Integer -> Integer

-- Mas isso está semanticamente errado: à esquerda do igual a gente tem o "type constructor", que define o novo tipo, e à direita a gente tem o "data constructor", que dá uma função que constroi elementos desse novo tipo. 
-- O correto é:

data SelfApplicable = Box (SelfApplicable -> Integer -> Integer)

-- Veja, à esquerda do igual estamos falando "existe um tipo chamado SelfApplicable", e à direita estamos falando "existe uma função Box que recebe uma função do tipo SelfApplicable -> Integer -> Integer, põe ela numa caixinha e diz que o resultado é do tipo SelfApplicable"
-- Para aplicar uma SelfApplicable, a gente tem que tirar a função da caixinha. Para passar como argumento, a gente tem que por na caixinha.

factPartial :: SelfApplicable -> Integer -> Integer
factPartial (Box p) n = 
  if n == 0
    then 1
    else n * p (Box p) (n-1)

fact :: Integer -> Integer
fact = factPartial( Box factPartial )

---data Tree a = Empty | Node { left :: Tree a, value :: a, right :: Tree a}
