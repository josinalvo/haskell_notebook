class Equilibrium a where  
    (==) :: a -> a -> Bool  
    (/=) :: a -> a -> Bool  
    x == y = not (x Main./= y)  
    x /= y = not (x Main.== y)  

data Month = Month Integer

instance Equilibrium Month where
    (==) (Month a) (Month b) = a Prelude.== b