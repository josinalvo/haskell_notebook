-- Nap surpreendentemente, um typeclass pode
-- exigir outro

-- Prelude F M> (1, 2) == "puppies!"
-- Couldn't match expected type ‘(t0, t1)’
-- with actual type ‘[Char]’
-- gerei uma funcao com type (t1,t2)
-- e char nao foi legal
-- Eq a => a -> a -> Bool.

-- In the second argument of ‘(==)’, namely ‘"puppies!"’
-- In the expression: (1, 2) == "puppies!"
-- In an equation for ‘it’: it = (1, 2) == "puppies!"


-- instance Eq Word -- Defined in ‘GHC.Classes’
-- (word is Eq)

-- instance (Eq a, Eq b) => Eq (a, b) -- Defined in ‘GHC.Classes’
-- A tuple is eq if both elements are Eq

data Trivial = Trivial'
        
instance Eq Trivial where
      (==) Trivial'  Trivial' = True

-- :set -Wall -- warnings, incluindo padrao nao exaustivo

f :: Int -> Bool
f 2 = True

-- Pattern match(es) are non-exhaustive
-- In an equation for ‘f’:
-- Patterns not matched:
-- GHC.Types.I# #x with #x `notElem` [2#]

data Holder a = Holder a

instance Eq a => Eq (Holder a) where
    (==) (Holder v) (Holder v') = v == v' -- isso tem a msm sint.
    -- de uma definicao de funcao?

data TisAnInteger = TisAn Integer

instance Eq TisAnInteger where
    (/=) (TisAn x) (TisAn y) = (x /= y)
    (==) (TisAn x) (TisAn y) = (x == y)

data StringOrInt = Tint Int | Ts String

instance Eq StringOrInt where
    (==) (Ts s1) (Ts s2) = s1 == s2
    (==) (Tint s1) (Tint s2) = s1 == s2
    (==) (Ts s1) (Tint s2) = (s1 == (show s2))

data Pair a =
    Pair a a
        
instance Eq a => Eq (Pair a) where
    (==) (Pair x1 x2)  (Pair y1 y2) = x1==y1

data EitherOr a b =
        Hello a
        | Goodbye b
        
instance (Eq a, Eq b) => Eq (EitherOr a b) where 
    (==) (Hello a1) (Hello a2) = (a1 == a2)
    (==) (Goodbye b1) (Goodbye b2) = (b1 == b2)

-- typeclass inheritance is only additive

-- class (Num a) => Fractional a where
-- (/)  :: a -> a -> a
-- recip  :: a -> a
-- -- fromRational :: Rational -> a
-- Isso me leva a crer que voce nao se torna um fractional,
-- voce se torna um tipo especifico

-- default Num Integer
-- default Real Integer
-- default Enum Integer
-- default Integral Integer
-- default Fractional Double
-- default RealFrac Double
-- default Floating Double
-- default RealFloat Double

-- Integral é class, Int e Integer é type. Posso forçar uma var
--  a ter type, mas não class

a :: (Integral a) => a -> a -> Int
a x y = fromIntegral (x * y) -- Int implementa from integral

--type variable -- a in a def
-- how can we have non overwriting and, at the
-- same time, partial implementations?

-- We wanted an instance of Show because we (indirectly)
-- invoked print which has type print :: Show a => a -> IO ()

data DayOfWeek = Mon | Tue | Weds | Thu | Fri | Sat | Sun deriving(Ord,Show,Eq)
--we had implemented Eq, but did not have to

days = [Mon , Tue , Weds , Thu , Fri , Sat , Sun]
instance Enum DayOfWeek where
    toEnum n =  days !! idx where idx = (n+7) `rem` 7
    fromEnum day = head [n | n<-[0..], days!!n == day ]


data Triplet a b =
        T1 a
        | T2 b
        | T3 deriving (Eq)

-- Prelude> :info Ord
-- class Eq a => Ord a where
-- That means, in particular, that definitions with Ord have a ==     

-- IO() -- returns ()
-- When you have a value of type IO String it’s more of a means of
-- producing a String, which may require performing side effects

-- tem que por o deriving (Show) -- com ou sem ()
data Person = Person Bool deriving Show
printPerson :: Person -> IO ()
printPerson person = putStrLn (show person)

-- two type errors
-- No instance for (Type1 Type2) arising from the literal ‘12’
-- Gave you one and you wanted the other
-- 2)Couldn't match expected type ‘Mood’ with actual type ‘Int’

--  No instance for (Ord Mood) arising from a use of ‘>’
--  In the expression: Woot > Blah

data Rocks = Rocks String deriving (Eq, Show)
data Yeah = Yeah Bool deriving (Eq, Show)
data Papu = Papu Rocks Yeah
    deriving (Eq, Show)

instance Ord Papu where
    (<=) (Papu (Rocks a2) (Yeah a)) (Papu (Rocks b2) (Yeah b)) =  a <= b


    
equalityForall :: Papu -> Papu -> Bool
equalityForall p p' = p == p'


    
comparePapus :: Papu -> Papu -> Bool
comparePapus p p' = p > p'

-- ter um tipo concreto já implica seus tipos abstratos

-- No instance for (Ord Mood) arising from a use of ‘>’
-- > usa ord, recebi Mood, que nao é Ord

-- i :: a
-- i = 1 --nao funciona!

--f :: Num a => a
-- f = 1.0  --nao funciona, num generico demais

-- The long
-- and short of it is that if you want to write an instance of
-- Fractional for some a, that type a, must already have an
-- instance of Num before you may do so.

-- An instance is the definition of how a typeclass should
-- work for a given type.



