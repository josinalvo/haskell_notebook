-- import Data.Sequence hiding (Empty)
import Prelude hiding (Left, Right)

-- data Arvore = Arvore Int (Seq Arvore)

data ArvoreB = ArvoreB {value :: Int, left :: ArvoreB, right :: ArvoreB} | Empty deriving Show

data Z = Z {current :: ArvoreB, stack :: [Piece]} deriving Show

data Piece = WentLeft {pai :: Int, rightSib :: ArvoreB} 
             | WentRight {pai :: Int, leftSib :: ArvoreB} deriving Show

goUp :: Z -> Z
goUp z@(Z {stack=[]}) = error "foi pra cima demais, rapaiz"
goUp z@(Z {current=curr, stack=((WentLeft paiV sib):pis)}) = Z newCurr pis
    where newCurr = ArvoreB paiV curr sib
goUp z@(Z {current=curr, stack=((WentRight paiV sib):pis)}) = Z newCurr pis
    where newCurr = ArvoreB paiV sib curr

changeVal :: Z -> Int -> Z
changeVal arv@(Z {current=Empty}) val = arv{current=newCurr}
    where newCurr = ArvoreB val Empty Empty
changeVal arv@(Z {current=currTree}) val = arv{current=newCurr}
    where newCurr = currTree{value=val}

goRight :: Z -> Z
goRight z@(Z {current=Empty}) = error "direita de vazio"
goRight z@(Z {current=curr, stack=s}) = Z newCurr newStack
    where newCurr = right curr
          newStack = (WentRight (value curr) (left curr)):s

goLeft :: Z -> Z
goLeft z@(Z {current=Empty}) = error "esquerda de vazio"
goLeft z@(Z {current=curr, stack=s}) = Z newCurr newStack
    where newCurr = left curr
          newStack = (WentLeft (value curr) (right curr)):s

emptyZ = Z Empty []

--       10
--   5*      50
-- 2   3

-- Z {current = ArvoreDo5, [WentLeft (10,arvoredo50)]

z1 = emptyZ
z2 = changeVal z1 10
z3 = (`changeVal` 5) $ goLeft z2
z4 = (`changeVal` 2) $ goLeft z3
z5 = (`changeVal` 3) $ goRight $ goUp z4
z6 = (`changeVal` 50) $ goRight $ goUp $ goUp z5
z7 =  goLeft $ goUp z6

-- |
-- >>> z7
-- Z {current = ArvoreB {value = 5, left = ArvoreB {value = 2, left = Empty, right = Empty}, right = ArvoreB {value = 3, left = Empty, right = Empty}}, stack = [WentLeft {pai = 10, rightSib = ArvoreB {value = 50, left = Empty, right = Empty}}]}
