import Control.Monad
import Distribution.Simple.Utils (die')

data Name = Name {firstName ::String, lastName ::String}

instance Show Name where
    show (Name a b) = a ++ " " ++ b

data GradeLevel = Freshman | Sophmore | Junior 
    | Senior deriving (Eq, Ord, Show, Enum)

data Student = Student {studentId ::Int, 
                       
                        grade ::GradeLevel,
                         studentName ::Name
                        }

students :: [Student]
students = [(Student 1 Senior (Name "Andre" "F"))
            ,(Student 2  Junior (Name "Benjamin" "Sisko"))
            ,(Student 3  Freshman (Name "Odette" "Roitman"))
            ,(Student 4  Senior (Name "Galo" "Galiha"))
            ,(Student 5  Sophmore (Name "Vitor" "Vitória"))
            ,(Student 6  Junior (Name "Julia" "Jura"))]

_select :: (a->b) -> [a] -> [b]
_select f vals = do
    val <- vals
    return (f val)

-- |
-- >>> _select (firstName . studentName) students
-- ["Andre","Benjamin","Odette","Galo","Vitor","Julia"]
-- >>> _select grade students
-- [Senior,Junior,Freshman,Senior,Sophmore,Junior]

_where2 :: (a-> Bool) -> [a] -> [a]
_where2 f list = do
    val <- list
    if (f val)
        then return val
        else []

_where3 :: (a-> Bool) -> [a] -> [a]
_where3 f list = do
    val <- list
    guard (f val)
    return val

_where :: (a-> Bool) -> [a] -> [a]
_where f list = list >>= (\x -> (guard $ f x) >>= (\_ -> return x))

-- |
-- >>> _where (\x -> True) [1,2,3]
-- [1,2,3]
-- >>> _where (\x -> rem x 2 == 1) [1,2,3]
-- [1,3]
-- >>> _where (\x -> False) [1,2,3]
-- []

-- |
-- >>> do {guard False ; ["copa","cabana"]}
-- []
-- >>> guard False >> do {["copa","cabana"]}
-- []
-- >>> guard True >> do {["copa","cabana"]}
-- ["copa","cabana"]
-- >>> do {["copa","cabana"]}
-- ["copa","cabana"]
-- >>> [] >> do {["copa","cabana"]}
-- []
-- >>> [()] >> do {["copa","cabana"]}
-- ["copa","cabana"]
-- >>> [(),()] >> do {["copa","cabana"]}
-- ["copa","cabana","copa","cabana"]
-- >>> [(),()] >>= \_ -> do {["copa","cabana"]}
-- ["copa","cabana","copa","cabana"]

data Teacher = Teacher {teacherId :: Int,  
                        teacherName :: Name} deriving Show

teachers :: [Teacher]
teachers = [Teacher {teacherId = 100, teacherName =  (Name "Albert" "Einstein")},
            Teacher 200 (Name "Paul" "Erdos")]

data Course = Course
    { courseId :: Int
    , courseTitle :: String
    , teacher :: Int } deriving Show


courses :: [Course]
courses = [Course 101 "French" 100
           ,Course 201 "English" 200]

-- _join :: Eq c => [a] -> [b] -> (a -> c) -> (b -> c) -> [(a,b)]

_join :: Eq c => [a] -> [b] -> (a -> c) -> (b -> c) -> [(a,b)]
_join l1 l2 f1 f2 = do
    e1 <- l1
    e2 <- l2
    guard $ f1 e1 == f2 e2
    return (e1,e2)

join :: Eq c => [a] -> [b] -> (a -> c) -> (b -> c) -> [(a,b)]
join data1 data2 prop1 prop2 = do
    d1 <- data1
    d2 <- data2
    let dpairs = (d1,d2)
    guard ((prop1 (fst dpairs)) == (prop2 (snd dpairs)))
    return dpairs

-- |
-- >>> _join teachers courses teacherId teacher
-- [(Teacher {teacherId = 100, teacherName = Albert Einstein},Course {courseId = 101, courseTitle = "French", teacher = 100}),(Teacher {teacherId = 200, teacherName = Paul Erdos},Course {courseId = 201, courseTitle = "English", teacher = 200})]

_hinq' selectQuery joinQuery whereQuery = (\joinData ->
                                            (\whereData ->
                                            selectQuery whereData)
                                            (whereQuery joinData)
                                            ) joinQuery

_hinq selectQuery joinQuery whereQuery = selectQuery $ whereQuery joinQuery


finalResult :: [Name]
finalResult = _hinq (_select (teacherName . fst))
            (_join teachers courses teacherId teacher)
            (_where ((== "English") .courseTitle . snd))

-- |
-- >>> finalResult
-- [Paul Erdos]


data Foo = Foo { bar :: Int, baz :: Int, quux :: Int } deriving Show
fooDefault = Foo { bar = 1, baz = 2, quux = 3 }
newRecord = fooDefault { quux = 43 }


-- |
-- >>> newRecord
-- Foo {bar = 1, baz = 2, quux = 43}

main = print 42