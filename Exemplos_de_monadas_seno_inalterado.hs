module Play where

class Functor functor where
  fmap :: (a -> b) -> functor a -> functor b

instance Play.Functor [] where
  fmap f [] = []
  fmap f (x:xs) = f x : Play.fmap f xs

instance Play.Functor ( (,) t) where
  fmap f (t, a) = (t, f a)

instance Play.Functor ( (->) t) where
  -- fmap :: (a -> b) -> (t -> a) -> (t -> b)
  fmap f g = f . g

l = [2, 3, 7]

f :: Integer -> Integer
f = (+1)

data Tres = Zero | Um | Dois deriving Show

g :: Tres -> Integer 
g Zero = 2
g Um = 3
g Dois = 7

filter' :: (a -> Bool) -> [a] -> [a]
filter' p [] = []
filter' p (x:xs) = if p x then x: filter' p xs else filter' p xs

filter'' :: (a -> Bool) -> [a] -> [a]
filter'' p l = do
  x <- l
  if p x
    then return x
    else []

-- instance Monad [] where
return' :: a -> [a]
return' x = [x]
-- (Play.>=) :: [a1] -> (a1 -> [a2]) -> [a2]
list >= f = concat (map f list)

filter''' p l = concat $ map (\x -> if p x then return' x else [] ) l

cart l1 l2 = do
  x <- l1
  y <- l2
  return (x, y)

cart' l1 l2 = [(x, y) | x <- l1, y <- l2]

add :: (t -> Int) -> (t -> Int) -> (t -> Int)
add f g = \x -> f (x) + g (x)

add' :: (t -> Int) -> (t -> Int) -> (t -> Int)
add' f g = do
  y <- f
  z <- g
  return ( y + z )

-- instance Monad ( (->) t) where
return'' :: a -> (t -> a)
return'' x = const x
(>>>=) :: (t -> a) -> (a -> (t -> b)) -> (t -> b)
f >>>= g = \x -> g ( f x ) x

add'' f g = f >>>= \y -> (g >>>= \z -> return'' ( y + z ))

