{-# OPTIONS_GHC -Wno-unrecognised-pragmas #-}
{-# HLINT ignore "Redundant bracket" #-}
{-# HLINT ignore "Eta reduce" #-}
import Control.Monad.State
import System.Random

data TurnstileInput = Coin | Push deriving(Eq,Show)

data TurnstileState = Unlocked | Locked deriving(Eq,Show)

data TurnstileAns   = Thank | GoAhead | Sorry deriving(Eq,Show)

turnS :: TurnstileInput -> State TurnstileState TurnstileAns
turnS input = state (turn input) where
    turn :: TurnstileInput -> (TurnstileState -> (TurnstileAns,TurnstileState))
    turn Coin _ = (Thank,Unlocked)
    turn Push Unlocked = (GoAhead,Locked)
    turn Push Locked   = (Sorry,Locked)

s = mkStdGen 667

rollRange:: Int -> State StdGen Int
-- rollRange n = state $ aux
--     where aux seed = randomR (0,n-1) seed
rollRange n = state $ randomR (0,n-1)

randomElem :: [a] -> State StdGen a
randomElem l = do
    -- pos <- rollRange (length l)
    pos <- state $ randomR(0,length l - 1)
    return (l !! pos)


randomTurn :: State (StdGen,TurnstileState) (TurnstileInput,TurnstileAns)
randomTurn = do
    (gen,tRam)         <- get
    let (action,gen2)  = runState (randomElem [Coin,Push]) gen
    let (tResul,tRam2) = runState (turnS action) tRam
    put (gen2,tRam2)
    return (action,tResul)

-- |
-- >>> (ans,s2) = runState randomTurn (s,Locked)
-- >>> ans
-- (Coin,Thank)
-- >>> (ans,s3) = runState randomTurn s2
-- >>> ans
-- (Push,GoAhead)
-- >>> (ans,s4) = runState randomTurn s3
-- >>> ans
-- (Coin,Thank)
-- >>> (ans,s5) = runState randomTurn s4
-- >>> ans
-- (Coin,Thank)
-- >>> (ans,s6) = runState randomTurn s5
-- >>> ans
-- (Push,GoAhead)
-- >>> (ans,s7) = runState randomTurn s6
-- >>> ans
-- (Push,Sorry)

actA :: State a ans -> State (a,b) ans
actA stateAction = do
    (ramA,ramB) <- get
    let (ans,ramA2) = runState stateAction ramA
    put (ramA2, ramB)
    return ans


actB :: State b ans -> State (a,b) ans
actB stateAction = do
    (ramA,ramB) <- get
    let (ans,ramB2) = runState stateAction ramB
    put (ramA, ramB2)
    return ans


randomTurn2 :: State (StdGen,TurnstileState) (TurnstileInput,TurnstileAns)
randomTurn2 = do
    choice <- actA (randomElem [Coin,Push])
    result <- actB (turnS choice)
    return (choice,result)

-- |
-- >>> (ans,s2) = runState randomTurn2 (s,Locked)
-- >>> ans
-- (Coin,Thank)
-- >>> (ans,s3) = runState randomTurn2 s2
-- >>> ans
-- (Push,GoAhead)
-- >>> (ans,s4) = runState randomTurn2 s3
-- >>> ans
-- (Coin,Thank)
-- >>> (ans,s5) = runState randomTurn2 s4
-- >>> ans
-- (Coin,Thank)
-- >>> (ans,s6) = runState randomTurn2 s5
-- >>> ans
-- (Push,GoAhead)
-- >>> (ans,s7) = runState randomTurn2 s6
-- >>> ans
-- (Push,Sorry)


