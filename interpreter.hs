

-- newtype M a = M a
-- unitM :: a -> M a
-- unitM a =  (M a)
-- bindM :: M a -> (a -> M b) -> M b
-- (M a) `bindM` k =  k a
-- showM (M a) = showval a


data M a = GSuccess a | Error String
unitM :: a -> M a
unitM a =  GSuccess a
errorM :: String -> M a
errorM = Error
bindM :: M a -> (a -> M b) -> M b
(GSuccess a) `bindM` k =  k a
(Error s) `bindM` k =  Error s
showM (GSuccess a) = "Great Success " ++ showval a
showM (Error s) = "Error: " ++ s

-- showM :: Show a => M a -> String
-- showM a = show a


type Name = String

data Term = Var Name
          | Con Int
          | Add Term Term
          | Lam Name Term
          | App Term Term

data Value = Wrong
           | Num Int
           | Fun (Value -> M Value) 


showval ::  Value -> String
showval Wrong =  "<wrong>"
showval (Num i) = show i
showval (Fun f) = "<function>"

type Environment = [(Name, Value)]



interp ::  Term -> Environment -> M Value
interp (Var x) e = lookup2 x e
interp (Con i) e = unitM (Num i)
interp (Add u v) e = interp u e `bindM` (\a -> interp v e `bindM` (\b -> add a b))
interp (App t u) e = interp t e `bindM` (\f -> interp u e `bindM` (\a -> apply f a))
interp (Lam x v) e = unitM (Fun (\x' -> interp v ((x,x'):e)))

-- lookup2 :: Name -> Environment -> M Value
-- lookup2 x [] = unitM Wrong
-- lookup2 x ((y,b):e) = if x==y then unitM b else lookup2 x e

lookup2 :: Name -> Environment -> M Value
lookup2 x [] = errorM  $ "unbound variable " ++ x
lookup2 x ((y,b):e) = if x==y then unitM b else lookup2 x e


add :: Value -> Value -> M Value
add (Num i) (Num j) = unitM (Num (i+j))
add a b = errorM  $ "should be numbers " ++ showval a ++ "," ++ showval b


apply :: Value -> Value -> M Value
apply (Fun k) a = k a
apply f a = errorM  $ showval f ++ "should be function "   

term0 = (App 
            (Lam "x" (Add (Var "x") (Var "x")))
            (Add (Con 10) (Con 11)))

-- |
-- >>> (\x -> ((+) x x)) ((+) 10 11)
-- 42

-- interp term0 []
-- interp (App (Lam "x" (Add (Var "x") (Var "x")))             (Add (Con 10) (Con 11))) []
-- linha 41
--      interp t e
--      interp (Lam "x" (Add (Var "x") (Var "x"))) []
--      unitM (Fun (\a -> interp (Add (Var "x") (Var "x")) [("x",a)] ))
--
-- linha 41
--      (\f -> interp u e)
--      (interp u e)     
--      interp (Add (Con 10) (Con 11))) []
--      interp u e `bindM` (\a -> interp v e `bindM` (\b -> add a b))
--      interp (Con 10) [] `bindM` (\a -> interp (Con 11) [] `bindM` (\b -> add a b))
--      unitM (Num 10) `bindM` (\a -> (Num 11) `bindM` (\b -> add a b))
--      unitM (Num (10+11))


test :: Term -> String
test t = showM (interp t [])


-- |
-- >>> test term0
-- "Great Success 42"

term1 = (App 
            (Con 10)
            (Add (Con 10) (Con 11)))

-- |
-- >>> test term1
-- "Error should be function 10"

main = print 42
