import Text.ParserCombinators.ReadP
import Data.Ratio

data Node = Num Integer | Sum Node Node | Mult Node Node | Div Node Node | Power Node Node 
            | Subt Node Node deriving Show  


posNum :: ReadP Node
posNum = do s <- many1 $ choice (map char "0123456789")
            return $ Num $ read s

negNum :: ReadP Node
negNum = do char '-'
            (Num n) <- posNum
            return $ Num (n*(-1))

num :: ReadP Node
num = posNum +++ negNum

-- |
-- >>> readP_to_S num "33"
-- [(Num 3,"3"),(Num 33,"")]
-- >>> readP_to_S num "-33"
-- [(Num (-3),"3"),(Num (-33),"")]

brackets :: ReadP b -> ReadP b
brackets p = do char '('
                val <- p
                char ')'
                return val

atom = num +++ brackets tree

tree = sumSubtTree +++ divMultTree +++ expTree +++ atom

expTree = do a <- atom
             char '^'
             b <- atom +++ expTree
             return $ Power a b

-- |
-- >>> readP_to_S tree "33"
-- [(Num 3,"3"),(Num 33,"")]
-- >>> readP_to_S tree "3^5"
-- ...(Power (Num 3) (Num 5),"")...
-- >>> readP_to_S tree "3^5^4"
-- ...(Power (Num 3) (Power (Num 5) (Num 4)),"")...

-- |
-- >>> readP_to_S tree "(3^5)^4"
-- ...(Power (Power (Num 3) (Num 5)) (Num 4),"")...

-- |
-- >>> readP_to_S tree "(3^5)^(-4)"
-- ...(Power (Power (Num 3) (Num 5)) (Num (-4)),"")...

divMultTree :: ReadP Node
divMultTree = do a <- atom +++ expTree
                 ope <- choice $ map char "*/"
                 b <- atom +++ expTree +++ divMultTree
                 if ope == '*' then return $ Mult a b else return $ Div a b


-- |
-- >>> readP_to_S tree "2*3"
-- ...(Mult (Num 2) (Num 3),"")]
-- >>> readP_to_S tree "2*3^4"
-- ...(Mult (Num 2) (Power (Num 3) (Num 4)),"")]

-- |
-- >>> last $ readP_to_S tree "2^1/3^4"
-- (Div (Power (Num 2) (Num 1)) (Power (Num 3) (Num 4)),"")
-- >>> last $ readP_to_S tree "(2^1)/3^4"
-- (Div (Power (Num 2) (Num 1)) (Power (Num 3) (Num 4)),"")

sumSubtTree = do a <- atom +++ divMultTree +++ expTree
                 ope <- choice $ map char "+-"
                 b <- atom +++ sumSubtTree +++ expTree +++ divMultTree
                 if ope == '+' then return $ Sum a b else return $ Subt a b

-- |
-- >>> last $ readP_to_S tree "2^1/3^4+5"
-- (Sum (Div (Power (Num 2) (Num 1)) (Power (Num 3) (Num 4))) (Num 5),"")
-- >>> last $ readP_to_S tree "2^1/3^4-5"
-- (Subt (Div (Power (Num 2) (Num 1)) (Power (Num 3) (Num 4))) (Num 5),"")
-- >>> last $ readP_to_S tree "2^1/3^4+(-5)"
-- (Sum (Div (Power (Num 2) (Num 1)) (Power (Num 3) (Num 4))) (Num (-5)),"")

treeFromString = fst . last . (readP_to_S tree) . noSpaces

eval :: Node -> Integer
eval (Num a)     = a
eval (Sum a b)   = (+) (eval a) (eval b)
eval (Power a b) = (^) (eval a) (eval b)
eval (Subt a b)  = (-) (eval a) (eval b)
eval (Mult a b)  = (*) (eval a) (eval b)
eval (Div a b)   = takeMod $ (*) (eval a) (fastPow (eval b) (prime-2) prime)
-- eval (Div a b)   = (/) (eval a) (eval b)

prime = (10^9+7)

takeMod :: Integer -> Integer
-- takeMod n_old = if n >= 0 then n else n + prime
--             where n = rem n_old prime
takeMod = (`mod` prime)

evalMod :: Node -> Integer
evalMod = takeMod . eval
          

-- copied, maybe understand later
fastPow :: Integer -> Integer -> Integer -> Integer
fastPow base 0 m = 1
fastPow base 1 m = mod base m
fastPow base pow m | even pow  = mod ((fastPow base (div pow 2) m) ^ 2) m
                   | odd  pow  = mod ((fastPow base (div (pow-1) 2) m) ^ 2 * base) m
                   | otherwise = error "wow"

-- |
-- >>> evalMod $ treeFromString "22*79-21"
-- 1717
-- >>> evalMod $ treeFromString "4/-2/2+8"
-- 4
-- >>> evalMod $ treeFromString "55+3-45*33-25"
-- 999998605
-- >>> evalMod $ treeFromString "4/-2/(2+8)"
-- 999999987

noSpaces :: String -> String
noSpaces = filter (/= ' ')

s :: String
s = "65*91+87/97+(2/1)-(64*(15+20)/32-(55-37+44*26-20-(18/7*10*36/(20/(15+3/58)+21/85)+91*80+43)-50+94-62+70)+90*26-84/69)-3*82*43+(18+14*62*44-65*54*92*22)-100*39+23*11*(40*(91+35)-(65+50+(32+86+68)*12/83-50*(85+51*(13-(48/51/18)/9/98*68/(8+(90-60)/65+65+1+31+49+49+37)/98+85+42-65/14)+81+100/41)*54)*14*51)-49+68/16/53-83-78/27/10"

solve = evalMod . treeFromString


-- >>> treeFromString s
-- Sum (Mult (Num 65) (Num 91)) (Sum (Div (Num 87) (Num 97)) (Subt (Div (Num 2) (Num 1)) (Subt (Subt (Mult (Num 64) (Div (Sum (Num 15) (Num 20)) (Num 32))) (Sum (Subt (Num 55) (Sum (Num 37) (Subt (Mult (Num 44) (Num 26)) (Subt (Num 20) (Subt (Sum (Div (Num 18) (Mult (Num 7) (Mult (Num 10) (Div (Num 36) (Sum (Div (Num 20) (Sum (Num 15) (Div (Num 3) (Num 58)))) (Div (Num 21) (Num 85))))))) (Sum (Mult (Num 91) (Num 80)) (Num 43))) (Sum (Num 50) (Subt (Num 94) (Sum (Num 62) (Num 70))))))))) (Subt (Mult (Num 90) (Num 26)) (Div (Num 84) (Num 69))))) (Sum (Mult (Num 3) (Mult (Num 82) (Num 43))) (Subt (Sum (Num 18) (Subt (Mult (Num 14) (Mult (Num 62) (Num 44))) (Mult (Num 65) (Mult (Num 54) (Mult (Num 92) (Num 22)))))) (Sum (Mult (Num 100) (Num 39)) (Subt (Mult (Num 23) (Mult (Num 11) (Subt (Mult (Num 40) (Sum (Num 91) (Num 35))) (Mult (Sum (Num 65) (Sum (Num 50) (Subt (Mult (Sum (Num 32) (Sum (Num 86) (Num 68))) (Div (Num 12) (Num 83))) (Mult (Num 50) (Mult (Sum (Num 85) (Sum (Mult (Num 51) (Subt (Num 13) (Sum (Div (Div (Num 48) (Div (Num 51) (Num 18))) (Div (Num 9) (Mult (Num 98) (Div (Num 68) (Div (Sum (Num 8) (Sum (Div (Subt (Num 90) (Num 60)) (Num 65)) (Sum (Num 65) (Sum (Num 1) (Sum (Num 31) (Sum (Num 49) (Sum (Num 49) (Num 37)))))))) (Num 98)))))) (Sum (Num 85) (Subt (Num 42) (Div (Num 65) (Num 14))))))) (Sum (Num 81) (Div (Num 100) (Num 41))))) (Num 54)))))) (Mult (Num 14) (Num 51)))))) (Sum (Num 49) (Subt (Div (Num 68) (Div (Num 16) (Num 53))) (Subt (Num 83) (Div (Num 78) (Div (Num 27) (Num 10)))))))))))))

s_tree = Sum (Mult (Num 65) (Num 91)) (Sum (Div (Num 87) (Num 97)) (Subt (Div (Num 2) (Num 1)) (Subt (Subt (Mult (Num 64) (Div (Sum (Num 15) (Num 20)) (Num 32))) (Sum (Subt (Num 55) (Sum (Num 37) (Subt (Mult (Num 44) (Num 26)) (Subt (Num 20) (Subt (Sum (Div (Num 18) (Mult (Num 7) (Mult (Num 10) (Div (Num 36) (Sum (Div (Num 20) (Sum (Num 15) (Div (Num 3) (Num 58)))) (Div (Num 21) (Num 85))))))) (Sum (Mult (Num 91) (Num 80)) (Num 43))) (Sum (Num 50) (Subt (Num 94) (Sum (Num 62) (Num 70))))))))) (Subt (Mult (Num 90) (Num 26)) (Div (Num 84) (Num 69))))) (Sum (Mult (Num 3) (Mult (Num 82) (Num 43))) (Subt (Sum (Num 18) (Subt (Mult (Num 14) (Mult (Num 62) (Num 44))) (Mult (Num 65) (Mult (Num 54) (Mult (Num 92) (Num 22)))))) (Sum (Mult (Num 100) (Num 39)) (Subt (Mult (Num 23) (Mult (Num 11) (Subt (Mult (Num 40) (Sum (Num 91) (Num 35))) (Mult (Sum (Num 65) (Sum (Num 50) (Subt (Mult (Sum (Num 32) (Sum (Num 86) (Num 68))) (Div (Num 12) (Num 83))) (Mult (Num 50) (Mult (Sum (Num 85) (Sum (Mult (Num 51) (Subt (Num 13) (Sum (Div (Div (Num 48) (Div (Num 51) (Num 18))) (Div (Num 9) (Mult (Num 98) (Div (Num 68) (Div (Sum (Num 8) (Sum (Div (Subt (Num 90) (Num 60)) (Num 65)) (Sum (Num 65) (Sum (Num 1) (Sum (Num 31) (Sum (Num 49) (Sum (Num 49) (Num 37)))))))) (Num 98)))))) (Sum (Num 85) (Subt (Num 42) (Div (Num 65) (Num 14))))))) (Sum (Num 81) (Div (Num 100) (Num 41))))) (Num 54)))))) (Mult (Num 14) (Num 51)))))) (Sum (Num 49) (Subt (Div (Num 68) (Div (Num 16) (Num 53))) (Subt (Num 83) (Div (Num 78) (Div (Num 27) (Num 10)))))))))))))


-- | funciona!
-- >>> tree = Sum (Mult (Num 65) (Num 91)) (Sum (Div (Num 87) (Num 97)) (Subt (Div (Num 2) (Num 1)) (Subt (Subt (Mult (Num 64) (Div (Sum (Num 15) (Num 20)) (Num 32))) (Sum (Subt (Num 55) (Sum (Num 37) (Subt (Mult (Num 44) (Num 26)) (Subt (Num 20) (Subt (Sum (Div (Num 18) (Mult (Num 7) (Mult (Num 10) (Div (Num 36) (Sum (Div (Num 20) (Sum (Num 15) (Div (Num 3) (Num 58)))) (Div (Num 21) (Num 85))))))) (Sum (Mult (Num 91) (Num 80)) (Num 43))) (Sum (Num 50) (Subt (Num 94) (Sum (Num 62) (Num 70))))))))) (Subt (Mult (Num 90) (Num 26)) (Div (Num 84) (Num 69))))) (Sum (Mult (Num 3) (Mult (Num 82) (Num 43))) (Subt (Sum (Num 18) (Subt (Mult (Num 14) (Mult (Num 62) (Num 44))) (Mult (Num 65) (Mult (Num 54) (Mult (Num 92) (Num 22)))))) (Sum (Mult (Num 100) (Num 39)) (Subt (Mult (Num 23) (Mult (Num 11) (Subt (Mult (Num 40) (Sum (Num 91) (Num 35))) (Mult (Sum (Num 65) (Sum (Num 50) (Subt (Mult (Sum (Num 32) (Sum (Num 86) (Num 68))) (Div (Num 12) (Num 83))) (Mult (Num 50) (Mult (Sum (Num 85) (Sum (Mult (Num 51) (Subt (Num 13) (Sum (Div (Div (Num 48) (Div (Num 51) (Num 18))) (Div (Num 9) (Mult (Num 98) (Div (Num 68) (Div (Sum (Num 8) (Sum (Div (Subt (Num 90) (Num 60)) (Num 65)) (Sum (Num 65) (Sum (Num 1) (Sum (Num 31) (Sum (Num 49) (Sum (Num 49) (Num 37)))))))) (Num 98)))))) (Sum (Num 85) (Subt (Num 42) (Div (Num 65) (Num 14))))))) (Sum (Num 81) (Div (Num 100) (Num 41))))) (Num 54)))))) (Mult (Num 14) (Num 51)))))) (Sum (Num 49) (Subt (Div (Num 68) (Div (Num 16) (Num 53))) (Subt (Num 83) (Div (Num 78) (Div (Num 27) (Num 10)))))))))))))
-- >>> evalMod tree
-- 472995917

main :: IO ()
main = interact (show . evalMod . treeFromString)
