{-# OPTIONS_GHC -Wno-incomplete-patterns #-}
import Data.List

-- There are as many red balls as green balls.
-- There are as many yellow balls as blue balls.
-- Difference between the number of red balls and green balls in every prefix of the sequence is at most 1.
-- Difference between the number of yellow balls and blue balls in every prefix of the sequence is at most 1.

data Color = G | R | Y | B deriving (Eq,Show,Read)

goodBiColor list = evenLength && notTooMuch list
    where evenLength   = even (length list)
        --   alternating :: Bool
        --   alternating  = and $ zipWith (/=) list (tail list)
          notTooMuch (a:b:list) 
              | a == b = False
              | a /= b = notTooMuch list
          notTooMuch [] = True
          
goodList list  = goodBiColor l1 && goodBiColor l2
    where (l1,l2) = partition xmasColors list
          xmasColors R = True
          xmasColors G = True
          xmasColors _     = False


colors :: String -> [Color]
colors ('R':cs) = R:(colors cs)
colors ('B':cs) = B:(colors cs)
colors ('Y':cs) = Y:(colors cs)
colors ('G':cs) = G:(colors cs)
colors []       = []

main = do
    _ <- getLine
    problems <- lines <$> getContents
    let answers = map (goodList.colors) problems
    (putStr . unlines) (map show answers)