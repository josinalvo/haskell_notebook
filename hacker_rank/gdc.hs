-- small version control test

gcd' :: Integral a => a -> a -> a
gcd' 0 a = a
gcd' a 0 = a
gcd' n m = gcd' small next
        where big   = max n m
              small = min n m
              next  = big `rem` small