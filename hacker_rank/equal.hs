import Control.Monad

-- custo pra levar um cara qq no menor, dada a diferenca
equalize 1 = 1
equalize 2 = 1
equalize 3 = 2
equalize 4 = 2
equalize 0 = 0
equalize difference = (difference `div` 5) + equalize (difference `mod` 5)


type Target = Int
type Cost = Int

f :: Target -> [Int] -> Cost
f target elems = sum $ map equalize newElems
    where newElems = map (\l -> l - target) elems

solve :: [Int] -> Cost
solve allPeople = minimum solutions
    where naiveTarget = minimum allPeople
          targets     = map (naiveTarget - ) [0,1,2]--[0..4]
          solutions   = map (`f` allPeople) targets

-- |
-- >>> solve [2,2,3,7]
-- 2
-- >>> solve [1,5,5]
-- 3
-- >>> solve [10,7,12]
-- 3

runTest = do
    _      <- getLine
    people <- (map read . words) <$> getLine
    print $ solve people

mainSeno = do
    n <- read <$> getLine
    forM_ [1..n] (\_ -> runTest)

evens (x1:x2:list) = x2:(evens list)
evens [x]          = error "ai carai"
evens []           = []

mainMendes = do
    _            <- getLine
    problemLines <- lines <$> getContents
    let problems = map (map read . words) $ evens problemLines
        answers  = map solve problems
    putStr $ unlines (map show answers)


main = mainMendes