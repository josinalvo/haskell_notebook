
sols n power =  if length (waster $ 2^27) /= 0 then aux n power 33
                                               else 1000

aux n power try 
    | n < 0     = 0
    | n == 0    = 1
    | try == 0  = 0
    | otherwise = aux (n-subtract) power (try-1) + aux n power (try-1)
    where subtract = try^power

waster :: Int -> [Int]
waster n = filter (/= n) list
    where list = [1..n]

main = do
    n <- read <$> getLine
    p <- read <$> getLine
    print $ sols n p
                           
main2 = print $ sols 800 2