{-# LANGUAGE TupleSections #-}
import Control.Monad

--(Line, Column)
-- Fill column by column
queens :: Int -> Int
queens n = aux []
  where
    nextPos list = map (,nextColum) $ filter (not . ( `elem` occupied list nextColum)) [1 .. n]
      where
        nextColum = length list
    aux list =
      if length list == n
        then 1
        else sum $ (map aux) (map (: list) (nextPos list))
    occupied :: [(Int, Int)] -> Int -> [Int]
    occupied list nextColum = usedLines list ++ usedDiags list nextColum ++ usedHorses list
    usedLines list = map fst list
    -- usedDiags list col = map fst [(lin, col) | lin <- [1 .. n], (l2, c2) <- list, abs (lin - l2) == abs (col - c2)]
    usedDiags list col = do
                      (l2,c2) <- list
                      let offset = col - c2
                      line  <- [l2 + offset,l2 - offset]
                      guard (inRange line)
                      [line]
    usedHorses ((last_l,_):(scnd_l,_):_) = filter inRange possibilities
        where possibilities = [last_l-2,last_l+2,scnd_l+1,scnd_l-1]
    usedHorses ((last_l,_):_) = filter inRange possibilities
        where possibilities = [last_l-2,last_l+2]
    usedHorses [] = []
    inRange line = line >= 1 && line <= n

main = interact (show . queens . read)