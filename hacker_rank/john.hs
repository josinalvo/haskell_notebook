import Data.Foldable
--                    __
-- 8         __      |  |
-- 7        |  |     |  |
-- 6      __|  |     |  |
-- 5     |  |  |__   |  |
-- 4     |  |  |  |  |  |
-- 3   __|  |  |  |  |  |
-- 2  |  |  |  |  |__|  |
-- 1  |__|__|__|__|__|__|
--     h1 h2 h3 h4 h5 h6
-- 2 5 7 4 1 8

data Possibility = Possibility {start:: Int, height:: Int} deriving (Show)

data Mem = Mem {stack::[Possibility], areaAcc:: Int} deriving (Show)

updateMem :: Mem -> Possibility -> Mem
updateMem mem poss@(Possibility end _) = aux mem poss where
    aux mem@(Mem stack@(top:rest) areaAcc) poss@(Possibility newStart newHeight) = 
        case (compare newHeight heightTop) of
            EQ -> Mem stack areaAcc
            GT -> Mem (poss:stack) areaAcc
            LT -> aux (Mem rest ltArea) (Possibility startTop newHeight)
            where startTop  = start  top
                  heightTop = height top
                  ltArea    = max areaAcc $ (end-startTop)*heightTop
    aux mem@(Mem [] areaAcc) poss@(Possibility _ newHeight) = Mem [(Possibility 0 newHeight)] areaAcc

heights2pos :: [Int] -> [Possibility]
heights2pos list = map tuple2possib (zip [0..] list) ++ [Possibility {start = (length list), height = 0}]
    where tuple2possib (a,b) = Possibility {start = a, height = b}

solve :: [Int] -> Int
solve list = areaAcc banana
    where posList = heights2pos list
          banana  = foldl' updateMem (Mem [] 0) posList
          
-- |
-- >>> solve [2,5,7,4,1,8]
-- 12
-- >>> solve [1..3]
-- 4
-- >>> solve [3,2,1]
-- 4


main = do
    getLine
    heights <- getLine
    print $ solve . fmap read . words $ heights
