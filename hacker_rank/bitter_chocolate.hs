{-# OPTIONS_GHC -Wno-incomplete-patterns #-}

import Data.Map (fromList, Map, (!))
data Chocolate = Chocolate Int Int Int deriving (Eq, Ord, Show)

data Move = Move {row :: Int, col :: Int}

data Result = Win | Lose deriving (Eq)

instance Show Result where
    show Win = "WIN"
    show Lose = "LOSE"

reduceTo :: Ord a => a -> a -> a
reduceTo a b = min a b

eat choc@(Chocolate a b c) move@Move{row = 3, col=col} = Chocolate a b $ reduceTo col c
eat choc@(Chocolate a b c) move@Move{row = 2, col=col} = Chocolate a (reduceTo b col) $ reduceTo col c
eat choc@(Chocolate a b c) move@Move{row = 1, col=col} = Chocolate (reduceTo a col) (reduceTo b col) (reduceTo c col)


range :: [Int]
-- range = [0..25]
range = [0..25]
allChocs :: [Chocolate]
allChocs = [Chocolate a b c | a <- range, a >= 1, b <- range, a >= b, c <- range,  b >= c]

reachFrom :: Chocolate -> [Chocolate]
reachFrom choc = filter (not . (`elem` [choc,zeroChoc])) [eat choc (Move a b)  | a <- [1,2,3], b <- range]
    where zeroChoc = Chocolate 0 0 0

results :: Map Chocolate Result
results = fromList $ (Chocolate 1 0 0, Lose):[(choc, calculate choc) | choc <- allChocs]

calculate :: Chocolate -> Result
calculate choc = if Lose `elem` map (results !) (reachFrom choc)
                 then Win else Lose

solve :: String -> Result
solve s = results ! Chocolate n1 n2 n3
    where [n1,n2,n3] = map read (words s)


-- main = do 
--     getLine
--     problemsStrings <- lines <$> getContents
--     sequence $ map (putStrLn . show .solve) problemsStrings

main = do 
    getLine
    a <- sequence . map (putStrLn . show .solve) . lines <$> getContents
    a
    