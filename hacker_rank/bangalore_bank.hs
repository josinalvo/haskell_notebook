import Data.Foldable
type Position = Int

data Current = Current {pressed :: Position, costs :: [Int]}

distance :: (Position,Position) -> (Position,Position) -> Int
distance (a,b) (c,d) = min (abs (a-c) + abs (b-d)) (abs (a-d) + abs(b-c))

-- |
-- >>> distance (1,2) (9,2)
-- 8
-- >>> distance (1,9) (2,7)
-- 3

step :: Int -> Current -> Current
step nextPress c1@(Current pressed curCosts) = Current nextPress newCosts
    where oldGuys           = [(pressed,c)   | c <- [1..10]]
          newGuys           = [(nextPress,c) | c <- [1..10]]
          cost4newguy (a,b) = let moves = map (distance (a,b)) oldGuys
                                  in minimum (zipWith (+) moves curCosts)
          newCosts          = map cost4newguy newGuys

starter :: Position -> Current
starter a = Current a $ replicate 10 0

solve :: String -> Int
solve string = minimum lastCosts + length cleanString
    where nums                        = map (repl . read . (:[])) cleanString
          repl 0 = 10
          repl x = x
          (Current pressed lastCosts) = foldl' (flip step) (starter $ head nums) (tail nums)
          cleanString                 = filter (/= ' ') string

-- |
-- >>> solve "1 2"
-- 2
-- >>> solve "1 0 3"
-- 5
-- >>> solve "1 0 1 0 1 0 1"
-- 7
-- >>> solve "3 3"
-- 2
-- >>> solve "0 0"
-- 2
-- >>> solve "0"
-- 1

main = do
         getLine
         query <- getLine
         print $ solve query
