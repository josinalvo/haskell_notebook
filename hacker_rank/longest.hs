-- Enter your code here. Read input from STDIN. Print output to STDOUT

longest p (x:xs) (y:ys) = if x == y
                          then longest (x:p) xs ys
                          else (reverse p,(x:xs),(y:ys))
                          
longest p ""     y      = (reverse p,"",y)

longest p x     ""      = (reverse p,x,"")

ansString string = do putStr $ (show $ length string) ++ " "
                      putStrLn string

main = do
    x <- getLine
    y <- getLine
    let (p,xf,yf) = longest "" x y
    ansString p
    ansString xf
    ansString yf