data Tree = Leaf {val :: Int, start :: Int, finish :: Int} | 
            Internal {left:: Tree, right :: Tree, val :: Int, start :: Int, finish :: Int}
            deriving Show

-- VERIFICAR se nao ficou quadratico
mkTree :: [Int] -> Tree
mkTree list = aux list (0,length list - 1)
    where aux :: [Int] -> (Int,Int) -> Tree
          aux list (start,finish)
              | start == finish = Leaf (head list) start start
              | otherwise = Internal leftTree rightTree minVal start finish
                where 
                    size = finish - start + 1 -- 10 15: 10,11,12,13,14,15
                    firstSize = (size `div` 2)
                    -- scndSize  = size - firstSize
                    (firstList,scndList) = splitAt firstSize list
                    leftTree = aux firstList (start,start+firstSize-1)
                    rightTree = aux scndList (start+firstSize,finish)
                    minVal  = min (val leftTree) (val rightTree)

tree = mkTree [1,2,3]

-- |
-- >>> tree
-- Internal {left = Leaf {val = 1, start = 0, finish = 0}, right = Internal {left = Leaf {val = 2, start = 1, finish = 1}, right = Leaf {val = 3, start = 2, finish = 2}, val = 2, start = 1, finish = 2}, val = 1, start = 0, finish = 2}

--  Internal {left = Leaf {val = 1, position = 0}, 
--            right = Internal {left = Leaf {val = 2, position = 1}, 
--                              right = Leaf {val = 3, position = 2}, 
--                              val = 2, start = 1, finish = 2}, 
--            val = 1, start = 0, finish = 2}

search :: Tree -> (Int, Int) -> Int
search tree i@(startI,finishI) 
    | start tree == startI && finish tree == finishI = val tree
    | finishI <= finish (left tree)                  = search leftTree i
    | startI  >= start (right tree)                  = search rightTree i
    | otherwise                                      = joinedAns
    where leftTree  = left tree
          rightTree = right tree
          joinedAns = min (search leftTree  (startI, finish leftTree))
                          (search rightTree (start rightTree, finishI))

-- |
-- >>> search tree (0,2)
-- 1
-- >>> search tree (1,2)
-- 2
-- >>> search tree (2,2)
-- 3
-- >>> search tree (0,1)
-- 1

readInt :: String -> Int
readInt = read

main = do
    _         <- getLine
    list      <- (map readInt . words) <$> getLine
    intervals <- (map ((\[a,b]->(a,b)) . map readInt . words)) . lines <$> getContents
    let tree = mkTree list
    mapM_ print $ map (search tree) intervals
              