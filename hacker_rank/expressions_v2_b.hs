import Text.ParserCombinators.ReadP

data NodeString = Exp String | Sum NodeString NodeString | Mult NodeString NodeString | Div NodeString NodeString | Power NodeString NodeString 
                 | Subt NodeString NodeString deriving Show

s :: String
s = "65*91+87/97+(2/1)-(64*(15+20)/32-(55-37+44*26-20-(18/7*10*36/(20/(15+3/58)+21/85)+91*80+43)-50+94-62+70)+90*26-84/69)-3*82*43+(18+14*62*44-65*54*92*22)-100*39+23*11*(40*(91+35)-(65+50+(32+86+68)*12/83-50*(85+51*(13-(48/51/18)/9/98*68/(8+(90-60)/65+65+1+31+49+49+37)/98+85+42-65/14)+81+100/41)*54)*14*51)-49+68/16/53-83-78/27/10"

s2 :: String
s2 = "65*91+87/97+(2/1)"

s3 :: String
s3 = "65*(91+(87/97))+(2/1)"

noParensChar :: ReadP Char
noParensChar = choice $ map char "0123456789+-*/^"

noParens :: ReadP NodeString
noParens = fmap Exp $ many1 noParensChar

firstExp n = do
             noParens
             char '('
             a <- firstExp (n+1)
             char ')'


-- brackets :: ReadP b -> ReadP b
-- brackets p = do char '('
--                 val <- p
--                 char ')'
--                 return val

-- bracketLevel 0 = noParens
-- bracketLevel n = brackets (bracketLevel $ n - 1)

-- |
-- >>> parseSeno s2
-- Sum (Exp "65*91+87/97") (Exp "2/1")
