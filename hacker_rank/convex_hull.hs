{-# OPTIONS_GHC -Wno-incomplete-patterns #-}

import Data.List.Split
import Data.List
import GHC.Exts ( sortWith )
import Control.Monad.State


type Point = (Double,Double)

listOfPairs :: [String] ->  [ Point ]
listOfPairs strings   = readPairs
    where noNumPoints = (tail strings)
          readPairs   = map line2tuple noNumPoints
-- f l | trace ("l " ++ show l) False = undefined
f (x:y:[]) = (x,y)
f (x:[]) = undefined 
f ([]) = undefined 
f list = error "fuck"


line2tuple :: String -> Point
-- line2tuple l | trace ("" ++ show l) False = undefined
line2tuple l = f . (map read) . (splitOn " ") $ l

centerPoints :: [Point] -> [Point]
centerPoints points = map decreaseByCenter points
    where centerX                = sum (map fst points) / fromIntegral (length points)
          centerY                = sum (map snd points) / fromIntegral (length points)
          decreaseByCenter (x,y) = (,) (x - centerX) (y - centerY)



size :: Point -> Double
size (x,y) = sqrt(x^2+y^2)

angP :: Point -> Double 
angP p@(x,y) = if y > 0
               then ang
               else (2*pi - ang)
    where cos = x/size p
          ang = acos cos


sortPoints :: [Point] -> [Point]
sortPoints unsortedPoints = sorted
    where points   = sortWith angP unsortedPoints
          maxX     = last $ sortWith fst unsortedPoints
          sorted   = take (length points + 1) $ dropWhile ((/=) maxX) $ points ++ points

addPoint :: Point -> [Point] -> [Point]
addPoint p_new [] = [p_new]
addPoint p_new [p0] = [p_new,p0]
addPoint p_new all@(p_mid:p_old:ps) 
      | areaOfThree p_old p_mid p_new >= 0 = addPoint p_new (p_old:ps)
      | otherwise                          = p_new:all

addPointS :: Point -> State [Point] () 
addPointS point = state $ \points -> ((), addPoint point points)

hull :: [Point] -> [Point]
hull list = snd $ runState other_state []
      where other_state = sequence $ map addPointS list

a :: [(Double, Double)]
a = [(3,2),(2,5),(4,5)]
-- |
-- >>> solveHull a
-- [(1.0,1.0),(0.0,-2.0),(-1.0,1.0),(1.0,1.0)]

-- >>> centerPoints a
-- [(0.0,-2.0),(-1.0,1.0),(1.0,1.0)]

-- >>> sortPoints $ centerPoints a
-- [(1.0,1.0),(-1.0,1.0),(0.0,-2.0),(1.0,1.0)]

-- >>> addPoint (1,1) []
-- [(1.0,1.0)]
-- >>> addPoint (-1.0,1.0) [(1.0,1.0)]
-- [(-1.0,1.0),(1.0,1.0)]

-- >>> addPoint (0.0,-2.0) [(-1.0,1.0),(1.0,1.0)]
-- [(0.0,-2.0),(-1.0,1.0),(1.0,1.0)]

solveHull :: [Point] -> [Point]
solveHull = hull . sortPoints . centerPoints 

distance :: Point -> Point -> Double
distance p1 p2 = size $ takeAway p1 p2

distances :: [Point] -> Double
distances points = sum $ zipWith distance points (tail points ++ points)


takeAway :: Point -> Point -> Point
takeAway (x1,y1) (x2,y2) = (x1-x2,y1-y2)

-- BEWARE of the order of the points
areaOfThree p1 p_mid p2 = signedArea (p1 `takeAway` p_mid) (p2 `takeAway` p_mid)

signedArea :: Floating a => (a, a) -> (a, a) -> a
signedArea (x1,y1) (x2,y2) = (x1*y2 - x2*y1)/2

-- |
-- >>> signedArea (1,0) (0,-1)
-- -0.5
-- >>> areaOfThree (1,0) (0,0) (0,-1)
-- -0.5

-- |
-- >>> distances $ solveHull [(1,1),(2,5),(3,3),(5,3),(3,2),(2,2)]
-- 12.20079285608123


main = 
     do
     f <- getContents  
     print $ (distances . solveHull . listOfPairs . lines) f
     
     
     
