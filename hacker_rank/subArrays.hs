{-# OPTIONS_GHC -Wno-unrecognised-pragmas #-}
{-# HLINT ignore "Eta reduce" #-}
import Data.Foldable
import Data.Set
-- import Test.DocTest

sumsMod :: Integer -> [Integer] -> [Integer]
sumsMod m list = scanl1 add mods
    where add a b = (a + b) `mod ` m
          mods    = Prelude.map (`mod` m) list

-- |
-- >>> sumsMod 2 [1,2,3]
-- [1,1,0]
-- >>> sumsMod 3 [1,2,3,4,7]
-- [1,0,0,1,2]
-- >>> sumsMod 5 (Prelude.take 10 [1,1..])
-- [1,2,3,4,0,1,2,3,4,0]
-- >>> lookupGT 5 (fromList [3, 5])
-- Nothing





allSets :: [Integer] -> [Set Integer]
allSets list = scanl (flip insert) empty list

solve :: [Integer] -> Integer -> Integer
solve list m = maximum $ zipWith fSeno sets modsOfSum
    where sets           = allSets modsOfSum
          modsOfSum      = sumsMod m list
          fSeno set curr = case lookupGT curr set of
                                 Nothing   -> curr
                                 Just bestPrev -> (m + curr - bestPrev)

solveIO :: IO ()
solveIO = do 
            [_,m] <- Prelude.map read . words <$> getLine
            list  <- Prelude.map read . words <$> getLine
            print $ solve list m

mainAnswer = do
    n <- read <$> getLine
    -- sequence $ Prelude.map (\_ -> solveIO) [1..n]
    mapM_ (\_ -> solveIO) [1..n]

-- mainTest = doctest ["./subArrays.hs"]

main = mainAnswer

-- |
-- >>> solve [1,2,3] 2
-- 1
-- >>> solve [3, 3, 9, 9, 5] 7
-- 6

m = 184803527
list = [412776092,1424268981,1911759957,749241874,137806863,42999171,982906997,135497282,511702306,2084420926,1937477085,1827336328,572660337,1159126506,805750847,1632621730,1100661314,1433925858,1141616125,84353896,939819583,2001100546,1998898815,1548233368,610515435,1585990365,1374344044,760313751,1477171088,356426809,945117277,1889947179,1780695789,709393585,491705404,1918502652,752392755,1474612400,2053999933,1264095061,1411549677,1843993369,943947740,1984210013,855636227,1749698587,1469348095,1956297540,1036140796,463480571]

-- |
-- >>> solve list m
-- 184770427

