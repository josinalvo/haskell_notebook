amount_trees :: [Integer]
amount_trees = map f [0,1..]

f :: Int -> Integer
f 0 = 1
f 1 = 1
f n = sum $ zipWith (*) partial (reverse partial)
    where partial = take n amount_trees

ans n = (amount_trees !! n) `rem` (10^8+7)

-- |
-- >>> ans 4
-- 14
-- >>> ans 3
-- 5
-- >>> ans 6
-- 132
-- >>> ans 100
-- 25666077


main = do 
        getLine
        problems <- fmap lines getContents
        putStr $ unlines $ map (show . ans . read) problems
