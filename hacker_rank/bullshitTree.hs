-- valid bst from hacker hank

import Data.Vector (fromList, (!))

bst []           = True -- ??
bst (pivot:list) = bigValid && recurseValid
    where (small, maybeBig) = span (< pivot) list
          bigValid          = all  (> pivot) maybeBig
          recurseValid      = bst small && bst maybeBig

bstString :: Ord a => [a] -> [Char]
bstString a = if bst a then "YES" else "NO"

line2ints :: String -> [Integer]
line2ints = (map read) . words

list = "cbababa"

-- interstint Positions
iP' []         = []
iP' (_:a:list) = a : iP list

iP :: [a] -> [a]
iP list = map (v ! ) positions
    where positions = [2,4..(length list - 1)]
          v         = fromList list
          

main = interact $ unlines . (map bstString) . (map line2ints) . iP . lines