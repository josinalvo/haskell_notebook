
import Data.List.Split
import Data.List
import GHC.Exts ( sortWith )


type Point = (Float,Float)

listOfPairs :: [String] ->  [ Point ]
listOfPairs strings   = readPairs
    where noNumPoints = (tail strings)
          readPairs   = map line2tuple noNumPoints
-- f l | trace ("l " ++ show l) False = undefined
f (x:y:[]) = (x,y)
f (x:[]) = undefined 
f ([]) = undefined 
f list = error "fuck"


line2tuple :: String -> Point
-- line2tuple l | trace ("" ++ show l) False = undefined
line2tuple l = f . (map read) . (splitOn " ") $ l

centerPoints :: [Point] -> [Point]
centerPoints points = map decreaseByCenter points
    where centerX                = sum (map fst points) / fromIntegral (length points)
          centerY                = sum (map snd points) / fromIntegral (length points)
          decreaseByCenter (x,y) = (,) (x - centerX) (y - centerY)



-- |
-- >>> centerPoints [(1,3),(-3,1)]
-- WAS WAS [(2.0,1.0),(-2.0,-1.0),(2.0,1.0)]
-- WAS NOW [(1.3333334,0.66666675),(-2.6666667,-1.3333333),(1.3333334,0.66666675)]
-- NOW [(2.0,1.0),(-2.0,-1.0)]

size (x,y) = sqrt(x^2+y^2)

angP :: Point -> Float 
angP p@(x,y) = if y > 0
               then ang
               else (2*pi - ang)
    where cos = x/size p
          ang = acos cos

rep ang = (cos ang,sin ang)


signsOfTrios :: [Point] -> [Int]
signsOfTrios unsortedPoints = map toSign areas
    where points   = sortWith angP unsortedPoints
          areas    = zipWith3 areaOfThree points skip1 skip2
          skip1    = tail points ++ points
          skip2    = tail skip1
          toSign n = if n > 0; then 1 else -1

-- allSame :: [Int] -> Bool 
-- allSame l = length (nub l) == 1

allNegative :: [Int] -> Bool 
allNegative l = not $ 1 `elem` l

takeAway (x1,y1) (x2,y2) = (x1-x2,y1-y2)

areaOfThree p1 p_mid p2 = signedArea (p1 `takeAway` p_mid) (p2 `takeAway` p_mid)

signedArea :: Floating a => (a, a) -> (a, a) -> a
signedArea (x1,y1) (x2,y2) = (x1*y2 - x2*y1)/2


main = 
     do
     f <- getContents  
     if (allNegative . signsOfTrios . centerPoints . listOfPairs . lines) f
     then putStrLn "NO"
     else putStrLn "YES"

     
     
