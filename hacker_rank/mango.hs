{-# OPTIONS_GHC -Wno-unrecognised-pragmas #-}
{-# HLINT ignore "Redundant bracket" #-}
import Data.List
data Friend = Friend {happiness :: Int, appetite :: Int}

cost :: [Friend] -> FriendAmount -> MangoAmount
cost fs size = sum $ take size' costs
    where costs           = sort $ map costForFriend fs
          costForFriend f = toInteger $ (appetite f) + (happiness f) * (size'-1)
          size'           = fromIntegral size

mangoes = 200
s1 = "2 5 3 2 4"
s2 = "30 40 10 20 30"
-- |
-- >>> cost (readFriends s1 s2) 4
-- 123


type MangoAmount  = Integer
type FriendAmount = Integer

solve :: [Friend] -> MangoAmount -> FriendAmount
solve fs nMangoes = aux 0 (fromIntegral $ length fs)
    where aux start finish 
            | start == finish = start
            | otherwise = aux newStart newFinish
            where midPoint = fromIntegral $ (start + finish) `div` 2
                  midVal   = (cost fs midPoint :: MangoAmount)
                  (newStart,newFinish) = if midVal > nMangoes 
                                           then (start, midVal -1)
                                           else (midVal, finish)


readFriends :: String -> String -> [Friend]
readFriends stringApp stringHap = zipWith Friend ((map read) $ words stringApp) 
                                                 ((map read) $ words stringHap)




main = do
    [_,mangoes] <- ((map read) . words ) <$> getLine
    app         <- getLine
    hap         <- getLine
    print $ solve (readFriends app hap) mangoes
