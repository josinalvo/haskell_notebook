{-# OPTIONS_GHC -Wno-incomplete-patterns #-}
import Data.Sequence
import Control.Monad.State
import Data.List hiding (delete)
import Data.Maybe

newtype NodeL = NodeL (Seq (Int,NodeL)) deriving Show

data ZNode = ZNode {curr :: NodeL, stack :: [Piece], mpos :: Maybe Int} deriving Show

data Piece = Piece {nodeL :: NodeL, pos :: Int} deriving Show

emptyNL = NodeL empty
-- emptyZ  = ZNode emptyNL []
startZ  = ZNode (NodeL $ singleton (0,emptyNL)) [] (Just 0)

changeValue :: ZNode -> Int -> ZNode
changeValue zipp@ZNode{mpos = Nothing} newVal = zipp{curr = NodeL $ singleton (newVal,emptyNL), 
                                                      mpos  = Just 0}
changeValue zipp@ZNode{curr=(NodeL oldSeq), mpos = (Just pos)} newVal = zipp{curr=NodeL newSeq}
            where newSeq   = update pos replacement oldSeq  
                  replaceInPos (oldVal, oldChildSeq) = (newVal, oldChildSeq)
                  replacement = replaceInPos $ index oldSeq pos


visitLeft :: ZNode -> ZNode
visitLeft zip@ZNode{mpos=(Just 0)}   = error "nothing to the left"
visitLeft zip@ZNode{mpos=(Just pos)} = zip{mpos = Just (pos-1)}
visitLeft zip@ZNode{mpos=Nothing}    = error "left of nothing"

visitRight :: ZNode -> ZNode
visitRight zip@ZNode{mpos=(Just pos)} = zip{mpos = Just (pos+1)}
visitRight zip@ZNode{mpos=Nothing}    = error "right of nothing"
--TOMAYBE: detect wrong right

visitParent :: ZNode -> ZNode
visitParent ZNode{curr=childSeq,stack=(top:rest)} = ZNode{curr=(NodeL newSeq), stack=rest, mpos=Just (pos top) }
    where  newSeq       = update (pos top) replacement oldSeq :: Seq (Int,NodeL)
           oldSeq :: Seq (Int,NodeL)
           NodeL oldSeq = nodeL top 
           replaceInPos :: (Int,NodeL) -> (Int,NodeL)
           replaceInPos (oldVal, oldChildSeq) = (oldVal, childSeq)
           replacement :: (Int,NodeL)
           replacement = replaceInPos $ index oldSeq (pos top) 
visitParent ZNode{curr=curr,stack=[]} = error "went up on empty parent"

visitChild :: ZNode -> Int -> ZNode
visitChild zip@ZNode{curr= c@(NodeL oldSeq), stack=oldStack, mpos = (Just oldPos)} target = ZNode newCurr (newTopPiece:oldStack) newMPos
    where newTopPiece = Piece c oldPos
          newMPos     = Just target
          newCurr     = snd $ index oldSeq oldPos
visitChild zip@ZNode{mpos = Nothing} target = error "going down on undefined node"

-- new = (a.b.c := 12)
-- weird idea

insertLeft :: ZNode -> Int -> ZNode
insertLeft zip@ZNode{mpos=(Just pos), curr= NodeL oldSeq} val = 
                                             zip{curr=NodeL newSeq, mpos = Just $ pos+1}
    where newSeq = insertAt pos (val,emptyNL) oldSeq
insertLeft zip@ZNode{mpos=Nothing, curr= NodeL oldSeq} _ = error "insert on left of empty"

insertRight :: ZNode -> Int -> ZNode
insertRight zip@ZNode{mpos=(Just pos), curr= NodeL oldSeq} val = zip{curr=NodeL newSeq}
    where newSeq = insertAt (pos+1) (val,emptyNL) oldSeq
insertRight zip@ZNode{mpos=Nothing, curr= NodeL oldSeq} _ = error "insert on right of empty"

insertChild :: ZNode -> Int -> ZNode
insertChild zip@ZNode{mpos=(Just pos), curr= NodeL oldCurr} val = zip{curr=NodeL newCurr}
    where c :: NodeL
          (num,c) = index oldCurr pos
          oldChild :: Seq (Int,NodeL)
          NodeL oldChild = c
          newChildSeq = insertAt 0 (val,emptyNL) oldChild
          newCurr = update pos (num,NodeL newChildSeq) oldCurr

insertChild zip@ZNode{mpos=Nothing, curr= NodeL oldSeq} _ = error "insert on child of empty"

delete :: ZNode -> ZNode
delete z@ZNode {curr=(NodeL curr), mpos=(Just pos)} = visitParent zipperWithRemoved
    where withRemoved       = deleteAt pos curr
          zipperWithRemoved = z{curr=NodeL withRemoved}

printZ :: ZNode -> Int
printZ ZNode {curr=(NodeL curr), mpos=(Just pos)} = fst $ index curr pos
    


sVisitParent :: State ZNode (Maybe Int)
sDelete :: State ZNode (Maybe Int)
sVisitRight :: State ZNode (Maybe Int)
sVisitLeft :: State ZNode (Maybe Int)
[sDelete, sVisitParent, sVisitRight, sVisitLeft] = map f [delete, visitParent, visitRight, visitLeft]
    where  f :: (ZNode -> ZNode) -> State ZNode (Maybe Int)
           f func = state (\z -> (Nothing, func z))

sVisitChild :: Int ->  State ZNode (Maybe Int)
sVisitChild n = state f
    where f = \z -> (Nothing, visitChild z n)


modifySeno zipperChanger n = state f 
    where f = \z -> (Nothing, zipperChanger z n)

[sInsertLeft, sInsertRight, sInsertChild, sChange] = map modifySeno [insertLeft, insertRight, insertChild, changeValue]
-- modify2 zipperChanger = f
--     where f = \n -> state $ \z -> (Nothing, zipperChanger z n)

sPrint :: State ZNode (Maybe Int)
sPrint = state $ \z -> (Just $ printZ z , z)

translate :: String -> State ZNode (Maybe Int)
translate "delete"       = sDelete
translate "visit left"   = sVisitLeft
translate "visit right"  = sVisitRight
translate "visit parent" = sVisitParent
translate "print"        = sPrint
translate str | "change"       `isPrefixOf` str = sChange      (getNum str)
              | "visit child"  `isPrefixOf` str = sVisitChild  (getNum str-1)
              | "insert left"  `isPrefixOf` str = sInsertLeft  (getNum str)
              | "insert right" `isPrefixOf` str = sInsertRight (getNum str)
              | "insert child" `isPrefixOf` str = sInsertChild (getNum str)

getNum :: String -> Int
getNum = read .last . words

showBowels :: State ZNode (Maybe Int) -> State ZNode (Maybe Int, ZNode)
showBowels s = do
    val <- s
    mem <- get
    return (val,mem)
                

main = do
    _ <- getLine
    stringCommands       <- lines <$> getContents
    let stateCommands    = map translate stringCommands
    -- let stateCommands    = map (showBowels . translate) stringCommands
    let oneStateSolution = sequence stateCommands
    let ans              = runState oneStateSolution startZ
    -- (putStrLn . unlines . map ((++ "\n") . show) ) (fst ans)
    (putStrLn . unlines . map show . relevant ) (fst ans)

relevant :: [Maybe a] -> [a]
relevant = catMaybes 