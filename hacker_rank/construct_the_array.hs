
-- 1 _ _ x
-- 1 _ x'

-- f(k=3)(n=4,x/=1) =   f(k=3)(n'=3,x'=1) 
--                    + f(k=3)(n'=3,x'/=1)*(k-2)
                                                   
-- f(k=3)(n=4, x=1) = f(k=3)(n'=3,x'/=1)*(k-1)

-- table : 10^5*10^5
--              ----> maybe 2
-- ops   : t * 10^5

--            n=3   n=4
--  1          v    v2
--  not 1      v1   v3

-- https://www.hackerrank.com/challenges/construct-the-array/problem

data Answer = Answer {xNotOne :: Integer, xIsOne :: Integer} deriving Show

takeMod :: Integer
takeMod = 10^9+7

step :: Integer -> Answer -> Answer
step k prevAns = Answer (xNotOne' `mod` takeMod) (xIsOne' `mod` takeMod)
    where
        xIsOne'  = (xNotOne prevAns) * (k-1) 
        xNotOne' = (xNotOne prevAns) * (k-2) + (xIsOne prevAns)

initialAns ::  Answer
initialAns = Answer 1 0

finalAns :: Int -> Integer -> Answer
finalAns n k = answers !! howManySteps
    where answers      = iterate (step k) initialAns
          howManySteps = (n-2)

solve :: Int -> Integer -> Int -> Integer
solve n k x = if x == 1 then xIsOne' else xNotOne'
    where (Answer xNotOne' xIsOne') = finalAns n k

-- |
--- >>> solve 4 3 2
-- 6

-- 1 2 1 2
-- 1 2 3 2
-- 1 3 1 2

-- |
-- >>> a = initialAns
-- >>> a
-- Answer {xNotOne = 1, xIsOne = 0}
-- >>> b = step 3 a
-- >>> b
-- Answer {xNotOne = 1, xIsOne = 2}
-- >>> step 3 b
-- Answer {xNotOne = 3, xIsOne = 2}




-- 1 2
-- 1 3
-- 1 2 3
-- 1 3 2
-- 1 2 1
-- 1 3 1
-- 1 2 3 1
-- 1 3 2 1

-- 1 2 3 
-- 1 3 2 3
-- 1 2 1 3
-- 1 3 1 3

-- 1 2 3 2
-- 1 3 2 
-- 1 2 1 2
-- 1 3 1 2



main = print 12
