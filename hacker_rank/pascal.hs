

nextpascal l = zipWith (+) ([0] ++ l) (l ++ [0])

allPascal :: Int -> String 
allPascal n = allPascalR n [1]

allPascalR 0 _    = ""
allPascalR n list = showList' list ++ "\n" ++ allPascalR (n-1) (nextpascal list)

showList' [x]    = show x
showList' (a:as) = show a ++ " " ++ showList' as
showList' []     = error "nooo"

-- line n = nTimes n nextpascal

main = do
    n <- getLine 
    putStrLn (allPascal $ read n)
