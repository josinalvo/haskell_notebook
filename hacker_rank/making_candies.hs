import Data.List (words)
import Control.Monad
import System.Environment
import System.IO
import System.IO.Unsafe
import Data.Text hiding(last, takeWhile, map)

media :: Integral a => a -> a -> a
media a b = (a + b) `div` 2

data Classificacao = Cres | Decr | MinPoint deriving Eq

tester1 :: Num a => Integer -> a
tester1 a = vals !! fromIntegral a
    where vals = [99,98,98,98,9,3,3,100]


findPlateau :: (Integer->Integer) -> Integer -> Integer -> Integer -> (Integer,Integer)
findPlateau f start finish m = (pStart,pFinish)
    where -- powersOfTwo      = map (2^) [0..]
          pPotenStarts     = [m,m-1..start]
          pStart           = last $ takeWhile (\n -> f n == f_m) pPotenStarts
          f_m              = f m
          pPotenFinishes   = [m,m+1..finish]
          pFinish          = last $ takeWhile (\n -> f n == f_m) pPotenFinishes

-- |
-- >>> findPlateau tester1 0 7 2         
-- (1,3)
-- >>> findPlateau tester1 0 7 5         
-- (5,6)
-- >>> findPlateau tester1 0 7 4         
-- (4,4)
-- >>> f1 x = (x+2)*(x+2)
-- >>> findPlateau f1 (-10) 10 (-2)
-- (-2,-2)
-- >>> findPlateau f1 (-10) 10 0
-- (0,0)




classifica :: (Integer-> Integer) -> Integer -> Integer -> Integer -> (Classificacao,Integer)
classifica f start finish m 
    | (f_small > f_m) && (f_m > f_big) = (Decr,p2+1)
    | (f_small < f_m) && (f_m < f_big) = (Cres,p1-1)
    | (f_small > f_m) && (f_m < f_big) = (MinPoint,p1)
    | otherwise = error "que pena, a culpa ainda eh do seno"
    where f_m     = f m
          (p1,p2) = findPlateau f start finish m
          f_small = if p1-1 < start then 10^13     else f (p1-1)
          f_big   = if p2+1 > finish then 10^13
                    else f (p2+1)
          

findMin f start finish
    | start     == finish = f start
    | start + 1 == finish = min (f start) (f finish)
    | c == MinPoint       = f new_m
    | c == Cres           = findMin f start new_m
    | c == Decr           = findMin f new_m finish
    | otherwise = error "unimplemented"
    where m         = media start finish
          (c,new_m) = classifica f start finish m -- A \ B \/ C /          




-- |
-- >>> f2 x = x
-- >>> findMin f2 (-100) 10
-- -100
-- >>> f3 x = x*x*x
-- >>> findMin f3 (-100) 10
-- -1000000
-- >>> findMin tester1 3 5
-- 3
-- >>> f1 x = (x+2)*(x+2)
-- >>> findMin f1 (-100) 10
-- 0
-- >>> findMin tester1 0 7
-- 3

buy machines men candy price maxBuy = (newSmaller,newLarger,newCandy,bought)
    where bought     = min (candy `div` price) maxBuy
          smaller    = min machines men
          larger     = max machines men
          diff       = larger - smaller
          caralhinho = (bought - diff) `div` 2
          caralhao   = (bought - diff) - caralhinho
          newSmaller = if smaller + bought <= larger then smaller + bought
                       else larger + caralhinho
          newLarger  = if smaller + bought <= larger then larger
                       else larger + caralhao
          newCandy   = candy - bought * price


days :: Integer -> Integer -> Integer -> Integer -> Integer -> Integer -> Integer
days machines men price currentCandy target buys 
    | currentCandy >= price  && buys > 0  = days newMachines newMen price candyAfterBuy target (buys-bought)
    | currentCandy >= target && buys == 0 = 0
    where (newMachines,newMen,candyAfterBuy,bought) = buy machines men currentCandy price buys
          
days machines men price currentCandy target buys = days4step + recursiveCall
    where speed     = machines*men
          nextGoal  = if buys > 0 then price else target
        --   days   = o menor inteiro tal que days*speed + currentCandy >= (min target buys)
          days4step = ceiling  $ fromInteger (nextGoal - currentCandy) / fromInteger speed
          recursiveCall = days machines men price (currentCandy + speed*days4step) target buys
          
    
test2 = days 3 3 1 0 30

explicit2 :: [Integer]
explicit2 = map test2 [0..30]

-- |
-- >>> explicit2
-- [4,3,3,3,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2]
-- >>> findMin test2 0 10
-- 2
-- >>> findPlateau test2 0 10 5
-- (4,10)

test3 = days 2 1 7 0 50

explicit3 :: [Integer]
explicit3 = map test3 [0..50]

-- |
-- >>> explicit3
-- [25,17,14,13,12,12,12,12,12,12,12,12,12,12,12,12,12,12,13,13,13,13,13,13,13,13,13,13,13,13,13,13,13,13,13,13,13,13,13,13,13,13,13,13,14,14,14,14,14,14,14]







-- >>> findMin test2 0 10
-- 2
-- >>> findPlateau test2 0 10 5
-- (4,10)

minimumPasses m w p n = findMin (days m w p 0 n) 0 n

lstrip = Data.Text.unpack . Data.Text.stripStart . Data.Text.pack
rstrip = Data.Text.unpack . Data.Text.stripEnd . Data.Text.pack

-- main = print 42
main :: IO()
main = do
    stdout <- getEnv "OUTPUT_PATH"
    fptr <- openFile stdout WriteMode

    firstMultipleInputTemp <- getLine
    let firstMultipleInput = Data.List.words $ rstrip firstMultipleInputTemp

    let m = read (firstMultipleInput !! 0) :: Integer
    let w = read (firstMultipleInput !! 1) :: Integer

    let p = read (firstMultipleInput !! 2) :: Integer

    let n = read (firstMultipleInput !! 3) :: Integer

    let result = minimumPasses m w p n

    hPutStrLn fptr $ show result

    hFlush fptr
    hClose fptr
