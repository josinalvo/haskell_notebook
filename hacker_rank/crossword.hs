import Data.Map 
import Data.Maybe
import GHC.Exts (sortWith)
import Data.List ( sort ) 

data Direction = Horizontal | Vertical deriving (Eq,Ord, Show)
data Slot = Slot {line:: Int, column:: Int, direction :: Direction, size :: Int} deriving (Eq, Ord, Show)


possibleWords = ["duck","cello","dark", "dork"]

allSlots = [Slot 3 1 Horizontal 5, 
          Slot 1 1 Vertical 4,
          Slot 2 5 Vertical 4, 
          Slot {line = 1, column = 7, direction=Vertical, Main.size=4}]

-- - x x x x x -    D x x x x x D
-- - x x x - x -    U x x x D x A
-- - - - - - x -    C E L L O x R
-- - x x x - x -    K x x x R x K
-- x x x x - x x    x x x x K x x
-- x x x x x x x    x x x x x x x

wordsBySize :: [String] -> Map Int [String]
wordsBySize words = fromListWith (++) tuples
    where tuples = Prelude.map str2tuple words
          str2tuple s = (length s, [s])

-- >>> wordsBySize possibleWords
-- fromList [(4,["dork","dark","duck"]),(5,["cello"])]

slot2words :: [String] -> [Slot] -> [(Slot,[String])]
slot2words words pos = fmap getListOfSize pos
    where bySize = wordsBySize words
          getListOfSize pos = (pos, bySize ! Main.size pos)

-- >>> slot2words possibleWords allSlots
-- [(Slot {line = 3, column = 1, direction = Horizontal, size = 5},["cello"]),(Slot {line = 1, column = 1, direction = Vertical, size = 4},["dork","dark","duck"]),(Slot {line = 2, column = 5, direction = Vertical, size = 4},["dork","dark","duck"]),(Slot {line = 1, column = 7, direction = Vertical, size = 4},["dork","dark","duck"])]

-- TODO: começar com menos opcoes


allGuesses :: [(Slot,[String])] -> [(Slot,String)]
allGuesses allPossibilities = do
    (slot,words) <- orderedPossibilities -- TODO: does this help?
    word         <- words
    [(slot,word)]
    where orderedPossibilities = sortWith (length . snd) allPossibilities

type Delta = Int



validGuess :: [String] -> [(Slot,String)] -> Bool
validGuess availableWords guess = undefined 
    where guessedWords = fmap snd guess
          correctAmounts = (==) (sort availableWords) (sort guessedWords)


    

backtrack :: [String] -> [Slot] -> Map Slot String
backtrack = undefined 
