postcard (c:cs) = aux c 1 cs
    where aux c n full
           | (full /= "") && c == (head full) = aux c (n+1) (tail full)
           | n == 1  = c: postcard full
           | n >  1  = c: (show n) ++ (postcard full)
postcard ""     = ""



-- | 
-- >>> postcard "aabbbbcdddddeff"
-- "a2b4cd5ef2"

main = interact postcard