{-# OPTIONS_GHC -Wno-unrecognised-pragmas #-}
{-# HLINT ignore "Eta reduce" #-}
import qualified Data.IntMap as I
import Data.List

type Tree = I.IntMap (Int,Int)

readTree :: [(Int,Int)] -> Tree
readTree pairs = I.fromList $ zip [1..] pairs

readPairs :: [String] -> [(Int,Int)]
readPairs = fmap readPair
    where readPair :: String -> (Int,Int)
          readPair = (\[a,b] -> (a,b)) . fmap read . words

type Root  = Int
type Depth = Int
type Flip  = Int

-- TODO: talvez aqui fizemos merda, pq confiamos na lazyness    
swapAndOrder'' :: Tree -> Root -> Depth -> Flip -> (Tree,[Int])
swapAndOrder'' tree (-1) _ _ = (tree,[])
swapAndOrder'' tree root currDepth flip = (finishedTree, orderLeft ++ [root] ++ orderRight)
    where (left,right) = if shouldFlip then tupleFlip  $ tree I.! root
                                       else  tree I.! root
          shouldFlip   = currDepth `mod` flip == 0
          (treeLeft, orderLeft)   = swapAndOrder'' tree left (currDepth + 1) flip 
          (treeRight, orderRight) = swapAndOrder'' treeLeft right (currDepth + 1) flip
          finishedTree = if shouldFlip then I.adjust tupleFlip root treeRight
                                       else treeRight
          tupleFlip = (\(a,b)-> (b,a))

type TreePos = (Tree,Root,Depth)

tupleFlip    = (\(a,b)-> (b,a))

foldInorderTree :: TreePos -> (b-> Root -> Depth -> b) -> b -> b
foldInorderTree (tree,root,depth) f acc
    | root == (-1) = acc
    | otherwise             = r_acc
        where l_acc = foldInorderTree (tree,left ,depth+1) f acc
              (left,right)  = tree I.! root
              m_acc = f l_acc root depth
              r_acc = foldInorderTree (tree,right,depth+1) f m_acc


myF :: Flip -> (Tree,[Int]->[Int]) -> Root -> Depth -> (Tree,[Int]->[Int])
myF flip (tree,f_list) root depth = (tree',f_list')
    where  tree'        = if shouldFlip then I.adjust tupleFlip root tree else tree
           shouldFlip   = depth `mod` flip == 0
           list         = f_list []
           f_list'      = if shouldFlip then \l -> l ++ [root] ++ (f_list [])
                                        else f_list . (root:)

swapAndOrder' :: Tree -> Root -> Depth -> Flip -> (Tree,[Int])
swapAndOrder' tree root depth flip = (tree,f_list [])
    where (tree,f_list) = foldInorderTree (tree,root,depth) (myF flip) (tree,id)



swapAndOrder :: Tree -> Flip -> (Tree, [Int])
swapAndOrder tree flip = swapAndOrder' tree 1 firstDepth flip

sampleTreeList = [( 2, 3),(-1, 4),(-1, 5),(-1,-1),(-1,-1)]
sampleTree = readTree sampleTreeList
firstDepth :: Depth
firstDepth = 1

-- |
-- >>> swapAndOrder sampleTree 2 
-- (fromList [(1,(2,3)),(2,(4,-1)),(3,(5,-1)),(4,(-1,-1)),(5,(-1,-1))],[4,2,1,5,3])

solve :: Tree -> [Flip] -> [[Int]]
solve tree k_s = (reverse . snd) $ foldl' update (tree,[]) k_s
    where update (currTree, ordersList) flip  = (newTree, newOrder:ordersList)
            where (newTree,newOrder) = swapAndOrder currTree flip


manyLines :: Int -> IO [String]
manyLines 0 = return []
manyLines n = do
    a <- getLine 
    rest <-  manyLines (n-1)
    return $ a : rest 

main = do
    n <- read <$> getLine
    stringTree <- manyLines n
    nCases <- (read :: String -> Int) <$> getLine 
    cases <- manyLines nCases
    let ans  = solve (readTree . readPairs $ stringTree) (map read cases)
    let ansS = map (unwords . map show) ans
    putStr $ unlines ansS

-- 11
-- 2 3
-- 4 -1
-- 5 -1
-- 6 -1
-- 7 8
-- -1 9
-- -1 -1
-- 10 11
-- -1 -1
-- -1 -1
-- -1 -1
-- 2
-- 2
-- 4
