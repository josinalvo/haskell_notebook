

import Data.List.Split
import Data.List

type FunctionCandidate = [(Int,Int)]

listsOfPairs :: [String] ->  [ FunctionCandidate ]
listsOfPairs strings  = map (map line2tuple) onePreCandidatePerList 
    where onePreCandidatePerList = filter (/= []) . splitWhen ( not . (' ' `elem` )) $ strings

line2tuple :: String -> (Int,Int)
line2tuple = (\ (x:y:[]) -> (x,y)) . (map read) . (splitOn " ")

isFunction :: [(Int,Int)] -> Bool
isFunction pairs = (length firstElems == length uniqueFirsts)
    where uniquePairs  = nub pairs
          firstElems   = map fst uniquePairs
          uniqueFirsts = nub firstElems
          
yesNos :: [[(Int,Int)]] -> [String]
yesNos l = map oneGuy l
    where oneGuy list = if isFunction list
                        then "YES"
                        else "NO"

main :: IO ()
main = do txt <- getContents
          (putStrLn . unlines . yesNos . listsOfPairs . lines) txt
