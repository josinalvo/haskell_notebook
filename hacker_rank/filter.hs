-- NS se passou

import Data.IntMap hiding (foldr)
import GHC.Exts hiding (toList)

type Position = Int
type Amount   = Int
-- f :: [Int] -> Int -> [Int]
f list need = order
    where pairs              = zip list [0..]
          dic                = foldr addToSet empty pairs
          addToSet (v,p) map = insertWith (\(_,p) (c,_) -> (c+1,p)) v (1,p) map
          ansPairs           = [(v,p) | (v,(c,p)) <- (toList dic) ,c >= need]
          order              = fmap fst (sortWith snd ansPairs)

-- |
-- >>> f [1,2,3,4,5,6,2,2,4,4,4,4,4,9] 4
-- WAS NOW [2,4]
-- NOW [4]

separate :: [String] -> [(String, String)]
separate (s1:s2:rst) = (s1,s2):(separate rst)
separate [] = []
separate [a] = error "extra line"

prep :: (String, String) -> ([Int], Int)
prep (s1,s2) = (list,need)
    where [_,need] = fmap read (words s1)
          list     = fmap read $ words s2

h :: [Int] -> String
h [] = "-1"
h l  = unwords $ fmap show l

main = do
    getLine
    problems <- lines <$> getContents
    let sols = fmap (uncurry f . prep) (separate problems) :: [[Int]]
    putStr $ unlines (fmap h sols)


