import Data.Maybe
import Data.Foldable
import Data.Set as Set hiding(filter,foldl')
import Data.List hiding (insert)
extractInOrder n []   = [[]]
extractInOrder 0 list = [[]]

extractInOrder n list = do
    e <- list
    let rest = filter ( /= e) list
    other <- extractInOrder (n-1) rest
    return (e : other)

-- |
-- >>> a = extractInOrder 2 [1,2,3,4]
-- >>> length a
-- 12
-- >>> b = extractInOrder 4 [1..8]
-- >>> length b
-- 1680
-- >>> 8*7*6*5
-- 1680
type Card = ([Integer],[Integer])

allCards :: [Card]
allCards = fmap order2card $ extractInOrder 4 [1..8]

order2card :: [Integer] -> Card
order2card lst = (lst,rest)
    where rest = filter (`notElem` lst) [1..(toInteger $ (length lst)*2)]


equivalentCards1 :: Card -> [Card]
equivalentCards1 a@([],[]) = [a]
equivalentCards1 ((f1:rst),(f2:rst2)) = do
    (rest1,rest2) <- equivalentCards1 (rst,rst2)
    (f1',f2') <- [(f1,f2),(f2,f1)]
    return $ toCanonicalForm $ (,) (f1':rest1) (f2':rest2)
equivalentCards1 _ = error "uai"

toCanonicalForm :: Card -> Card
toCanonicalForm (l1,l2) = unzip ordered
    where unordered = zip  l1 l2
          ordered = sortOn snd unordered
    

-- [3.4.2.1]  5 6 7 8
-- 3--5
-- 4--6
-- 2--7
-- 1--8

-- |
-- >>> length $ equivalentCards1 ([1,2,3,4],[5,6,7,8])
-- 16
-- >>> length $ equivalentCards1 $ order2card [1,2,3,4]
-- 16

-- >>> length $ equivalentCards1 $ order2card [1,2,3]
-- 8


shift2 :: Card -> Card
shift2 (l1,l2) = toCanonicalForm (fmap numberShift2 l1, fmap numberShift2 l2)
    where numberShift2 7 = 1
          numberShift2 8 = 2
          numberShift2 a = a+2
shift4 :: Card -> Card
shift4 = shift2 . shift2
shift6 :: Card -> Card
shift6 = shift2 . shift4



addCardToMap :: (Set Card) -> Card ->  (Set Card)
addCardToMap setCards card = newMap
    where newMap = if any (`member` setCards) toConsider
                      then setCards
                      else insert card setCards
          toConsider = equivalentCards1 card ++ equivalentCards1 ( shift2 card ) ++
                                                equivalentCards1 ( shift4 card ) ++
                                                equivalentCards1 ( shift6 card )

clean :: Set Card
clean = foldl' addCardToMap Set.empty allCards

-- |
-- >>> size clean
-- 35

-- |
--- >>> clean
-- fromList [([1,2,3,4],[5,6,7,8]),([1,2,3,5],[4,6,7,8]),([1,2,3,6],[4,5,7,8]),([1,2,3,7],[4,5,6,8]),([1,2,4,3],[5,6,7,8]),([1,2,4,5],[3,6,7,8]),([1,2,4,6],[3,5,7,8]),([1,2,4,7],[3,5,6,8]),([1,2,5,4],[3,6,7,8]),([1,2,5,6],[3,4,7,8]),([1,2,5,7],[3,4,6,8]),([1,2,6,3],[4,5,7,8]),([1,2,6,4],[3,5,7,8]),([1,2,6,5],[3,4,7,8]),([1,3,2,5],[4,6,7,8]),([1,3,2,6],[4,5,7,8]),([1,3,2,7],[4,5,6,8]),([1,3,5,7],[2,4,6,8]),([1,3,6,2],[4,5,7,8]),([1,3,6,4],[2,5,7,8]),([1,3,6,5],[2,4,7,8]),([1,4,3,2],[5,6,7,8]),([1,4,3,5],[2,6,7,8]),([1,4,3,6],[2,5,7,8]),([1,4,5,2],[3,6,7,8]),([1,4,5,3],[2,6,7,8]),([1,4,6,2],[3,5,7,8]),([1,4,6,3],[2,5,7,8]),([1,5,3,4],[2,6,7,8]),([1,5,4,3],[2,6,7,8]),([2,1,4,3],[5,6,7,8]),([2,1,4,6],[3,5,7,8]),([2,1,6,4],[3,5,7,8]),([2,1,6,5],[3,4,7,8]),([2,4,6,1],[3,5,7,8])]


