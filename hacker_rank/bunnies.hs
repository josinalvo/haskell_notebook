solve = foldr1 lcm

main = do
    getLine
    sol <- solve <$> (map read . words) <$> getLine
    print sol