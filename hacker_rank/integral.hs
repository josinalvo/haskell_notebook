solve :: Int -> Int -> [Int] -> [Int] -> [Double]
solve l r a b = [area l2 r2 a b, volume l2 r2 a b]
    where l2 = fromIntegral l
          r2 = fromIntegral r

eval :: [Int] -> [Int] -> Double -> Double
eval a b x = sum $ zipWith (*) coefs potencias
    where potencias = map (x ^^) b :: [Double]
          coefs     = map fromIntegral a

-- Prelude> a = (^) :: (Int -> Int -> Int)
-- Prelude> b = (^^) :: (Int -> Int -> Int)

size = 0.001

interval :: Double -> Double -> [Double]
interval l r = [l,l+size .. r-size]

area :: Double -> Double -> [Int] -> [Int] -> Double
area l r a b = (* size) $ sum $ map (eval a b) (interval l r)

volume :: Double -> Double -> [Int] -> [Int] -> Double
volume l r a b = (* (size * pi)) $ sum $ map ((^2) . eval a b) (interval l r)


-- |
-- >>> area 0 5 [1] [1]
-- 12.5...
-- >>> area 0 8 [5] [0]
-- 40...

readInt :: String -> Int
readInt = read

main2 = do
    all <- getContents 
    let [a,b,[l,r]] = map (map readInt) $ (map words . lines) all  
    putStrLn $ unlines $ map show $ solve l r a b

main = do
    a <- map read . words <$> getLine 
    b <- map read . words <$> getLine 
    [l,r] <- map read . words <$> getLine 
    putStrLn $ unlines $ map show $ solve l r a b

    
