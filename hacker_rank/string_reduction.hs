import Data.Foldable
import Control.Monad


solve :: String -> String
solve string = reverse $ foldl' function "" string
    where function string char = if char `elem` string
                                 then string
                                 else char:string 

-- |
-- >>> solve "aabbaacc"
-- "abc"

-- main = fmap (solve) getLine >>= putStrLn 
-- main = join $ fmap (print . solve) getLine


main :: IO ()
main = interact solve
          