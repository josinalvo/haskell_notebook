import Data.List

-- permutations
changes0 :: [Int] -> [Int]
changes0 coins = 1: (map f [1..])
  where f n = sum $ map (nChanges0 coins) $ map (n -) coins

nChanges0 :: [Int] -> Int -> Int
nChanges0 coins n
  | n < 0 = 0
  | otherwise = changes0 coins !! n

-- combinations, but slow
changes1 :: [Int] -> [Int]
changes1 [] = 1:[0, 0..]
changes1 (c:coins) = 1: (map f [1..])
  where f n = let m = div n c
              in sum $ map (nChanges1 coins) $ map (n -) [0, c .. (m*c)]

nChanges1 :: [Int] -> Int -> Int
nChanges1 _ 0 = 1
nChanges1 [] _ = 0
nChanges1 coins n
  | n < 0 = 0
  | otherwise = changes1 coins !! n

-- combinations
changes2 :: [Int] -> [Integer]
changes2 coins = allChanges2 !! lng
  where cs = sort coins
        lng = length cs
        allChanges2 =  (1:[0,0..]): run [1..lng] cs
        run [] _ = []
        run (i:is) (c:cs) = chgs i c : run is cs
        chgs i c = map f [0..]
          where f n = let m = div n c
                      in sum $ map (allChanges2 !! (i-1) !!) $ map (n -) [0, c .. (m*c)]

nChanges2 :: [Int] -> Int -> Integer
nChanges2 coins n = changes2 coins !! n

-- sums values in positions 0 mod p
skipingSum :: Int -> [Integer] -> Integer
skipingSum p list = skpSum list 0 0
  where skpSum [] _ total = total
        skpSum (n:ns) i total = if mod i p == 0
                                  then skpSum ns (i+1) $! (total+n)
                                  else skpSum ns (i+1) total

-- not lazy tail recursion
changesR :: [Int] -> Int -> [Integer]
changesR coins n = run coins $! list0 n [1]
  where -- generate a list of n 0s and one 1
        list0 0 lst = lst
        list0 n lst = list0 (n-1) (0:lst)

        -- return list of number of changesR based on ns0
        chgs _ [] slatot = reverse slatot
        chgs c (n:ns) slatot = chgs c ns $! (:slatot) $! skipingSum c (n:ns)

        -- 
        run [] lst0 = lst0
        run (c:cs) lst0 = run cs $! chgs c lst0 []

changes coins n = reverse $! changesR coins n

nChanges :: [Int] -> Int -> Integer
nChanges coins n = head $! changesR coins n

-- lazy tail recursion
changesR' :: [Int] -> Int -> [Integer]
changesR' coins n = run coins $ list0 n [1]
  where -- generate a list of n 0s and one 1
        list0 0 lst = lst
        list0 n lst = list0 (n-1) (0:lst)

        -- return list of number of changes based on ns0
        chgs _ [] slatot = reverse slatot
        chgs c (n:ns) slatot = chgs c ns $ (:slatot) $ skipingSum c (n:ns)

        -- 
        run [] lst0 = lst0
        run (c:cs) lst0 = run cs $ chgs c lst0 []

changes' coins n = reverse $ changesR' coins n

nChanges' :: [Int] -> Int -> Integer
nChanges' coins n = head $ changesR' coins n


main = do
  print $ nChanges' [1,2] 10000
