{-# OPTIONS_GHC -Wno-incomplete-patterns #-}
import Data.Sequence
import Data.Sequence as S
import Data.List (foldl')
import Control.Monad.State

data Status = Status (Seq (Seq Int)) deriving Show 

findPos :: Ord a => Seq a -> a -> Int
findPos seq n = aux 0 originalEnd n
    where originalEnd = (S.length seq - 1)
          aux start finish target
            -- smaller than cases 
            | index seq originalEnd < target = originalEnd
            | index seq 0 > target           = (-1)
            -- invariant:  s start <= target <= s finish
            -- found cases
            | index seq midPoint == target   = midPoint
            | index seq start == target      = start
            | index seq finish == target     = finish
            -- smaller then case
            | start + 1 == finish            = start
            -- recursive cases (keep invariant: from between to between)
            | index seq midPoint > target    = aux start midPoint  target
            | index seq midPoint < target    = aux midPoint finish target
            where midPoint = (start + finish) `div` 2

-- |
-- >>> findPos (fromList [10,20,30,40,50]) 40
-- 3
-- >>> findPos (fromList [10,20,30,40,50]) 43
-- 3
-- >>> findPos (fromList [10,20,30,40,50]) 19
-- 0
-- >>> findPos (fromList [10,20,30,40,50]) 10
-- 0
-- >>> findPos (fromList [10,20,30,40,50]) 9
-- -1

add :: Int -> Status -> Status
add n (Status seqs) 
    | S.length seqs == 0 = Status $ fromList [fromList [n]]
add n (Status seqs) = Status $ insertAt 0 nextSeq seqs
    where lastSeq = index seqs 0
          pos     = findPos lastSeq n 
          nextSeq = insertAt (pos+1) n lastSeq
       


a = add 30 $ add 20 $ add 50 $ add 40 $ add 10 b
b = Status (fromList [])


-- |
-- >>> add 11 a
-- Status (fromList [fromList [10,11,20,30,40,50],fromList [10,20,30,40,50],fromList [10,20,40,50],fromList [10,40,50],fromList [10,40],fromList [10]])
-- >>> add 9 a
-- Status (fromList [fromList [9,10,20,30,40,50],fromList [10,20,30,40,50],fromList [10,20,40,50],fromList [10,40,50],fromList [10,40],fromList [10]])
-- >>> add 90 a
-- Status (fromList [fromList [10,20,30,40,50,90],fromList [10,20,30,40,50],fromList [10,20,40,50],fromList [10,40,50],fromList [10,40],fromList [10]])
-- >>> add 25 a
-- Status (fromList [fromList [10,20,25,30,40,50],fromList [10,20,30,40,50],fromList [10,20,40,50],fromList [10,40,50],fromList [10,40],fromList [10]])

rollBackMany :: Int -> Status -> Status
rollBackMany n (Status seqs) = Status $ insertAt 0 correct seqs
    where correct = index seqs n
-- |
-- >>> a
-- Status (fromList [fromList [10,20,30,40,50],fromList [10,20,40,50],fromList [10,40,50],fromList [10,40],fromList [10]])
-- >>> rollBackMany 3 a
-- Status (fromList [fromList [10,40],fromList [10,20,30,40,50],fromList [10,20,40,50],fromList [10,40,50],fromList [10,40],fromList [10]])






updateS :: Int -> Status -> Status
updateS n status = if n > 0 then add n status else rollBackMany amount status
    where amount = (-1)*n -1

solve :: [Int] -> ([Int],Status)
solve commands   = runState fullState start
    where start  = Status $ fromList []
          step :: Int -> State Status Int
          step n = state aux
            where aux status1 = (median,newStatus)
                    where newStatus@(Status s) = updateS n status1
                          median               = index nextSeq $ ((S.length nextSeq+1) `div` 2) -1
                          nextSeq              = index s 0
          fullState = sequence $ map step commands

main = do
    getLine
    strings <- words <$> getContents
    let chars = unlines . map show . fst $ solve $ map read strings
    putStr chars

a2 :: ([Int], Status)
a2 = solve [1,5,-2,3,2,5,4,-7,2,-3]

-- |
-- >>> fst a2
-- [1,1,1,1,2,2,3,1,1,3]




