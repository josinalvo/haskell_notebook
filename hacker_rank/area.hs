
import Data.List.Split
import Data.List

type Point = (Float,Float)

listOfPairs :: [String] ->  [ Point ]
listOfPairs strings   = allPairs
    where readPairs   = map line2tuple noNumPoints
          noNumPoints = (tail strings)
          allPairs    = readPairs ++ [head readPairs]
    

line2tuple :: String -> Point
line2tuple = (\ (x:y:[]) -> (x,y)) . (map read) . (splitOn " ")

area = abs . areaR

areaR :: Floating p => [(p, p)] -> p
areaR (p1:p2:ps) = signedArea p1 p2 + areaR (p2:ps)         
areaR (p1:[]) = 0
areaR [] = 0

signedArea :: Floating a => (a, a) -> (a, a) -> a
signedArea (x1,y1) (x2,y2) = (x1*y2 - x2*y1)/2


main :: IO ()
main = do txt <- getContents
          (print . area . listOfPairs . lines) txt
