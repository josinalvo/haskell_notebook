
import Data.List.Split
import Data.List

type Point = (Float,Float)

listOfPairs :: [String] ->  [ Point ]
listOfPairs strings  =  allPairs
    where readPairs = map line2tuple (tail strings)
          allPairs  = readPairs ++ [ head readPairs]
    

line2tuple :: String -> Point
line2tuple = (\ (x:y:[]) -> (x,y)) . (map read) . (splitOn " ")

perimeter :: Floating p => [(p, p)] -> p
perimeter (p1:p2:ps) = distance p1 p2 + perimeter (p2:ps)         
perimeter (p1:[]) = 0
perimeter [] = 0

signedArea :: Floating a => (a, a) -> (a, a) -> a
signedArea (x1,y1) (x2,y2) = (x1*y2 - x2*y1)/2


main :: IO ()
main = do txt <- getContents
          (print . perimeter . listOfPairs . lines) txt
