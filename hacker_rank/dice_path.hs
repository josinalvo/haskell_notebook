-- CURRENTLY TLE

import Data.Map as Map hiding (foldl')
import Data.List (foldl')

data Die = Die {cima :: Int, frente :: Int, esquerda :: Int} deriving (Eq, Ord,Show)

data Position = Position {x :: Int, y :: Int} deriving (Eq, Ord, Show)


virarPDireita (Die cima frente esquerda) = Die cima' frente esquerda'
    where cima' = esquerda
          esquerda' = 7-cima

virarPFrente (Die cima frente esquerda) = Die cima' frente' esquerda
    where cima'   = 7-frente
          frente' = cima

startingDie = Die 1 2 3
startingPos = Position 1 1 
startingMap' = fromList [(,) startingDie 1]
startingMap = fromList [(,) startingPos startingMap']

type DiceMap = Map Position (Map Die Int)

pips = [1..6]

increaseMap :: DiceMap -> Position ->  DiceMap
increaseMap oldMap (Position 1 1) = oldMap

increaseMap oldMap p@(Position 1 n) = aux oldMap p [(,) (Position 1 (n-1)) virarPDireita]

increaseMap oldMap p@(Position m 1) = aux oldMap p [(,) (Position (m-1) 1) virarPFrente]

increaseMap oldMap p@(Position m n) = aux oldMap p [(,) (Position (m-1) n) virarPFrente, (,) (Position m (n-1)) virarPDireita]

aux :: Map Position (Map Die Int) -> Position -> [(Position, Die -> Die)] -> Map Position (Map Die Int)
aux oldMap (Position m n) list_Ps_and_Fs = insert (Position m n) newPosMap oldMap
    where newPosMap = fromListWith max temp
          temp = do
            (previous,f) <- list_Ps_and_Fs
            oldDie   <- keys $ oldMap ! previous
            let oldVal      = oldMap ! previous ! oldDie
                newDie      = f oldDie
                newVal      = oldVal + cima newDie
            [(,) newDie newVal]

range = [1..60]

allThePos :: [Position]
allThePos = [Position i j | i <- range, j <- range]

allTheSols = foldl' increaseMap startingMap allThePos

bestSols = Map.map (maximum . elems) allTheSols

getInt :: IO Int
getInt = read <$> getLine

getPos :: IO Position
getPos = do
    [a,b] <- words <$> getLine
    return $ Position (read a) (read b)
    

solveIO 0 = putStrLn ""
solveIO n = do
    p <- getPos
    print (bestSols ! p)    
    solveIO (n-1)

mainRapido = do
    n <- getInt
    solveIO n

mainLento = do
    n <- getInt
    let goal = replicate n ((bestSols !) <$> getPos) ::  [IO Int]
    let results' = sequence goal :: IO [Int]
    let strings' = fmap (fmap show) results' :: IO [String]
    strings <- strings'
    putStrLn $ unlines strings


mainLento' = do
    n <- getInt
    let goal = replicate n ((bestSols !) <$> getPos) ::  [IO Int]
    mapM_ (fmap print)  goal
    

main = mainLento

-- >>> bestSols ! Position 3 3
-- 19

-- >>> allTheSols ! Position 2 2
-- fromList [(Die {cima = 3, frente = 1, esquerda = 2},9),(Die {cima = 5, frente = 3, esquerda = 6},9)]

-- >>> allTheSols ! Position 3 3
-- fromList [(Die {cima = 1, frente = 5, esquerda = 4},16),(Die {cima = 2, frente = 3, esquerda = 1},17),(Die {cima = 4, frente = 6, esquerda = 2},19),(Die {cima = 6, frente = 2, esquerda = 4},17),(Die {cima = 6, frente = 5, esquerda = 3},19)]
    
-- >>> allTheSols ! Position 2 1
-- fromList [(Die {cima = 5, frente = 1, esquerda = 3},6)]

-- >>> allTheSols ! Position 1 2
-- fromList [(Die {cima = 3, frente = 2, esquerda = 6},4)]
