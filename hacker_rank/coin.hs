import qualified Data.Map as M
import qualified Data.Set as S
import Data.List (sort)
-- https://www.hackerrank.com/challenges/coin-change/problem?isFullScreen=true


-- c = {1,2,5,10}

--          0 10s         1 10s        2 10s        3 10s        4 10s       5 10s
-- f 50 = f50 sem 10 + f40 sem 10 + f30 sem 10 + f20 sem 10 + f10 sem 10 + (1|0)
--        f30 sem 5    f30 sem 5

getWays :: [Integer] -> Integer -> Integer
getWays (c1:cs) n = sum $ fmap (getWays cs) subtractions
    where 
           subtractions = [n,n-c1..0]

getWays [] 0 = 1
getWays [] n = 0

a= getWays [1] 10

-- | 
-- >>> getWays [1,2] 10
-- 6

type Memo = M.Map (Integer,Integer) Integer

-- getWays2 :: Memo -> [Integer] -> Integer -> (Memo, Integer)
-- getWays2 m (c1:cs) n = (m,sum $ ) 
--     where 
--            subtractions = [n,n-c1..0]
--            fmap (getWays2 m cs) subtractions

-- getWays2 m [] 0 = 1
-- getWays2 m [] n = 0

getWays3 :: [Integer] -> Integer -> Integer
getWays3 cs toExchange = toSolution (numberCoins-1,toExchange)
    where numberCoins = length cs
          positions   = [-1..numberCoins-1]
          amounts     = [0..toExchange]
          domain      = S.fromList [(c,a) | c <- positions, a <- amounts]
          memoizMap   = M.fromSet toSolution domain
          -- given max list position available and amount to exchange, produce #ways
          toSolution  ( _,0) = 1
          toSolution  (-1,_) = 0
          -- f 50 = f50 sem 10 + f40 sem 10 + f30 sem 10 + f20 sem 10 + f10 sem 10 + f0 sem 10
          -- see lines above, they check if the coin got to f0
          toSolution  (coinPosition,toExchange) = sum $ fmap (\n -> memoizMap  M.! (coinPosition-1,n)) subtractions
                where  bigCoin      = cs !! coinPosition
                       subtractions = [toExchange,toExchange-bigCoin..0]

-- |
-- >>> getWays3 [1,2] 14          
-- 8


-- |
-- >>> nChanges0 [1,2] 3
-- 3

-- combinations
changes2 :: [Int] -> [Integer]
changes2 coins = allChanges2 !! lng
  where cs  = sort coins
        lng = length cs
        --             caso base meu
        allChanges2 =  (1:[0,0..]): run [1..lng] cs
        run [] _ = []
        run (i:is) (c:cs) = chgs i c : run is cs
        chgs i c = map f [0..]
          where f n = let m = div n c
                          restsAfterCoin     = map (n -) [0, c .. (m*c)]
                          resultsForPrevCoin = allChanges2 !! (i-1)
                      in sum $ map (resultsForPrevCoin !!) restsAfterCoin

nChanges2 :: [Int] -> Int -> Integer
nChanges2 coins n = changes2 coins !! n

-- sums values in positions 0 mod p
skipingSum :: Int -> [Integer] -> Integer
skipingSum p list = skpSum list 0 0
  where skpSum [] _ total = total
        skpSum (n:ns) i total = if mod i p == 0
                                  then skpSum ns (i+1) $! (total+n)
                                  else skpSum ns (i+1) total

-- | pegar 0, 3 6 ... Realmente partindo do começo
-- >>> skipingSum 3 [1,40,500,6000,70000,80000,90000]
-- 96001

-- lazy tail recursion
changesR' :: [Int] -> Int -> [Integer]
changesR' coins n = run coins $ list0 n [1]
  where -- generate a list of n 0s and one 1
        list0 0 lst = lst
        list0 n lst = list0 (n-1) (0:lst)

        -- return list of number of changes based on ns0
        chgs _ [] slatot = reverse slatot -- reverse slatot == totals
        chgs c (n:ns) slatot = chgs c ns $ (:slatot) $ skipingSum c (n:ns)

        -- 
        run [] lst0 = lst0
        run (c:cs) lst0 = run cs $ chgs c lst0 []

-- foldl [a,b,c,d] op start
-- foldl  [b,c,d] op (op start a)

changes' coins n = reverse $ changesR' coins n

nChanges' :: [Int] -> Int -> Integer
nChanges' coins n = head $ changesR' coins n

main = do
  print $ nChanges' [1,2] 10000
