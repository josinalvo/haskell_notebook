import Control.Monad
perm []         = []
perm [b]        = [b]
perm (a:b:rest) = (b:a:perm rest)



main = do
    n <- read <$> getLine 
    replicateM_ n (getLine >>= (putStrLn . perm))
main3 = do
    _ <- getLine 
    strings <- getContents 
    putStrLn . unlines . (map perm) . lines $ strings
main2 = do
    n <- read <$> getLine 
    solve n

solve :: Int -> IO ()
solve 0 = return ()
solve n = do
    a <- getLine 
    putStrLn $ perm a
    solve (n-1)