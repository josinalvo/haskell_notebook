--Contributed by Ron Watkins
module Main where


fib n = fibl !! (n-1)
    where fibl = [0,1] ++ f fibl
          f l = zipWith (+) l (tail l)


-- This part is related to the Input/Output and can be used as it is
-- Do not modify it
main = do
    input <- getLine
    print . fib . (read :: String -> Int) $ input