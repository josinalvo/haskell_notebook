{-# LANGUAGE TupleSections #-}

import qualified Data.Array as A 

type Pos    = (Int, Int)
type Canvas = A.Array Pos Char
data Triangulo = Triangulo { pos :: Pos, altura :: Int } deriving Show 

lineQtd = 100
colQtd  = lineQtd*2-1


--range = ((1,1),(lineQtd,colQtd))
range = ((10,10),(40,100))


clearCanvas :: Canvas
clearCanvas = A.listArray range (repeat '_')

draw :: [Pos] -> A.Array (Int, Int) Char
draw lPos = clearCanvas A.// a
    where a = map (,'1') $ filter (A.inRange range) lPos

pontosTriangulo :: Triangulo -> [Pos]
pontosTriangulo (Triangulo ponta alturaT) = concat $ take alturaT $ iterate expande [ponta]

expande :: [Pos] -> [Pos]
expande lista = (lp,cp-1) : abaixo ++ [(lu,cu+1)]
    where abaixo           = map desce lista
          primeiro@(lp,cp) = head abaixo
          ultimo@(lu,cu)   = last abaixo
          desce    (l,c)   = (l+1,c)






showCanvas :: Canvas -> String
showCanvas = showCanvasR . A.elems

showCanvasR ""   = "\n"
showCanvasR list = (take cols list) ++ "\n" ++ rest
    where rest = showCanvasR $ drop cols list
          ((_,a),(_,b)) = range
          cols = b-a+1

explode :: Triangulo -> [Triangulo]
explode (Triangulo (l,c) alt1) = [ (Triangulo (l,c) alt2), (Triangulo (l+alt2,c-alt2) alt2), (Triangulo (l+alt2,c+alt2) alt2)]
    where  pots  = 1:(map (*2)) pots
           pots_cabem = filter (>= (alt1 `div` 2)) pots
           alt2  = head pots_cabem

sierpinskiR :: Int -> [Triangulo] ->  [Triangulo]
sierpinskiR 0 ts = ts
sierpinskiR n ts = do
    t1 <- ts
    sierpinskiR (n-1) (explode t1)

sierpinski n = sierpinskiR n [Triangulo (1,lineQtd) lineQtd]


ans n = putStr $ showCanvas $ draw pontos
    -- where pontos = concat $  (map pontosTriangulo) $ sierpinski n
    -- where pontos = do
    --         triang <- sierpinski n
    --         pontosTriangulo triang
    where pontos = sierpinski n >>= pontosTriangulo
main = readLn >>= ans
    