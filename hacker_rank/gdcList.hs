import Data.List (foldl1')
gdcList = foldl1' gdc

gdc n m = if resto == 0 then menor else gdc menor resto
    where maior = max n m
          menor = min n m 
          resto = maior `mod` menor


type Factor = (Int,Int)

gcd' :: [Factor] -> [Factor] -> [Factor]
gcd' a@((p1,n1):rest) a2@((p2,n2):rest2) 
   | p1 == p2 = (p1, min n1 n2) : gcd' rest rest2
   | p1 < p2  = gcd' rest a2
   | p1 > p2  = gcd' a    rest2
   | otherwise  =  error "carai"
gcd' [] _ = []
gcd' _ [] = []

show' :: [Factor] -> String
show' [(a,b)]      = show a ++ " " ++ show b
show' ((a,b):rest) = show a ++ " " ++ show b ++ " " ++ show' rest
show' []           = ""

readNum :: String -> [(Int,Int)]
readNum s = pairs nums
    where nums = map read $ words s
          pairs (x:y:xs) = (x,y): pairs xs
          pairs [] = []
          pairs [x] = error "ai carai"


a = show' . foldr1 gcd' . (map readNum) . tail . lines

main = interact a