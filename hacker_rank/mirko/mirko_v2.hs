{-# OPTIONS_GHC -Wno-incomplete-patterns #-}
{-# OPTIONS_GHC -Wno-unrecognised-pragmas #-}
{-# HLINT ignore "Use camelCase" #-}
import GHC.Exts (sortWith)
import Data.Sequence (fromList, length, Seq, (!?))
import Data.Maybe
import Debug.Trace
import qualified Data.Map.Strict as M


data Building = Building {start :: Int, growth :: Int, startIndex :: Int} deriving Eq
data WinningB = WinningB {building :: Building, day :: Int}

instance Show Building where
    show (Building s g ix) = mconcat ["sHeight: ",show s, " grow: ", show g, " pos: ", show ix]

instance Show WinningB where
    show (WinningB b day) = mconcat ["(", show b, " at ", show day, ")"]

pairsToBuildings :: [(Int,Int)] -> [Building]
pairsToBuildings tuples = map (\((a,b),c) -> Building a b c) withIndex
    where withIndex = zip tuples [0..]

findFirst :: [Building] -> Building
findFirst bs = (head . reverse )sorted
    where sorted = sortWith compar bs
          compar (Building {start = a, startIndex = idx}) = (a,idx)

ex1 = pairsToBuildings [(1,5),(2,5),(3,3),(1,0)]
ex2 = pairsToBuildings [(1,5),(2,5),(3,3),(3,1),(2,0),(1,7)]
-- |
-- >>> findFirst ex1
-- sHeight: 3 grow: 3 pos: 2
-- >>> findFirst ex2
-- sHeight: 3 grow: 1 pos: 3

-- altura inicial diferente, 1 morre
-- altura inicial igual, o de indice menor morre
-- para cada growth, so sobra 1
eliminateSameGrowth :: [Building] -> [Building]
eliminateSameGrowth buildings = map snd goodPairs
    where mapPrecursor  = zip (map growth buildings) buildings
          chooser b1 b2
            | start b1 > start b2 = b1
            | start b1 < start b2 = b2
            | startIndex b1 > startIndex b2 = b1
            | startIndex b1 < startIndex b2 = b2
          buildingMap :: M.Map Int Building
          buildingMap = M.fromListWith chooser mapPrecursor
          goodPairs :: [(Int,Building)]
          goodPairs   = M.toList buildingMap

eliminateSmallSlopes :: [Building] -> [Building]
eliminateSmallSlopes buildings = filter goodSlope buildings
    where minimal     = growth $ findFirst buildings
          goodSlope b = (growth b >= minimal)

eliminateStarting :: [Building] -> [Building]
eliminateStarting buildings = filter (/= first) buildings
    where first  = findFirst buildings


compareB :: Int -> Building -> Building -> Ordering
compareB day (Building{start = s1, growth= g1}) 
             (Building{start = s2, growth= g2})     
                  = compare height1 height2
    where height1 = s1 + g1*day
          height2 = s2 + g2*day

-- winners :: [Building] -> Seq WinningB
winners allBs = fromList . reverse $ (aux cleanList [])
-- winners allBs = allBs
    where aux :: [Building] -> [WinningB] -> [WinningB]
          aux  bs    []         = aux bs [WinningB (findFirst allBs) 0]
          aux  []    ans        = ans
          aux (b:bs) all@(top@(WinningB btop topDay):rest) 
            | (compareB topDay) b btop == GT = aux (b:bs) rest
            | (compareB topDay) b btop == LT = aux bs (winsAt b btop:all)
            | (compareB topDay) b btop == EQ = aux bs newQueue
               where (bigGrowth,smallGrowth) = if growth b > growth btop
                                               then (b,btop) else (btop,b)
                     newQueue                = if startIndex smallGrowth > startIndex bigGrowth
                                               then (WinningB bigGrowth $ topDay+1):(WinningB smallGrowth topDay):rest
                                            --    else error "2"
                                               else (WinningB bigGrowth topDay):rest
          winsAt b btop      = WinningB b vDay
            where vDay = calculateDay b btop
          calculateDay :: Building -> Building -> Int
          calculateDay b1 b2 = max 0 victoryDay
              where startDist   = start b1 - start b2
                    slopeDiff   = growth b2 - growth b1
                    equalityDay = fromIntegral startDist / 
                                  fromIntegral slopeDiff
                    candidateDay= ceiling equalityDay
                    victoryDay  = if compareB candidateDay b1 b2 == EQ && startIndex b2 > startIndex b1
                                     then candidateDay+1
                                     else candidateDay
          cleanList = sortWith growth . eliminateStarting . eliminateSameGrowth . eliminateSmallSlopes  $ allBs
          
stringsToBuildings :: String -> String -> [Building]
stringsToBuildings sHeights sGrowths = pairsToBuildings pairs
    where pairs   = zip heights growths
          heights = map read $ words sHeights
          growths = map read $ words sGrowths

queryable :: [Char] -> [Char] -> Seq WinningB
queryable hs gs = winners $ stringsToBuildings hs gs

query :: Seq WinningB -> Int -> WinningB
query arr queryDay = aux arr (fromIntegral queryDay) 0 (Data.Sequence.length arr - 1)
    where aux :: Seq WinningB -> Int -> Int -> Int -> WinningB
          aux arr queryDay start finish
            -- | trace ("" ++ show start ++ "|" ++ show finish) False = undefined
            | day finishval <= queryDay = finishval
            | queryDay < day startval  = error "shiiiit"
            -- estou entre finish e start
            | start + 1 == finish      = startval
            | day midval > queryDay    = aux arr queryDay start midpoint
            | day midval < queryDay    = aux arr queryDay midpoint finish
            | day midval == queryDay   = midval
            where midpoint  = (finish + start) `div` 2
                  midval :: WinningB 
                  midval    = fromJust ((!?) arr midpoint)
                  startval  = fromJust ((!?) arr start )
                  finishval = fromJust ((!?) arr finish)

winningB2Pos :: WinningB -> Int
winningB2Pos = (+1) . startIndex . building 

main_partial = do
    _       <- getLine
    heights <- getLine
    growths <- getLine
    let buildings = stringsToBuildings heights growths
    let structure = winners buildings
    putStr $ show structure

main_full = do
    _       <- getLine
    heights <- getLine
    growths <- getLine
    let structure = queryable heights growths
    queries <- (map read) <$> lines <$> getContents
    let results = map winningB2Pos $ map (query structure) queries
    putStr $ unlines $ (map show) results

main = main_full