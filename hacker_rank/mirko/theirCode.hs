{-# LANGUAGE BangPatterns #-}
module Main where

import Data.List
import Data.Maybe
import Control.Applicative
import Control.Monad
import Control.Monad.State.Strict
import Data.Set (Set)
import qualified Data.Set as Set
import Data.Map (Map)
import qualified Data.Map as Map
import qualified Data.Vector as V

data BTree = BNil
           | BNode Int Int Int BTree BTree
             deriving (Show)

makeTree :: Int -> [(Int, Int, Int)] -> Int -> Int -> BTree
makeTree mq xs l r | l == r = BNil
makeTree mq xs l r = BNode l r bi (makeTree mq ys' l mid) (makeTree mq zs' (mid+1) r)
  where  mid = (l + r) `div` 2
         f bh bs []          = ((-1, -1, -1), [], [])
         f bh bs ((i, h, s):xs) =
           let ((i', h', s'), ys, zs) = f bh bs xs
           in ( if h+mid*s > h'+mid*s' || (h+mid*s == h'+mid*s' && i > i')
                then (i, h, s)
                else (i', h', s')
              , if s < bs && h > bh then (i, h, s):ys else ys
              , if s > bs && h+mq*s > bh+mq*bs then (i, h, s):zs else zs
              )
  
         ((bi, bh, bs), ys, zs) = f bh bs xs
         ys' = (bi, bh, bs):ys
         zs' = (bi, bh, bs):zs
         uai = bh
   
   

query :: Int -> BTree -> Int
query q BNil = error $ "error " ++ show q
query q (BNode l r i x y) = case compare q ((l + r) `div` 2) of
  LT -> query q x
  EQ -> i
  GT -> query q y

main :: IO ()
main = do
  [n, q] <- fmap read . words <$> getLine
  !h     <- fmap read . words <$> getLine
  !s     <- fmap read . words <$> getLine
  qs     <- forM [1..q] $ const $ read <$> getLine
  let mq = maximum qs
  let mt = makeTree mq (zip3 [1..] h s) 0 (mq+1)
  let a = (zip h s :: [(Int,Int)])
  print $ "we had buildings" ++ show a
  print mt
  forM_ qs $ \q -> putStrLn.show $ query q mt