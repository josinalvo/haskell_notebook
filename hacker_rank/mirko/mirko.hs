{-# OPTIONS_GHC -Wno-incomplete-patterns #-}
{-# OPTIONS_GHC -Wno-unrecognised-pragmas #-}
{-# HLINT ignore "Use camelCase" #-}
import GHC.Exts (sortWith)
import Data.Sequence (fromList, length, Seq, (!?))
import Data.Maybe
import Debug.Trace


data Building = Building {start :: Int, growth :: Int, startIndex :: Int} 
data WinningB = WinningB {building :: Building, day :: Double}

instance Show Building where
    show (Building s g ix) = mconcat ["sHeight: ",show s, " grow: ", show g, " pos: ", show ix]

instance Show WinningB where
    show (WinningB b day) = mconcat ["(", show b, " at ", show day, ")"]

sortBuildings :: [Building] -> [Building]
sortBuildings = sortWith (\(Building a b c) -> (b,-a,c))



winners :: [Building] -> Seq WinningB
winners bs = fromList . reverse $ (aux (appropriateSort bs) [])
    where aux :: [Building] -> [WinningB] -> [WinningB]
          aux (b:bs) []         = aux bs [winsAt b 0]
          aux  []    ans        = ans
          aux (b:bs) (top:rest) = let bDay = calculateDay b top
                                      wb   = WinningB b bDay in 
                                      if bDay < day top then aux (b:bs) rest
                                                        else aux bs     (wb:top:rest)
          winsAt b vDay      = WinningB b vDay
          calculateDay :: Building -> WinningB -> Double
          calculateDay b1 (WinningB b2 day2) 
            | start1 == start2  && growth1 == growth2 = day2
            | start1 == start2  && growth1 >  growth2 = 0
            | start1 == start2  && growth1 <  growth2 = 1/0
            | start1 <  start2  && growth1 == growth2 = day2
            | otherwise              = fromIntegral startDist / 
                                       fromIntegral slopeDiff
              where startDist  = start b1 - start b2
                    slopeDiff  = growth b2 - growth b1
                    start1     = start b1
                    start2     = start b2
                    growth1    = growth b1
                    growth2    = growth b2 
          appropriateSort  = sortWith (\(Building a b c) -> (b,a,c))

pairsToBuildings :: [(Int,Int)] -> [Building]
pairsToBuildings tuples = map (\((a,b),c) -> Building a b c) withIndex
    where withIndex = zip tuples [0..]



l1 = pairsToBuildings [(1,10),(3,5)]

-- |
-- >>> winners l1
-- [(sHeight: 1 grow: 10 pos: 0 at 0.4),(sHeight: 3 grow: 5 pos: 1 at 0.0)]

l2 = pairsToBuildings [(7,1), (5,2), (1,3)]

-- |
-- >>> winners l2
-- [(sHeight: 1 grow: 3 pos: 2 at 4.0),(sHeight: 5 grow: 2 pos: 1 at 2.0),(sHeight: 7 grow: 1 pos: 0 at 0.0)]


l3 = pairsToBuildings [(7,1), (5,2), (1,3), (4,2)]


-- |
-- >>> winners l3
-- fromList [(sHeight: 7 grow: 1 pos: 0 at 0.0),(sHeight: 1 grow: 3 pos: 2 at 3.0)]



stringsToBuildings :: [Char] -> [Char] -> [Building]
stringsToBuildings sHeights sGrowths = pairsToBuildings pairs
    where pairs   = zip heights growths
          heights = map read $ words sHeights
          growths = map read $ words sGrowths


queryable :: [Char] -> [Char] -> Seq WinningB
queryable hs gs = winners $ stringsToBuildings hs gs

query :: Seq WinningB -> Int -> WinningB
query arr queryDay = aux arr (fromIntegral queryDay) 0 (Data.Sequence.length arr - 1)
    where aux :: Seq WinningB -> Double -> Int -> Int -> WinningB
          aux arr queryDay start finish
            -- | trace ("" ++ show start ++ "|" ++ show finish) False = undefined
            | day finishval <= queryDay = finishval
            | queryDay < day startval  = error "shiiiit"
            -- estou entre finish e start
            | start + 1 == finish      = startval
            | day midval > queryDay    = aux arr queryDay start midpoint
            | day midval < queryDay    = aux arr queryDay midpoint finish
            | day midval == queryDay   = midval
            where midpoint  = (finish + start) `div` 2
                  midval :: WinningB 
                  midval    = fromJust ((!?) arr midpoint)
                  startval  = fromJust ((!?) arr start )
                  finishval = fromJust ((!?) arr finish)

example = queryable "7 5 1" "1 2 3"

-- |
-- >>> query example 3
-- (sHeight: 5 grow: 2 pos: 1 at 2.0)

-- winning day nao eh inteiro no momento
-- igual mata ?


winningB2Pos :: WinningB -> Int
winningB2Pos = (+1) . startIndex . building 

naive :: [Building] -> Int -> Building
naive buildings day = snd $ head filtered
    where listOfEndHeigths = map buildingToHeigth buildings
          buildingToHeigth Building {start=a, growth=b} = a+day*b

          maxHeight        = maximum listOfEndHeigths
          pairs = zip listOfEndHeigths buildings
          sorted = sortWith (((-1)*) . startIndex . snd) pairs
          filtered = filter ((== maxHeight) . fst) sorted

naive_queryable :: String -> String -> (Int -> Building)
naive_queryable sHeights sGrowths = naive buildings
    where buildings = stringsToBuildings sHeights sGrowths

main_complicated = do
    _       <- getLine
    heights <- getLine
    growths <- getLine
    let structure = queryable heights growths
    queries <- (map read) <$> lines <$> getContents
    let results = map winningB2Pos $ map (query structure) queries
    putStr $ unlines $ (map show) results

main_partial = do
    _       <- getLine
    heights <- getLine
    growths <- getLine
    let structure = queryable heights growths
    putStr $ show structure


main_naive = do
        _       <- getLine
        heights <- getLine
        growths <- getLine
        let structure = naive_queryable heights growths
        queries <- (map read) <$> lines <$> getContents
        let results = map ((+1) . startIndex) $ map structure queries
        putStr $ unlines $ (map show) results

main :: IO ()
main = main_naive
