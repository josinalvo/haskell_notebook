import Data.Map as M
import Data.Maybe
import Data.Set as S

-- Bizarramente, o problema final foi de precisao numerica.
-- Mudei de 0.1^8 para 0.1^10 o 'zero', e foi

--p(0,3) = 1/2 * p(0,2) + 1/2 * p(1,3)
--p(1,3) = 1/3 * 1 + 1/3*0 + 1/3*p(0,3)

--   0  = 1/2 * p(0,2) + 1/2 * p(1,3) - p(0,3)
-- -1/3 = 1/3*p(0,3) - p(1,3) 


type System        = [Equation]
type MazePosition  = (Int,Int)
data Equation      = Equation {getCoefs :: M.Map MazePosition Double, getB :: Double} deriving Show


eliminate :: System -> System
eliminate []  = []
eliminate (eq1:rest) = if nullEq eq1 then eliminate rest
                                     else eq1:(eliminate newRest)
    where 
        pivotName = (head . keys . getCoefs) eq1
        pivotCoef = (getCoefs eq1) M.! pivotName
        update :: Equation -> Equation
        update eq2 = if isJust maybeCoef2 then newEq2 else eq2
            where  maybeCoef2    = ((getCoefs eq2) M.!? pivotName)
                   coef2         = fromJust maybeCoef2
                   factor        = -coef2/pivotCoef
                   coefs1TimesF  = fmap (* factor) (getCoefs eq1)
                   b1TimesF      = (getB eq1) * factor
                   newCoefs2     = M.filter  (\a -> (abs a) > 0.1^10 ) 
                                      $ M.unionWith (+) coefs1TimesF (getCoefs eq2)
                   newEq2        = Equation newCoefs2 (getB eq2 + b1TimesF)
        
        newRest   = fmap update rest


classifyVars :: System -> ([(MazePosition,Equation)], Set MazePosition)
classifyVars [] = ([],S.empty)
-- Row Echelon form == escalonado
classifyVars rowEchelonFormSystem@(eq1:rest) = if S.size newVars >= 2 || infectedPivot
                                                then (cleanSystem,S.union newVars freeVars)
                                                else (newSystem,freeVars)
        where   (cleanSystem,freeVars) = classifyVars rest
                activeVars             = S.fromList $ (keys . getCoefs) eq1
                -- non-unique vars appear elsewhere in the equation, and therefore the current
                -- pivot is non-unique as well
                infectedPivot          = not (S.null $ S.intersection activeVars freeVars)
                newVars                = S.difference 
                                                (S.difference activeVars freeVars)                    
                                                (S.fromList $ fmap fst cleanSystem)
                firstNewVar            = head $ S.toList newVars
                newSystem              = (firstNewVar,eq1):cleanSystem



nullEq eq              = M.null (getCoefs eq) && (getB eq == 0)


solveRowEchFormSystem :: [(MazePosition,Equation)] -> M.Map MazePosition Double
solveRowEchFormSystem [] = M.empty
solveRowEchFormSystem system@((pivotName,eq1): rest) = M.insert pivotName valPosition partialSolution
        where partialSolution        = solveRowEchFormSystem rest
              valPosition            = newB / (getCoefs eq1 M.! pivotName)
              newB                   = (getB eq1) - 
                           (M.foldl (+) 0 $ M.intersectionWith (*) (getCoefs eq1) partialSolution)
                 

solveSystem :: System -> Map MazePosition Double
solveSystem = (solveRowEchFormSystem . fst . classifyVars . eliminate )

eq1 = Equation (M.fromList [((0,1),2)]) 1
eq2 = Equation (M.fromList [((0,0),1),((0,1),1),((0,2),4)]) 6
eq3 = Equation (M.fromList [((0,0),1),((0,1),4),((0,2),1)]) 6

system = [eq1,eq2,eq3]
a = solveSystem system


-- |
-- >>> a
-- fromList [((0,0),3.5),((0,1),0.5),((0,2),0.5)]

readMaze :: String -> System
readMaze string = catMaybes $ (fmap buildEq) everyPos
    where sizes:mapNTunnels   = lines string
          [max_line,max_col,tunnels] = fmap read (words sizes)
          everyPos            = (,) <$> [1..max_line] <*> [1..max_col]
          readPos (x,y)       = mapNTunnels !! (x-1) !! (y-1)
          readTunnel :: String -> [(MazePosition,MazePosition)]
          readTunnel string         = [(p1,p2),(p2,p1)]
                where [a,b,c,d]     = read <$> words string
                      p1            = (a,b) :: MazePosition
                      p2            = (c,d) :: MazePosition
          justTunnels         = ( Prelude.take tunnels . Prelude.drop max_line  ) mapNTunnels
          everyTunnel         = M.fromList $ concat $ Prelude.fmap readTunnel justTunnels
          replaceByTunnelIfApplicable pos = findWithDefault pos pos everyTunnel
          findNeighs pos      = Prelude.fmap replaceByTunnelIfApplicable $
                                Prelude.filter validPoint $
                                       pure add <*> [(1,0),(0,1),(-1,0),(0,-1)] <*> [pos]
          add (x,y) (z,w)     = (x+z,y+w)
          inGrid (x,y)        = (x >= 1) && (y >= 1) && (x <= max_line) && (y <= max_col)
          notObstacle pos     = (readPos pos ) /= '#'
          validPoint (a@(x,y))= inGrid a && notObstacle a
          buildEq pos         
                        | (length $ findNeighs pos) == 0   = Just trapEq
                        | (readPos pos) == '0'             = Just normalEq
                        | (readPos pos) == 'O'             = Just normalEq
                        | (readPos pos) == 'A'             = Just normalEq
                        | (readPos pos) == '*'             = Just mineEq
                        | (readPos pos) == '%'             = Just exitEq
                        | (readPos pos) == '#'             = Nothing
                        | otherwise                        = error "invalidSymbol"
                where neighbors = findNeighs pos
                      probs     = repeat $ 1/ fromIntegral (length neighbors) :: [Double]
                      coefs     :: M.Map MazePosition Double
                      coefs     = M.fromList $ (pos,-1 :: Double):(zip neighbors probs)
                      normalEq  = Equation coefs 0
                      singletonCoefs = M.fromList [(pos,1)]
                      mineEq    = Equation singletonCoefs 0
                      exitEq    = Equation singletonCoefs 1
                      trapEq    = Equation singletonCoefs 0


findFrog :: String -> (Int,Int)
findFrog string = fst $ head $ Prelude.filter isFrog $ fmap (\pos -> (pos, readPos pos)) everyPos
    where sizes:mapNTunnels   = lines string
          [max_line,max_col,tunnels] = fmap read (words sizes)
          everyPos            = pure ((,)) <*> [1..max_line] <*> [1..max_col]
          readPos (x,y)       = mapNTunnels!! (x-1) !! (y-1)
          isFrog  (_,c)       = c == 'A'

solve :: String -> Double
solve string = findWithDefault 0 frogPos allProbs
    where frogPos  = findFrog string
          allProbs = solveSystem $ readMaze string

s1 :: Map MazePosition Double
s1 = solveSystem $ readMaze maze1           
s2 = solveSystem $ readMaze maze2

--    m , n, k, dimensions and number of tunnels
--    # denotes an obstacle.
--    A denotes a free cell where Alef is initially in.
--    * denotes a cell with a mine.
--    % denotes a cell with an exit.
--    O denotes a free cell (which may contain an entrance to a tunnel).

maze1 = "1 5 0\n" ++
        "*OAO%\n"
maze2 = "2 9 0\n" ++
        "###OOO###\n"++
        "*OAO#OOO%\n"

maze3 = "1 11 2\n" ++
        "*OO#OOO#OO%\n"++
        "1 3 1 5\n" ++
        "1 7 1 9\n" 
s3 = solveSystem $ readMaze maze3

maze4 = "3 6 1\n"++
        "###*OO\n"++
        "O#OA%O\n"++
        "###*OO\n"++
        "2 3 2 1\n"

s4 = solveSystem $ readMaze maze4

maze5 = "1 5 0\n"++
        "OO#O%\n"

s5 = solveSystem $ readMaze maze5

mazeBrno = "3 3 0\n"++
           "#000\n"++
           "A0%0\n"++
           "#000\n"

sBrno = solveSystem $ readMaze mazeBrno
  
-- |
-- >>> sBrno
-- fromList [((1,2),0.9999999999999999),((1,3),1.0),((2,1),0.9999999999999998),((2,2),0.9999999999999998),((2,3),1.0),((3,2),0.9999999999999999),((3,3),0.9999999999999999)]


-- B = I + A  + A^2 + A^3 ...
-- AB = A + A^2 + A^3...
-- AB = B-I
-- AB - B = -I
-- (A-I)B = -I
-- (I-A)B = I
-- B = I*((I-A)^-1)
-- B = (I-A)^-1

-- x y z w
-- 2 0 0 5  = 0  z 
-- 0 2 1 3  = 0  (y <-> z)
-- 0 0 0 5  = 0  w = 0




main = interact (show . solve)
