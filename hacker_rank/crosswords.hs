{-# LANGUAGE TupleSections #-}
import Data.Array
import Control.Monad
import Data.List
import Data.Maybe
import qualified Data.Map as M
import Data.List.Split

data Direction = Vertical | Horizontal deriving (Show,Eq)
data Slot = Slot {direction :: Direction, i :: Int, j :: Int, len :: Int} deriving (Show, Eq)

maxIndex :: Int
maxIndex = 10
minIndex :: Int
minIndex = 1

map2Slots :: Array (Int,Int) Char -> [Slot]
map2Slots array = slots
    where isDown  i j = (read i j == '-') && (i == minIndex || read (i-1) j == '+') 
                                          && (i <  maxIndex && read (i+1) j == '-')
          isRight i j = (read i j == '-') && (j == minIndex || read i (j-1) == '+') 
                                          && (j <  maxIndex && read i (j+1) == '-')
          lenDown i j = length $ takeWhile (\ni -> read ni j == '-' ) [i..maxIndex] 
          lenRight i j= length $ takeWhile (\nj -> read i nj == '-' ) [j..maxIndex] 
          allPos = do 
                  i <- [minIndex .. maxIndex]
                  j <- [minIndex .. maxIndex]
                  return (i,j)
          addDown :: (Int,Int) -> Maybe Slot
          addDown (i,j) = do
                             guard $ isDown i j 
                             return (Slot Vertical i j (lenDown i j))
          addRight :: (Int,Int) -> Maybe Slot
          addRight (i,j) = do
                             guard $ isRight i j 
                             return (Slot Horizontal i j (lenRight i j))
          slots = catMaybes $ join [[addDown pos,addRight pos] | pos <- allPos]
                                    
          read   i j = array ! (i,j)

parseMap :: String -> [Slot]
parseMap = map2Slots . listArray ((minIndex,minIndex),(maxIndex,maxIndex)) . filter (/= '\n')

showMap :: IO ()
showMap = interact (show . parseMap)

type PartialPuzzle = M.Map (Int,Int) Char

backTrack :: [String] -> [Slot] -> PartialPuzzle -> Maybe PartialPuzzle
backTrack (word:words) slots partial = head $ (filter isJust tests) ++ [Nothing]
    where goodSlots = filter (\slot -> fits word slot partial) slots
          -- TODO here there be dragons
          tests     = map step goodSlots
          step slot = backTrack words (slots `clear` slot) (addToPartial word slot partial)
          clear list elem = filter (/= elem) list
          addToPartial word slot partial = M.union 
                                           (M.fromList $ wordPos word slot) 
                                           partial


backTrack [] _ partial = Just partial

wordPos :: String -> Slot -> [((Int, Int),Char)]
wordPos string (Slot Horizontal i j length) = zip validPos string
    where validJs  = [j..j+length-1]
          validPos = map (i,) validJs

wordPos string (Slot Vertical i j length) = zip validPos string
    where validIs  = [i..i+length-1]
          validPos = map (,j) validIs


fits :: String -> Slot -> PartialPuzzle -> Bool
fits word slot partial = rightSize && noColision
    where rightSize   = length word == len slot
          noColision  = and $ zipWith compatible puzzleAsPos (map snd wordAsPos)
          wordAsPos   = wordPos word slot
          positions   = map fst wordAsPos
          puzzleAsPos = map (partial M.!?) positions
          compatible (Just c1) c2 
                      = c1 == c2
          compatible Nothing c2   
                      = True

puzzleRange :: [Int]
puzzleRange = [minIndex..maxIndex]

showPuzzle :: Maybe PartialPuzzle -> String
showPuzzle (Just partial)= unlines $ map line listOfPairs
    where listOfPairs    = do i <- puzzleRange
                              return [(i,j) | j <- puzzleRange]
          line pairs     = map pos2Char pairs
          pos2Char (i,j) 
              | Just c <- partial M.!? (i,j) = c
              | otherwise                    = '+'
showPuzzle Nothing = error "shite"

a :: [String]
a = splitOn ";" "bananas;melonas;yeah"
-- |
-- >>> a
-- ["bananas","melonas","yeah"]

countries = splitOn ";"

restAndLast :: String -> (String,String)
restAndLast string =  (unlines rest, last)
    where list = lines string
          rest = reverse $ tail $ reverse list
          last = head $ reverse list 

main = do
       puzzleString <- getContents
       let (puzzleMap,cities) = restAndLast puzzleString
       putStr $ showPuzzle $ backTrack (splitOn ";" cities) (parseMap puzzleMap) M.empty
    --    print $ parseMap puzzleMap
