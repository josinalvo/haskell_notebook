
import Control.Monad.State
import Data.Maybe
import Data.Sequence hiding (length, update, filter)

data Tree = Leaf {val :: Integer, start :: Int , end:: Int} |
            Branch {val :: Integer, start :: Int , end:: Int, left :: Tree, right :: Tree} deriving Show

mkTree :: [Integer] -> Tree
mkTree list = aux (fromList list) 0 (length list - 1)
    where --aux s f | trace ("ola seno" ++ show s ++ "--" ++ show f) False = undefined
          aux seq start finish
            | start == finish = Leaf (index seq start) start start
            | otherwise = Branch calculated start finish leftTree rightTree
            where size       = (finish - start + 1)
                  sizeLeft   = (size `div` 2)
                  midPoint   = start + sizeLeft - 1
                  leftTree   = aux seq start midPoint
                  rightTree  = aux seq (midPoint+1) finish
                  calculated = lcm (val leftTree) (val rightTree)


-- |
-- >>> a
-- Branch {val = 60, start = 0, end = 4, left = Branch {val = 6, ... Branch {val = 20...

query :: Tree -> Int -> Int -> Integer
query tree startQ endQ 
    | endQ == end tree && startQ == start tree = val tree
    | startQ >= start rightTree  = query rightTree startQ endQ 
    | endQ   <= end   leftTree   = query leftTree  startQ endQ 
    | otherwise                  = lcm (query leftTree startQ $ end leftTree) 
                                       (query rightTree (start rightTree) endQ)
        where leftTree  = left tree
              rightTree = right tree

-- |
-- >>> query a 0 4
-- 60
-- >>> query a 0 2
-- 12
-- >>> query a 0 0
-- 2
-- >>> query a 1 4
-- 60
-- >>> query a 2 4
-- 20
-- >>> query a 3 4
-- 10

update :: Tree -> Int -> Integer -> Tree
update l@(Leaf{val = oldVal}) pos factor = l{val = oldVal*factor}
update l@(Branch{left = leftTree, right = rightTree}) pos factor = newTree
    where newTree = if pos <= end leftTree then l{left  = newLeft , val = lcm (val newLeft) (val rightTree)}
                                           else l{right = newRight, val = lcm (val leftTree) (val newRight)}
          newLeft = update leftTree  pos factor
          newRight= update rightTree pos factor

a = mkTree [2,3,4,5,10]

-- |
-- >>> a' = update a 2 2
-- >>> query a' 0 2
-- 24
-- >>> query a' 0 4
-- 120
-- >>> query a 0 2
-- 12

data Command =   Query  {from :: Int, to    :: Int}
               | Update {pos  :: Int, factor :: Integer}

runCommand :: Command -> State Tree (Maybe Integer)
runCommand (Query from to)     = state $ \tree -> (Just $ query tree from to, tree)
runCommand (Update pos factor) = state $ \tree -> (Nothing, update tree pos factor)

readInput :: String -> Tree
readInput = mkTree . map read . words

readCommands :: String -> State Tree [Integer]
readCommands commandString = do
                             results <- sequence oneStatePerLine
                             return $ map (`mod` (10^9+7)) $ map fromJust $ filter isJust results
    where oneListPerLine         = map words $ lines commandString
          oneCommandPerLine      = map list2command oneListPerLine
          oneStatePerLine        = map runCommand oneCommandPerLine
          list2command ["U",a,b] = Update (read a) (read b)
          list2command ["Q",a,b] = Query  (read a) (read b)

main = do
    getLine
    start <- readInput <$> getLine
    getLine
    commands <- readCommands <$> getContents 
    putStr $ unlines . (map show) $ evalState commands start
        
