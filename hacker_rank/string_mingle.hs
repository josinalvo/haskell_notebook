

mingle :: [Char] -> Int -> [Char] -> Int-> [Char]
mingle a    la    ""  lb    = a
mingle ""   la    b   lb      = b
mingle a@(a1:as) la b@(b1:bs) lb = if la >= lb
                                   then (a1: mingle as (la-1) b lb)
                                   else (b1: mingle a la bs (lb-1))


main = do
    a <- getLine 
    b <- getLine 
    putStrLn $ mingle a (length a) b (length b)