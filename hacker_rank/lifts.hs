import Control.Applicative

myLiftA3 :: Applicative f => (a1 -> a2 -> a3 -> b) -> f a1 -> f a2 -> f a3 -> f b
myLiftA3 f l1 l2 l3 = pure f <***> l1 <***> l2 <***> l3

f a b c = a+b+c

(<***>) :: Applicative f => f (a -> c) -> f a -> f c
(<***>) lf la = liftA2 ($) lf la 

-- | 
-- >>> myLiftA3 f (ZipList [1,2,3]) (ZipList [1,2,3]) (ZipList [1,2,3])
-- ZipList {getZipList = [3,6,9]}

