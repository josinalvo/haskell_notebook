{-# OPTIONS_GHC -Wno-unrecognised-pragmas #-}
{-# HLINT ignore "Eta reduce" #-}
import Data.Sequence (Seq, fromList, index, insertAt, update, foldMapWithIndex)

data Sets = Sets {parents :: Seq Int, weights :: Seq Int } deriving Show

start :: Int -> Sets
start n = Sets {parents = fromList [0..n], weights = fromList $ take (n+1) [1,1..]}

union :: Int -> Int -> Sets -> Sets
union v1 v2 sets@(Sets parents weights) = merged
    where s1 = find sets v1
          s2 = find sets v2
          totalWeight = weights `index` s1 + weights `index` s2
          (small,big) = orderBySize s1 s2
          merged = if small == big 
                      then sets
                      else Sets newParents newWeights
          parentsToChange = [(a,big) | a <- toChangeRoot]
          newParents      = foldr (uncurry update) parents parentsToChange 
          newWeights      = update big totalWeight weights
          toChangeRoot = (pathToRoot sets v1) ++ (pathToRoot sets v2)
          orderBySize s1 s2 = if weights `index` s1 >= weights `index` s2 
                                 then (s2,s1) else (s1,s2)

find :: Sets -> Int -> Int 
find sets@(Sets parents _) elem = if parent == elem
                                then elem
                                else find sets parent
                        where parent = parents `index` elem

pathToRoot :: Sets -> Int -> [Int]
pathToRoot  sets@(Sets parents _) elem = if parent == elem
                                         then [elem]
                                         else elem : (pathToRoot sets parent)
                        where parent = parents `index` elem

getInt :: IO Int
getInt = read <$> getLine

string2edge :: String -> (Int,Int)
string2edge s = (read s1, read s2)
    where [s1,s2] = words s

solve :: Int -> [(Int,Int)] -> Sets
solve vertexCount edges = endSets
    where startSets = start vertexCount
          endSets   = foldr (uncurry union) startSets edges

sizes :: Sets -> [Int]
sizes (Sets parents weights) = rootWeights
    where roots :: [Int]
          roots = filter (/= 0) $ valEqIndex parents
          checkWeight :: Int -> Int
          checkWeight = index weights
          rootWeights = map checkWeight roots

valEqIndex :: Seq Int -> [Int] 
valEqIndex s = foldMapWithIndex like s
    where like a b = if a == b then [a] else []

cost :: [Int] -> Int
cost list = sum (map (ceiling . sqrt . fromIntegral) list)

main :: IO ()
main = do
    vertexCount <- getInt
    edgeCount   <- getInt
    a           <- lines <$> getContents
    let edges = fmap string2edge a
    print $ cost . sizes $ solve vertexCount edges

    
