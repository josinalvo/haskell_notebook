import Data.Array
import Data.Foldable (foldr', foldl')
import Data.Array.IO
import Control.Applicative
import Control.Monad

data Sets = Sets {parents :: IOArray Int Int, weights :: IOArray Int Int } 

start :: Int -> IO Sets
start n = do 
              parents <- newListArray (1, n) [1..n]
              weights <- newListArray (1,n) [1,1..]
              return $ Sets {parents = parents, weights = weights}

union :: Int -> Int -> Sets -> IO Sets
union v1 v2 sets@(Sets parents weights) = 

    do    s1 <- find sets v1
          s2 <- find sets v2
          totalWeight <- liftA2 (+) (readArray weights s1) (readArray weights s2)
          (small,big) <- orderBySize s1 s2
          toChangeRoot <- liftA2 (++) (pathToRoot sets v1) (pathToRoot sets v2)
          let parentsToChange = [(a,big) | a <- toChangeRoot]
          let weightsToChange = [(big,totalWeight)]
          if small == big 
                      then return sets
                      else do
                           sequence $ map (uncurry  $ writeArray parents) parentsToChange
                        --    join $ print <$> getAssocs parents -- que carai
                           writeArray weights big totalWeight
                           return sets
                        --    Sets (parents // parentsToChange) (weights // weightsToChange)
          where
              orderBySize :: Int -> Int -> IO (Int,Int) 
              orderBySize s1 s2 = do
                                  w1 <- readArray weights s1
                                  w2 <- readArray weights s1
                                  if w1 >= w2
                                  then return (s2,s1) else return (s1,s2)

find :: Sets -> Int -> IO Int 
-- find sets@(Sets parents _) elem = do
--                                   parent <- mparent
--                                   if parent == elem
--                                   then return elem
--                                   else find sets parent
--                         where mparent = readArray parents elem
-- IDK why this is bad
find sets@(Sets parents _) elem
  = do
    parent <- readArray parents elem
    if parent == elem
      then return elem
      else do
        root <- find sets parent
        writeArray parents elem root
        return root

pathToRoot :: Sets -> Int -> IO [Int]
pathToRoot  sets@(Sets parents _) elem = do parent <- mparent
                                            if parent == elem 
                                            then return [elem]
                                            else fmap (elem :) (pathToRoot sets parent) -- WOW
                        where mparent = readArray parents elem
getInt :: IO Int
getInt = read <$> getLine

string2edge :: String -> (Int,Int)
string2edge s = (read s1, read s2)
    where [s1,s2] = words s

solve :: Int -> [(Int,Int)] -> IO Sets
solve vertexCount edges = 
    do    startSets <- start vertexCount
          sequence $ map (\(x,y) -> union x y startSets) edges
          return startSets

sizes :: Sets -> IO [Int]
sizes (Sets parents weights) = rootWeights
    where mroots :: IO [Int]
          mroots = do
                  pairs <- getAssocs parents
                  return $ map fst $ filter (uncurry (==)) pairs
          checkWeight :: Int -> IO Int
          checkWeight = readArray weights
        --   rootWeights = join $ fmap (sequence . map checkWeight) roots :: IO [Int]
          rootWeights = do
              roots <- mroots
              sequence $ map checkWeight roots
              


cost :: [Int] -> Int
cost list = sum (map (ceiling . sqrt . fromIntegral) list)

main :: IO ()
main = do
    vertexCount <- getInt
    edgeCount   <- getInt
    a           <- lines <$> getContents
    let edges = fmap string2edge a
    endSets  <- solve vertexCount edges
    endSizes <- sizes endSets
    print $ cost endSizes
    -- print $ cost . sizes $ solve vertexCount edges
    
