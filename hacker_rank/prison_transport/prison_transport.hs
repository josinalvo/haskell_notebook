import Data.Array
import Data.Foldable (foldr', foldl')

data Sets = Sets {parents :: Array Int Int, weights :: Array Int Int } deriving Show

start :: Int -> Sets
start n = Sets {parents = listArray (1, n) [1..n], weights = listArray (1,n) [1,1..]}

union :: Int -> Int -> Sets -> Sets
union v1 v2 sets@(Sets parents weights) = merged
    where s1 = find sets v1
          s2 = find sets v2
          totalWeight = weights ! s1 + weights ! s2
          (small,big) = orderBySize s1 s2
          merged = if small == big 
                      then sets
                      else Sets (parents // parentsToChange) (weights // weightsToChange)
          parentsToChange = [(a,big) | a <- toChangeRoot]
          weightsToChange = [(big,totalWeight)]
          toChangeRoot = (pathToRoot sets small) ++ (pathToRoot sets big)
          orderBySize s1 s2 = if weights ! s1 >= weights ! s2 
                                 then (s2,s1) else (s1,s2)

find :: Sets -> Int -> Int 
find sets@(Sets parents _) elem = if parent == elem
                                then elem
                                else find sets parent
                        where parent = parents ! elem

pathToRoot :: Sets -> Int -> [Int]
pathToRoot  sets@(Sets parents _) elem = if parent == elem
                                         then [elem]
                                         else elem : (pathToRoot sets parent)
                        where parent = parents ! elem

getInt :: IO Int
getInt = read <$> getLine

string2edge :: String -> (Int,Int)
string2edge s = (read s1, read s2)
    where [s1,s2] = words s

solve :: Int -> [(Int,Int)] -> Sets
solve vertexCount edges = endSets
    where startSets = start vertexCount
          endSets   = foldr (uncurry union) startSets edges

sizes :: Sets -> [Int]
sizes (Sets parents weights) = rootWeights
    where roots :: [Int]
          roots = map fst $ filter (uncurry (==)) $ assocs  parents
          checkWeight :: Int -> Int
          checkWeight = (weights !)
          rootWeights = map checkWeight roots

cost :: [Int] -> Int
cost list = sum (map (ceiling . sqrt . fromIntegral) list)

main :: IO ()
main = do
    vertexCount <- getInt
    edgeCount   <- getInt
    a           <- lines <$> getContents
    let edges = fmap string2edge a
    print $ cost . sizes $ solve vertexCount edges
    -- print $ solve vertexCount edges
    -- print $ last edges