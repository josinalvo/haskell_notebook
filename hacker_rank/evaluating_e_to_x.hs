import Control.Monad.Except (forM_)
eval :: (Fractional a, Enum a) => a -> a
eval x = sum terms
    where factors = 1 : map (x /) [1..] -- 1 x/1 x/2 x/3
          term n  = (product . take n) factors
          terms   = fmap term [1..10]

-- >>> eval 3
-- [1.0,3.0,4.5,4.5,3.375,2.025,1.0125,0.4339285714285714,0.16272321428571426,5.4241071428571416e-2]


-- #card 1
main = do
    _ <- getLine 
    targets <- getContents
    -- let numbers = map (read :: String -> Double ) $ lines targets
    let numbers = map read $ lines targets -- auto double. Avoid float!
    let results = map (show . eval) numbers
    putStrLn $ unlines results

-- #card 2
main2 :: IO()
main2 = do
    n <- readLn :: IO Int

    forM_ [1..n] $ \_ -> do  --forM is (flip mapM) -- forM_ has type IO ()
        x <- readLn :: IO Double
        print (eval x)