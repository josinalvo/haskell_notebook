import Data.Foldable


prime = 10^9+7

getNum :: [Integer] -> Integer
getNum list = foldl' multMod 1 list
    where multMod x y = (x*y) --`mod` prime

main = do
    getLine
    l1 <- (map read) <$> (words <$> getLine)
    getLine
    l2 <- (map read) <$> (words <$> getLine)
    print $ (`mod` prime) $ gcd (getNum l1) (getNum l2)
