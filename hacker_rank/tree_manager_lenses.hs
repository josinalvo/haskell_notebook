{-# OPTIONS_GHC -Wno-incomplete-patterns #-}
{-# LANGUAGE TemplateHaskell #-}

import Control.Lens hiding (index)
import Control.Lens.Tuple
import Control.Lens.At
import Data.Sequence
import Control.Monad.State
import Data.List hiding (delete)
import Data.Maybe


-- Order was relevant!!! :((

newtype NodeL = NodeL {_values :: Seq (Int,NodeL)} deriving Show
makeLenses ''NodeL

data Piece = Piece {_nodeL :: NodeL, _pos :: Int} deriving Show
makeLenses ''Piece

data ZNode = ZNode {_curr :: NodeL, _stack :: [Piece],
                    _mpos :: Maybe Int} deriving Show
makeLenses ''ZNode


emptyNL = NodeL empty

startZ  = ZNode (NodeL $ singleton (0,emptyNL)) [] (Just 0)

changeValue :: ZNode -> Int -> ZNode
changeValue zipp@ZNode{_mpos = Nothing} newVal = zipp{_curr = NodeL $ singleton (newVal,emptyNL), 
                                                      _mpos  = Just 0}
changeValue zipp@ZNode{_mpos = (Just pos)} newVal = newZipp
            where newZipp = over (curr . values) (update pos (newVal,oldChildSeq)) zipp
                  (oldVal, oldChildSeq) = (`index` pos) $ view (curr . values) zipp

visitLeft :: ZNode -> ZNode
visitLeft zip@ZNode{_mpos=(Just 0)}   = error "nothing to the left"
visitLeft zip@ZNode{_mpos=Nothing}    = error "left of nothing"
visitLeft zip                         = over mpos (fmap $ \n -> n-1) zip

visitRight :: ZNode -> ZNode
visitRight zip@ZNode{_mpos=Nothing}   = error "right of nothing"
visitRight zip                        = over mpos (fmap (+1)) zip
--TOMAYBE: detect wrong right



visitParent :: ZNode -> ZNode
visitParent ZNode{
                    _curr=childSeq,
                    _stack=(top:rest)} = ZNode{_curr=newSeq, _stack=rest, _mpos=Just (_pos top) }
    where 
           newTop       = set (nodeL.values. (ix $ _pos top). _2) childSeq top
           newSeq       = _nodeL newTop  

visitParent ZNode{_stack=[]} = error "went up on empty parent"

visitChild :: ZNode -> Int -> ZNode
visitChild zip@ZNode{_curr= c@(NodeL oldSeq), _stack=oldStack, _mpos = (Just oldPos)} target = ZNode newCurr (newTopPiece:oldStack) newMPos
    where newTopPiece = Piece c oldPos
          newMPos     = Just target
          newCurr     = snd $ index oldSeq oldPos
visitChild zip@ZNode{_mpos = Nothing} target = error "going down on undefined node"

-- -- new = (a.b.c := 12)
-- -- weird idea

insertLeft :: ZNode -> Int -> ZNode
insertLeft zip@ZNode{_mpos=(Just pos), _curr= NodeL oldSeq} val = 
                                             zip{_curr=NodeL newSeq, _mpos = Just $ pos+1}
    where newSeq = insertAt pos (val,emptyNL) oldSeq
insertLeft zip@ZNode{_mpos=Nothing} _ = error "insert on left of empty"

insertRight :: ZNode -> Int -> ZNode
insertRight zip@ZNode{_mpos=(Just pos), _curr= NodeL oldSeq} val = zip{_curr=NodeL newSeq}
    where newSeq = insertAt (pos+1) (val,emptyNL) oldSeq
insertRight zip@ZNode{_mpos=Nothing} _ = error "insert on right of empty"



-- view (segmentEnd . positionY) testSeg                  
-- set (segmentEnd.positionY) 9 testSeg
-- over (segmentEnd.positionY) (*2) testSeg
insertChild :: ZNode -> Int -> ZNode
insertChild zip@ZNode{_mpos=(Just pos)} val = newZip
    where 
          oldChild = view (curr . values . ix pos . _2 . values) zip
          newChildSeq = insertAt 0 (val,emptyNL) oldChild
          newZip = set (curr . values . ix pos . _2 . values) newChildSeq zip

insertChild zip@ZNode{_mpos=Nothing} _ = error "insert on child of empty"

delete :: ZNode -> ZNode
delete z@ZNode {_curr=(NodeL curr), _mpos=(Just pos)} = visitParent zipperWithRemoved
    where withRemoved       = deleteAt pos curr
          zipperWithRemoved = z{_curr=NodeL withRemoved}

printZ :: ZNode -> Int
printZ ZNode {_curr=(NodeL curr), _mpos=(Just pos)} = fst $ index curr pos
    


sVisitParent :: State ZNode (Maybe Int)
[sDelete, sVisitParent, sVisitRight, sVisitLeft] = map f [delete, visitParent, visitRight, visitLeft]
    where  f :: (ZNode -> ZNode) -> State ZNode (Maybe Int)
           f func = state (\z -> (Nothing, func z))

sVisitChild :: Int ->  State ZNode (Maybe Int)
sVisitChild n = state f
    where f = \z -> (Nothing, visitChild z n)


modifySeno zipperChanger n = state f 
    where f = \z -> (Nothing, zipperChanger z n)

sInsertLeft :: Int ->  State ZNode (Maybe Int)
[sInsertLeft, sInsertRight, sInsertChild, sChange] = map modifySeno [insertLeft, insertRight, insertChild, changeValue]
-- -- modify2 zipperChanger = f
-- --     where f = \n -> state $ \z -> (Nothing, zipperChanger z n)

sPrint :: State ZNode (Maybe Int)
sPrint = state $ \z -> (Just $ printZ z , z)

translate :: String -> State ZNode (Maybe Int)
translate "delete"       = sDelete
translate "visit left"   = sVisitLeft
translate "visit right"  = sVisitRight
translate "visit parent" = sVisitParent
translate "print"        = sPrint
translate str | "change"       `isPrefixOf` str = sChange      (getNum str)
              | "visit child"  `isPrefixOf` str = sVisitChild  (getNum str-1)
              | "insert left"  `isPrefixOf` str = sInsertLeft  (getNum str)
              | "insert right" `isPrefixOf` str = sInsertRight (getNum str)
              | "insert child" `isPrefixOf` str = sInsertChild (getNum str)

getNum :: String -> Int
getNum = read .last . words

-- showBowels :: State ZNode (Maybe Int) -> State ZNode (Maybe Int, ZNode)
-- showBowels s = do
--     val <- s
--     mem <- get
--     return (val,mem)
                

main = do
    _ <- getLine
    stringCommands       <- lines <$> getContents
    let stateCommands    = map translate stringCommands
    -- let stateCommands    = map (showBowels . translate) stringCommands
    let oneStateSolution = sequence stateCommands
    let ans              = runState oneStateSolution startZ
    -- (putStrLn . unlines . map ((++ "\n") . show) ) (fst ans)
    (putStrLn . unlines . map show . relevant ) (fst ans)

relevant :: [Maybe a] -> [a]
relevant = catMaybes 