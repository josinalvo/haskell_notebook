-- {-# LANGUAGE FlexibleInstances #-}
-- {-# LANGUAGE UndecidableInstances #-}
-- Estavam aqui pra tentar fazer uma instancia que funciona com todos os Num

module TypeClasses where

-- class (Eq a) => Num a where é ok
-- mas normalmente nao colocamos restricoes (Eq a) na definicao dos data

-- instance (Eq m) => Eq (Maybe m) where  
--     Just x == Just y = x == y  
--     Nothing == Nothing = True  
--     _ == _ = False  


-- we want all types of the form Maybe m to be part of the Eq typeclass, but only those types where the m (so what's contained inside the Maybe) is also a part of Eq. 

-- class constraint possible in TypeClasses as well
-- class (Eq a) => Num a where  
--     ...    
-- Every num must be an Eq

class YesNo a where
    yesno :: a -> Bool

instance YesNo Int where
    yesno 0 = False
    yesno _ = True

instance (YesNo a) => YesNo (Maybe a) where
    yesno Nothing = False
    yesno (Just a) = yesno a


-- instance (Num a, Eq a) => YesNo a where
--     yesno 0 = False
--     yesno _ = True

-- TODO define to every num
-- |
-- >>> yesno (0 :: Int)
-- False
-- >>> yesno (15 :: Int)
-- True
-- >>> yesno (Just (10 :: Int))
-- True
-- >>> yesno (Just (0 :: Int))
-- False
-- >>> yesno (Nothing :: Maybe Int)
-- False

