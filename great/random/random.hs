import System.Random

data Phalanx = Greek | Roman | Green deriving(Show,Enum)
-- TODO: There should be a way to make that work with random


a = random (mkStdGen 100) :: (Int, StdGen)
five_randos = take 5 $ randoms (mkStdGen 11) :: [Int] 

-- | copy of randoms, the generator of lists of random numbers -- test not running , import problems, but line should be correct
-- >>> (take 5 $ randoms (mkStdGen 10)) == (take 5 $ randoms' (mkStdGen 10))
-- True


randoms' :: StdGen -> [Int]
randoms' gen = map fst $ random_ts gen

random_ts :: StdGen -> [(Int,StdGen)]
random_ts gen = tuples
    where first_tuple = random $ gen
          tuples = first_tuple : (random_ts $ snd first_tuple)

randoms'' :: (RandomGen g, Random a) => g -> [a]  
randoms'' gen = let (value, newGen) = random gen in value:randoms'' newGen
--better let from the tutorial


-- there exists a randomR for range (say, 1 to 6 pips of a die). And randomRs for a list of dice

-- there is an IO action getStdGen to get a good seed (same for every call in execution, beware)
-- you can use an infinite list, or newStdGen to change the seed (as a standalone command, to be followed by a new getStdGen, or as a provider itself (???) both seem to work afaik)

-- If you don't want your program to crash on erronous input, use reads, which returns an empty list when it fails to read a string. When it succeeds, it returns a singleton list with a tuple that has our desired value as one component and a string with what it didn't consume as the other.


