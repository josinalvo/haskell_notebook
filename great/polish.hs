module Polish where


type Stack = [Int]

polish' :: String -> Stack -> Stack
polish'   "*" (x:y:xs) = (x*y):xs
polish'   "+" (x:y:xs) = (x+y):xs
polish'   "/" (x:y:xs) = (y `div` x):xs
polish'   "-" (x:y:xs) = (y-x):xs
polish'   num stack    = (read num :: Int):stack

polish'' :: Stack -> String -> Stack
polish'' = flip polish'

polish :: String -> Int
--polish string = head $ foldl (polish'') [] $ words string
polish string = head (foldl polish'' [] (words string))

-- |
-- >>> polish "12 13 +"
-- 25
-- >>> polish "12 13 + 2 *"
-- 50
-- >>> polish "12 13 + 5 /"
-- 5
-- >>> polish "12 12 - 5 /"
-- 0
    
main = print 42