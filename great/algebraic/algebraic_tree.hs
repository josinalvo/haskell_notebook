module Tree where


nums = [8,6,4,1,7,3,5]  
data Tree a = Tree (Tree a) (Tree a) a | EmptyTree deriving Show-- Type of the inner trees!

treeInsert :: (Ord a) => (Tree a) -> a -> (Tree a)
treeInsert EmptyTree a = Tree EmptyTree EmptyTree a
treeInsert (Tree tl tr root) a = if a > root
                            then Tree tl (treeInsert tr a) root
                            else Tree (treeInsert tl a) tr root


numsTree = foldr (flip treeInsert) EmptyTree nums  

treeFind :: (Ord a) => (Tree a) -> a -> Bool
treeFind EmptyTree _ = False
treeFind (Tree tl tr root) a
    | (a == root) = True
    | (a > root)  = treeFind tr a
    | (a < root)  = treeFind tl a

-- | test trees
-- >>> treeFind numsTree 8
-- True
-- >>> treeFind numsTree 10
-- False
-- >>> treeFind numsTree 5
-- True

-- instance Functor Maybe where
--      fmap f (Just x) = Just (f x)
--      fmap f Nothing = Nothing

instance Functor Tree where
    fmap f (Tree tl tr val) = Tree (fmap f tl) (fmap f tr) (f val)
    fmap f EmptyTree = EmptyTree

-- | test map
-- >>> j = fmap (*10) numsTree
-- >>> treeFind j 5
-- False
-- >>> treeFind j 50
-- True

