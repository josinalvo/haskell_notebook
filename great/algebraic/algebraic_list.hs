module List where


-- A fixity states how tightly the operator binds and whether it's left-associative or right-associative. For instance, *'s fixity is infixl 7 * and +'s fixity is infixl 6. That means that they're both left-associative (4 * 3 * 2 is (4 * 3) * 2) but * binds tighter than +, because it has a greater fixity, so 5 * 4 + 3 is (5 * 4) + 3.
infixr 5 :-:  -- lists are a good example of infixr!!
data List a = a :-: List a | Empty

instance (Show a) => Show (List a) where
    show Empty = "[]"
    show (a :-: as) = "[" ++ show(a) ++ ", " ++ show'(as)
        where show' (a :-: Empty) = show(a) ++ "]"
              show' ( a :-: as) = show(a) ++ ", " ++ show'(as)

infixr 5 .++
(.++) :: List a -> List a -> List a              
(.++) (a :-: as) list2 = a :-: as .++ list2
(.++) Empty list2 = list2

-- |
-- >>> a = 12 :-: 13 :-: Empty
-- >>> a
-- [12, 13]
-- >>> 15 :-: a
-- [15, 12, 13]
-- >>> a .++ a
-- [12, 13, 12, 13]