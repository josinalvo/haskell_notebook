module MyEither where

import qualified Data.Map as Map

type Code = String
type InUse = Bool
type LockerId = Int
type Locker = (InUse,Code)
type LockerMap = Map.Map LockerId Locker
type Error = String

a :: LockerMap
a = Map.fromList [(1,(False, "123")),(2,(True,"456"))]

-- | gets locker code if available, error if locker does not exist or is taken
-- >>> getLocker a 3
-- Left "no such locker"
-- >>> getLocker a 2
-- Left "locker taken"
-- >>> getLocker a 1
-- Right "123"
getLocker :: LockerMap -> LockerId -> (Either Error Code)
getLocker map id = case found of
    Nothing -> Left "no such locker"
    Just (True,_) -> Left "locker taken"
    Just (False,code) -> Right code
    where found = Map.lookup id map
