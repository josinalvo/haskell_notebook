module GreatModules where
import Data.List hiding (nub)

data Point = Point Float Float deriving(Show)
data Shape = Circle Point Float | Rectangle Point Point deriving(Show)


-- |area of a shape
-- >>> area (Rectangle (Point 1 1) (Point 5 5))
-- 16.0
-- >>> area $ Circle (Point 1 1) 10
-- 314.15927

area :: Shape -> Float
area (Circle _ radius) = radius^2 * pi
area (Rectangle (Point x1 y1) (Point x2 y2)) = abs (x1-x2) * abs (y1-y2)


-- | cars can be construted using currying. Imagine. Maps!
-- >>> c
-- Car {company = "ford", model = "mustang", year = 1980}


data Car = Car {company :: String, model :: String, year :: Int} deriving (Show)

a = Car "ford"
b = a "mustang"
c = b 1980

data MyMaybe a = MyNothing | MyJust a
-- MyMaybe constructs the type, Nothing and Just a construct values (have I seen the expression data constructor before?)

-- | types and data constructors
-- >>> :t Just "me"
-- Just "me" :: Maybe [Char]

-- Instead of adding a typeclass constraint (eg, Ord k) in the data declaration, it is considered better to add it only on the functions that need it

-- module Shapes   
-- ( Point(..)  
-- , Shape(..)  
-- , surface  
-- , nudge  
-- , baseCircle  
-- , baseRect  
-- ) where  

-- estamos definindo o que exportar, em particular Shape(..) exporta todos os value constructors -- Poderia ser Shape(Rectangle, Circle)

data Vector a = Vector a a a deriving (Show) 
-- ao declarar tipos passo apenas um argumento
-- |
-- >>> Vector 1 2 3 :: Vector Int
-- Vector 1 2 3

-- what is the problem with read "Just 't'" :: Maybe a
-- Maybe Char!

data Day = Monday | Tuesday | Wednesday | Thursday | Friday | Saturday | Sunday   
           deriving (Eq, Ord, Show, Read, Bounded)

days = [Monday, Tuesday, Wednesday, Thursday, Friday, Saturday, Sunday]

instance Enum Day where
    fromEnum day = n 
       where Just n = elemIndex day days
    toEnum n = days !! ((n+7) `rem` 7)

-- | finds a value associated with k
-- >>> tels = [("lucas",2),("cicero",3)]
-- >>> findKey "lucas" tels
-- Just 2

findKey :: (Eq k) => k -> [(k,v)] -> Maybe v
findKey key list = foldr aux Nothing list
       where aux (k,v) rest = if k == key
                              then Just v
                              else rest

