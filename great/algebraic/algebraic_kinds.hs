module Kinds where

a=2

-- |
-- >>> :k Int
-- Int :: *

-- A star means a concrete type

-- |
-- >>> :k Maybe
-- Maybe :: * -> *

-- takes a concrete type and produces a concrete type

-- |
-- >>> :k Maybe Int
-- Maybe Int :: *

-- took it

-- note that this is a type constructor, not Just | Nothing, the value constructors

-- |
-- >>> :k Either
-- Either :: * -> * -> *
-- >>> :k Either Int
-- Either Int :: * -> *
-- >>> :k Either Int String
-- Either Int String :: *

-- When we looked at functor
--     class Functor f where   
--         fmap :: (a -> b) -> f a -> f b  
-- see that  it wants a type of kind * -> *

class Tofu t where  
    tofu :: j a -> t a j  
-- we dont receive "j" and "a", but "j a"
-- ja is concrete
-- j is (*->*); and a is * (well, a bit of a guess here, right?)
-- t * -> (*->*) -> *

data Frank a b = Frank (b a) deriving Show
-- recebi um tipo concreto e um (* -> *), e produzi um (*). Sou do tipo * -> (* -> *) -> *

instance Tofu Frank where
     tofu x = Frank x

-- Se eu receber um Just 'a', vou chamar o data (value?) constructor Frank, que vai producir um Frank do tipo char maybe

t_frank = tofu (Just 'a') :: Frank Char Maybe

data Barry  p t k = Barry { yabba :: p, dabba :: t k }  deriving Show


-- |
-- >>> fmap (+1) $ Barry 2 [1,2,3]
-- Barry {yabba = 2, dabba = [2,3,4]}
instance (Functor b) => Functor (Barry a b) where
    fmap f Barry {yabba = x, dabba = listoid} = Barry { yabba = x, dabba = fmap f listoid}


