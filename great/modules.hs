module GreatModules where



-- import Data.List (nub, sort)  
import Data.List hiding (nub)
-- import qualified Data.Map as M (that is, call only with M.funcion)
import qualified Data.Map as Map
-- | Intercala pontos
--
-- Examples
-- >>> dotit "MONKEY"
-- "M.O.N.K.E.Y"
dotit str = intersperse '.' str

-- | Isso é um abuso, pois nao tem funcao 
-- >>> b
-- "MONKEY.BALLS"
b = intercalate "." ["MONKEY","BALLS"] -- list and list of lists

-- | 
-- >>> c
-- "TERRIBLE.BALLS"
c = intercalate "." ["TERRIBLE","BALLS"] -- list and list of lists

-- |
-- soma de coeficientes de polinomios
-- >>> d
-- [18,8,6,17]
d = map sum $ transpose [[0,3,5,9],[10,0,0,9],[8,5,1,-1]]

-- | 
-- concat achata lista
-- >>> e
-- "eu tenho a forca cavaleiro de jedi"
e = concat ["eu"," ","tenho a forca cavaleiro de jedi"]

-- | faz map depois concat
-- >>> concatMap' (replicate 4) [1..3]
-- [1,1,1,1,2,2,2,2,3,3,3,3]
(...) = (.).(.)
concatMap' = concat ... map

-- or and for bool lists

-- |
-- >>> any (==4) [1,2,3,4]
-- True

-- |
-- iterate faz x, f(x), f$f(x) f$f$f(x)
-- >>> take 3 $ iterate (++ "ha") ""
-- ["","ha","haha"]

-- | takeWhile and dropWhile return complements
-- >>> takeWhile (/=' ') "This is a sentence"
-- "This"
-- >>> dropWhile (/=' ') "This is a sentence"
-- " is a sentence"
-- >>> span (/=' ') "This is a sentence"
-- ("This"," is a sentence")
-- >>> break (==' ') "This is a sentence"
-- ("This"," is a sentence")

a1 = [1,2,2,2,3,1,1]
-- | 
-- lambda com pattern match, conta ocorrencias
-- >>> map (\(x:xs) -> (x, length $ x:xs)) . group . sort $ a1
-- [(1,3),(2,3),(3,1)]

-- inits e tails (lista de todos os prefixos e sufixos)
-- isInfixOf, isPrefixOf, isSuffixOf

-- | partition quebra em 2, recebe predicado e faz (respeita,nao respeita)
-- >>> partition (< 3) [1,2,3,4,1]
-- ([1,2,1],[3,4])

-- | the just refers to a maybe algebraic data type
-- >>> find (>4) [1,2,3,4,5,6]  
-- Just 5  
-- >>> find (>9) [1,2,3,4,5,6]  
-- Nothing  

-- | Queres indice
-- >>> elemIndex 'c' "abcde"
-- Just 2

-- se queres indices, te retorno uma lista sem maybe (elemIndices)
-- Temos findIndex e findIndices

-- | ++ com folds
-- >>> joinl "abc" "def"
-- "abcdef"
joinl l1 l2 = foldr (:) l2 l1

-- | ++ com folds
-- >>> joinl2 "abc" "def"
-- "abcdef"
joinl2 :: [a] -> [a] -> [a]
joinl2 = flip (foldr (:))

-- MAPS
-- |
-- >>> m = Map.insert 5 600 $ Map.insert 4 200 $ Map.insert 3 100 $ Map.empty
-- >>> m
-- fromList [(3,100),(4,200),(5,600)]
-- >>> m2 = fromList' [("lucas","verde"),("cicero","azul")]
-- >>> Map.lookup "cicero" m2
-- Just "azul"

-- | Functions to join repetitions
-- >>> m = Map.fromListWith (+) [(2,3),(2,5),(2,100),(4,22),(4,15)]
-- >>> m
-- fromList [(2,108),(4,37)]
-- >>> Map.insertWith (-) 2 100 m
-- fromList [(2,-8),(4,37)]

fromList' :: (Ord k) => [(k,v)] -> Map.Map k v -- TIPO?
fromList' list = foldr aux Map.empty list
                    where aux (k,v) m = Map.insert k v m
