-- User Either or Maybe to show that code might have failed. We will not see exceptions for pure code today

-- We can't just use doesFileExist in an if expression directly. We need an arrow, so that we can beware

import System.IO.Error
import Control.Exception

main = do
    addBanana `catch` notExist `catch` noPermission

notExist e 
    | isDoesNotExistError e = putStrLn "arquivo nao existe"
    | otherwise = ioError e

noPermission e 
    | isPermissionError e = putStrLn "NAO PODJE"
    | otherwise = ioError e

addBanana = appendFile "banana" "banana\n"
