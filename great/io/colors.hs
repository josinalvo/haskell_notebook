import Control.Monad  
      
main = do   
    colors <- mapM (\a -> do  
        putStrLn $ "Which color do you associate with the number " ++ show a ++ "?"  
        --color <- getLine  
        --return color
        getLine)  [1,2,3,4] 
    --the IO action above returns a list
    putStrLn "The colors that you associate with 1, 2, 3 and 4 are: "  
    mapM putStrLn colors  

--Don't think of a function like putStrLn as a function that takes a string and prints it to the screen. Think of it as a function that takes a string and returns an I/O action. That I/O action will, when performed, print beautiful poetry to your terminal.