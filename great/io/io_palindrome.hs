module Pal where

main = interact judgeAll

-- WISH: doctest + main

judgeLine :: String -> String
judgeLine line = if palindrome line
                 then "palindrome"
                 else "not palindrome"
-- | Checks if string is palindrome
-- >>> palindrome "a"
-- True
-- >>> palindrome "gawag"
-- True
-- >>> palindrome "gawbg"
-- False
-- >>> palindrome "gaag"
-- True

palindrome :: String -> Bool
-- palindrome "" = True
-- palindrome string = if first /= last
--                     then False
--                     else palindrome middle
--                     where size = length string
--                           first = string!!0
--                           last = string!!(size - 1) 
--                           middle = (drop 1) . (take (size-1)) $ string
palindrome x = x == (reverse x)

judgeAll = unlines . (map judgeLine) . lines

