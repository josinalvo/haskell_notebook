{-# LANGUAGE FlexibleInstances #-}

import System.Environment   
import System.Directory  
import System.IO  
import Data.List 
import Data.Maybe --with this I get isNothing, that is better than == Nothing
import System.IO.Error
import Control.Exception

dispatch :: [(String, [String] -> IO())]
dispatch = [("add",add), ("begin",begin), ("bump",bump),
            ("view", view),
            ("remove", (\ args -> (do 
                                        remove args
                                        view args))),
            ("just_remove", remove),
            ("test", test)]

-- was necessary for == Nothing
-- instance Eq ([String] -> IO()) where
--     (==) _       _       = False

main = do
    task:args <- getArgs
    let t = lookup task dispatch
    let (Just task_runner) = t
    -- if t == Nothing
    if isNothing t
       then putStrLn error_no_such_verb
       else (task_runner args) `catch` noFileHandler

-- for some reason pattern matching is not available
noFileHandler e
   | isDoesNotExistError e = putStrLn "file does not exist"
   | otherwise = ioError e

error_no_such_verb = "unknown verb. Available: " ++ options    
    where options = (foldr1 (\ a b -> a++", "++b)). (map fst) $ dispatch


    
test list = putStrLn "mamma mia"

add   (fileName:text:excess:_) = putStrLn "too many arguments. Provide filename and task"
add   (fileName:text:_) = appendFile fileName (text ++ "\n")
add   _ = putStrLn "too few arguments. Provide filename and task"

begin   (fileName:text:excess:_) = putStrLn "too many arguments. Provide filename and task"
begin   (fileName:text:_) = do
    contents <- readFile fileName 
    let ready_text = (text ++ "\n") ++ contents
    writeByTempFile ready_text fileName
begin   _ = putStrLn "too few arguments. Provide filename and task"


view   (fileName:_) = do
    contents <- readFile fileName
    putStr . unlines . (zipWith (\a b -> (show a) ++ ") " ++ b) [0..]) . lines $ contents

removeAt :: Int -> [a] -> [a]
removeAt pos list = start ++ finish
       where start = take (pos) list
             finish = drop (pos+1) list

remove (fileName: line: _)= do
    contents <- readFile fileName
    let clean = unlines . (removeAt . read $ line) . lines $ contents
    writeByTempFile clean fileName

bump (fileName: line: _ ) = do
    contents <- readFile fileName
    let clean = unlines . (removeAt . read $ line) . lines $ contents
    let bumped = (++ "\n") . (!! (read line)) . lines $ contents
    writeByTempFile (bumped++clean) fileName
    
writeByTempFile :: String -> String -> IO()
writeByTempFile text targetFileName = do
    (tempName, tempHandle) <- openTempFile "." "temp"
    hPutStr tempHandle text
    hClose tempHandle
    removeFile targetFileName
    renameFile tempName targetFileName