module SuperExp where

import Data.List 
dispatch = [("add",1),
            ("show", 2),
            ("remove", 2),
            ("test",2)]

-- |
-- >>> lookup "add" dispatch
-- Just 1
-- >>> lookup "addo" dispatch
-- Nothing
-- >>> (lookup "addo" dispatch) == Nothing
-- True
