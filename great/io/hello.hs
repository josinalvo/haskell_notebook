module Hi where
-- Uncommented Module seems necessary for doctest, commented to compile
import Data.Char

-- |
-- >>> :t putStrLn
-- putStrLn :: String -> IO () 
-- >>> :t putStrLn "hello"
-- putStrLn "hello" :: IO ()

-- IO () is an IO action that does not return anything. The () is the empty tuple

-- main = do 
--      foo <- putStrLn "state your (self) designation"
--      designation <- getLine
--      let appelation = map toLower designation
--          vocative = map toUpper appelation
--      putStrLn ("we hope to achieve mutualy beneficial ends together, " ++ vocative)
--      putStr' "hello"
--      print 2

putStr' :: String -> IO ()  
putStr' [] = return ()  
putStr' (x:xs) = do  
    putChar x  
    putStr' xs  

-- main = do  
--     putStrLn "What's your first name?"  
--     firstName <- getLine  
--     putStrLn "What's your last name?"  
--     lastName <- getLine  
--     let bigFirstName = map toUpper firstName  
--         bigLastName = map toUpper lastName  
--     putStrLn $ "hey " ++ bigFirstName ++ " " ++ bigLastName ++ ", how are you?"  

-- |
-- >>> :t getLine
-- getLine :: IO String


-- <- is the construct "open pandoras box and get whatever came there"

-- main = do   
--     line <- getLine  
--     if null line  
--         then return ()  -- then is IO () -- return makes IO out of a pure value
--         else do  -- else must be IO, and we want 2 different actions, whence do
--             putStrLn $ reverseWords line  
--             main  
--
-- reverseWords :: String -> String  
-- reverseWords = unwords . map reverse . words  

-- main = do  
--     return ()  
--     return "HAHAHA"  
--     line <- getLine  
--     return "BLAH BLAH BLAH"
--     a <- return "hi"
--     return 4  
--     putStrLn line  
-- THIS RUNS TO THE END!!!

-- main = do  
--     c <- getChar  
--     when (c /= ' ') $ do  --equiv to if then (do block) else return  ()
--         putChar c  
--         main  

main = do  
    rs <- sequence [getLine, getLine, getLine]  
    print rs
-- WILL PRINT A LIST  

-- | the map just produces a list of IO actions, does not execute it
-- >>> sequence (map print [1,2,3,4]) 
-- 1
-- 2
-- 3
-- 4
-- [(),(),(),()]

-- that last thing is ghci printing the value of the expression
-- though it ommits a simple () -- see also mapM, mapM_