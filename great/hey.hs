import System.IO  
  
-- main = do  
--     handle <- openFile "girlfriend.txt" ReadMode  
--     contents <- hGetContents handle  
--     putStr contents  
--     hClose handle

--     data IOMode = ReadMode | WriteMode | AppendMode | ReadWriteMode  


--withFile :: FilePath -> IOMode -> (Handle -> IO a) -> IO a    
main = do     
    withFile "girlfriend.txt" ReadMode (\handle -> do  
        contents <- hGetContents handle     
        putStr contents)

main = do  
    contents <- readFile "girlfriend.txt"  
    putStr contents  
-- withFile' :: FilePath -> IOMode -> (Handle -> IO a) -> IO a  
-- withFile' path mode f = do  
--     handle <- openFile path mode   
--     result <- f handle  
--     hClose handle  
--     return result  


-- Buffering
-- NoBuffering, LineBuffering or BlockBuffering (Maybe Int)
-- hSetBuffering handle $ BlockBuffering (Just 2048)