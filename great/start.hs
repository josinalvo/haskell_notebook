-- if needs else because it is an expression -- returns a value
    -- he says a definition (like a variable) is just a function 
    -- with 0 parameters

-- listas sao lista ligadas, é facil inserir no inicio
-- !! acesso de posicao
-- listas sao comparadas na ordem lexicográfica
--head tail e seus irmaos caoticos, init e last
-- (last pega o ultimo, init tudo menos o last)

-- null para listas vazias. Length da pau com listas infinitas,
-- == [] da  pau com tipos - == requer listas com elementos
-- comparaveis

--take 3 lista pega os 3 primeiros, drop tira os 3 primeiros

-- [2,4..20] range com passo -- sempre PA, assumida positiva se 
-- nao ser o passo

-- take 24 [13,26..] (24 multiplos de 13)

a=take 10 (cycle [1,2,3])  
--[1,2,3,1,2,3,1,2,3,1] 
b = [x*2 | x <- [1..10]]
c = [x*2 | x <- [1..10], x*2 >= 12]
-- list comprehension
boomBangs xs = [ if x < 10 then "BOOM!" else "BANG!" | x <- xs, odd x] -- function!

-- combinatorial explosion!
d = [ x*y | x <- [2,5,10], y <- [8,10,11]] 


removeNonUppercase st = [ c | c <- st, c `elem` ['A'..'Z']]
-- ghci> let xxs = [[1,3,5,2,3,1,2,4,5],[1,2,3,4,5,6,7,8,9],[1,2,4,2,1,6,3,1,3,2,3,6]]  
-- ghci> [ [ x | x <- xs, even x ] | xs <- xxs]  
-- [[2,2,4],[2,4,6,8],[2,4,2,6,2,6]] 

--tupla: caracterizada por tamanho e por tipos

-- fst snd -- so para tipo tupla

-- ghci> zip [1 .. 5] ["one", "two", "three", "four", "five"]  
-- [(1,"one"),(2,"two"),(3,"three"),(4,"four"),(5,"five")]  

--which right triangle that has integers for all sides and all sides equal to or smaller than 10 has a perimeter of 24?

ans = [(x,y,z)|x <-[1..10], y<-[1..10], z<-[1..10],(x*x+y*y) == z*z,x+y+z == 24, x <= y]
f lst = [(x,y,z)|x <-lst, y<-lst, z<-lst,(x*x+y*y) == z*z,x+y+z == 24, x <= y]
ans2 = f [1..10]
ans3 = (\ lst -> [(x,y,z)|x <-lst, y<-lst, z<-lst,(x*x+y*y) == z*z,x+y+z == 24, x <= y]) [1..10]

--ghci> :t read  
--read :: (Read a) => String -> a  

--read "4" Fails, you need to constrain

-- Enum can be used on list ranges

b1 = minBound :: Int 
b2 = maxBound :: (Bool, Int, Char)

--    ghci> let xs = [(1,3), (4,3), (2,4), (5,3), (5,6), (3,1)]  
-- ghci> [a+b | (a,b) <- xs]  
-- [4,7,6,8,11,4]

-- general pattern matching

-- x: xs only matches lists of 1 or more elements

length' :: (Num b) => [a] -> b  
length' [] = 0  
length' (_:xs) = 1 + length' xs

--all@(x:xs) pattern for the whole -- in all -- and the parts

bmiTell weight height -- no equals sign here :P
    | bmi <= skinny = "You're underweight, you emo, you!"  
    | bmi <= normal = "You're supposedly normal. Pffft, I bet you're ugly!"  
    | bmi <= fat = "You're fat! Lose some weight, fatty!"  
    | otherwise   = "You're a whale, congratulations!"  
    where bmi = weight/height^2
          (skinny, normal, fat) = (18.5, 25.0, 30.0)
-- a guard is a boolean function

max' a b
   | a > b = a
   | otherwise = b

-- the pattern match had to be done by element (receives list of tuples)
calcBmis xs = [bmi w h | (w, h) <- xs]  
    where bmi weight height = weight / height ^ 2  

--where associated with functions - syntax for funcions
-- let -- very local, no need for function
r = 2
h = 2

cylinderArea = let sideArea = 2 * pi * r * h
                   topArea = pi * r^2
               in sideArea + 2 * topArea

high = (let (a,b,c) = (1,2,3) in a+b+c) * 100  

-- RealFloat (Floating is not enougth, does not have, e.g., order
--Counter example: complex numbers)

--ghci> let zoot x y z = x * y + z  
--ghci> zoot 3 9 2  
--29  
--ghci> let boot x y z = x * y + z in boot 3 4 2  
--14  
--ghci> boot  
-- <interactive>:1:0: Not in scope: `boot' -- let was local to in


-- remember the just pattern matching syntax for funcions!
describeList :: [a] -> String  
describeList xs = "The list is " ++ what xs  
        where what [] = "empty."  
              what [x] = "a singleton list."  
              what xs = "a longer list." 

replicate' :: Integral(b) => b -> a -> [a]
replicate' 0      x = []
replicate' amount x = [x] ++ replicate' (amount - 1) x

take' :: Int -> [a] -> [a]
take' 0   list = []
take' n   [] = []
take' n   (x:xs) = [x] ++ (take' (n-1) xs)

take'' :: (Num i, Ord i) => i -> [a] -> [a]  
take'' n _  
    | n <= 0   = []  -- se nao match nenhum guard, vc vai pra prox
take'' _ []     = []  
take'' n (x:xs) = x : take'' (n-1) xs

zip' :: [a] -> [b] -> [(a,b)]
zip'  _  [] = []
zip' []  _  = []
zip' (x:xs) (y:ys) = ((x,y) :) $ zip' xs ys -- duvida besta sobre $

elem' :: Eq(a) => a -> [a] -> Bool
elem' x [] = False
elem' x (y:ys) -- errei: coloquei [] no lugar dos ()
   | x == y = True
   | x /= y = elem' x ys -- tinha esquecido o s tb. tipar mó ajuda

quicksort :: Ord(a) => [a] -> [a]
quicksort [] = []
quicksort (x:xs) = quicksort(small) ++ x:quicksort(large)
                   where large = [y | y <- xs, y >= x]
                         small = [y | y <- xs, y < x]

-- para funcoes infixas, eu posso passar um dos parametros qualquer
--(e eu posso transformar uma funcao em infixa usando ``)

per x y z = x*z/y
always_by_two = (`per` 2)

applyTwice :: (a -> a) -> (a -> a)
applyTwice f  = f . f  

applyTwice' :: (a -> a) -> a -> a --mesma coisa, mas muito mais dificil pra mim entender
applyTwice' f x  = f (f x) 

--Now we're going to use higher order programming to implement a really useful function that's in the standard library. It's called zipWith. It takes a function and two lists as parameters and then joins the two lists by applying the function between corresponding elements. Here's how we'll implement it:

zipWith' :: (a->b->c) -> [a] -> [b] -> [c]
zipWith' f l1 l2 = [(f a b) | (a,b) <- (zip l1 l2)]

-- *Main> zipWith (+) [1,2,3] (take 3 (repeat 10))
-- [11,12,13]
-- *Main> zipWith' (+) [1,2,3] (take 3 (repeat 10))
-- [11,12,13]

zipWith'' :: (a->b->c) -> [a] -> [b] -> [c]
zipWith'' f [] _ = []
zipWith'' f _ [] = []
zipWith'' f (x:xs) (y:ys) = [(f x y)] ++ (zipWith'' f xs ys)

-- zip does not demand equal sizes

--NOTA: bonitissimo, por causa do currying
flip' :: (a->b->c) -> (b->a->c)
-- flip' :: (a->b->c) -> b->a->c
-- flip' :: (a->b->c) -> (b->a->c)
-- flip' :: (a->b->c) -> (b->(a->c))

flip' f x y = f y x
               1
--(flip' f) x y = f y x --versao do seno q tb funciona!
-- flip' f = \x y -> f y x  -- versao mais normal

map' :: (a->b) -> [a] -> [b]
map' _ [] = []
map' f (x:xs) = [f(x)] ++ map' f xs

filter' :: (a->Bool) -> [a] -> [a]
filter' f [] = []
filter' f (x:xs)
    | (f x)     =   x: (filter' f xs)
    | otherwise =  (filter' f xs)

--find the largest number under 100,000 that's divisible by 3829
-- remainder :: (Integral a) => a -> a
remainder = (flip rem 3829)
divisible x = (remainder x) == 0
-- why not? largest :: (Integral a) => a
largest = last (filter divisible [1..100000]) -- you can save a lot of effort if
    -- you just invert the order

largestDivisible :: (Integral a) => a  
largestDivisible = head (filter p [100000,99999..])  
        where p x = x `mod` 3829 == 0  

-- so para poder fazer o seguinte
chain :: (Integral a) => a -> [a]  
chain 1 = [1]  
chain n  
    | even n =  n:chain (n `div` 2)  
    | odd n  =  n:chain (n*3 + 1)

-- quais sao os numeros de 1 a 100 tais que a sequencia de colliatz é maior q 15
cools = filter ((> 15) . length . chain) [1..100]

--find the sum of all odd squares that are smaller than 10,000

filter_and f g x = (f x) && (g x)
compr = sum (filter (odd `filter_and` (< 10000)) [x^2| x <- [1..10000]])

-- could do takeWhile (/=' ') "elephants know how to party" and it would return "elephants"

-- map (\(a,b) -> a + b) [(1,2),(3,5),(6,3),(2,6),(2,5)] 
-- a lambda has only one pattern! Do or fail

sum' :: (Num a) => [a] -> a
sum' = foldl (+) 0 

-- :info Integral reveals implied typeclasses

test :: (Eq a) => a ->Bool -> a ->Bool
test y b x = (x==y) || b

elem'' :: (Eq a) => a -> [a] -> Bool
elem'' y =  foldr (\ x acc -> (x==y) || acc) False 
--elem'' y ys = foldl (\ acc x -> if x == y then True else acc) False ys

--acrescentar no fim é muito mais caro que no começo :(

-- map' f xs = foldl (\ acc x -> f x : acc ) [] xs


data Sopa a b = Tijela a | Cumbuca b

data Sopa' = I Integer |  F Float

f :: (Sopa Integer Float) -> Float
f (Tijela x)  = fromInteger x + 2 
f (Cumbuca x) = x/2

f' :: (Sopa') -> Float
f' (I x)  = fromInteger $ x `div` 2 
f' (F x) = x/2