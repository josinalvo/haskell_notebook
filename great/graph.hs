-- module Graph where

--data Path a = (Num a) => Path { upper:: a, lower::a, acrossB4:: a}
data Path a = Path { upper::a, lower::a, acrossB4::a} deriving Show
data Solution a = Solution {above::(a,String), below::(a,String)} deriving Show

solve :: (Num a, Ord a) => [(Path a)] -> Solution a
solve [] = (Solution (0,"") (0,""))
solve (x:xs) = Solution (min' startAbove2) $ min' startBelow2
    where Solution (valAb, pathAb) (valB, pathB) = solve xs
          startAbove2 = [(sum [upper x,valAb],'a':pathAb),(sum[acrossB4 x, lower x,valB],'b':pathB)]
          startBelow2 = [(sum [lower x,valB],'b':pathB), (sum[acrossB4 x,upper x,valAb],'a':pathAb)]
          min' [(a,b),(c,d)] = if a <= c then (a,b) else (c,d)
          

-- todo read??
-- readInput :: (Num a, Ord a, Read a) => IO([Path a])
-- readInput = do
--     u <- getLine
--     l <- getLine
--     a <- getLine
--     let (u_n, l_n, a_n) = (read u, read l, read a)
--     if a_n == 0
--        then return [(Path u_n l_n a_n)]
--        else do rest <- readInput
--                return $ (Path u_n l_n a_n):rest

groupsOf n [] = []
groupsOf n list = (take n list):groupsOf n (drop n list)


readBetter ::(Num a, Ord a, Read a) => IO([Path a])
readBetter = do
    stdin <- getContents
    let triples = (groupsOf 3) . (map read) . lines $ stdin
        paths = [(Path a b c) | [a,b,c] <- triples]
    return paths


main = do
    list <- readBetter
    (putStrLn . show . solve) list