superdigit [n1,n2] = (`mod` 9) $ (n1 `mod` 9) * (n2 `mod` 9)
clear0 n = if n == 0 then 9 else n


main = interact $ show . clear0 . superdigit . (map read) .words
