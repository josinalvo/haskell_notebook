module MonadReadiness where

import Control.Applicative

-- ($ y) is the function that supplies y as an argument to another function.

-- functors are mappables

-- props: fmap id and fmap (g.h) reasonable

-- fmap for type (->) ?
-- (.) "para toda entrada, fazemos a mudança"

class Functor functor where
  fmap :: (a -> b) -> functor a -> functor b

instance MonadReadiness.Functor ( (->) t) where
  fmap f g = f . g


-- applicative functors allow for functions inside the functor

-- |
-- >>> [(+1),(+2)]<*>[10,20]
-- [11,21,12,22]
-- >>> [(+)]<*>[1,2]<*>[10,20]
-- [11,21,12,22]

-- if you desire, other sintaxes
-- >>> pure (+) <*> [1,2] <*> [10,20]
-- [11,21,12,22]
-- >>> (+) <$> [1,2] <*> [10,20]
-- [11,21,12,22]
-- <$> é a versão infixa do fmap
-- e isso faz sentido, pq tem um teorema fmap f x = pure f <*> x    
-- https://en.wikibooks.org/wiki/Haskell/Applicative_functors

-- | ziplist is another applicative functor "for list"
-- | "" because the type list can only implement typeclass applicative once
-- >>> getZipList $ pure (+) <*> ZipList [1,2,3] <*> ZipList [10,20,30]
-- [11,22,33]

-- na listas, o fmap "pula" os colchetes e aplica a função em todos os elementos
-- nas funções, o fmap "pula" o x -> aplica a função no resultado

-- | applicative instance for functions is analogous to ziplists
-- >>> g = pure (+1) <*> (+1)
-- >>> g 0
-- 2

-- You could also use pure (+) -- generic applicative functor, of the typeclass, waiting to choose its concrete type

a=2
