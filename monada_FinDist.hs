{-# LANGUAGE InstanceSigs #-}

module FinDist where 

import Data.List

import Data.Ratio

data FinDist a = FinDist {pairs :: [(a,Ratio Int)]} deriving (Show,Eq)

applyVal  f (a,b) = (f a, b)
applyProb f (a,b) = (a, f b)


instance Functor FinDist where
    fmap f (FinDist p) = FinDist $ map (applyVal f) p
        
a = FinDist [(1,0.5),(2,0.3),(3,0.2)]

-- |
-- >>> fmap (+1) a
-- FinDist {pairs = [(2,1 % 2),(3,3 % 10),(4,1 % 5)]}



instance Applicative FinDist where
    (<*>) (FinDist fs) (FinDist vs) = FinDist newList
       where applyPair :: (a->c,Ratio Int) -> (a,Ratio Int) -> (c,Ratio Int)
             applyPair (g,p) (a,b) = (g a, b*p)
             applyPairToList :: (a->c,Ratio Int) -> [(a,Ratio Int)] -> [(c,Ratio Int)]
             applyPairToList p list = map (applyPair p) list
             applyMany :: [(a->c,Ratio Int)] -> [(a,Ratio Int)] -> [(c,Ratio Int)]
             applyMany l1 l2 = concat $ map  ($ l2) manyFuncs
                 where manyFuncs = map (applyPairToList) l1
             newList                         = applyMany fs vs
    pure a = FinDist [(a,1)]

b = FinDist [(10,0.5),(20,0.5)]
-- a = FinDist [(1,0.5),(2,0.3),(3,0.2)]

-- |
-- >>> fmap (+) a <*> b
-- FinDist {pairs = [(11,1 % 4),(21,1 % 4),(12,3 % 20),(22,3 % 20),(13,1 % 10),(23,1 % 10)]}
-- >>> pure (+) <*> a <*> b
-- FinDist {pairs = [(11,1 % 4),(21,1 % 4),(12,3 % 20),(22,3 % 20),(13,1 % 10),(23,1 % 10)]}
-- >>> FinDist[((+),1)] <*> a <*> b
-- FinDist {pairs = [(11,1 % 4),(21,1 % 4),(12,3 % 20),(22,3 % 20),(13,1 % 10),(23,1 % 10)]}


multiplyBy :: Ratio Int -> FinDist a -> FinDist a
multiplyBy p (FinDist l) = FinDist $ map (applyProb (*p)) l

-- |
-- >>> multiplyBy 0.2 a
-- FinDist {pairs = [(1,1 % 10),...

instance Monad FinDist where
    -- return a  = FinDist [(a,1)]
    -- return = pure
    -- g recebe uma a e devolve uma FinDist
    (>>=)    :: (FinDist a) -> (a -> FinDist b) -> FinDist b
    (FinDist l) >>= g = FinDist tuplas
        where g_executou = map g (map fst l) -- g recebe nro e devolve FinDist b, 
                                             -- Aqui temos [FinDist b]
              com_probs  = zipWith multiplyBy (map snd l) g_executou
                                  -- multyplyBy aplica probabilidade em todos os elementos
                                  -- cada FinDist b de g_executou é multiplicada, em cada
                                  -- termo, pela probabilidade
                                  -- (a,p) -> (FinDist b,p) e multiplico
                                  -- os termos da FinDist por p

              tuplas     = concat $ map pairs com_probs
              
monadic = do 
    p1 <- a 
    p2 <- b
    return (p1+p2)

-- |
-- >>> monadic
-- FinDist {pairs = [(11,1 % 4),(21,1 % 4),(12,3 % 20),(22,3 % 20),(13,1 % 10),(23,1 % 10)]}

monadic2 = do 
    p1 <- a 
    p2 <- b
    return (p1+p2*10)

-- |
-- >>> monadic2
-- FinDist {pairs = [(101,1 % 4),(201,1 % 4),(102,3 % 20),(202,3 % 20),(103,1 % 10),(203,1 % 10)]}

-- untested, but bad idea --- monads with restrictions are hard
merge :: Ord a => FinDist a -> FinDist a -> FinDist a
merge (FinDist a) (FinDist b) = FinDist $ merger s_a s_b
    where sorter (a,_) (b,_)  = compare a b
          (s_a, s_b)          = (sortBy sorter a, sortBy sorter b)
          merger [] s_b       = s_b
          merger s_a []       = s_a
          merger f_a@((a,p):as) f_b@((b,p2):bs)
            | a == b          = (a,p+p2): merger as bs
            | a <  b          = (a,p)   : merger as f_b 
            | a >  b          = (b,p2)  : merger f_a bs 

combine :: Ord a => FinDist a -> FinDist a
combine (FinDist a) = FinDist $ merger s_a
    where sorter (a,_) (b,_)  = compare a b
          s_a                 = sortBy sorter a
          merger (fa@(a,p_a):fb@(b,p_b):as)
            | a == b          = merger $ (a,p_a+p_b):as
            | a <  b          = fa : merger (fb:as)
            | a >  b          = fb : merger (fa:as)
          merger [fa]         = [fa]
          merger []           = []

-- gera repeticoes bestas, só deixei aqui pra pensar depois
combineDoSeno :: Eq a => FinDist a -> FinDist a
combineDoSeno (FinDist fd) = FinDist joinVals
    where joinVal :: Eq a => [(a,b)] -> (a,b) -> [(a,b)]
          joinVal fd (val,_) = filter ((== val) . fst) fd
          joinVals = do
            v <- fd
            [sumvals $ joinVal fd v]
          sumvals :: Eq a => [(a,Ratio Int)] -> (a,Ratio Int)
          sumvals list@((val,_):_) = (val, sum $ map snd list)

combineSeno3 :: Eq x => FinDist x -> FinDist x
combineSeno3 (FinDist list) = FinDist $ do
  let elems = (nub . map fst) list
  x <- elems
  let allX = filter ( (== x) . fst ) list
  let p = sum $ snd <$> allX
  return (x, p)


dice = FinDist [(1,1 % 6),(2,1 % 6),(3,1 % 6),(4,1 % 6),(5,1 % 6),(6,1 % 6)]

monadic3 = combineSeno3 $ do
    d1 <- dice
    d2 <- dice
    return (d1+d2)


monadic32 = combine $ do
    d1 <- dice
    d2 <- dice
    return (d1+d2)

-- |
-- >>> monadic3
-- ...(2,1 % 36)...(7,1 % 6)...(11,1 % 18)...(12,1 % 36)...
-- >>> monadic32
-- ...(2,1 % 36)...(7,1 % 6)...(11,1 % 18)...(12,1 % 36)...

