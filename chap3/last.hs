module Last where

last_word :: String -> String
last_word word = last_word_aux word 0
last_word_aux word index = if index >= (length word)
                             then word
                             else if (word !! index) == ' '
                                then last_word_aux (drop (index+1) word) 0
                                else last_word_aux word (index+1)

main :: IO()
main = print(last_word "last disco kebab")