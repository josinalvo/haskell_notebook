module Print where

soap :: String
soap = "jabones"

cow :: [Char]
cow = "Una vaca sorda"
--concat or ++ work 
main :: IO()
main = do
    print "nieve"
    print "curvas impossibles"
    putStr "y una obra sin razon"
    putStrLn soap
    putStr cow
    print (cow ++ soap)
    print characters
    print hero
    where characters :: String
          characters = "caracteres syn conpreension"
          hero :: [Char]
          hero = "Super hero herrido"

-- let and where create a limited scope

-- works on source file but not on repl
calculated  = pi * r * r
r = 10/2

area d = pi * (r*r)
    where r = d/2

--Prelude> :t (++)
--(++) :: [a] -> [a] -> [a]
--Prelude> :t concat
--concat :: [[a]] -> [a] (*)
-- concat flattens

{-
<interactive>:14:13:
No instance for (Num Char) arising
from the literal ‘1’
In the expression: 1
In the second argument of ‘(++)’,
namely ‘[1, 2, 3]’
In the expression: "hello" ++ [1, 2, 3]
Errinho descritivo :D 
-}

-- TODO create right associative and left associative that print to check
-- order of operations

a = 'P':"rint" -- note que o primeiro eh char e o segundo String == [Char]
p = head a -- tive que usar minuscula pro nome de var
pr = take 2 a
int = drop 2 a
i = a !! 2 -- unsafe, error if out of bounds, considered ugly

last_word word = drop 5 word
third word = word !! 3

last_word_real word = last_word_aux word 0
last_word_aux word index = if index >= (length word)
                             then word
                             else if (word !! index) == ' '
                                then last_word_aux (drop (index+1) word) 0
                                else last_word_aux word (index+1)