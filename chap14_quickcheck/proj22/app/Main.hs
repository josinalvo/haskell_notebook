module Main where

import Lib

import Test.Hspec
import Test.QuickCheck
import Data.Map as M
import Data.Int

main :: IO ()
main = hspec $ do
    describe "Addition" $ do
        it "1 + 1 is greater than 1" $ do
            (1+1) > 1 `shouldBe` True
        it "not Oceania" $ do
            (2+2) `shouldBe` 4
            (2+1) `shouldBe` 3

proper :: IO()
proper = hspec $ do
    describe "Add" $ do
        it "x+1 is always greater than x" $ do
            property $ (\x -> x + 1 > (x :: Int))

test5 :: IO ()
test5 = hspec $ do
  describe "Addition" $ do
    it "x + 1 is always\
      \ greater than x" $ do
      property $ \x -> x + 1 > (x :: Int)

-- sample e sample' (imprime e retorna lista)
-- sample (arbitrary :: Gen Int)
-- sample' (elements ['a' .. 'z'])
-- se quiser frequencia nao uniforme, coloque repeticoes na lista

-- *Main Lib Paths_proj22> :t (elements ['a' .. 'z'])
-- (elements ['a' .. 'z']) :: Gen Char

-- *Main Lib Paths_proj22> :t choose
-- choose :: random-1.1:System.Random.Random a => (a, a) -> Gen a
-- alguem tem acesso, mas nao o meu arquivo em si "hidden package"

prop_additionGreater :: Int -> Bool
prop_additionGreater x = x + 1 > x
runQc :: IO ()
runQc = quickCheck prop_additionGreater

prop_additionGreater2 :: Int8 -> Bool
prop_additionGreater2 x = x + 1 > x
runQc2 :: IO ()
runQc2 = quickCheck prop_additionGreater2



genTuple :: (Arbitrary a, Arbitrary b) => Gen (a, b)
genTuple = do
    a <- arbitrary
    b <- arbitrary
    return (a, b)

getEither :: (Arbitrary a, Arbitrary b) => Gen (Either a b)
getEither = do
    a <- arbitrary
    b <- arbitrary
    elements [Right a, Left b]

type Morse = String

letterToMorse_ = M.fromList [   ('a', ".-")
    , ('b', "-...")
    , ('c', "-.-.")
    , ('d', "-..")
    , ('e', ".")
    --, ('z', ".-") -- sorry for the mistake, hopefully you can detect it
    ]


letterToMorse = M.insert 'f' "banana" letterToMorse_
-- essa compreensao do M.insert, faz com que morseToLetter faça sentido

-- Prelude> f a b c = a/b +c
-- Prelude> (flip f) 10 20 30
-- 32.0
-- Isso eh meio inevitavel, por causa do currying

morseToLetter :: M.Map Morse Char
morseToLetter = M.foldrWithKey (flip M.insert) M.empty letterToMorse
charToMorse :: Char -> Maybe Morse
charToMorse c = M.lookup c letterToMorse

stringToMorse :: String -> Maybe [Morse]
stringToMorse s = sequence $ Prelude.map charToMorse s
-- sequence :: (Traversable t, Monad m) => t (m a) -> m (t a)
-- puxa a monada pra fora
morseToChar :: Morse -> Maybe Char
morseToChar m = M.lookup m morseToLetter


allowedChars :: [Char]
allowedChars = M.keys letterToMorse
allowedMorse :: [Morse]
allowedMorse = M.elems letterToMorse
charGen :: Gen Char
charGen = elements allowedChars
morseGen :: Gen Morse
morseGen = elements allowedMorse

prop_thereAndBackAgain :: Property
prop_thereAndBackAgain =
    forAll charGen
    (\c -> ((charToMorse c)
    >>= morseToChar) == Just c)

-- unica diferenca foi o do (e o lambda, em homenagem ao chong)
prop_thereAndBackAgain2 :: Property
prop_thereAndBackAgain2 =
    forAll charGen
    checkComeAndGo

comeAndGo c = do
    m <- charToMorse c
    morseToChar m

checkComeAndGo c = (comeAndGo c) == Just c

checkMorse :: IO ()
checkMorse = quickCheck prop_thereAndBackAgain2

-- instances of Arbitrary have arbitrary (returns a gen)
-- and might have a shrink

