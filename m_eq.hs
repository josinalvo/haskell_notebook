module Main where

class M_Eq a where  
    (==) :: a -> a -> Bool  
    (/=) :: a -> a -> Bool  
    x == y = not (x Main./= y)  
    x /= y = not (x Main.== y)

instance M_Eq Char where
    (==) a b = (a Prelude.== b)

instance M_Eq a => M_Eq [a] where
    (==) (a:as) (b:bs) = (a Main.== b) && (as Main.== bs)
    (==) (a:as) []     = False
    (==) []     (b:bs) = False
    (==) []     []     = True

-- |
-- >>> "red" Main.== "red"
-- True
-- >>> "red" Main.== "october"
-- False
-- >>> "red" Main./= "october"
-- True


main = print 42
