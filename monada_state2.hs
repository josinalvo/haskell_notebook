import Control.Monad
import Control.Monad.State


-- newtype State s a = State { runState :: s -> (a, s) }

-- instance Monad (State s) where
--       return  a = State (\s -> (a,s))
--       -- (>>=) f g = State fog
--       --       where fog s = (out3, s3)
--       --              where (out2,s2) = runState f s
--       --                    (out3,s3) = runState (g out2) s2
--       (>>=) f g = State $ \s0 ->
--                      let (out1,s1) = runState f s0
--                      in runState (g out1) s1

-- instance Applicative (State s) where
--       (<*>) = ap
--       pure = return

-- instance Functor (State s) where
--       fmap = liftM

data TurnstileState = Locked | Unlocked Int deriving (Eq,Show)
type TurnstileState2 = (TurnstileState,Int)

data TurnstileAnswer = Thanks | GoAhead | SorryButNo deriving (Eq,Show)



addCoin :: TurnstileState2 -> (TurnstileAnswer, TurnstileState2)
addCoin (Locked,count_thru)       = (Thanks, (Unlocked 1    ,count_thru))
addCoin ((Unlocked n),count_thru) = (Thanks, (Unlocked $ n+1,count_thru))


goThru :: TurnstileState2 -> (TurnstileAnswer, TurnstileState2)
goThru (Locked,count_thru)       = (SorryButNo,(Locked,count_thru))
goThru (Unlocked 1,count_thru)   = (GoAhead   ,(Locked,count_thru+1))
goThru (Unlocked n,count_thru)   = (GoAhead   ,(Unlocked $ n-1,count_thru+1))

sAddCoin :: State TurnstileState2 TurnstileAnswer
sAddCoin = state addCoin

sGoThru :: State TurnstileState2 TurnstileAnswer
sGoThru = state goThru

niceDay :: State TurnstileState2 TurnstileAnswer
niceDay = do
      sAddCoin
      sAddCoin
      sAddCoin
      sGoThru
      -- a <- sGoThru
      -- return a

runNiceDay :: (TurnstileAnswer, TurnstileState2)
runNiceDay = runState niceDay (Locked,0)

-- |
-- >>> runNiceDay
-- (GoAhead,(Unlocked 2,1))

niceDay2 :: State TurnstileState2 [TurnstileAnswer]
niceDay2 = do
      a <- sAddCoin
      b <- sAddCoin
      c <- sGoThru
      d <- sAddCoin
      e <- sGoThru
      f <- hasty2'
      return [a,b,c,d,e,f]



hasty :: State TurnstileState2 TurnstileAnswer
hasty = do
        a <- sGoThru
        if a == GoAhead then return a else sAddCoin >> sGoThru


hasty2 :: State TurnstileState2 TurnstileAnswer
hasty2 = state handHasty 
      where handHasty :: TurnstileState2 -> (TurnstileAnswer,TurnstileState2)
            handHasty a@(Locked,_) = runState (sAddCoin >> sGoThru) a
            handHasty b            = runState sGoThru b

hasty2' :: State TurnstileState2 TurnstileAnswer
hasty2' = state $ f
               where 
                   f ram = if ans == GoAhead then (ans,ram2) else runState (sAddCoin >> sGoThru) ram2
                      where (ans,ram2) = runState sGoThru ram

runNiceDay2 :: ([TurnstileAnswer], TurnstileState2)
runNiceDay2 = runState niceDay2 (Unlocked 3,0)

niceDay3 :: State TurnstileState2 [TurnstileAnswer]
niceDay3 = sequence [sAddCoin,sAddCoin, sGoThru, sAddCoin, sGoThru, hasty2]

runNiceDay3 :: ([TurnstileAnswer], TurnstileState2)
runNiceDay3 = runState niceDay3 (Unlocked 3,0)

-- |
-- >>> runNiceDay3
-- ([Thanks,Thanks,GoAhead,Thanks,GoAhead,GoAhead],(Unlocked 3,3))

-- pushS = state $ \s -> case s of
--   Locked   -> (Tut , Locked)
--   Unlocked -> (Open, Locked)

-- get :: State s s
-- get = state $ \s -> (s, s)

-- pushS2 = do
--   s <- Control.Monad.State.get
--   put Locked
--   case s of
--     Locked   -> return Tut
--     Unlocked -> return Open


testTurnstile :: State TurnstileState2 Bool
testTurnstile = do
  initial <- get --get ram = (ram, ram)
  put' (Locked,0) -- (put ram)  = \ram_old -> ((),ram)
  check1 <- sGoThru
  put' (Unlocked 1, 0)
  check2 <- sGoThru
  put' initial
  return (check1 == SorryButNo && check2 == GoAhead)

put' ram = state $ \old_ram -> ((),ram)

-- |
-- >>> runState testTurnstile (Locked,30)
-- (True,(Locked,30))

a :: ([TurnstileAnswer], TurnstileState2)
a = runState (f sGoThru) s
      where f = sequence . (replicate 6)
            s = (Unlocked 2,15) :: TurnstileState2
-- |
-- >>> a
-- ([GoAhead,GoAhead,SorryButNo,SorryButNo,SorryButNo,SorryButNo],(Locked,17))


main = print 42