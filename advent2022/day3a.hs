import Data.Char
import Data.Set hiding (take,drop)
priority c  
    | isLower c =  fromEnum c - fromEnum 'a' + 1
    | isUpper c =  fromEnum c - fromEnum 'A' + 1 + 26
    | otherwise = error "Jesus"

-- >>> priority 'a'
-- 1
-- >>> priority 'z'
-- 26
-- >>> priority 'A'
-- 27
-- >>> priority 'B'
-- 28
-- >>> priority 'Z'
-- 52

halfs :: String -> (String,String)
halfs string = f string
    where n = length string `div` 2
          f = pure ((,)) <*> (take n) <*> (drop n)

-- >>> halfs "banana"
-- ("ban","ana")

valString :: [Char] -> Int
valString string = priority (head $ toList inter)
    where  (string1,string2) = halfs string
           [set1,set2] = fmap fromList [string1,string2]
           inter = intersection set1 set2

-- >>> valString "vJrwpWtwJgWrhcsFMMfFFhFp"
-- 16

main1 = do
    a <- lines <$> getContents
    let vals = fmap valString a
    print $ sum vals

main = main1

