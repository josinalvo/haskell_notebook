import Data.Map as Map hiding (foldl')
import Data.Char
import Data.Set as Set hiding (foldl')
import Data.List (foldl')
import Data.List.HT

data Terreno a = Terreno {arvores :: Map (Int,Int) a, altura :: Int, largura :: Int} deriving Show

readTerreno :: String -> Terreno Int
readTerreno s = Terreno (Map.fromList lista) altura largura
    where linhas                = zip [0..] (fmap readString $ lines s) :: [(Int,[Int])]
          readString            = fmap digitToInt :: String -> [Int]
          linha2pares (n,linha) = zip (zip (repeat n) [0..]) linha :: [((Int,Int),Int)]
                                --zip    [(n,0),(n,1)..]     linha
          lista                 = concat $ fmap linha2pares linhas :: [((Int,Int),Int)]
          largura               = length $ head $ lines s
          altura                = length $ lines s

terreninho = "123\n456\n789\n"

-- |
-- >>> readTerreno terreninho
-- Terreno {arvores = fromList [((0,0),1),((0,1),2),((0,2),3),((1,0),4),((1,1),5),((1,2),6),((2,0),7),((2,1),8),((2,2),9)], altura = 3, largura = 3}

percorre :: [(Int,Int)] -> Terreno Int -> Set (Int, Int)
percorre posicoes (Terreno{arvores=a}) = resultingSet
    where (resultingSet, maxVal) = foldl' f (Set.empty,-1) posicoes
          f (set,maxVal) (nLinha,nColuna) = if newVal > maxVal then (Set.insert (nLinha,nColuna) set,newVal)
                                                                    else (set,maxVal)
                where newVal = a ! (nLinha,nColuna)

percorreLinha :: Int -> Terreno Int -> Set (Int,Int)
percorreLinha nLinha terreno@(Terreno {largura=l}) = Set.union resultingSet1 resultingSet2
    where posicoes      = zip (repeat nLinha) [0..(l-1)]
          resultingSet1 = percorre posicoes terreno
          resultingSet2 = percorre (reverse posicoes) terreno

percorreColuna :: Int -> Terreno Int -> Set (Int,Int)
percorreColuna nColuna terreno@(Terreno {altura=h}) = Set.union resultingSet1 resultingSet2
    where posicoes      = zip [0..(h-1)] (repeat nColuna)
          resultingSet1 = percorre posicoes terreno
          resultingSet2 = percorre (reverse posicoes) terreno

percorreTudo :: Terreno Int -> Set (Int,Int)
percorreTudo terreno@(Terreno{altura=h, largura=l})= Set.unions $ colunas ++ linhas
    where colunas = fmap (flip percorreColuna terreno) [0..l-1]
          linhas  = fmap (flip percorreLinha terreno)  [0..h-1] 

solve1 :: String -> String
solve1 = show . Set.size . percorreTudo . readTerreno

onlyFirstGEQ :: (Eq a, Ord a) => a -> [a]  -> Int
onlyFirstGEQ val l = if rest /= [] then length smaller + 1 else length smaller
    where (smaller, rest) = Prelude.span (<val) l

scenicScore :: (Int, Int) -> Terreno Int -> Int
scenicScore pos@(nLinha,nColuna) terreno@(Terreno {altura=h, largura=l, arvores=a}) = product vals
    where goUp    (x,y)    = (x+1,y)
          goDown  (x,y)    = (x-1,y)
          goLeft  (x,y)    = (x  ,y-1)     
          goRight (x,y)    = (x  ,y+1)     
          generate start f =  onlyFirstGEQ (a!pos) . takeWhileJust . fmap (a !?) $ iterate f (f start)
          vals = fmap (generate pos) [goUp, goDown, goLeft, goRight]
         

bestScenicScore :: Terreno Int -> Int
bestScenicScore t@(Terreno {arvores=a}) = maximum $ fmap (flip scenicScore t) (keys a) 


allScenicScores :: Terreno Int -> [Int]
allScenicScores t@(Terreno {arvores=a}) = fmap (flip scenicScore t) (keys a) 

solve2 = show . bestScenicScore . readTerreno

main :: IO ()
main = interact solve2