{-# OPTIONS_GHC -Wno-incomplete-patterns #-}
import Data.Char
import Data.Set hiding (take,drop)
priority c  
    | isLower c =  fromEnum c - fromEnum 'a' + 1
    | isUpper c =  fromEnum c - fromEnum 'A' + 1 + 26
    | otherwise = error "Jesus"

-- >>> priority 'a'
-- 1
-- >>> priority 'z'
-- 26
-- >>> priority 'A'
-- 27
-- >>> priority 'B'
-- 28
-- >>> priority 'Z'
-- 52


valStrings :: (String,String,String) -> Int
valStrings (string1,string2,string3) = priority (head $ toList inter)
    where  [set1,set2,set3] = fmap fromList [string1,string2,string3]
           inter = (intersection set1 set2) `intersection` set3

-- >>> valStrings "vJrwpWtwJgWrhcsFMMfFFhFp" "jqHRNqRjqzjGDLGLrsFMfFZSrLrFZsSL" "PmmdzqPrVvPwwTWBwg"
-- 18

trios :: [c] -> [(c, c, c)]
trios (a:b:c:rest) = (a,b,c):trios rest
trios [] = []

main2 = do
    a <- trios . lines <$> getContents
    let vals = fmap valStrings a
    print $ sum vals

main2b :: IO ()
main2b = interact (show . sum . fmap valStrings . trios . lines) >> putStr "\n"

main = main2b

