import Data.List.Split

data Interval = Interval Integer Integer

(Interval a b) `contains` (Interval c d) = (a <= c) && (b >= d)

redundant i1 i2 = (i1 `contains` i2) || (i2 `contains` i1)

overlaps (Interval a b) (Interval c d) = (small <= big)
    where small = max a c
          big   = min b d

toInterval :: String -> Interval
toInterval = (\[a, b] -> Interval a b) . map read . splitWhen (== '-')

toIntervals :: String -> (Interval, Interval)
toIntervals = (\[a, b] -> (a,b)) . map toInterval . splitWhen (== ',')

solve1 = length . (filter (uncurry redundant)) . fmap toIntervals . lines

solve2 = length . (filter (uncurry overlaps)) . fmap toIntervals . lines

main = interact (show . solve2)
