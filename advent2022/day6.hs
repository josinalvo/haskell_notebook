import Data.List (nub)
start (a:b:c:d:lista) = if length elements == 4 then 4 else (+1) $ start (b:c:d:lista)
    where elements = nub [a,b,c,d]
start _ = error "lista zoada"

start2 :: String -> Int
start2 lista = if valid then 14 else (+1) . start2 $ tail lista
    where elements = take 14 lista
          valid    = length (nub elements) == 14
          

-- |
-- >>> start "abcdef" 
-- 4
-- >>> start "aaaaabcdef" 
-- 8


main1 = do
    s <- getContents
    print $ start s

main2 = do
    s <- getContents
    print $ start2 s


main = main2