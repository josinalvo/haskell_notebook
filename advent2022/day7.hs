import Data.Map as Map hiding (map)
import Data.Maybe (catMaybes, listToMaybe)
import Data.List (sort)
import Data.Set as Set (Set, empty, unions, insert, lookupGT)
data Tree = Folder {contents :: Map String Tree, name :: String} | File {size :: Int, name :: String} deriving (Ord, Eq, Show)

createMap :: [Tree] -> Map String Tree
createMap list = fromList pairs
    where pairs = zip (Prelude.map name list) list 

data Crumb = Crumb {contentsS :: (Map String Tree), nameS :: String}
data Zipper = Zipper {crumbs :: [Crumb], current :: Tree}

-- getElement contents "rock"

emptyTree = Folder (createMap []) "/"

cd :: Zipper -> String -> Zipper
cd zipper@(Zipper [] cur_tree) ".." = zipper

cd zipper@(Zipper crumbs cur_tree) ".." = Zipper (tail crumbs) new_tree
    where new_tree_map = Map.insert (name cur_tree) cur_tree $ contentsS (head crumbs) :: Map String Tree
          new_tree  = Folder new_tree_map (nameS $ head crumbs) 


cd zipper@(Zipper [] cur_tree) "/" = zipper

cd zipper@(Zipper crumbs cur_tree) "/" = cd up "/"
    where up = cd zipper ".."

cd zipper@(Zipper crumbs cur_tree) dir_name  = Zipper (new_crumb:crumbs) new_tree
    where  new_tree    = contents cur_tree ! dir_name :: Tree
           without_dir = delete dir_name (contents cur_tree) :: Map String Tree
           new_crumb   = Crumb without_dir (name cur_tree)

emptyZipper = Zipper [] emptyTree

ls :: Zipper -> [Tree] -> Zipper
ls zipper trees = zipper{current=Folder new_map curr_name}
    where curr_map = contents (current zipper)
          curr_name = name (current zipper)
          new_map  = curr_map `union` createMap trees

readLsLine :: String -> Tree
readLsLine s = if a == "dir" then Folder Map.empty b
                             else File (read a) b
    where [a,b] = words s

readLsLines :: [String] -> ([Tree],[String])
readLsLines [] = ([],[])
readLsLines all@(first:rest) 
   | head first == '$' = ([],all)
   | otherwise         = (firstLine:trees,strings)
    where firstLine = readLsLine first
          (trees,strings) = readLsLines rest

readLines :: [String] -> Zipper -> Zipper
readLines [] z = z
readLines (first:rest) z = readLines restOfStrings oneStepZip
    where oneStepZip              = case first of
                                    ('$':' ':'c':'d':' ':stringRest) -> cd z stringRest
                                    "$ ls" -> ls z lsTrees 
          (lsTrees,stringsAfterLs)= readLsLines rest
          restOfStrings           = case first of
                                    ('$':' ':'c':'d':' ':stringRest) -> rest
                                    "$ ls" -> stringsAfterLs

createTree :: String -> Tree
createTree a = tree
    where commands = lines a :: [String]
          lastZipper = readLines commands emptyZipper
          up   = cd lastZipper "/"
          tree = current up

data Solution1 = Solution1 {sizeOfTree :: Int, sumOfTree :: Int, allSizes :: Set Int} deriving Show

solve1 :: Tree -> Solution1
solve1 (Folder t name) = Solution1 mySize mySum listOfSizes
    where subTrees    = map solve1 ((map snd) . toList $ t)
          subSizes    = map sizeOfTree subTrees
          subSums     = map sumOfTree subTrees
          a           = map allSizes subTrees :: [Set Int]
          listOfSizes'= Set.unions $ map allSizes subTrees :: Set Int
          mySize      = sum subSizes
          listOfSizes = Set.insert mySize listOfSizes' :: Set Int
          mySum       = sum subSums + if mySize < 10^5 then mySize else 0

solve1 (File size name) = Solution1 size 0 Set.empty


-- goodToDelete totalOcup dirSize = target > free + dirSize
--     where free = driveSize - totalOcup

-- data Solution2 = Solution2 {sizeOfTree2 :: Int, ans :: Maybe Int} deriving Show
-- solve2 :: Solution1 -> Tree -> Solution2
-- solve2 s (File size _ ) = Solution2 size Nothing
-- solve2 s@(Solution1 sizeOfTree _) (Folder t _) = Solution2 mySize myAns
--     where check = goodToDelete sizeOfTree
--           subTrees = map (solve2 s) ((map snd) . toList $ t)
--           subSizes = map sizeOfTree2 subTrees
--           myAns    = listToMaybe $ sort $ catMaybes $ (mySizeMaybe :) $ map ans subTrees
--           mySize   = sum subSizes
--           mySizeMaybe = if check mySize then Just mySize else Nothing

driveSize = 70000000
target    = 30000000
solve2 :: Solution1 -> Maybe Int
solve2 s@(Solution1{sizeOfTree = totalOcup, allSizes= setSizes}) = Set.lookupGT needed setSizes
    where needed  = target - free
          free    = driveSize - totalOcup


mainTest = do
    a <- getContents
    let tree = createTree a
    -- print tree
    print "solution 1"
    let s1 = solve1 tree
    print s1
    print "solution 2"
    print $ solve2 s1

main = mainTest

