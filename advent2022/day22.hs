import Data.List.Split (splitWhen)
import Data.Maybe (catMaybes)

data Point = Point {x :: Integer, y :: Integer,  z :: Integer} deriving Show

data Cube = Cube {small :: Point, big :: Point, sign :: Integer} deriving Show

combine :: (Integer -> Integer -> Integer) -> Point -> Point -> Point
combine f (Point x1 y1 z1) (Point x2 y2 z2) = Point (f x1 x2) (f y1 y2) (f z1 z2)

intersect :: Cube -> Cube -> Maybe Cube
intersect c1 c2 = valid $ Cube small' big' sign'
    where small' = combine max (small c1) (small c2)
          big'   = combine min (big c1) (big c2)
          sign'  = 1
          valid  c@(Cube p_small p_big sign) 
            | x p_small > x p_big = Nothing
            | y p_small > y p_big = Nothing
            | z p_small > z p_big = Nothing
            | otherwise           = Just c




volume cube = sign cube * (x dimens + 1) * (y dimens + 1) * (z dimens + 1)
    where dimens = combine (-) (big cube) (small cube)

sample1 = "on x=10..12,y=10..12,z=10..12"
sample2 = "on x=11..15,y=10..12,z=10..12"

readCube string =  Cube (Point xs ys zs) (Point xb yb zb) sign
    where [sign,xs,xb,ys,yb,zs,zb]  = fmap toInteger $ filter (/= "") $ splitWhen (`elem` " xyz=.,") string
          toInteger "off" = (-1)
          toInteger "on"  = (1)
          toInteger a     = read a

-- >>> readCube sample1
-- Cube {small = Point {x = 10, y = 10, z = 10}, big = Point {x = 12, y = 12, z = 12}, sign = 1}

insersectWithSign :: Cube -> Cube -> Maybe Cube
insersectWithSign newC oldC = do
                                 inter <- intersect newC oldC
                                 return inter{sign = sign'}
        where 
              sign'  = if (sign newC == (-1)) then (-1)
                                              else ((-1) * sign oldC)

joinCube :: Cube -> [Cube] -> [Cube]
joinCube c [] = [c | sign c > 0]
joinCube c cubes = cubeItself ++ (catMaybes inters) ++ cubes
    where inters = fmap (insersectWithSign c) cubes
          cubeItself = [c | sign c > 0]

fullVolume :: [Cube] -> Integer
fullVolume = sum . (fmap volume) 

-- >>> l1 = []
-- >>> l2 = joinCube (readCube sample1) l1
-- >>> fullVolume l2
-- 27
-- >>> l3 = joinCube (readCube sample2) l2
-- >>> fullVolume l3
-- 54

limit :: Cube -> Maybe Cube
limit cube = Just cube
limit cube = intersect cube (Cube (Point (-50) (-50) (-50)) (Point (50) (50) (50)) (-1))

solve1 :: String -> Integer
solve1 = fullVolume . (foldl (flip joinCube) []) . catMaybes . (fmap limit) . (fmap readCube). nonEmpty . lines

nonEmpty = (filter (/= ""))

sample_string1 = sample1 ++ "\n" ++ sample2

-- >>> solve1 sample_string1
-- 54

sample3 = "on x=10..12,y=10..12,z=10..12"
sample4 = "off x=10..12,y=10..12,z=10..12"

sample_string2 = sample3 ++ "\n" ++ sample4

-- >>> solve1 sample3
-- 27
-- >>> solve1 sample_string2
-- 54

l4 = []
l5 = joinCube (readCube sample3) l4 
r1 = fullVolume l5
-- >>> r1
-- 27
l6 = joinCube (readCube sample4) l5

-- >>> l6
-- [Cube {small = Point {x = 10, y = 10, z = 10}, big = Point {x = 12, y = 12, z = 12}, sign = 1},Cube {small = Point {x = 10, y = 10, z = 10}, big = Point {x = 12, y = 12, z = 12}, sign = 1}]

main :: IO ()
main = interact (show . solve1) >> putStrLn ""

