import Data.List.Split (chunksOf)
import Data.List (transpose, union)
import Data.Maybe
import Control.Monad
import Data.List
import qualified Data.IntMap as M 
test1 = "[S] [B] [B] [F] [H] [C] [B] [N] [L]"
test3 = "[W] [Q] [D] [M] [T]     [L] [T]    " 
test2 :: String
test2 = "[S] [B] [B] [F]"

toRow :: String -> [String]
toRow string = (map toChar) . (chunksOf 4) $ string
    where toChar ('[':x:']':' ':[]) = x:[]
          toChar ('[':x:']':'\n':[]) = x:[]
          toChar ('[':x:']':[]) = x:[]
          toChar "    " = ""
          toChar "   " = ""
          toChar x = error $ "fuck this shit: " ++ x



-- >>> toRow test1
-- ["S","B","B","F","H","C","B","N","L"]
-- >>> toRow test3
-- ["W","Q","D","M","T","","L","T",""]
-- >>> toRow test2
-- ["S","B","B","F"]

input = "[T] [V]                     [W]    \n[V] [C] [P] [D]             [B]    \n[J] [P] [R] [N] [B]         [Z]    \n[W] [Q] [D] [M] [T]     [L] [T]    \n[N] [J] [H] [B] [P] [T] [P] [L]    \n[R] [D] [F] [P] [R] [P] [R] [S] [G]\n[M] [W] [J] [R] [V] [B] [J] [C] [S]\n[S] [B] [B] [F] [H] [C] [B] [N] [L]"


toStacks ::  String -> M.IntMap [String]
toStacks string = M.fromList . (zip [1..]) . (map (dropWhile (== ""))) . transpose . (map toRow) . lines $ string


-- >>> toStacks input
-- fromList [(1,["T","V","J","W","N","R","M","S"]),(2,["V","C","P","Q","J","D","W","B"]),(3,["P","R","D","H","F","J","B"]),(4,["D","N","M","B","P","R","F"]),(5,["B","T","P","R","V","H"]),(6,["T","P","B","C"]),(7,["L","P","R","J","B"]),(8,["W","B","Z","T","L","S","C","N"]),(9,["G","S","L"])]

move :: M.IntMap [String] -> (Int,Int,Int) -> M.IntMap [String]
move stacks (amount, from, to) = M.union smallMap stacks
    where listFrom        = stacks M.! from :: [String]
          listTo          = stacks M.! to :: [String]
          (add,listFrom') = splitAt amount listFrom
          listTo'         = (reverse add)  ++ listTo
          smallMap        = M.fromList [(from,listFrom'),(to,listTo')]

move2 :: M.IntMap [String] -> (Int,Int,Int) -> M.IntMap [String]
move2 stacks (amount, from, to) = M.union smallMap stacks
    where listFrom        = stacks M.! from :: [String]
          listTo          = stacks M.! to :: [String]
          (add,listFrom') = splitAt amount listFrom
          listTo'         = add  ++ listTo  -- Only difference
          smallMap        = M.fromList [(from,listFrom'),(to,listTo')]

-- >>> a = toStacks input
-- >>> move a (1, 9, 1)
-- fromList [(1,["G","T","V","J","W","N","R","M","S"]),(2,["V","C","P","Q","J","D","W","B"]),(3,["P","R","D","H","F","J","B"]),(4,["D","N","M","B","P","R","F"]),(5,["B","T","P","R","V","H"]),(6,["T","P","B","C"]),(7,["L","P","R","J","B"]),(8,["W","B","Z","T","L","S","C","N"]),(9,["S","L"])]

parseMove :: String -> (Int,Int,Int)
parseMove string = (amount,from,to)
    where [_,amount,_,from,_,to] = map read . words $ string -- lazy was clever here

m1 = "move 10 from 8 to 4"

-- >>> parseMove m1
-- (10,8,4)

solve movesString stacksString = topString
    where stacks = toStacks stacksString
          moves  = map parseMove . lines $ movesString
          ans = foldl' move stacks moves
          tops = catMaybes . map (listToMaybe . snd) $ M.toList ans
          topString = join tops


solve2 movesString stacksString = topString
    where stacks = toStacks stacksString
          moves  = map parseMove . lines $ movesString
          ans = foldl' move2 stacks moves
          tops = catMaybes . map (listToMaybe . snd) $ M.toList ans
          topString = join tops
main1 :: IO ()
main1 = do
    movesS <- getContents
    print $ solve movesS input


main2 :: IO ()
main2 = do
    movesS <- getContents
    print $ solve2 movesS input

inputSmall :: String
inputSmall = "    [D]    \n[N] [C]    \n[Z] [M] [P]"


movesSmall :: String
movesSmall = "move 1 from 2 to 1\nmove 3 from 1 to 3\nmove 2 from 2 to 1\nmove 1 from 1 to 2"

mainSmall =  do
    print $ solve movesSmall inputSmall

main = main2
