module TimeSeries where

import Data.List
import qualified Data.Map as Map
import Data.Semigroup
import Data.Maybe

file1 :: [(Int,Double)]
file1 = [ (1, 200.1), (2, 199.5), (3, 199.4)
         , (4, 198.9), (5, 199.0), (6, 200.2)
         , (9, 200.3), (10, 201.2), (12, 202.9)]
file2 :: [(Int,Double)]
file2 = [(11, 201.6), (12, 201.5), (13, 201.5)
 ,(14, 203.5), (15, 204.9), (16, 207.1)
 ,(18, 210.5), (20, 208.8)]
file3 :: [(Int,Double)]
file3 = [(10, 201.2), (11, 201.6), (12, 201.5)
 ,(13, 201.5), (14, 203.5), (17, 210.5)
 ,(24, 215.1), (25, 218.7)]
file4 :: [(Int,Double)]
file4 = [(26, 219.8), (27, 220.5),(28, 223.8)
 ,(29, 222.8), (30,223.8), (31, 221.7)
 ,(32, 222.3), (33,220.8), (34, 219.4)
 ,(35, 220.1), (36,220.6)]

fastMeanDiff = mean1 where 
    alldates = concat [file1, file2, file3, file4]
    fst_date = fst $ head file1
    lst_date = fst $ last file4
    range    = [fst_date..lst_date]
    mapDates = Map.fromList alldates
    diffe n  = do val1 <- mapDates Map.!? n
                  val2 <- mapDates Map.!? (n-1)
                  return (val1-val2)
    diffs :: [Maybe Double]
    diffs    = map diffe range
    validDifs= catMaybes diffs
    -- corr is a correct response
    corr1    = (sum validDifs) / (fromIntegral $ length validDifs)
    corr2    = mean validDifs
    pairs    = filter (not . isNothing . snd) $ zip range diffs
    clean (a,Just b) = (a,b)
    corr3    = meanTS $ loadTS $ map clean pairs
    shifted  = (0,Nothing):pairs
    mean1    = meanTS $ diffTS bigTS

-- | oracle test that shows that there is some bug below
-- >>> fastMeanDiff
-- ... 0.6076923076923071

-- create time series
-- (from smallest to biggest, using a map and with appropriate nothings)
data TS a = TS [Int] [Maybe a]

loadTS :: [(Int,a)] -> TS a
loadTS [] = TS [] []
loadTS l = TS range $ map (tSmap Map.!?) range
   where tSmap = Map.fromList l
         indexes   = map fst l
         range = [minimum indexes..maximum indexes]


-- | 
-- >>> (TS a b) = loadTS [(1,"banana"),(3,"avocado")]
-- >>> (a,b)
-- ([1,2,3],[Just "banana",Nothing,Just "avocado"])

showPoint :: Show a => (Int,Maybe a) -> String 
showPoint (a,(Just v)) = concat [show a,"|",show v,"\n"]
showPoint (a,Nothing)  = concat [show a,"|", "NA" ,"\n"]

instance (Show a) => Show (TS a) 
    where show (TS idxs vals) = concat $ map showPoint $ zip idxs vals
                     -- or zipWith (curry showPoint) (CARD ME)

instance Semigroup (TS a) where
    (<>) (TS r1 v1) (TS r2 v2) = TS range $ map unjust $ map (map_all Map.!?) range
        where all_r   = r1 ++ r2
              isGoodDP (_,Nothing) = False
              isGoodDP (_,_      ) = True
              buildPartMap r v = Map.fromList $ filter isGoodDP $ zip r v
              map_all = Map.union (buildPartMap r1 v1) (buildPartMap r2 v2)
              range   = [minimum all_r .. maximum all_r]
              unjust :: Maybe (Maybe a) -> Maybe a
              unjust (Just x) = x
              unjust Nothing  = Nothing

instance Monoid (TS a) where
    mempty = (TS [] [])

-- | 
-- >>> t1 = loadTS [(1,"banana"),(3,"avocado")]
-- >>> t2 = loadTS [(5,"strawberry")]
-- >>> (TS c d) = t1 <> t2
-- >>> (c,d)
-- ([1,2,3,4,5],[Just "banana",Nothing,Just "avocado",Nothing,Just "strawberry"])


bigTS  = mconcat $ map loadTS [file1,file2,file3,file4]

mean' :: [Int] -> Double 
mean' l = (summ/size) 
      where summ = (fromIntegral . sum) l
            size = (fromIntegral . length) l

mean :: Real a => [a] -> Double 
mean = pure (/) <*> (realToFrac . sum) <*> (realToFrac . length)
-- CARD, e entenda melhor, nao quero usar Num, mas sim Real

-- |
-- >>> mean [1,2,3]
-- 2.0
-- >>> mean [1,2,3,4]
-- 2.5

meanTS :: Real a => TS a -> Maybe Double
meanTS (TS [] []) = Nothing
meanTS (TS _ y)   = Just $ mean vals
    where vals = map fromJust $ filter isJust y


-- |
-- >>> t1 = loadTS [(1,5),(3,5)]
-- >>> t2 = loadTS [(5,2)]
-- >>> meanTS $ t1 <> t2
-- Just 4.0
-- >>> meanTS $ loadTS []
-- Nothing

makeSafe :: (DP a ->DP a ->DP a ) -> (DP a ->DP a ->DP a )
makeSafe f = g
    where g (_,Nothing) b = b
          g a (_,Nothing) = a
          g a b = f a b

type DP a = (Int,Maybe a) --DataPoint
transSelector :: Eq a =>(a->a->a) -> (DP a -> DP a -> DP a)
transSelector f = makeSafe res
    where res (x1,Just y1) (x2, Just y2) = if f y1 y2 == y1
                                           then (x1,Just y1)
                                           else (x2,Just y2)

getBiggestByF :: Eq a =>(a->a->a) -> TS a -> Maybe (DP a)
getBiggestByF _ (TS [] []) = Nothing 
getBiggestByF f (TS xs ys) = summary2res summary
    where list = zip xs ys
          transF = transSelector f
          summary = foldr1 transF list
          summary2res (_,Nothing) = Nothing 
          summary2res a = Just a

getBiggest :: (Eq a , Ord a) => TS a -> Maybe (DP a)
getBiggest = getBiggestByF (\x y -> maximum [x,y])

getSmallest :: (Eq a , Ord a) => TS a -> Maybe (DP a)
getSmallest = getBiggestByF min    -- CARD
-- |
-- >>> t1 = loadTS [(1,5),(3,6)]
-- >>> getBiggest t1
-- Just (3,Just 6)
-- >>> getSmallest t1
-- Just (1,Just 5)
-- >>> getSmallest (TS [1,2][Nothing,Nothing])
-- Nothing
-- >>> getSmallest (TS [][])
-- Nothing

-- the variable mempty :(
-- sum' mempty  = head mempty
-- sum' (x:xs)  = x + sum' xs

ans 1 = "one"
ans 2 = "two"
ans x = "many" -- you cant do as sum (pattern match on value)
               -- but can do as ans (pattern match on patterns)
               -- but of course the difference is not that clear now

sum'' l
  | l == mempty = 0
  | otherwise   = head l + sum'' (tail l)

-- | sum'' solves the problem
-- >>> sum'' [1,2,3]
-- 6 



diffPair :: (Num a) => (DP a -> DP a -> DP a)
diffPair (_ ,_      ) (x2 , Nothing) = (x2, Nothing)
diffPair (_ ,Nothing) (x2 , _      ) = (x2, Nothing)
diffPair (x1,Just y1) (x2, Just y2 ) = (x2, Just (y2-y1))


diffTS :: Num a => TS a -> TS a
diffTS (TS xs ys) = TS (map fst ans_list) (map snd ans_list)
    where dps      = zip xs ys
          ts       = zipWith diffPair dps (tail dps)
          fst_time = head xs
          ans_list = (fst_time, Nothing) : ts

getPair :: TS a -> Int -> DP a
getPair (TS xs ys) i = (xs!!i, ys!!i)

-- |
-- >>> loadTS [(1,5),(3,6)]
-- 1|5
-- 2|NA
-- 3|6
-- ...
-- >>> diffPair (1,Just 2) (2,Just 5)
-- (2,Just 3)
-- >>> diffPair (1,Just 2) (2,Nothing)
-- (2,Nothing)
-- >>> diffTS (loadTS [(1,5),(3,6)])
-- 1|NA
-- 2|NA
-- 3|NA
-- ...
-- >>> diffTS (loadTS [(1,5),(2,5),(3,6)])
-- 1|NA
-- 2|0
-- 3|1
-- ...

movingAverage :: Real a => TS a -> Int -> TS Double
movingAverage (TS x y) sampleSize = recover $ map (oneDP . extremes) range
    where range       = [head x..last x]
          myPairTS    = zip x y
          extremes p  = (p - (div sampleSize 2), p - (div sampleSize 2) + sampleSize)
          middle (s,f)= (s+f) `div` 2 --TODO artifact
          region' s   = dropWhile (not . ( (/= s) . fst)) myPairTS
          region (s,f)= takeWhile ((/= (f+1)) . fst) $ region' s
          recover pair= TS (map fst pair) (map snd pair)
          oneDP :: (Int,Int) -> (Int, Maybe Double)
          oneDP  (s,f)= (middle (s,f), meanTS $ recover $ region (s,f))