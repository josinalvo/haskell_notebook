# semigroup
1 operacao a -> a -> a
propr extra: associativa

#monoid
ganha identidade (mempty)
operacao chama mappend (ou <>. qie vem do semigroup)
-> consegue derivar um mconcat
* identidade esq, ident direita, associatividade (movecao de parenteses) e mconcat coerente (se vc quiser implementar. Mas talvez nunca implementar? mconcat = foldr mappend mempty -- pode ser util por motivo de eficiencia)
lembrando q concat amassa uma lista de listas: concat ["a","b","cde"] -> "abcde"

#exorcicio
This is because there are other text types in Haskell (discussed in unit 4) that support mconcat , but not ++

* tipo PTable (Events eh lista de strings, probs, lista de dobles)
* createPTable (normaliza se necessario)
* showPair: imprime um evento a sua probabilidade. Usar mconcat para juntar
strings, ele diz ser mais flexivel
* instance show de Ptable
* cartCombine :: (a -> b -> c) -> [a] -> [b] -> [c]
(clever use of repeats, cycles) and zips
