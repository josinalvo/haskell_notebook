module MonoidExamples where

-- monoid capstone from get programming

combineWith :: (a->b->c) -> [a] -> [b] -> [c]
combineWith f las lbs = [f a b | a <- las, b <- lbs]

combineWith'' :: (a->b->c) -> [a] -> [b] -> [c]
combineWith'' f las lbs = zipWith f much_as such_bs
    where much_as = mconcat (map (take (length lbs) . repeat) las)
          such_bs = cycle lbs

combineWith' :: (a->b->c) -> [a] -> [b] -> [c]
combineWith' f las lbs = pure f <*> las <*> lbs

-- |
-- >>> combineWith (+) [10,20] [1,2]
-- [11,12,21,22]
-- >>> combineWith' (+) [10,20] [1,2]
-- [11,12,21,22]
-- >>> combineWith' (+) [10,20,30] [1,2]
-- [11,12,21,22,31,32]
-- >>> combineWith' (+) [10,20] [1,2,3]
-- [11,12,13,21,22,23]
-- >>> combineWith' (+) [10,20,30] [1,2,3]
-- [11,12,13,21,22,23,31,32,33]

type Event = String 
type Prob  = Double
data PTable = PTable { events :: [Event], probs :: [Prob] }

createPTable :: [Event] -> [Prob] -> PTable
createPTable evs probs = PTable evs normPs
    where sumPs  = sum probs
          normPs = map (/sumPs) probs

-- |
-- >>> probs $ createPTable ["a","b"] [1,3]
-- [0.25,0.75]

showPair :: Event -> Prob -> String 
showPair e p = mconcat ["event ", e, " has probability ", show p]

instance Show PTable 
 where show (PTable e p)= unlines $ zipWith showPair e p

-- |
-- >>> createPTable ["a","b"] [1,3]
-- event a has probability 0.25
-- event b has probability 0.75
-- ...

-- capturing a trailing newline demanded silliness

instance Semigroup PTable
    where (<>) (PTable e1 p1) (PTable e2 p2) = PTable ecart pcart
                where pcart       = combineWith (*) p1 p2
                      ecart       = combineWith strJoin e1 e2
                      strJoin "" b = b
                      strJoin a "" = a
                      strJoin a b  = a ++ " " ++ b

instance Monoid PTable
    where mempty = PTable [""] [1]

-- |
-- >>> ev = createPTable ["a","b"] [1,3]
-- >>> ev <> ev
-- event a a has probability 6.25e-2
-- event a b has probability 0.1875
-- event b a has probability 0.1875
-- event b b has probability 0.5625
-- ...

coin :: PTable
coin = createPTable ["heads","tails"] [0.5,0.5]
spinner :: PTable
spinner = createPTable ["red","blue","green"] [0.1,0.2,0.7]

-- |
-- >>> coin <> spinner
-- ...
-- event tails blue has probability 0.1
-- ...
