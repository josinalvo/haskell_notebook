import System.Random
import Control.Monad.State
import Control.Applicative

s = mkStdGen 666
a = randomR (1,6) s :: (Int,StdGen)

rollOne :: State StdGen Int
rollOne = state $ aux
    where aux seed = randomR (1,6) seed

rollPair :: State StdGen (Int,Int)
rollPair = do
    a <- rollOne
    b <- rollOne
    return (a,b)

rollTwoSummedS = liftA2 (+) rollOne rollOne

rollPair2 :: State StdGen (Int,Int)
rollPair2 = (,) <$> rollOne <*> rollOne

rollPair3 :: State StdGen (Int,Int)
rollPair3 = liftA2 (,) rollOne rollOne

happyDoubles :: State StdGen Int
happyDoubles = aux <$> rollOne <*> rollOne
    where aux a b = if a == 6 then 2*(a+b) else a+b

-- |
-- >>> evalState happyDoubles s
-- 24

fmap' :: (Monad m) => (a->b) -> (m a) -> (m b)
fmap' func m = do
    val <- m
    return (func val)

fmap'' :: (a -> b) -> State s a -> State s b
fmap'' func stateToChange = state (\ram -> let (val,ram2) = runState stateToChange ram
                                           in (func val,ram2))
                                           

getRandom :: (RandomGen g, Random a) => State g a
getRandom = state random

trio :: RandomGen g => State g (Int, Bool, Char)
trio = liftA3 (,,) getRandom getRandom getRandom

trio1 = runState trio s

trio' :: RandomGen g => State g (Int, Bool, Char)
trio' =  (,,) <$> getRandom <*> getRandom <*> getRandom

trio2 = runState trio' s

-- |
-- >>> trio1 == trio2
-- True

rollRange:: Int -> State StdGen Int
-- rollRange n = state $ aux
--     where aux seed = randomR (0,n-1) seed
rollRange n = state $ randomR (0,n-1)

randomElem :: [a] -> State StdGen a
randomElem l = do
    -- pos <- rollRange (length l)
    pos <- state $ randomR(0,length l)
    return (l !! pos)

randomTrio :: State StdGen (String, String, String)
randomTrio = liftA3 (,,) card card card
    where card = randomElem $ ["A","K","J","Q"] ++ map show [2..10]

-- |
-- >>> (cards, s2) = runState randomTrio s
-- >>> cards
-- ("3","3","7")
-- >>> runState randomTrio s2
-- (("5","10","Q"),StdGen {unStdGen = SMGen 9444179912324030971 9609244898844701819})
main = print 42
