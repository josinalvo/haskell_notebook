-- the affirmation of folding seems to be that this is an abstractable pattern

length' :: [a] -> Integer
length' [] = 0
length' (_:xs) = 1 + length' xs

product' :: [Integer] -> Integer
product' []  = 1
product' (x:xs) = x * product' xs

concat' :: [[a]] -> [a]
concat' [] = []
concat' (x:xs) = x ++ concat xs

foldr' :: (a -> b -> b) -> b -> [a] -> b
foldr' f z [] = z
foldr' f z (x:xs) = f x (foldr f z xs)

-- f combines x and the rest
-- but also it can mess with x

--xs = map show [1..5]
xs = ["1","2","3","4","5"]
y = foldr (\x z -> concat ["(",x,"+",z,")"]) "0" xs
-- se eu pensava em juntar com o +, agora estou juntando com uma funcao que representa o +


myAny :: (a -> Bool) -> [a] -> Bool
myAny f xs =
    foldr (\x b -> f x || b) False xs

-- || non strict
--Prelude> myAny even [1..]
--True
-- The function of the fold could return independent of its second argument

-- Prelude> xs = [1,2,3,4,undefined] only if I need the fifth element
    -- will there be trouble
-- Prelude> xs = [1,2,3,4] ++ undefined -- if I need the spine up to the fifth, there will be trouble (eg: length)

-- plain weird
-- Prelude> let xs = [1, 2] ++ undefined
-- Prelude> length $ take 2 $ take 4 xs


-- Prelude> xs = [1,2,3,4] :: [Int]
-- Prelude> morexs = xs ++ undefined 
-- Prelude> a = take 5 morexs 
-- Prelude> take 2 a
-- [1,2]
-- Prelude> :sprint morexs 
-- morexs = 1 : 2 : _
-- Prelude> b = take undefined morexs 
-- Prelude> 
-- Prelude> b -> dá pau

-- Prelude> foldr (\_ _ -> 9001) 0 undefined
-- *** Exception: Prelude.undefined
-- Prelude> let xs = [undefined, undefined]
-- Prelude> foldr (\_ _ -> 9001) 0 xs
-- 9001


-- foldl :: (b -> a -> b) -> b -> [a] -> b
-- foldl f acc [] = acc
-- foldl f acc (x:xs) = foldl f (f acc x) xs

-- foldr :: (a -> b -> b) -> b -> [a] -> b
-- foldr f z [] = z
-- foldr f z (x:xs) = f x (foldr f z xs)

-- (replace fold with scan to see intermediate results)

-- foldr is classic recursion. Maybe I am happy with my result,
-- maybe I continue

-- foldl is classic while. The acumumation function does just that

-- the argument function varies as necessary

-- Prelude> l = [1..5] :: [Int]
-- Prelude> foldr const 0 l
-- 1
-- Prelude> :sprint l
-- l = 1 : _
-- Prelude> foldl const 0 l
-- 0
-- Prelude> :sprint l
-- l = [1,2,3,4,5]

-- Prelude> l = [1..5] :: [Int]
-- Prelude> foldl (flip const) 0 l
            --  mais facil de pensar como funcao binaria
            -- que retorna o segundo argumento
-- 5
-- Prelude> :sprint l
-- l = [1,2,3,4,5]

-- Prelude> foldr (flip const) 0 "burritos"
-- 0
-- Prelude> foldr (const) 0 "burritos"
-- pau
-- pau pq? pq o tipo da resposta é o tipo do acumulador

-- foldl has a problem: evaluates the whole spine before the first element. He says there exists a foldl' for this
-- https://hoogle.haskell.org/?hoogle=foldl%27&scope=set%3Astackage

-- Prelude> trios string = foldr (\s acc -> (take 3 s) ++ " " ++ acc ) "" string
-- Prelude> trios ["wise", "mens", "speak", "love", "game"]
-- "wis men spe lov gam "
-- Prelude> concat $ map (take 3) ["wise", "mens", "speak", "love", "game"]
-- "wismenspelovgam"

-- Prelude> trios string = foldl (\acc s -> acc ++ " " ++ (take 3 s)) "" stringPrelude> trios ["wise", "mens", "speak", "love", "game"]
-- " wis men spe lov gam"
-- Prelude> drop 1 $ trios ["wise", "mens", "speak", "love", "game"]
-- "wis men spe lov gam"
-- Prelude> 

-- Prelude> :{
-- *Prelude| let f a b = take 3
-- *Prelude|
--  (a :: String) ++
-- *Prelude|
--  (b :: String)
-- *Prelude| :}
-- Prelude> foldr f "" pab
-- "PizAppBan"


--typechecking trick
threes :: [String] -> String  
threes = foldr (\x l -> (take 3 x)++(l :: String)) [] 

-- foldr f z xs =
-- foldl (flip f) z (reverse xs)
-- But only for finite lists!

myOr = foldr1 (||) :: ([Bool] -> Bool)

(...) = (.).(.)
myAny' :: (a->Bool) -> [a] -> Bool
myAny' =  myOr ... map 

myElem el = myAny' (el ==)

-- Nota
myElem' :: Eq a => a -> [a] -> Bool
myElem' = myAny' . (==) 

myElem'' x xs = foldr (\y acc -> (y == x) || acc) False xs

myElem''' x = foldr (\y acc -> (y == x) || acc) False

test :: Eq a => a -> [a] -> [Bool]
test = map . (==)

myElem'''' :: Eq a => a -> [a] -> Bool
myElem'''' = (foldr1 (||)) ... (map . (==))

-- foldl e foldl' : o fold' nao acumula as computacoes pro final. 

-- foldl :: (b -> a -> b) -> b -> [a] -> b
-- foldl f acc [] = acc
-- foldl f acc (x:xs) = foldl f (f acc x) xs
-- Olhe para o codigo do foldl, ele nao precisa avaliar (f acc x), entao acaba acumulando uma quantidade de f nao avaliado (com argumento f nao avaliado). O foldl' nao tem isso