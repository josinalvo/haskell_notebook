
{-# LANGUAGE FlexibleInstances #-}

newtype Constant a b =
  Constante { getConstant :: a }
  deriving (Eq, Ord, Show)

instance Functor (Constant a) where
  -- f :: b -> c
  -- Constant a :: * -> *
  -- Constante azinho :: Constant azao bzao 
  fmap f (Constante azinho) = Constante azinho

-- instance Applicative (Constant Int) where
--   (Constante x) <*> (Constante y) = Constante x
--   pure x = Constante 42


instance Monoid azao => Applicative (Constant azao) where
  (Constante x) <*> (Constante y) = Constante (x <> y)
  pure x = Constante mempty

fmap id = id                   -- 1st functor law
fmap (g . f) = fmap g . fmap f -- 2nd functor law

pure f <*> pure x = pure (f x)               -- Homomorphism
[show] <*> [4] = [show 4] = ["4"]  
pure id <*> v = v                            -- Identity
[id] <*> [1,2,3,4] = [id(1),id(2),id(3),id(4)] = [1,2,3,4]

#f x             ($ y)      f
u <*> pure y = pure ($ y) <*> u              -- Interchange 
[(+1), (^2)] <*> [1]  = [($ 1)] <*> [(+1), (^2)] 
[2, 1]                = [($ 1)(+1), ($ 1)(^2)] = [(+1)1, (^2)1] =  [2, 1]

pure (.) <*> u <*> v <*> w = u <*> (v <*> w) -- Composition 
[(.)] <*> [show] <*> [(+1), (^2)] <*> [3]
[(.) show] <*> [(+1), (^2)] <*> [3]
[(.) show (+1), (.) show (^2)] <*> [3]
[show . (+1), show . (^2)] <*> [3]
[(show . (+1)) 3, (show . (^2)) 3]
[(show 4), (show 9)]
["4","9"]

do outro lado...
[show] <*> ([(+1), (^2)] <*> [3])
[show] <*> ([(+1), (^2)] <*> [3])
[show] <*> ([4, 9])
["4", "9"]

fmap f v = pure(f) <*> v  -- theorema 1
fmap f (Constante azinho) = Constante mempty <*> Constante azinho
  Constante azinho        =   Constante azinho

canditado a teorema (suponha q existe um pure, ele é igual a isso)
pure (f)
g = \x -> f
fmap g pure("banana")

aplicar o teorema 1
fmap g pure("banana") = pure(g) <*> pure("banana")
Homomorphism
pure (g "banana") = pure(f)

ou seja, pure (f) = fmap (\x -> f) pure("banana")