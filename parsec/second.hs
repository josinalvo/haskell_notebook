import Text.Parsec
import Text.Parsec.String (Parser)
import Data.Char

digits :: Parser String
digits = many1 digit

a = parse digits "ignored banana" "123 caralhoanus"
-- !
-- >>> a
-- Right "123"

-- | fmap in parsers is analogous to in functions
-- >>> f = fmap (+1) (10*)
-- >>> f 2
-- 21
-- >>> g = fmap ((+1) . read :: String -> Int) digits
-- >>> parse g "ignored orangutan" "123 caralhoanus"
-- Right 124

-- | and now for the applicative
-- >>> parse (pure 2) "ignored koala" "123 caralhoanus"
-- Right 2

idiotic_parser = do 
    pure 2
    digits

-- | continuing example
-- >>> parse idiotic_parser "ignore larvae" "123 cacete de agulha"
-- Right "123"
main = print 42

-- | the applicative is also similar to functions
-- >>> f = pure (,,) <*> (+1) <*> (+2) <*> (+3)
-- >>> f 10
-- (11,12,13)
-- >>> g = pure (,,) <*> digits <*> spaces <*> digits
-- >>> parse g "ignored rhino" "123   456"
-- Right ("123",(),"456")

-- |
-- >>> h = pure (,,) <*> (fmap (read :: String -> Int) digits) <*> spaces <*> digits
-- >>> parse h "ignored risk of rain" "123   456"
-- Right (123,(),"456")

parenthesis = fmap (read :: String -> Int) $ between (char '(') (char ')') digits

parenthesis2 = pure(read :: String -> Int) <*> (char '(' *> digits <* char ')')

-- |
-- >>> parse parenthesis "ig" "(123) 456"
-- Right 123
-- >>> parse parenthesis2 "ig" "(123) 456"
-- Right 123

strToLower = map toLower


t3 :: Parsec String () String
t3 =  fmap (map toUpper) (pure (\x y -> [x,y]) <*> oneOf "abc" <*> oneOf "xyz")


-- |
-- >>> parse t3 "ignored zebra" "by"
-- Right "BY"