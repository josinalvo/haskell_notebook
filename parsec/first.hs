
{-# LANGUAGE FlexibleContexts #-}

-- I import qualified so that it's clear which
-- functions are from the parsec library:
import qualified Text.Parsec as Parsec

-- I am the error message infix operator, used later:
import Text.Parsec ((<?>))

-- Imported so we can play with applicative things later.
-- not qualified as mostly infix operators we'll be using.
import Control.Applicative

-- Get the Identity monad from here:
import Control.Monad.Identity (Identity)

-- alias Parsec.parse for more concise usage in my examples:
parse rule text = Parsec.parse rule "(source)" text



ex1 string = case result of
        Right v -> "success!"++ show v
        Left err -> "whoops, error: "++show err
    where result = parse (Parsec.char 'H') string
    
-- $
-- >>> ex1 "Hollow"
-- "success...
-- >>> ex1 "Lower"
-- "whoops,...
main = print "hi"

-- |
-- >>> parse (Parsec.string "hello") "hello there"
-- Right "hello"
-- >>> parse (Parsec.oneOf "acbde") "ello there"
-- Right 'e'

-- Parsec.anyChar, Parsec.letter, Parsec.noneOf

-- |
-- >>> parse (Parsec.many (Parsec.char 'a')) "aahello"
-- Right "aa"
-- >>> parse (Parsec.many (Parsec.char 'a')) "hello"
-- Right ""
-- >>> parse (Parsec.count 3 (Parsec.char 'a')) "aaaa"
-- Right "aaa"


-- | many1 exige ao menos uma ocorrencia
-- >>> parse (Parsec.many1 (Parsec.char 'a')) "hello"
-- Left ...
-- ...


-- | o manytill exige seu finalizador
-- >>> parse (Parsec.manyTill (Parsec.anyChar) (Parsec.char 'a')) "hello"
-- Left ...
-- ...

-- | repare que o delimitador nao aparece
-- >>> parse (Parsec.manyTill (Parsec.anyChar) (Parsec.char 'a')) "hello amigo"
-- Right "hello "
-- >>> parse (Parsec.manyTill (Parsec.anyChar) (Parsec.char 'a')) "ahello amigo"
-- Right ""
-- >>> parse (Parsec.manyTill (Parsec.digit) (Parsec.letter)) "123abc"
-- Right "123"
-- >>> parse (Parsec.manyTill (Parsec.digit) (Parsec.letter)) "abc"
-- Right ""


-- there is no many1Till, but 2 matches do it nicely

myParser :: Parsec.Parsec String () (String,Int)
myParser = do 
    letter <- Parsec.many1 Parsec.letter
    Parsec.spaces
    digits <- Parsec.many1 Parsec.digit
    return (letter, read digits)


--                         in    ????   out
--                               ???? to keep state, like identation ???? -- https://www.cnblogs.com/ncore/p/6892500.html
myParser2 :: Parsec.Parsec String () (String,Int)
myParser2 = myParser

-- "just a" monad transformer
myParser1 :: Parsec.ParsecT String () Identity (String,Int)
myParser1 = myParser


-- | 
-- >>> parse myParser1 "hello 123"
-- Right ("hello",123)

--                                    no return
mySeparator :: Parsec.Parsec String () ()
mySeparator = do
    Parsec.spaces
    Parsec.char ','
    Parsec.spaces


mySeparator1dot5 :: Parsec.Parsec String () ()
mySeparator1dot5 = do 
    Parsec.spaces; Parsec.char ','; Parsec.spaces


-- "desugar" (is do sugar for monads?)
mySeparator2 :: Parsec.Parsec String () ()
mySeparator2 = Parsec.spaces >> Parsec.char ',' >> Parsec.spaces

myPairs :: Parsec.Parsec String () [(String,Int)]
myPairs = Parsec.many $ do
    pair <- myParser
    mySeparator2
    return pair

-- |
-- >>> parse myPairs "banana 2 , orange 2,avocado 3, "
-- Right [("banana",2),("orange",2),("avocado",3)]

-- | many explains next, mySeparator, second
-- >>> parse myPairs ""
-- Right []

-- | sadly I demand the last comma
-- >>> parse myPairs "banana 2, avocado 2"
-- Left ...
-- ...

myPairs2 = Parsec.sepBy myParser mySeparator2
-- | to fix, use sepBy
-- >>> parse myPairs2 ""
-- Right []
-- >>> parse myPairs2 "banana 2, avocado 2"
-- Right ...

myPairs3 = Parsec.endBy myParser mySeparator2
-- | endBy is sepBy but demands the last :(
-- >>> parse myPairs3 "banana 2, avocado 2"
-- Left ...
-- ...
-- >>> parse myPairs3 "banana 2, avocado 2,"
-- Right [("banana",2),("avocado",2)]


myPairs4 :: Parsec.Parsec String () [(String,Int)]
myPairs4 = Parsec.many $ do
    pair <- myParser
    Parsec.choice [Parsec.eof, mySeparator2]
    return pair

-- | The solution is choice
-- | Actually A solution not THE. This is just a bad matrix joke
-- >>> parse myPairs4 "banana 2, avocado 2,"
-- Right [("banana",2),("avocado",2)]
-- >>> parse myPairs4 "banana 2, avocado 2"
-- Right [("banana",2),("avocado",2)]

-- with choice, or its infix equivalent <|>

-- | the first rule that consumes goes to the end (or the failure)
-- >>> parse (Parsec.string "hello" <|> Parsec.string "howdy") "howdy"
-- Left...
-- unexpected "o"
-- ...

helloOrHowdy :: Parsec.Parsec String () String
helloOrHowdy = do
    first <- Parsec.char 'h'
    rest <- Parsec.string "ello" <|> Parsec.string "owdy"
    return (first:rest)
-- | bad solution
-- >>> parse helloOrHowdy "howdy"
-- Right "howdy"

helloOrHowdy2 :: Parsec.Parsec String () String
helloOrHowdy2 = Parsec.try (Parsec.string "hello") <|> Parsec.string "howdy"

-- | expensive the try may be
-- >>> parse helloOrHowdy2 "howdy"
-- Right "howdy"

-- | errors only talk about howdy!!
-- >>> parse helloOrHowdy2 "helo"
-- Left...
-- unexpected "e"
-- expecting "howdy"
helloOrHowdy3 :: Parsec.Parsec String () String
helloOrHowdy3 = Parsec.try (Parsec.string "hello") <|> Parsec.string "howdy" <?> "hello or howdy"

-- | no fix, would only help if no rule had comsumed input (see ex 2)
-- >>> parse helloOrHowdy3 "helo"
-- Left ...
-- unexpected "e"
-- expecting "howdy"
-- >>> parse helloOrHowdy3 "ahelo"
-- Left ...
-- unexpected "a"
-- expecting hello or howdy

-- | encase the whole damn thing on a try (makes sense!)
-- >>> parse ((Parsec.try helloOrHowdy2) <?> "one of two") "helo"
-- Left ...
-- unexpected "e"
-- expecting one of two

-- funny that we got unexpected "e", though. Might be a clever use of monads, with the try error being used since "one of two" does not give any natural stopping point

-- applicative style
myParserAppSmol :: Parsec.Parsec String () (String,String)
myParserAppSmol = pure ((,)) <*> Parsec.many1 Parsec.letter <*> Parsec.many1 Parsec.digit

-- |
-- >>> parse myParserAppSmol "abc123"
-- Right ("abc","123")

