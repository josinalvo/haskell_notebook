
from inspect import signature

class TooManyArgsException(Exception):
    pass

def curry(f):
    def curried(*args):
        if hasattr(f,'remaining_args'):
            remaining_args = f.remaining_args
        else:
            remaining_args = len(signature(f).parameters)
        if (remaining_args == len(args)):
            return f(*args)
        elif (remaining_args > len(args)):
            curried_args_received = args
            def waiting(*args):
                list_all_args = curried_args_received+args
                return f(*list_all_args)
            waiting.remaining_args = remaining_args-len(args)
            return curry(waiting)
        else:
            raise TooManyArgsException
    return curried

def sum_pair(a,b,c,d,e, secret=True):
    return a+b+c+d+e

ssum = curry(sum_pair)
