{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE MultiParamTypeClasses #-}
import Control.Monad
import Control.Monad.State (MonadState, MonadState(get,put))
import Control.Monad.Trans


newtype State s a = State { runState :: (s -> (a,s)) }

instance Monad (State s) where
    (>>=) a f = State $ \mem0 -> let (ans1,mem1) = runState a mem0
                                     (ans2,mem2) = runState (f ans1) mem1
                                 in (ans2,mem2)
    return  a = State (\s -> (a,s))

instance Applicative (State s) where
    (<*>) = ap
    pure  = return

instance Functor (State s) where
    fmap = liftM













newtype StateT s m a = StateT { runStateT :: (s -> m (a,s)) }

instance (Monad m) => Monad (StateT s m) where
    (>>=) (StateT a) f = StateT $ \mem0 -> do (ans1,mem1) <- a mem0
                                              let (StateT s2) = f ans1
                                              s2 mem1

                                    
    return  a = StateT $ \mem -> return (a,mem)

instance (Monad m) => Applicative (StateT s m) where
    (<*>) = ap
    pure  = return

instance (Monad m) => Functor (StateT s m) where
    fmap = liftM

instance (Monad m) => MonadState s (StateT s m) where
  get   = StateT $ \s -> return (s,s)
  put s = StateT $ \_ -> return ((),s)

instance MonadTrans (StateT s) where
    lift ma = StateT $ \ram -> do 
                                a <- ma
                                return (a,ram)



howMuchTime 5 = ("five",4)
howMuchTime 4 = ("four",3)
howMuchTime 3 = ("three",2)
howMuchTime 2 = ("two",1)
howMuchTime 1 = ("one",0)
howMuchTime 0 = ("zero",0)
howMuchTime n = ("enough",n-1)

howMuchS :: (Monad m) => StateT Integer m String
howMuchS = StateT $ \n -> return (howMuchTime n)

getInteger :: IO Integer
getInteger = do print "please type an extra time"
                read <$> getLine 


countdown :: StateT Integer IO ()
countdown = do n <- lift getInteger
               a <- get
               put $ n + a
               a <- sequence $ replicate 10 howMuchS 
               lift $ print a



main :: IO ((), Integer)
main = runStateT countdown 3