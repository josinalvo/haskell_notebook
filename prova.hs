import Data.Maybe

hasMark :: String -> Maybe Int
hasMark "" = Nothing
hasMark ('[':a:']':rest) = if valA `elem` [0..9] then (Just valA)
                         else hasMark rest
                             where valA = (read (a:""))
hasMark (_:rest) = hasMark rest

marks :: String -> Int
marks = (sum . catMaybes . (fmap hasMark) .lines)

solve :: String -> String
solve s = (show marks_total) ++ " from a total of " ++ (show maxMarks) ++ ", resulting in " ++ (show grade) ++ "\n"
    where
        marks_total = marks s
        grade       = (100 * marks_total) `div` maxMarks


stringTest = "[1] dois tres [4]\n esas nao tem nada\n banana [5]"


maxMarks :: Int
maxMarks = 32

-- >>> solve stringTest
-- 6

main = interact solve
