data Perhaps a = Only a | Nope deriving Show 

test = Only 12

-- $
-- >>> test
-- Only 12

instance Functor Perhaps
    where fmap f Nope = Nope
          fmap f (Only a) = Only (f a)

-- $ Functors podem sofrer uma coisa tipo map
-- >>> fmap (+1) test
-- Only 13

instance Applicative Perhaps
   where
       pure a = Only a
       (<*>) Nope _ = Nope
       (<*>) _ Nope = Nope
       (<*>) (Only f) (Only a) = Only (f a)

-- $ Applicatives sao functors que podem conter funcoes. 
-- >>> pure (+1) <*> test
-- Only 13
-- >>> you = 10
-- >>> a = pure (*) <*> Only you 
-- >>> a <*> Only 17
-- Only 170

instance Monad Perhaps
   where
       --return me = Only me -- I commented this conjecturing that pure would be enough, and seems so. It still works
       return me = Only me
       (>>=) Nope _ = Nope
       (>>=) (Only you) f = f you
      
-- ze rulez for ze do
-- do { x }  -- >  x
-- do {x ; <xs> }  -- >  x >> do { <xs> }
-- do { a <- x ; <xs> }  -- >  x >>= \a -> do { <xs> }
-- do { let <declarations> ; xs } --> let <declarations> in do { xs }

-- seek2 n1 n2 container = do
--     step1 <- container n1
--     step1 n2             -- isso daqui vai virar uma funcao, 
                            -- f = (\step1 -> step1 n2) e receber o valor sem o Only
                            -- Ou ... se step1 for Nope, vai ser ignorado totalmente


-- estamos emulando uma estrutura de acesso em que um acesso erroneo retorna Nope
-- O exemplo que eu tinha era com Vector, mas isso vai complicar desnecessariamente, acho
f1 1 = Only 100
f1 2 = Only 200
f1 3 = Only 300
f1 _ = Nope

f2 1 = Only 10
f2 2 = Only 400
f2 3 = Only 90000
f2 _ = Nope

g 1 = Only f1
g 2 = Only f2
g _ = Nope

seek1 n1 n2 container = container n1 >>= \step1 -> step1 n2

-- or we could use a do
-- do { a <- x ; <xs> }  -- >  x >>= \a -> do { <xs> }

seek2 n1 n2 container = do
    step1 <- container n1
    step1 n2

-- Only x (nao estou falando sobre x) esta no step1, ele é descascado pela definicao de >>=

-- $
-- >>> seek1 1 1 g
-- Only 100
-- >>> seek1 1 4 g
-- Nope
-- >>> seek1 4 1 g
-- Nope


-- $
-- >>> seek2 1 1 g
-- Only 100
-- >>> seek2 1 4 g
-- Nope
-- >>> seek2 4 1 g
-- Nope


main = print 42 -- to stop doctest from complaining
