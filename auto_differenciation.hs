{-# LANGUAGE TypeSynonymInstances #-}
{-# LANGUAGE InstanceSigs #-}
{-# LANGUAGE FlexibleInstances #-}
data Dual d = D Float d deriving Show
type Float' = Float

diff :: (Dual Float' -> Dual Float') -> Float -> Float'
diff f x = y'
    where D y y' = f (D x 1)

class VectorSpace v where
    zero  :: v
    add   :: v -> v -> v
    scale :: Float -> v -> v

instance VectorSpace Float' where
    zero  = 0
    add   = (+)
    scale :: Float -> Float' -> Float'
    scale = (*)

instance VectorSpace d => VectorSpace (d,d) where
    zero  = (zero,zero)
    add  (a,b) (c,d) = (,) (add a b) (add c d)
    scale s (a,b) = (,) (scale s a) (scale s b)

instance VectorSpace d => Num (Dual d) where
    (+) (D u u') (D v v') = D (u+v) (add u' v')
    (*) (D u u') (D v v') = D (u*v) (add (scale u v') (scale v u'))
    negate (D u u')       = D (negate u) (scale (-1) u')
    signum (D u u')       = D (signum u) zero
    abs    (D u u')       = D (abs u) (scale (signum u) u')
    fromInteger n         = D (fromInteger n) zero

instance VectorSpace d => Fractional (Dual d) where
    (/) (D u u') (D v v') = D (u/v) (scale (1/v^2) (add (scale v u') (scale (-u) v')))
    fromRational n        = D (fromRational n) zero

instance VectorSpace d => Floating (Dual d) where
    pi             = D pi zero
    exp   (D u u') = D (exp u)  (scale (exp u) u')
    log   (D u u') = D (log u)  (scale (1/u) u')
    sin   (D u u') = D (sin u)  (scale (cos u) u')
    cos   (D u u') = D (cos u)  (scale (-sin u) u')
    sinh  (D u u') = D (sinh u) (scale (cosh u) u')
    cosh  (D u u') = D (cosh u) (scale (sinh u) u')

f x = x + 10

-- |
-- >>> f 1
-- 11
-- >>> f (D 1 1) :: Dual Float'
-- D 11.0 1.0
-- >>> ((D 1 0)+2) :: Dual Float'
-- D 3.0 0.0
-- >>> (2+(D 1 0)) :: Dual Float'
-- D 3.0 0.0

g x y = x^2 + 3*y

-- |
-- >>> x=4;y=5
-- >>> g (D x (1,0)) (D y (0,1)) :: Dual (Float',Float')
-- D 31.0 (8.0,3.0)




