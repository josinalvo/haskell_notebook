{-# LANGUAGE InstanceSigs #-}
{-# LANGUAGE ScopedTypeVariables #-}


-- exemplo bobo que eu usei pra localizar o pragma certo
data T a = MkT a a
instance Eq a => Eq (T a) where
  (==) :: T a -> T a -> Bool   -- The signature
  (==) (MkT x1 x2) (MkT y1 y2) = superequal x1
         where  superequal :: a -> Bool
                superequal x1 = x1 == y1 && x2 == y2