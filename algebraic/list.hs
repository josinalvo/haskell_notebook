import Data.Int
bound = maxBound :: Int8

data ListT a = Empty | List a (ListT a) deriving (Eq)

tinha = List 2 Empty
ta = List 3 tinha

fst :: ListT a -> a
fst (List x l) = x
