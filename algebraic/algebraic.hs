{-# LANGUAGE FlexibleInstances #-}
-- see tuple checksize, for some reason it was not allowed -- sb else had this problem when creating an instance for [Char]

module Algebraic where


-- #type level versus term level

-- #:k describes a type constructor, such as List above
-- # * Means concrete type
-- # * -> * Means I await a type to become concrete
-- # :k Empty gives and error
-- # To be used with type constructor List a, not with data constructors Empty, List a (List a)
-- #Type constructor in the sense that it can receive a type and create a new type
-- # (also in the sense "left side")

-- #procuct
-- -- product of Int and String
-- data Example2 =
-- Example2 Int String
-- deriving (Eq, Show)

newtype Tel = Tel Int deriving (Show, Eq, Ord)
-- cant be product, cant be sum, but still checks type (if a function wants a Tel, then it only works with tel)
-- saves computation. It is only useful for typechecking
-- https://wiki.haskell.org/Newtype
-- deriving necessary unless you use a pragma GeneralizedNewTypeDeriving

-- can have different instances than parent
class CheckSize a where
    tooBig :: a -> Bool

instance CheckSize Int where
    tooBig = (> billion)
            where billion = 1000*1000*1000

instance CheckSize Tel where
    tooBig = (>  (Tel $ 1000*1000))

type Cpf = Int

-- |
-- >>> tooBig $ Tel (1000*1000*2)
-- True
-- >>> a = 1000*1000* 2 :: Cpf
-- >>> tooBig a
-- False


instance (Integral a) => CheckSize (a,String) where
    tooBig (i,s) = (i > 1000)

instance CheckSize (Int, Int) where
    tooBig (i,s) = (i > 1000)

instance (CheckSize a) => CheckSize (Int, a) where
    tooBig (i,s) = (i>1000) || (tooBig s)

