module ThisOnlyHasANameForDocTest where



type Cpf = Int
-- it is removed by the compiler
f :: Cpf -> Cpf
f = (+ 1)
-- | works with pure Int
-- >>> f (2 :: Int)
-- 3

-- f is valid, f (12::Cpf) is valid, Cpf 12 is not
-- btw, there is no data constructor for Int, I think those are related

type AssocList k v = [(k,v)]

a = [("lucas",3),("cicero",5)] :: [(String, Int)]

-- | returns first value
-- >>> g a
-- 3
g :: (AssocList String Int) -> Int
g a = snd . (!! 0) $ a

