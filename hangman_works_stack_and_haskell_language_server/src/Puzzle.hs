module Puzzle where

import Data.List (intersperse) -- [4]
import Data.Maybe (isJust) -- [3]

type CharsAndDashes = [Maybe Char]
type Guesses        = [Char]

data Puzzle = Puzzle String CharsAndDashes Guesses

instance Show Puzzle where
  show (Puzzle _ p g) = chars_and_dashes ++ guesses
       where judge Nothing  = '_'
             judge (Just l) = l
             chars_and_dashes = intersperse ' ' $ map judge p
             guesses = (++) "\nGuesses so far: "  $ intersperse ' ' $ reverse g



fill :: Puzzle -> Char -> Puzzle
fill (Puzzle s cs_ds g) char = (Puzzle s (map newpos tuples) g)
   where tuples = zip s cs_ds
         newpos (_, Just a ) = (Just a)
         newpos (a, Nothing)     
           | (a == char)     = (Just a)
         newpos (_,  _     ) = Nothing

guess p@(Puzzle s cs_ds g) char = (Puzzle s new_chrs_dash new_guesses)
   where new_guesses                      = char:g
         (Puzzle _ new_chrs_dash _)       = fill p char

word2puzzle s = Puzzle s dashes []
  where dashes = map (\x -> Nothing) s


incorrectGuesses (Puzzle s _ g) = length $ filter notInS g
  where notInS c = c `notElem` s

maxGuesses = 5
loss puzzle = incorrectGuesses puzzle > maxGuesses

victory (Puzzle _ cs_or_ds _) = all isJust cs_or_ds

-- |
-- >>> a = word2puzzle "banana"
-- >>> a
-- _ _ _ _ _ _ 
-- Guesses so far:
-- >>> b = guess a 'c'
-- >>> b
-- _ _ _ _ _ _ 
-- Guesses so far: c
-- >>> c = guess b 'n'
-- >>> c
-- _ _ n _ n _ 
-- Guesses so far: c n
-- >>> incorrectGuesses c
-- 1
-- >>> d = guess c 'h'
-- >>> incorrectGuesses d
-- 2
-- >>> guess a 'a'
-- _ a _ a _ a 
-- Guesses so far: a




--main = print 42