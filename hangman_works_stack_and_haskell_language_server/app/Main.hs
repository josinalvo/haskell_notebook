module Main where

import Control.Monad (forever) -- [1]
import Data.Char (toLower) -- [2]


import System.Exit (exitSuccess) -- [5]
import System.Random (randomRIO) -- [6]

import Puzzle

type WordList = [String]
allWords :: IO WordList
allWords = do
           dict <- readFile "data/dict.txt"
           return (lines dict)

minWordLength = 5
maxWordLength = 9
goodSize w = (size >= minWordLength) && (size <= maxWordLength)
    where size = length w

gameWords :: IO WordList
gameWords = do fmap (filter goodSize) allWords -- apply filter goodSize to the content of allWords

randomWordAux :: WordList -> IO String
randomWordAux wl = do 
         idx <- randomRIO(0,length wl)
         return $ wl !! idx

randomWord :: IO String
randomWord = do
  wl <- gameWords
  randomWordAux wl

randomWord' :: IO String
randomWord' = gameWords >>= randomWordAux



game :: IO ()
game = forever $ do
  word <- randomWord'
  let puz = word2puzzle word
  print "a new game starts!"
  print puz
  step puz

checkOver :: Puzzle -> IO()
checkOver puz@(Puzzle s _ _) = case (loss puz, victory puz) of
   (_,True)     -> print "correct" >> return()
   (True,_)     -> print ("the word was:" <> s) >> return()
   (False,False)-> step puz

letterAsker :: IO Char
letterAsker = do  print "guess a letter, or '" 
                  c <- getLine
                  case c of
                    "" -> letterAsker
                    _  -> return $ head c

step :: Puzzle -> IO()
step puz = do
       char <- letterAsker
       let puz2  = guess puz char
       print puz2
       checkOver puz2

palindrome :: IO ()
palindrome = forever $ do
      line1 <- getLine
      case (line1 == reverse line1) of
        True -> putStrLn "It's a palindrome!"
        False -> putStrLn "Nope!" >> exitSuccess

main = game       

