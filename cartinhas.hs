parseMove :: String -> (Int,Int,Int)
parseMove string = (amount,from,to)
    where [_,amount,_,from,_,to] = map read . words $ string -- lazy was clever here

m1 = "move 10 from 8 to 4"