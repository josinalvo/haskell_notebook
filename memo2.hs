import Data.Map

-- para te lembrar: com state, map e aquela coisa log n, temos tudo

factorials = 1 : 1 : fmap factorial [2..]

factorial 0 = 1
factorial 1 = 1
factorial n = factorials !! (n-1) * n


fact n = aux n
    where myPairs = zip [0..n] $ 1: 1: fmap aux [2..n]
          aux  0  = 1
          aux  1  = 1
          aux  a  = a * myDict ! (a-1)
          myDict  = fromList myPairs

-- gdcM :: (Integral a,Show a) => a -> a -> a

gdcM n m = aux (n,m)
    where biggest = max m n
          indices = do 
                       a <- [0..biggest] 
                       b <- [0..biggest]
                       return (a,b)
          myPairs = zip indices $ fmap aux indices
          myDict  = fromList myPairs
          aux (a,b) = let (small, big) = (min a b, max a b) in
                        if small == 0 then big else myDict ! ( big `mod` small, small)

-- |
-- >>> gdcM 12 60
-- 12
-- >>> fact 5
-- 120


main = print 42