module Funny where


-- Definir um monoide a partir de um Semigroup
-- a qualquer

data Perhaps a = Nope | Only a deriving Show
instance Semigroup a => Semigroup (Perhaps a) where
        Nope <> x = x
        x <> Nope = x
        (Only a) <> (Only b) = Only (a <> b)

instance Semigroup a => Monoid (Perhaps a) where
        mempty = Nope

-- |
-- >>> mconcat [Only [1,2,3], Only [10,20]]
-- Only [1,2,3,10,20]
-- >>> mconcat [] :: Perhaps [Int]
-- Nope

newtype Max a = Max a deriving (Show, Eq)

instance Ord a => Semigroup (Max a) where
  (Max a) <> (Max b) = Max $ max a b

instance (Ord a, Bounded a) => Monoid (Max a) where
  mempty = Max minBound

-- |
-- >>> mappend (Max (10 :: Int)) (Max 100)
-- Max 100
-- >>> mconcat [(Max (10 :: Int)), (Max 15)]
-- Max 15
-- >>> (mempty :: (Max Int)) == Max minBound
-- True



