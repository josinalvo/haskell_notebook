import Data.Monoid
import Test.QuickCheck
monoidAssoc :: (Eq m, Monoid m) => m -> m -> m -> Bool
monoidAssoc a b c = (a <> (b <> c)) == ((a <> b) <> c)

type S = String
type B = Bool

specialized = (monoidAssoc :: S -> S -> S -> B)

-- run quickCheck specialized
-- or verboseCheck specialized
-- There is some care to be taken with type defaulting, see the past chapter "testing", that I did not see yet
-- An arbitraty instance exists to define frequencies on the test