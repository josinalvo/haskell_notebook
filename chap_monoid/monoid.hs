module TestMonoid where

import Data.Monoid



-- class Monoid m where
--     mempty :: m
--     mappend :: m -> m -> m -- <> is the infix version
--     mconcat :: [m] -> m
--     mconcat = foldr mappend mempty

-- my understanding: mappend is the end of the mconcat :P

-- btw, there are types Sum and Product to make Num into 2 monoids

a = mconcat $ map Sum [1,2,3,4,5]

-- |
-- >>> a 
-- Sum {getSum = 15}

-- NOTA; como eu volto isso pra um Num ou algo que o valha?

a_back = getSum a

-- Newtype: light wrapper, no overhead

-- monoid laws: id<>a=a, a<>id=a, assoc (a<>b<>c pode ser avaliado nas duas ordems possiveis com o mesmo resultado), definicao de mconcat


-- Prelude> import Data.Monoid
-- Prelude> All True <> All True
-- All {getAll = True}
-- Prelude> All True <> All False
-- All {getAll = False}
-- Prelude> Any True <> Any False
-- Any {getAny = True}
-- Prelude> Any False <> Any False
-- Any {getAny = False}

-- mappend can be interpreted as condense, reduce


--NOTE does frac imply eq? NOPE
apply :: (Fractional n, Eq n) =>  n -> Maybe n
apply n 
    | (n == 0)    = Nothing
    | otherwise   = Just (10 / n)

myFunc :: Monoid a => a -> a
myFunc = undefined   

results2 = map apply [3,4,5,0]
results3 = filter notNothing results2

notNothing Nothing = False
notNothing _ = True

data Optional a = Nada | Only a deriving (Eq, Show)

instance (Semigroup a) => Semigroup (Optional a) where
    (<>) (Only a) (Only b) = Only ((<>) a b)
    (<>) (Only a) Nada     = (Only a) 
    (<>) Nada (Only a)     = (Only a) 
    (<>) Nada Nada         = Nada
    

instance (Monoid a) => Monoid (Optional a) where
   -- this is generic. Did not test. No need to define it
    -- mconcat xs = foldr mappend Nada xs
    mempty = Nada

listOf :: (Integral a) =>  a -> (Optional [a])
listOf n 
    | (n < 0)    = Nada
    | otherwise   = Only [0..n]
   

results4 = map listOf [0,1,2,0,-1]
results5 = mconcat results4

--orphan instance: you gave a type (such as Int) an instance of a typeClass (such as Eq). It can happen twice if in different files! His solution is basically putting the instance in the definition of either (the type of the typeclass) -- creating a new (tyclasss?) if necessary

--"Dizemos que Int é um type que tem uma instancia da Typeclass Eq."


type Verb = String
type Adjective = String
type Adverb = String
type Noun = String
type Exclamation = String


madlibbinBetter' :: Exclamation -> Adverb -> Noun -> Adjective -> String
madlibbinBetter' e adv noun adj = mconcat [e, adv]
