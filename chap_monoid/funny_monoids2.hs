


{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE FlexibleContexts #-}

module Funny2 where
import Prelude(Ord, foldr, const, Int, (++))
import qualified Data.Map as Map

class Semigroup a where
    (<>) :: a -> a -> a

class Semigroup a => Monoid a where
        mempty  :: a
        mappend :: a -> a -> a
        mappend = (<>)
        {-# INLINE mappend #-} -- o que eh isso? Copiei e colei de
        -- https://hackage.haskell.org/package/base-4.15.0.0/docs/src/GHC-Base.html#Monoid
        mconcat :: [a] -> a
        mconcat = foldr mappend mempty

instance Semigroup [a] where
    (<>) a b = a ++ b

instance Monoid [a] where
    mempty = []

instance Semigroup b => Semigroup (a -> b) where
  f <> g = \x -> (f x) <> (g x)

instance forall b a . Monoid b => Monoid (a -> b) where
  mempty = \x -> (mempty ::b)

f :: Int -> [Int]
f a = [a]
g :: Int -> [Int]
g a = [a,a]
h :: Int -> [Int]
h = f <> g

-- |
-- >>> f a = [a]
-- >>> f 2
-- [2]
-- >>> g a = [a,a]
-- >>> g 3
-- [3,3]
-- >>> h = f <> g
-- >>> h 5
-- [5,5,5]
-- >>> h' = mconcat [f,g]
-- >>> h' 7
-- [7,7,7]
