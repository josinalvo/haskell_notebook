stack is build on top of cabal
build large libraries only once
import qualified Data.Bool as B

:set prompt "Lambda> " -> instead of keeping all the libraries in the prompt



Your compiler (not just Stack) will reject using a file that
isn’t a Main module as the entry point to executing the
program. (have compiled others b4...)

:browse Data.Maybe

# seno's golden path
stack new proj22 new-template
cp hask3/hie.yaml proj33/

se for necessario adicionar algo, mexer no package.yaml

parece estar funcionando

# stack

stack is build on top of cabal
build large libraries only once

stack build might need stack setup:

The setup command for Stack determines what version of
GHC you need based on the LTS snapshot specified in the
stack.yaml file of your project. The stack.yaml file is used to
determine the versions of your packages and what version

stack exec -- hello: roda
o build havia criado o binario. LInha do build: Linking .stack-work/dist/x86_64-linux-tinfo6/Cabal-2.4.0.1/build/hello/hello ...

# Monads
>>> return 12 :: [Int]
>>> [12]

## two lambdas
main = do
    a <- getLine 
    a <- getLine
    putStr a
recall that  `do { a <- x ; <xs> }  -- >  x >>= \a -> do { <xs> }`

## return 2
twoo :: IO Bool
twoo = do 
    c  <- getChar
    c' <- getChar
    c == c'

Pau? sim, Pq? pq twoo era pra ser IO Bool, nao bool


# IO
main = do
    hSetBuffering stdout NoBuffering -- <- so that putStr prints immediatelly
    putStr "Please input your name: "
    name <- getLine 
    a <- hello name -- arrow is completely useless, but did bind a result for later
    dog


Remember, getLine has type IO String, name has type String.




# hello.cabal

why did stack build create hello?
in hello.cabal, see "executable hello" stanza

library
  hs-source-dirs:      src
  exposed-modules:     Hello <- note: no hs
  (...)

in future: 
  exposed-modules:     Hello
                       , Dog

executable hello
  hs-source-dirs:      exe, src -> search the library on src
  main-is:             Main.hs
  default-language:    Haskell2010
  build-depends:       base >= 4.7 && < 5
                       , hello -> or just use it (if already declared elsewhere)
                               -> minuscule :(
