module Hello where

hello :: String -> IO ()
hello name = do
  putStrLn ("hello "++name)
