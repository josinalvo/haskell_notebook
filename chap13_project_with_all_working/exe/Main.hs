module Main where

import Hello
import Dog
import Hero
import Lib


import System.IO

main :: IO ()
main = do
    hSetBuffering stdout NoBuffering -- <- so that putStr prints immediatelly
    putStr "Please input your name: "
    name <- getLine 
    b <- hello name -- arrow is completely useless, but did bind a result for later
    dog >> hero
    print $ f 3


fool = getLine >>= (\a -> print $ "hi " ++ a) --needlessly similar to the reduction
fool2 = getLine >>= print . ("hi " ++ )
noo = print =<< getLine
