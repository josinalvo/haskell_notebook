import Control.Monad

infixl 1 *+* 
(*+*) xs f = concat (map f xs)
-- mesmo codigo do >>= que ja tinhamos para listas

a = [1,2] >>= \n -> (['a','b'] >>= \ch -> return (n,ch))
b = [1,2] >>= (\n -> ['a','b'] >>= \ch -> return (n,ch))
c = [1,2] *+*  \n -> ['a','b'] *+* \ch -> return (n,ch)

-- b and c are equivalent because of the prececence. -> is -1 and >>= is one
-- (even though the associativity suggests they would not be eq)

listOfTuples :: [(Int,Char)]  
listOfTuples = do  
    n <- [1,2]  
    ch <- ['a','b']  
    return (n,ch)

-- |
-- >>> listOfTuples
-- [(1,'a'),(1,'b'),(2,'a'),(2,'b')]

-- do { a <- x ; <xs> }  -- >  x >>= \a -> do { <xs> }
-- isso faz todo sentido, a eh variavel pra todas as outras linhas
-- In fact, list comprehensions are just syntactic sugar for using lists as monads.

j = do 
    x <- [6..10]
    ['7' `elem` show x]
    
-- |
-- >>> j
-- [False,True,False,False,False]

h = do 
    x <- [6..10]
    if ('7' `elem` show x); then [x]; else [0]

-- |
-- >>> h
-- [0,7,0,0,0]

-- h
-- segunda linha vira funcao que recebe x, por causa do do
-- fazemos concat (map f [lista da linha anterior])

-- |
-- >>> guard (5 > 2) >> return "cool" :: [String]  
-- ["cool"]
-- >>> guard (5 > 20) >> return "cool" :: [String]  
-- []

-- guard produziu ou a lista vazia ou uma lista [()]
-- se foi essa ultima, podemos rodar o map (que map meu caralho? nao eh >>=)

-- k >> f = k >>= \_ -> f
-- ele vai rodar, e vai querer saber o tamanho da lista, mas nao vai usar o conteudo da lista como input
-- ele, no caso, eh concat (map f [()]) e f é o return

g = do
    x <- [0..50]
    guard ('7' `elem` show x)  -- >> entre essas duas. Return soh eh executado com guard valido, valendo [()]
    return x -- lembre-se que o x foi passado no lambda das ultimas duas linhas. Basicamente foi bound

-- |
-- >>> g
-- [7,17,27,37,47]

abstractMoves = do
    x <- [1,(-1),2,(-2)]
    y <- [1,(-1),2,(-2)]
    guard (abs x /= abs y)
    [(x,y)]

movesFrom (x,y) = do
    delta <- abstractMoves
    (x', y') <- [(x + fst delta, y + snd delta)]
    guard (x' `elem` [1..8] && y' `elem` [1..8])
    return (x',y')

-- |
-- >>> movesFrom (8,8)
-- [(7,6),(6,7)]

path p_size start end = p' p_size [start] end
    where p' 1 starts end = end `elem` (starts >>= movesFrom)
          p' n starts end = p' (n-1) (starts >>= movesFrom) end


type Square = (Int,Int)
type Path = [Square]
fpath p_size start end = p' p_size [[start]] end
    where p' :: Int -> [Path] -> Square -> [Path]
          p' 0 starts end = [start | start <- starts, last start == end]
        --   p' 0 starts end = do
        --                     start <- starts
        --                     guard (last start == end)
        --                     return start
          p' n starts end = p' (n-1) paths end
                  --paths =  [start++[next] | start<-starts, next<-(movesFrom (last start))]
              where paths = do
                    start <- starts
                    next <- movesFrom (last start)
                    return $ start++[next]


-- confusing but correct syntax
surprise 1 (a,b) [[tender]] = fmap

--to_hit_in 1 start [[goal]] = do

-- |
-- >>> path3 = path 3
-- >>> (6,2) `path3` (6,1) 
-- True
-- >>> (6,2) `path3` (7,3)
-- False 


main = print 42