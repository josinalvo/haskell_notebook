module Laws where

-- | pure id <*> v = v                            -- Identity
-- >>> a = pure id
-- >>> a <*> [1,2,3,4]
-- [1,2,3,4]


-- | pure f <*> pure x = pure (f x)               -- Homomorphism
-- >>> [(+5)] <*> [10]
-- [15]
-- >>> a = pure(+5) :: [(Int->Int)]
-- >>> a <*> pure 10
-- [15]


-- | u <*> pure y = pure ($ y) <*> u              -- Interchange
-- u has to be of the relevant Applicative type... (<*>) :: f (a -> b) -> f a -> f b
-- >>> pure ($ 4) <*> [(+1)]
-- [5]
-- >>> [(+1)] <*> pure(4)
-- [5]

-- | pure (.) <*> u <*> v <*> w = u <*> (v <*> w) -- Composition
-- >>> pure(.) <*> [(+1)] <*> [(*2)] <*> [3]
-- [7]
-- >>> [(+1)] <*> ([(*2)] <*> [3])
-- [7]

-- demonstration of the extra law: fmap f l = pure f <*> l
-- pure id <*> v = v 
-- we also know that
-- fmap id v = v

-- pure (f) <*> pure ((.)) <*> pure g =? pure (f.g)
-- we have pure f <*> pure x = pure (f x) 
-- <*> is infixl
-- so yes

-- 
data Test = Test String deriving (Eq, Show)

(+++) :: Test -> Test -> Test
(Test a) +++ (Test b) = Test $ "(" ++ a ++ " +++ " ++ b ++ ")"

infixl 6 +++

-- |
-- >>> (Test "1") +++ (Test "2") +++ (Test "4")
-- Test "((1 +++ 2) +++ 4)"
main = print 42