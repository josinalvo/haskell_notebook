coinsDic = {}

import sys
sys.setrecursionlimit(20*1000)

def coins(lista,valor):
    key = (tuple(lista),valor)
    if key in coinsDic:
        return coinsDic[key]
    if valor == 0:
        return 1
    if valor < 0:
        return 0
    if lista == []:
        return 0
    maior_moeda = lista [0]
    resto       = lista [1:]
    ans = coins(lista,valor-maior_moeda) + coins(resto,valor)
    coinsDic[key] = ans
    return ans

print(coins([1,2],10*1000))
coins()