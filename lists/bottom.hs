data Maybe a = Nothing | Just a

f :: Bool -> Main.Maybe Int
f False = Main.Just 0
f _ = Main.Nothing
