import Data.Char -- for Cesar:

-- ‘Weak head normal form’ means the expression is
-- only evaluated as far as is necessary to reach a data constructor.
-- That is the default evaluation we do in haskell (which makes
-- sense, considering how much we like types)

a=(1,1+2) --is whnf, not nf
b=1+2 -- is neither

-- related: spine, the full structure of data constructors, without the values (can be in whnf without evaluating the whole spine, that might be infinite, btw)

-- Prelude> myNum = [1..10] :: [Int] -- Int to force type eval, to make sprint make sense
-- Prelude> :sprint myNum 
-- myNum = _
-- Prelude> take 2 myNum 
-- [1,2]
-- Prelude> :sprint myNum 
-- myNum = 1 : 2 : _
-- Prelude> 

-- by default, the list creation via funcion or ..
-- will stop at the first data constructor (ie, it does not know the first element neither the second)

-- not identical to function in Prelude
length' :: [a] -> Integer
length' [] = 0
length' (_:xs) = 1 + length' xs

lista = [1,undefined,3]
-- _ ajuda o GC, ou algo similar
amount = length lista



cesar :: Int -> Char -> Char
cesar n char 
   | (c <= 'z' && c >= 'a') = (chr . (+ (ord 'a')) . (`rem` (ord 'a')) . (`rem` ((ord 'z')+1)). (+ add) . ord) c 
   | otherwise = char
   where add = mod ((n `mod` 26) + 26) 26
         c = toLower char

cesarString :: Int -> String -> String
cesarString n cs = map (cesar n) cs

myAny :: (a-> Bool) -> [a] -> Bool
myAny f [] = False
myAny f (x:xs) = (f x) || myAny f xs

myAny' :: (a-> Bool) -> [a] -> Bool
myAny' f l = foldr (\x acc -> acc || f x) False l

squish :: [[a]] -> [a]
squish [] = []
squish (x:xs) = x ++ squish xs

squish' :: [[a]] -> [a]
squish' l = foldr (\x acc -> x ++ acc) [] l


squishMap :: (a-> [b])->[a] -> [b]
squishMap f [] = []
squishMap f (x:xs) = (++) (f x) $ squishMap f xs

squishMap' :: (a-> [b])->[a] -> [b]
squishMap' f list = foldr (\x acc -> (f x) ++ acc) [] list
-- goes right, acc on right

squishMap'' :: (a-> [b])->[a] -> [b]
squishMap'' f list = foldl (\acc x -> (f x) ++ acc) [] list

-- *Main> squishMap (\x -> [x,x*10,x*100]) [1..3]
-- [1,10,100,2,20,200,3,30,300]
-- squishMap (\x -> x) [[1,2],[3,4],[5,6,7]]
-- [1,2,3,4,5,6,7]

myFilter :: String -> [String]
myFilter x = filter (not . (`elem` ["a","an","the"])) $ words x
-- NOTE: why cant I just eliminate the x?

myWords :: String -> [String]
myWords [] = []
myWords string = [takeWhile (/= ' ') string] ++ 
                 (myWords . (drop 1) . (dropWhile (/= ' ')) $ string)

test = myWords "eu quisera ser um queijo"
--zipWith :: (a -> b -> c) -> [a] -> [b] -> [c]
-- juntar aplicando a funcao a um elemento da primeira lista (tipo a)
-- e um da segunda (tipo b) obtendo um da terceira (tipo c)

type Email = String -- | [Int] (does not work)

f :: Email -> Int
f = length
