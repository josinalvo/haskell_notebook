merge :: Ord a => [a] -> [a] -> [a]
merge [] l2 = l2
merge l1 [] = l1
merge (x:xs) (y:ys) 
   | y < x     = y : merge (x:xs) ys
   | otherwise = x : merge xs (y:ys)

mergeSort :: (Ord a) => [a] -> [a]
mergeSort [] = []
mergeSort [x] = [x]
mergeSort list = merge (mergeSort start) $ mergeSort end
          where up_to_middle = (length list) `div` 2
                start = take up_to_middle list
                end = drop up_to_middle list