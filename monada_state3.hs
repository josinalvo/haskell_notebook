{-# LANGUAGE TupleSections #-}
{-# OPTIONS_GHC -Wno-unrecognised-pragmas #-}
{-# HLINT ignore "Redundant bracket" #-}
import Control.Monad.State

data TurnstileInput = Coin | Push deriving(Eq,Show)

data TurnstileState = Unlocked | Locked deriving(Eq,Show)

data TurnstileAns   = Thank | GoAhead | Sorry deriving(Eq,Show)

turnS :: TurnstileInput -> State TurnstileState TurnstileAns
turnS input = state (turn input) where
    turn :: TurnstileInput -> (TurnstileState -> (TurnstileAns,TurnstileState))
    turn Coin _ = (Thank,Unlocked)
    turn Push Unlocked = (GoAhead,Locked)
    turn Push Locked   = (Sorry,Locked)

-- |
-- >>> runState (sequence $ map turnS [Coin, Coin, Push, Push, Coin]) Locked
-- ([Thank,Thank,GoAhead,Sorry,Thank],Unlocked)
-- >>> evalState (sequence $ map turnS [Coin, Coin, Push, Push, Coin]) Locked
-- [Thank,Thank,GoAhead,Sorry,Thank]
-- >>> evalState (mapM turnS [Coin, Coin, Push, Push, Coin]) Locked
-- [Thank,Thank,GoAhead,Sorry,Thank]

didIGetThru :: TurnstileInput -> State TurnstileState Bool
didIGetThru input = do
    a <- turnS input
    return $ a == GoAhead

peopleThru1 :: [(String,TurnstileInput)] -> State TurnstileState [String]
peopleThru1 list = chosen 
    where (people,actions) = unzip list
          results = sequence $ map didIGetThru actions :: State TurnstileState [Bool]
          -- results = mapM didIGetThru actions :: State TurnstileState [Bool]
          -- assoc   = fmap (zip people) results :: State TurnstileState [(String,Bool)]
          assoc = state f2   
            where f1 ram1 = runState results ram1 :: ([Bool],TurnstileState)
                  f2 ram1 = (,ram2) $ zip people ans :: ([(String,Bool)],TurnstileState)
                        where (ans,ram2) = f1 ram1
                  
          chosen  = fmap ((map fst) . (filter snd)) assoc :: State TurnstileState [String]

peopleThru2 :: [(String,TurnstileInput)] -> State TurnstileState [String]
peopleThru2 list = chosen 
    where oneStep :: (String,TurnstileInput) -> State TurnstileState (String,Bool)
          oneStep (name,action) = do
              result <- didIGetThru action
              return (name, result)
          assoc  = sequence $ map oneStep list :: State TurnstileState [(String,Bool)]
          chosen = fmap ((map fst) . (filter snd)) assoc :: State TurnstileState [String]

a = evalState (peopleThru2 [("Jao",Push),("Maria", Coin),("Maria2",Push),("Marta",Coin)] ) 
-- |
-- >>> a Locked
-- ["Maria2"]
-- >>> a Unlocked
-- ["Jao","Maria2"]

countOpen:: [(String,TurnstileInput)] -> State TurnstileState Int
countOpen list = do
    results <- sequence $ map (didIGetThru . snd) list
    return $ length $ filter ( == True) results

b = evalState (countOpen [("Jao",Push),("Maria", Coin),("Maria2",Push),("Marta",Coin)])

-- |
-- >>> b Locked
-- 1
-- >>> b Unlocked
-- 2



main = print 42