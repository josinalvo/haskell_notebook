{-# LANGUAGE RankNTypes #-}
import Control.Lens (Identity(Identity))
data Point a = Point
    { positionX :: a
    , positionY :: a
    } deriving (Show)

pointCoordinates
  :: Applicative f => (Double -> f Double) -> (Point Double) -> f (Point Double)
pointCoordinates g (Point x y) = Point <$> g x <*> g y

-- |
-- >>> pointCoordinates (\d -> [0,d]) (Point 3 4)
-- [Point {positionX = 0.0, positionY = 0.0},Point {positionX = 0.0, positionY = 4.0},Point {positionX = 3.0, positionY = 0.0},Point {positionX = 3.0, positionY = 4.0}]

type Traversal s t a b =
  forall f. Applicative f => (a -> f b) -> s -> f t
  --                          from D to applicative D
  --                                      the point
  --                                            the applicative point

--The forall f. on the right side of the type declaration means that any Applicative can be used to replace f. That makes it unnecessary to mention f on the left side, or to specify which f to pick when using a Traversal.

--pc2 = Traversal pointCoordinates


instance Functor Point where
    fmap f (Point a b) = Point (f a) (f b)

instance Foldable Point where
    foldMap = undefined

instance Traversable Point where
    traverse func (Point a b) = pure Point <*> func a <*> func b

a :: [Point Integer]
a = traverse (\d -> [0,d]) (Point 3 4)

b = traverse Identity (Point 3 4)

-- |
-- >>> a
-- [Point {positionX = 0, positionY = 0},Point {positionX = 0, positionY = 4},Point {positionX = 3, positionY = 0},Point {positionX = 3, positionY = 4}]
-- >>> b
-- Identity (Point {positionX = 3, positionY = 4})
