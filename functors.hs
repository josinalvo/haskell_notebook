module TryFunctors where



-- a <- fmap (unwords. reverse . words) getLine

-- | fmap (dois é somado a cada possivel resultado)
-- >>> map (+2) [1..3]
-- [3,4,5]
-- >>> f = fmap (+2) (/5)
-- >>> f 10
-- 4.0

-- | lifting (adder for lists and trees and functions)
-- >>> g = fmap (+2)
-- >>> g (/5) $ 10
-- 4.0
-- >>> g [1..3]
-- [3,4,5]


-- | fmap is right wing
-- >>> fmap (replicate 3) (Right "blah")  
-- Right ["blah","blah","blah"]
-- >>> fmap (replicate 3) (Left "blah")  
-- Left "blah"

-- ze laws: fmap id f is f (more concise fmap id = id)
-- fmap (f . g) = fmap f . fmap g
-- not sintatic, you check it or lose it

-- |
-- >>> :t fmap compare "A LIST OF CHARS"  
-- fmap compare "A LIST OF CHARS" :: [Char -> Ordering]  

-- APLICATIVE FUNCTORS

-- A better way of thinking about pure would be to say that it takes a value and puts it in some sort of default (or pure) context—a minimal context that still yields that value.

-- |
-- >>> pure (+) <*> [2,3] <*> [10]
-- [12,13]

-- |
-- >>> pure (+) <*> [2,3] <*> [10,20]
-- [12,22,13,23]

-- pure (+) <*> [2,3]
-- is the same as fmap (+) [2,3] (this is a property of applicative functors)
-- and can be written (+) <$> [2,3]

-- |
-- >>> (+) <$> [2,3] <*> [10,20]
-- [12,22,13,23]
-- >>> [(+3),(*2),(/5)] <*> [5]
-- [8.0,10.0,1.0]




-- the instance for list (actually for [])
-- instance Applicative [] where  
--    pure x = [x]  
--    fs <*> xs = [f x | f <- fs, x <- xs]  
-- the law we know so far: just (10*) <*> [1,2,3] is fmap (10*) [1,2,3]
-- indeed
-- It's easy to see how pure f <*> xs equals fmap f xs with lists. pure f is just [f] and [f] <*> xs will apply every function in the left list to every value in the right one, but there's just one function in the left list, so it's like mapping.

-- | pure can be ambigous, obviously, and was used reasonably b4
-- >>> pure "Hey" :: [String]
-- ["Hey"]
-- >>> pure "Hey" :: Maybe String
-- Just "Hey"

-- |
-- >>> [(+),(*)] <*> [1,5] <*> [100,1000] 
-- [101,1001,105,1005,100,1000,500,5000]

-- a <- (++) <$> getLine <*> getLine  


-- | funcoes como aplicatives
-- >>> (+) <$> [1,2,3] <*> [50,500]
-- [51,501,52,502,53,503]
-- >>> f = (+) <$> (+10) <*> (+5)
-- >>> f 100
-- 215
-- >>> g = (,,) <$> (+1) <*> (+2) <*> (+3)
-- >>> g 10
-- (11,12,13)

-- instance Applicative ((->) r) where
--     pure x = (\_ -> x)
--     f <*> g = \x -> f x (g x)
-- (+) <$> (+10) <*> (+5)
-- pure (+) <*> (+10) <*> (+5)
-- (\_ -> (+)) <*> (+10) <*> (+5)
-- (\ a -> (\_ -> (+)) a ((+10) a)) <*> (+5)

-- | <*> é infixl
-- >>> g = (((\ x y z -> (x,y,z)) <$> (+1)) <*> (+2)) <*> (+3)
-- >>> g 10
-- (11,12,13)


-- Acho que é um applicative possivel.

-- (,,) recebe 3 caras e monta uma tupla
