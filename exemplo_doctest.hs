-- stack install doctest
-- doctest exemplo_doctest.hs (no bash msm)
-- lucas@karaboudjan:~/hask3$ doctest exemplo_doctest.hs 
-- Examples: 3  Tried: 3  Errors: 0  Failures: 0

module GreatModules where



import Data.List hiding (nub)

-- | Intercala pontos
--
-- Examples
-- >>> dotit "MONKEY"
-- "M.O.N.K.E.Y"
dotit str = intersperse '.' str

-- | Isso é um abuso, pois nao tem funcao 
-- >>> b
-- "MONKEY.BALLS"
b = intercalate "." ["MONKEY","BALLS"] -- list and list of lists

-- | 
-- >>> c
-- "TERRIBLE.BALLS"
c = intercalate "." ["TERRIBLE","BALLS"] -- list and list of lists
