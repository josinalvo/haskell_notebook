
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE FlexibleContexts #-}
import Prelude(Ord, foldr, const, Int, (++))
import qualified Data.Map as Map

class Semigroup a where
    (<>) :: a -> a -> a

class Semigroup a => Monoid a where
        mempty  :: a
        mappend :: a -> a -> a
        mappend = (<>)
        {-# INLINE mappend #-} -- o que eh isso? Copiei e colei de
        -- https://hackage.haskell.org/package/base-4.15.0.0/docs/src/GHC-Base.html#Monoid
        mconcat :: [a] -> a
        mconcat = foldr mappend mempty

instance Semigroup b => Semigroup (a -> b) where
  f <> g = \x -> (f x) <> (g x)

instance Semigroup [a] where
    (<>) a b = a ++ b

instance forall b a . Monoid b => Monoid (a -> b) where
  mempty = \x -> (mempty ::b)

f :: Int -> [Int]
f a = [a]
g :: Int -> [Int]
g a = [a,a]
h :: Int -> [Int]
h = f <> g

